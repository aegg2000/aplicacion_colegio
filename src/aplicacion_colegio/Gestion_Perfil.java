package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.apache.commons.codec.binary.Base64;

public class Gestion_Perfil extends JPanel{
    public JLabel foto;
    public JTextField nombre_apellido,cedula,cargo,correo;
    public JPasswordField p_contraseña;
    public JButton cambiar;
    public MySQL db;
    public int Ancho,Alto,pos_y;
    
    public Gestion_Perfil(Ventana_Principal Ventana,int Anc,int Alt,String ID){
        Ancho = Anc;
        Alto = Alt;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setBackground(Color.WHITE);
        setLayout(null);
        
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
        
        pos_y = 0;
        
        foto = new JLabel(Ventana.cargarImagen("id.png", (int)(Ancho*0.4), (int)(Alto*0.5)));
        foto.setSize((int)(Ancho*0.4),(int)(Alto*0.5));
        foto.setLocation((int)(Ancho*0.05),pos_y);
//        foto.setBorder(BorderFactory.createLineBorder(Color.GRAY,2));
        add(foto);
                
        nombre_apellido = new JTextField();
        nombre_apellido.setSize((int)(Ancho*0.7),(int)(Alto/10));
        nombre_apellido.setLocation((int)(Ancho*0.3),pos_y);
        nombre_apellido.setForeground(Color.GRAY);
        nombre_apellido.setBackground(Color.WHITE);
        nombre_apellido.setFont(new Font("Arial",Font.ITALIC,28));
        nombre_apellido.setEditable(false);
        nombre_apellido.setBorder(BorderFactory.createLineBorder(Color.GRAY,0));
        add(nombre_apellido);
        
        pos_y += (int)(Alto/10);
                
        cedula = new JTextField();
        cedula.setSize((int)(Ancho*0.7),(int)(Alto/10));
        cedula.setLocation((int)(Ancho*0.3),pos_y);
        cedula.setForeground(Color.GRAY);
        cedula.setBackground(Color.WHITE);
        cedula.setFont(new Font("Arial",Font.BOLD,20));
        cedula.setEditable(false);
        cedula.setBorder(BorderFactory.createLineBorder(Color.GRAY,0));
        add(cedula);
        
        pos_y += (int)(Alto/10);
                
        cargo = new JTextField();
        cargo.setSize((int)(Ancho*0.7),(int)(Alto/10));
        cargo.setLocation((int)(Ancho*0.3),pos_y);
        cargo.setForeground(Color.GRAY);
        cargo.setBackground(Color.WHITE);
        cargo.setFont(new Font("Arial",Font.BOLD,20));
        cargo.setEditable(false);
        cargo.setBorder(BorderFactory.createLineBorder(Color.GRAY,0));
        add(cargo);
        
        pos_y += (int)(Alto/10);
                
        correo = new JTextField();
        correo.setSize((int)(Ancho*0.7),(int)(Alto/10));
        correo.setLocation((int)(Ancho*0.3),pos_y);
        correo.setForeground(Color.GRAY);
        correo.setBackground(Color.WHITE);
        correo.setFont(new Font("Arial",Font.BOLD,20));
        correo.setEditable(false);
        correo.setBorder(BorderFactory.createLineBorder(Color.GRAY,0));
        add(correo);
        
        pos_y += (int)(Alto/10);
                
        p_contraseña = new JPasswordField();
        p_contraseña.setSize((int)(Ancho*0.67),(int)(Alto/10));
        p_contraseña.setLocation((int)(Ancho*0.32),pos_y);
        p_contraseña.setForeground(Color.GRAY);
        p_contraseña.setBackground(Color.WHITE);
        p_contraseña.setFont(new Font("Arial",Font.BOLD,20));
        p_contraseña.setEditable(false);
        p_contraseña.setBorder(BorderFactory.createLineBorder(Color.GRAY,0));
        add(p_contraseña);
        
        pos_y += (int)(Alto/10)+10;
                
        cambiar = new JButton("Cambiar Contraseña"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        cambiar.setSize((int)(Ancho*0.9),(int)(Alto/10));
        cambiar.setLocation((int)(Ancho*0.05),pos_y);
        cambiar.setForeground(Color.GRAY);
        cambiar.setOpaque(true);
        cambiar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        cambiar.setContentAreaFilled(false);
        cambiar.setFocusable(false);
        cambiar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                JPanel cambiar_pass = new JPanel();
                cambiar_pass.setSize(300,120);
                cambiar_pass.setPreferredSize(new Dimension(300,120));
                cambiar_pass.setLayout(null);
                
                String mensaje = "";
                boolean pass_a = false, cambio = false;
        
                //PASS ACTUAL
                JLabel pass_actual = new JLabel("   Contraseña actual...");
                pass_actual.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                pass_actual.setLocation((int)(cambiar_pass.getWidth()*0.025),0);
                pass_actual.setForeground(Color.GRAY);
                pass_actual.setFont(new Font("Arial",Font.ITALIC,16));
                cambiar_pass.add(pass_actual);

                //PASS ACTUAL
                JPasswordField pass_actual_t = new JPasswordField(){
                    protected void paintComponent(Graphics g) {
                        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                            Graphics2D g2 = (Graphics2D) g.create();
                            g2.setPaint(getBackground());
                            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                                0, 0, getWidth() - 1, getHeight() - 1));
                            g2.dispose();
                            g2.setPaint(Color.RED);
                        }
                        super.paintComponent(g);
                    }
                    @Override public void updateUI() {
                      super.updateUI();
                      setOpaque(false);
                      setBorder(new RoundedCornerBorder());
                    }
                };
                pass_actual_t.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                pass_actual_t.setLocation((int)(cambiar_pass.getWidth()*0.025),0);
                pass_actual_t.setBackground(Color.WHITE);
                pass_actual_t.setForeground(Color.GRAY);
                pass_actual_t.addKeyListener(new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent ke) {
                    }

                    @Override
                    public void keyPressed(KeyEvent ke) {
                        if(pass_actual_t.getText().length()>0)
                            pass_actual.setVisible(false);
                        else
                            pass_actual.setVisible(true);
                    }

                    @Override
                    public void keyReleased(KeyEvent ke) {
                    }
                });
                pass_actual_t.setFont(new Font("Arial",Font.ITALIC,16));
                pass_actual_t.setEditable(true);
                cambiar_pass.add(pass_actual_t);

                //PASS NUEVA
                JLabel new_pass = new JLabel("   Nueva contraseña...");
                new_pass.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                new_pass.setLocation((int)(cambiar_pass.getWidth()*0.025),(int)(cambiar_pass.getHeight()/3));
                new_pass.setForeground(Color.GRAY);
                new_pass.setFont(new Font("Arial",Font.ITALIC,16));
                cambiar_pass.add(new_pass);

                //PASS NUEVA
                JPasswordField new_pass_t = new JPasswordField(){
                    protected void paintComponent(Graphics g) {
                        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                            Graphics2D g2 = (Graphics2D) g.create();
                            g2.setPaint(getBackground());
                            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                                0, 0, getWidth() - 1, getHeight() - 1));
                            g2.dispose();
                            g2.setPaint(Color.RED);
                        }
                        super.paintComponent(g);
                    }
                    @Override public void updateUI() {
                      super.updateUI();
                      setOpaque(false);
                      setBorder(new RoundedCornerBorder());
                    }
                };
                new_pass_t.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                new_pass_t.setLocation((int)(cambiar_pass.getWidth()*0.025),(int)(cambiar_pass.getHeight()/3));
                new_pass_t.setBackground(Color.WHITE);
                new_pass_t.setForeground(Color.GRAY);
                new_pass_t.addKeyListener(new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent ke) {
                    }

                    @Override
                    public void keyPressed(KeyEvent ke) {
                        if(new_pass_t.getText().length()>0)
                            new_pass.setVisible(false);
                        else
                            new_pass.setVisible(true);
                    }

                    @Override
                    public void keyReleased(KeyEvent ke) {
                    }
                });
                new_pass_t.setFont(new Font("Arial",Font.ITALIC,16));
                new_pass_t.setEditable(true);
                cambiar_pass.add(new_pass_t);

                //PASS NUEVA R
                JLabel new_pass_l = new JLabel("   Repita la contraseña...");
                new_pass_l.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                new_pass_l.setLocation((int)(cambiar_pass.getWidth()*0.025),(int)(cambiar_pass.getHeight()/3)+(int)(cambiar_pass.getHeight()/3));
                new_pass_l.setForeground(Color.GRAY);
                new_pass_l.setFont(new Font("Arial",Font.ITALIC,16));
                cambiar_pass.add(new_pass_l);

                //NEW PASS R
                JPasswordField new_pass_r = new JPasswordField(){
                    protected void paintComponent(Graphics g) {
                        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                            Graphics2D g2 = (Graphics2D) g.create();
                            g2.setPaint(getBackground());
                            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                                0, 0, getWidth() - 1, getHeight() - 1));
                            g2.dispose();
                            g2.setPaint(Color.RED);
                        }
                        super.paintComponent(g);
                    }
                    @Override public void updateUI() {
                      super.updateUI();
                      setOpaque(false);
                      setBorder(new RoundedCornerBorder());
                    }
                };
                new_pass_r.setSize((int)(cambiar_pass.getWidth()*0.95),(int)(cambiar_pass.getHeight()/3));
                new_pass_r.setLocation((int)(cambiar_pass.getWidth()*0.025),(int)(cambiar_pass.getHeight()/3)+(int)(cambiar_pass.getHeight()/3));
                new_pass_r.setBackground(Color.WHITE);
                new_pass_r.setForeground(Color.GRAY);
                new_pass_r.addKeyListener(new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent ke) {
                    }

                    @Override
                    public void keyPressed(KeyEvent ke) {
                        if(new_pass_r.getText().length()>0)
                            new_pass_l.setVisible(false);
                        else
                            new_pass_l.setVisible(true);
                    }

                    @Override
                    public void keyReleased(KeyEvent ke) {
                    }
                });
                new_pass_r.setFont(new Font("Arial",Font.ITALIC,16));
                new_pass_r.setEditable(true);
                cambiar_pass.add(new_pass_r);
                
                if(JOptionPane.showConfirmDialog(null, cambiar_pass,"Cambio de Contraseña",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                    //SI
                    if(pass_actual_t.getText().equalsIgnoreCase(p_contraseña.getText())){
                        pass_a = true;
                    }else{
                        mensaje += "Contraseña actual inválida.\n";
                    }

                    if(new_pass_t.getText().equalsIgnoreCase(new_pass_r.getText()) && new_pass_t.getText().length()>6){
                        if(new_pass_t.getText().length()>=6){
                            cambio = true;
                        }else{
                            mensaje += "La contraseña debe ser de al menos 6 caracteres.\n";
                        }
                    }else{
                        mensaje += "La nueva contraseña no coinciden.\n";
                    }

                    if(!pass_a || !cambio)
                        JOptionPane.showMessageDialog(null, mensaje,"Error de cambio de contraseña",JOptionPane.ERROR_MESSAGE);

                    if(pass_a && cambio){
                        try{            
                            String Query =  
                                "UPDATE empleado SET "
                                +"contrasena_acceso = '"+Encriptar(new_pass_t.getText())+"' "
                                +"WHERE cedula = '"+ID+"'";

                            db.comando = db.conexion.createStatement();
                            db.comando.execute(Query);

                            JOptionPane.showMessageDialog(null, "¡Cambio exitoso!", "Cambio de Contraseña", JOptionPane.PLAIN_MESSAGE);

                            p_contraseña.setText(new_pass_t.getText());

                        }catch(Exception e){
                            JOptionPane.showMessageDialog(null, "¡ERROR!", "Cambio de Contraseña", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                cambiar.setFont(new Font("Arial",Font.BOLD,24));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                cambiar.setFont(new Font("Arial",Font.BOLD,20));
            }
        });
        cambiar.setFont(new Font("Arial",Font.BOLD,20));
        add(cambiar);
        
        cargarDatos(ID);
    }
    
    public void cargarDatos(String ID){
        try{            
            String Query = "SELECT * FROM empleado WHERE cedula='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                nombre_apellido.setText("   "+rs.getString("nombres")+" "+rs.getString("apellidos"));
                cedula.setText("   "+rs.getString("cedula"));
                cargo.setText("   "+rs.getString("cargo"));
                correo.setText("   "+rs.getString("correo_electronico"));
                p_contraseña.setText(Desencriptar(rs.getString("contrasena_acceso")));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Personal", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public static String Encriptar(String texto) {
        String secretKey = "qualityinfosolutions"; //llave para encriptar datos
        String ret = "";
 
        try {
 
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
 
            byte[] plainTextBytes = texto.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            ret = new String(base64Bytes);
 
        } catch (Exception ex) {
        }
        return ret;
    }
    public static String Desencriptar(String textoEncriptado){
        String secretKey = "qualityinfosolutions"; //llave para desenciptar datos
        String ret = "";

        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            ret = new String(plainText, "UTF-8");

        } catch (Exception ex) {
        }
        return ret;
    }
}
