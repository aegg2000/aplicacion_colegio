package aplicacion_colegio;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Reporte_Estudiante_Lista {
    public Ventana_Principal Ventana;
    
    public Reporte_Estudiante_Lista(String ruta, String grado, Ventana_Principal Ventana){
        this.Ventana = Ventana;
                
        File LISTA_EXCEL = new File(ruta);
        
        Workbook workbook = new HSSFWorkbook();
        
        Sheet sheet_1 = workbook.createSheet("LISTA DE ESTUDIANTE");
        
        Row row = sheet_1.createRow(0);
        Cell NUMERO = row.createCell(0);
        NUMERO.setCellValue("N°");
        Cell IDENTIFICADOR = row.createCell(1);
        IDENTIFICADOR.setCellValue("IDENTIFICADOR");
        Cell APELLIDOS = row.createCell(2);
        APELLIDOS.setCellValue("APELLIDOS");
        Cell NOMBRES = row.createCell(3);
        NOMBRES.setCellValue("NOMBRES");
        Cell GRADO = row.createCell(4);
        GRADO.setCellValue("GRADO ESCOLAR");
        Cell PERIODO = row.createCell(5);
        PERIODO.setCellValue("PERIODO ESCOLAR");
        Cell FECHA = row.createCell(6);
        FECHA.setCellValue("FECHA DE NACIMIENTO");
        
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        org.apache.poi.ss.usermodel.Font font = workbook.createFont();
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setBold(true);
        font.setFontHeightInPoints((short)11);
        font.setFontName("Helvetica");
        
        style.setFont(font);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        
        NUMERO.setCellStyle(style);
        IDENTIFICADOR.setCellStyle(style);
        APELLIDOS.setCellStyle(style);
        NOMBRES.setCellStyle(style);
        GRADO.setCellStyle(style);
        PERIODO.setCellStyle(style);
        FECHA.setCellStyle(style);
        
        //CARGAR DATOS
        
        CellStyle style_2 = workbook.createCellStyle();
        
        org.apache.poi.ss.usermodel.Font font_2 = workbook.createFont();
        font_2.setColor(IndexedColors.BLACK.getIndex());
        font_2.setFontHeightInPoints((short)11);
        font_2.setFontName("Helvetica");
        
        style_2.setFont(font_2);
        style_2.setBorderBottom(BorderStyle.THIN);
        style_2.setBorderLeft(BorderStyle.THIN);
        style_2.setBorderRight(BorderStyle.THIN);
        style_2.setBorderTop(BorderStyle.THIN);
        
        int fila = 1;
        int con_filas = 1;
        
        List<String> estudiantes = getEstudiante(grado);
        
        for(int i=0;i<estudiantes.size();i+=5){
//            con_filas++;
            Row archivo = sheet_1.createRow(fila++);
            Cell info_numero = archivo.createCell(0);
            Cell info_identificador = archivo.createCell(1);
            Cell info_apellidos = archivo.createCell(2);
            Cell info_nombres = archivo.createCell(3);
            Cell info_grado = archivo.createCell(4);
            Cell info_periodo = archivo.createCell(5);
            Cell info_fecha = archivo.createCell(6);
            
            info_numero.setCellValue(con_filas++);
            info_identificador.setCellValue(estudiantes.get(i));
            info_apellidos.setCellValue(estudiantes.get(i+2));
            info_nombres.setCellValue(estudiantes.get(i+1));
            info_grado.setCellValue(grado);
            info_periodo.setCellValue(estudiantes.get(i+4));
            info_fecha.setCellValue(estudiantes.get(i+3));

            info_numero.setCellStyle(style_2);
            info_identificador.setCellStyle(style_2);
            info_nombres.setCellStyle(style_2);
            info_apellidos.setCellStyle(style_2);
            info_grado.setCellStyle(style_2);
            info_periodo.setCellStyle(style_2);
            info_fecha.setCellStyle(style_2);
        }
        
        //RESIZE COLUMNS
        sheet_1.autoSizeColumn(0);
        sheet_1.autoSizeColumn(1);
        sheet_1.autoSizeColumn(2);
        sheet_1.autoSizeColumn(3);
        sheet_1.autoSizeColumn(4);
        sheet_1.autoSizeColumn(5);
        sheet_1.autoSizeColumn(6);
        
//        sheet_1.setAutoFilter(CellRangeAddress.valueOf("A1:F"+con_filas));

        Object [] reporte = {"Aceptar","Abrir Listado"};
            
        int opcion = JOptionPane.showOptionDialog(
                null, "¡Tu archivo ha sido creado correctamente!", "NOTIFICACIÓN DE LISTADO",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                reporte, reporte
            );
        
        if(opcion==1){
            try {
                File archivo = new File(ruta);
                Desktop.getDesktop().open(archivo);
            } catch (Exception e) {
                try{
                    File archivo = new File(ruta);
                    Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + archivo.toURI());
                }catch(Exception ea){
                    JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                            + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                }
            }
        }      
        
        try {
            FileOutputStream output = new FileOutputStream(LISTA_EXCEL);
            workbook.write(output);
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(Reporte_Estudiante_Lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<String> getEstudiante(String grado){
        List<String> estudiantes = new ArrayList<>();
        int gradoid = Ventana.getGrado(grado);
        try{            
            String Query = "SELECT * FROM estudiante WHERE grado_id='"+gradoid+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("identificador_estudiante"));
                estudiantes.add(rs.getString("nombres"));
                estudiantes.add(rs.getString("apellidos"));
                estudiantes.add(
                    rs.getString("dia_nacimiento")+"/"
                    +rs.getString("mes_nacimiento")+"/"
                    +rs.getString("ano_nacimiento")
                );
                estudiantes.add(rs.getString("periodo_escolar"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return estudiantes;
    }
}
