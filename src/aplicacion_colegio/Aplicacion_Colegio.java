package aplicacion_colegio;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Aplicacion_Colegio {

    public static void main(String[] args) {
        Login Login = new Login();
        Login.setVisible(true);
        Login.Aceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Login.comprobarLogin();
                if(Login.login){
                    Login.setVisible(false);
                    Ventana_Principal Ventana_Principal = new Ventana_Principal(Login);
                    Ventana_Principal.setVisible(true);
                    Ventana_Principal.menu_home.Reloj.start();
                    Ventana_Principal.menu_home.cerrar_sesion.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent ae) {
                            if (JOptionPane.showConfirmDialog(null, "¿Desea realmente cerrar sesión?",
                                "Cerrar Sesión", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                                Ventana_Principal.setVisible(false);
                                new Thread(){
                                    public void run(){
                                        try {
                                            Thread.sleep(300);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Aplicacion_Colegio.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                }.run();
                                Login.user.setText("");
                                Login.Borde.setColor(Color.GRAY);
                                Login.h_user.setVisible(true);
                                Login.pass.setText("");
                                Login.h_pass.setVisible(true);
                                Login.login = false;
                                Login.setVisible(true);
                                Login.repaint();
                            }
                        }
                    });
                }
            }
        });
        
        
    }
}
