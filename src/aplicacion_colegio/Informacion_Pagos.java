package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Informacion_Pagos extends JPanel{
    public JTextField T_COMPROBANTE,T_FECHA,T_MONTO,T_CONCEPTO,T_DESCRIPCION;
    public JTextField COMPROBANTE,FECHA,MONTO,CONCEPTO,DESCRIPCION;
    public JComboBox cargo,grado_a_cargo;
    public JButton Editar;
    public Ventana_Principal Ventana;
    public int Ancho,Alto,pos_y;
    public boolean edicion;
    public MySQL db;
    
    public Informacion_Pagos(Ventana_Principal Ventana, String ID, String Comprobante){
        Ancho = 350;
        Alto = 150;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
        
        edicion = false;
        pos_y = 0;
        
        T_COMPROBANTE = new JTextField("COMPROBANTE");
        T_COMPROBANTE.setSize((int)(Ancho*0.3),(int)(Alto/5));
        T_COMPROBANTE.setLocation(0,pos_y);
        T_COMPROBANTE.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_COMPROBANTE.setEditable(false);
        T_COMPROBANTE.setFont(new Font("Arial",Font.BOLD,12));
        T_COMPROBANTE.setForeground(Color.WHITE);
        T_COMPROBANTE.setBackground(new Color(93, 193, 185));
        add(T_COMPROBANTE);
        
        pos_y += (int)(Alto/5);
        
        T_FECHA = new JTextField("FECHA");
        T_FECHA.setSize((int)(Ancho*0.3),(int)(Alto/5));
        T_FECHA.setLocation(0,pos_y);
        T_FECHA.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_FECHA.setEditable(false);
        T_FECHA.setFont(new Font("Arial",Font.BOLD,12));
        T_FECHA.setForeground(Color.WHITE);
        T_FECHA.setBackground(new Color(93, 193, 185));
        add(T_FECHA);
        
        pos_y += (int)(Alto/5);
        
        T_MONTO = new JTextField("MONTO");
        T_MONTO.setSize((int)(Ancho*0.3),(int)(Alto/5));
        T_MONTO.setLocation(0,pos_y);
        T_MONTO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_MONTO.setEditable(false);
        T_MONTO.setFont(new Font("Arial",Font.BOLD,12));
        T_MONTO.setForeground(Color.WHITE);
        T_MONTO.setBackground(new Color(93, 193, 185));
        add(T_MONTO);
        
        pos_y += (int)(Alto/5);
        
        T_CONCEPTO = new JTextField("CONCEPTO");
        T_CONCEPTO.setSize((int)(Ancho*0.3),(int)(Alto/5));
        T_CONCEPTO.setLocation(0,pos_y);
        T_CONCEPTO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_CONCEPTO.setEditable(false);
        T_CONCEPTO.setFont(new Font("Arial",Font.BOLD,12));
        T_CONCEPTO.setForeground(Color.WHITE);
        T_CONCEPTO.setBackground(new Color(93, 193, 185));
        add(T_CONCEPTO);
        
        pos_y += (int)(Alto/5);
        
        T_DESCRIPCION = new JTextField("DESCRIPCION");
        T_DESCRIPCION.setSize((int)(Ancho*0.3),(int)(Alto/5));
        T_DESCRIPCION.setLocation(0,pos_y);
        T_DESCRIPCION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_DESCRIPCION.setEditable(false);
        T_DESCRIPCION.setFont(new Font("Arial",Font.BOLD,12));
        T_DESCRIPCION.setForeground(Color.WHITE);
        T_DESCRIPCION.setBackground(new Color(93, 193, 185));
        add(T_DESCRIPCION);
        
        cargarInformacion(ID,Comprobante);
    }
    
    public void cargarInformacion(String ID, String Comprobante){
        //OBTENER PAGOS
        try{            
            String Query = "SELECT * FROM pagos WHERE identificador_estudiante='"+ID+"'"
                    + " and comprobante='"+Comprobante+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){             
                pos_y = 0;
        
                COMPROBANTE = new JTextField("  "+rs.getString("comprobante"));
                COMPROBANTE.setSize((int)(Ancho*0.7),(int)(Alto/5));
                COMPROBANTE.setLocation((int)(Ancho*0.3),pos_y);
                COMPROBANTE.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                COMPROBANTE.setEditable(false);
                COMPROBANTE.setFont(new Font("Arial",Font.PLAIN,12));
                COMPROBANTE.setForeground(Color.BLACK);
                COMPROBANTE.setBackground(Color.WHITE);
                setMaximo(COMPROBANTE,9);
                setNumber(COMPROBANTE);
                add(COMPROBANTE);

                pos_y += (int)(Alto/5);

                FECHA = new JTextField("  "+rs.getString("fecha"));
                FECHA.setSize((int)(Ancho*0.7),(int)(Alto/5));
                FECHA.setLocation((int)(Ancho*0.3),pos_y);
                FECHA.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                FECHA.setEditable(false);
                FECHA.setFont(new Font("Arial",Font.PLAIN,12));
                FECHA.setForeground(Color.BLACK);
                FECHA.setBackground(Color.WHITE);
                setMaximo(FECHA,65);
                add(FECHA);

                pos_y += (int)(Alto/5);

                MONTO = new JTextField("  "+rs.getString("monto"));
                MONTO.setSize((int)(Ancho*0.7),(int)(Alto/5));
                MONTO.setLocation((int)(Ancho*0.3),pos_y);
                MONTO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                MONTO.setEditable(false);
                MONTO.setFont(new Font("Arial",Font.PLAIN,12));
                MONTO.setForeground(Color.BLACK);
                MONTO.setBackground(Color.WHITE);
                setMaximo(MONTO,65);
                add(MONTO);

                pos_y += (int)(Alto/5);

                CONCEPTO = new JTextField("  "+rs.getString("concepto"));
                CONCEPTO.setSize((int)(Ancho*0.7),(int)(Alto/5));
                CONCEPTO.setLocation((int)(Ancho*0.3),pos_y);
                CONCEPTO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                CONCEPTO.setEditable(false);
                CONCEPTO.setFont(new Font("Arial",Font.PLAIN,12));
                CONCEPTO.setForeground(Color.BLACK);
                CONCEPTO.setBackground(Color.WHITE);
                add(CONCEPTO);
        
                pos_y += (int)(Alto/5);

                DESCRIPCION = new JTextField("  "+rs.getString("descripcion"));
                DESCRIPCION.setSize((int)(Ancho*0.7),(int)(Alto/5));
                DESCRIPCION.setLocation((int)(Ancho*0.3),pos_y);
                DESCRIPCION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                DESCRIPCION.setEditable(false);
                DESCRIPCION.setFont(new Font("Arial",Font.PLAIN,12));
                DESCRIPCION.setForeground(Color.BLACK);
                DESCRIPCION.setBackground(Color.WHITE);
                add(DESCRIPCION);
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡Error!", "Información de Pagos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String getTexto(String texto){
        return texto.substring(2, texto.length());
    }
    
    public void setMaximo(JTextField campo, int limite){
        campo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }
        });
    }
    
    public void setNumber(JTextField campo){
        campo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }
        });
    }
}