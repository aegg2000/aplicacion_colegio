package aplicacion_colegio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.sql.ResultSet;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Informacion_Estudiante extends JPanel{
    public JLabel representante, estudiante;
    
    //REPRESENTANTE
    public JLabel[] cedula,nombres_r,dir_habitacion,tlf_habitacion,tlf_celular, parentesco;
    
    //REPRESENTANTE
    public JLabel identificador,nombres_e,grado,fecha;
    
    public JLabel periodo_escolar;
    public Ventana_Principal Ventana;
    public int Ancho,Alto,pos_y;
    public MySQL db;
    
    public Informacion_Estudiante(Ventana_Principal Ventana, String ID){
        Ancho = 350;
        Alto = 450;
        this.Ventana = Ventana;
        
        setLayout(null);
        
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
        
        pos_y = 0;
        
        int n = nRepresentantes(ID);
        setSize(Ancho*n,Alto);
        setPreferredSize(new Dimension(Ancho*n,Alto));
        
        nombres_r = new JLabel[n];
        cedula = new JLabel[n];
        dir_habitacion = new JLabel[n];
        tlf_habitacion = new JLabel[n];
        tlf_celular = new JLabel[n];
        parentesco = new JLabel[n];
        
        
        representante = new JLabel("Representantes");
        representante.setSize(Ancho*n,(int)(Alto/13));
        representante.setLocation(0,pos_y);
        representante.setForeground(Color.GRAY);
        representante.setFont(new Font("Arial",Font.BOLD,16));
        representante.setHorizontalAlignment(SwingConstants.CENTER);
        add(representante);
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            nombres_r[i] = new JLabel();
            nombres_r[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            nombres_r[i].setSize(Ancho-5,(int)(Alto/13));
            nombres_r[i].setLocation(Ancho*i,pos_y);
            nombres_r[i].setForeground(Color.GRAY);
            nombres_r[i].setFont(new Font("Arial",Font.PLAIN,14));
            nombres_r[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(nombres_r[i]);
        }
        
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            cedula[i] = new JLabel();
            cedula[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            cedula[i].setSize(Ancho-5,(int)(Alto/13));
            cedula[i].setLocation(Ancho*i,pos_y);
            cedula[i].setForeground(Color.GRAY);
            cedula[i].setFont(new Font("Arial",Font.PLAIN,14));
            cedula[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(cedula[i]);
        }
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            dir_habitacion[i] = new JLabel();
            dir_habitacion[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            dir_habitacion[i].setSize(Ancho-5,(int)(Alto/13));
            dir_habitacion[i].setLocation(Ancho*i,pos_y);
            dir_habitacion[i].setForeground(Color.GRAY);
            dir_habitacion[i].setFont(new Font("Arial",Font.PLAIN,14));
            dir_habitacion[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(dir_habitacion[i]);
        }
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            tlf_habitacion[i] = new JLabel();
            tlf_habitacion[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            tlf_habitacion[i].setSize(Ancho-5,(int)(Alto/13));
            tlf_habitacion[i].setLocation(Ancho*i,pos_y);
            tlf_habitacion[i].setForeground(Color.GRAY);
            tlf_habitacion[i].setFont(new Font("Arial",Font.PLAIN,14));
            tlf_habitacion[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(tlf_habitacion[i]);
        }
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            tlf_celular[i] = new JLabel();
            tlf_celular[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            tlf_celular[i].setSize(Ancho-5,(int)(Alto/13));
            tlf_celular[i].setLocation(Ancho*i,pos_y);
            tlf_celular[i].setForeground(Color.GRAY);
            tlf_celular[i].setFont(new Font("Arial",Font.PLAIN,14));
            tlf_celular[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(tlf_celular[i]);
        }
        
        pos_y += (int)(Alto/13);
        
        for(int i = 0; i < n; i++){
            parentesco[i] = new JLabel();
            parentesco[i].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            parentesco[i].setSize(Ancho-5,(int)(Alto/13));
            parentesco[i].setLocation(Ancho*i,pos_y);
            parentesco[i].setForeground(Color.GRAY);
            parentesco[i].setFont(new Font("Arial",Font.PLAIN,14));
            parentesco[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(parentesco[i]);
        }
        
        pos_y += (int)(Alto/13);
        
        estudiante = new JLabel("Estudiante");
        estudiante.setSize(Ancho,(int)(Alto/13));
        estudiante.setLocation(0,pos_y);
        estudiante.setForeground(Color.GRAY);
        estudiante.setFont(new Font("Arial",Font.BOLD,16));
        estudiante.setHorizontalAlignment(SwingConstants.CENTER);
        add(estudiante);
        
        pos_y += (int)(Alto/13);
        
        identificador = new JLabel();
        identificador.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        identificador.setSize(Ancho,(int)(Alto/13));
        identificador.setLocation(0,pos_y);
        identificador.setForeground(Color.GRAY);
        identificador.setFont(new Font("Arial",Font.PLAIN,14));
        identificador.setHorizontalAlignment(SwingConstants.CENTER);
        add(identificador);
        
        pos_y += (int)(Alto/13);
        
        nombres_e = new JLabel();
        nombres_e.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        nombres_e.setSize(Ancho,(int)(Alto/13));
        nombres_e.setLocation(0,pos_y);
        nombres_e.setForeground(Color.GRAY);
        nombres_e.setFont(new Font("Arial",Font.PLAIN,14));
        nombres_e.setHorizontalAlignment(SwingConstants.CENTER);
        add(nombres_e);
        
        pos_y += (int)(Alto/13);
        
        grado = new JLabel();
        grado.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        grado.setSize(Ancho,(int)(Alto/13));
        grado.setLocation(0,pos_y);
        grado.setForeground(Color.GRAY);
        grado.setFont(new Font("Arial",Font.PLAIN,14));
        grado.setHorizontalAlignment(SwingConstants.CENTER);
        add(grado);
        
        pos_y += (int)(Alto/13);
        
        fecha = new JLabel();
        fecha.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        fecha.setSize(Ancho,(int)(Alto/13));
        fecha.setLocation(0,pos_y);
        fecha.setForeground(Color.GRAY);
        fecha.setFont(new Font("Arial",Font.PLAIN,14));
        fecha.setHorizontalAlignment(SwingConstants.CENTER);
        add(fecha);
        
        pos_y += (int)(Alto/13);
        
        periodo_escolar = new JLabel();
        periodo_escolar.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        periodo_escolar.setSize(Ancho,(int)(Alto/13));
        periodo_escolar.setLocation(0,pos_y);
        periodo_escolar.setForeground(Color.GRAY);
        periodo_escolar.setFont(new Font("Arial",Font.PLAIN,14));
        periodo_escolar.setHorizontalAlignment(SwingConstants.CENTER);
        add(periodo_escolar);
        
        cargarInformacion(ID);
    }
    
    public void cargarInformacion(String ID){
        try{          
            //OBTENER REPRESENTANTE  
            String Query = "SELECT * FROM representante WHERE identificador_estudiante='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
            int i = 0;
            while(rs.next()){
                if(!"null".equals(rs.getString("nombre_apellido_madre")) && rs.getString("nombre_apellido_madre") != null){
                    nombres_r[i].setText("<html><b>"+rs.getString("nombre_apellido_madre")+"</b></html>");
                    cedula[i].setText("<html><b>C.I: </b>"+rs.getString("cedula_madre")+"</html>");
                    dir_habitacion[i].setText("<html>"+rs.getString("direccion_habitacion_madre")+"</html>");
                    tlf_habitacion[i].setText("<html>"+rs.getString("telefono_habitacion_madre")+"</html>");
                    tlf_celular[i].setText("<html>"+rs.getString("telefono_celular_madre")+"</html>");
                    parentesco[i].setText("<html>Madre</html>");
                    i++;
                }
                if(!"null".equals(rs.getString("nombre_apellido_padre")) && rs.getString("nombre_apellido_padre") != null){
                    nombres_r[i].setText("<html><b>"+rs.getString("nombre_apellido_padre")+"</b></html>");
                    cedula[i].setText("<html><b>C.I: </b>"+rs.getString("cedula_padre")+"</html>");
                    dir_habitacion[i].setText("<html>"+rs.getString("direccion_habitacion_padre")+"</html>");
                    tlf_habitacion[i].setText("<html>"+rs.getString("telefono_habitacion_padre")+"</html>");
                    tlf_celular[i].setText("<html>"+rs.getString("telefono_celular_padre")+"</html>");
                    parentesco[i].setText("<html>Padre</html>");
                    i++;
                }
                if(!"null".equals(rs.getString("nombre_apellido_rlegal")) && rs.getString("nombre_apellido_rlegal") != null){
                    nombres_r[i].setText("<html><b>"+rs.getString("nombre_apellido_rlegal")+"</b></html>");
                    cedula[i].setText("<html><b>C.I: </b>"+rs.getString("cedula_rlegal")+"</html>");
                    dir_habitacion[i].setText("<html>"+rs.getString("direccion_habitacion_rlegal")+"</html>");
                    tlf_habitacion[i].setText("<html>"+rs.getString("telefono_habitacion_rlegal")+"</html>");
                    tlf_celular[i].setText("<html>"+rs.getString("telefono_celular_rlegal")+"</html>");
                    parentesco[i].setText("<html>"+rs.getString("parentesco_rlegal")+"</html>");
                    i++;
                }
            }
            
            //OBTENER ESTUDIANTE  
            Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            rs = db.comando.executeQuery(Query);
            while(rs.next()){
                String gradostr = Ventana.getGradoStr(Integer.parseInt(rs.getString("grado_id")));
                identificador.setText("<html><b>ID: </b>"+rs.getString("identificador_estudiante")+"</html>");
                nombres_e.setText("<html><b>"+rs.getString("nombres")+" "+rs.getString("apellidos")+"</b></html>");
                grado.setText("<html><b>GRADO ESCOLAR: </b>"+gradostr+"</html>");
                fecha.setText("<html><b>FECHA NAC.: </b>"+rs.getString("dia_nacimiento")+"/"+rs.getString("mes_nacimiento")+"/"+rs.getString("ano_nacimiento")+"</html>");
                periodo_escolar.setText("<html><b>PERIODO ESCOLAR: </b>"+rs.getString("periodo_escolar")+"</html>");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡Error!", "Información de Estudiante", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public int nRepresentantes(String ID){
        int n = 0;
        try{          
            
            String Query = "SELECT * FROM representante WHERE identificador_estudiante='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
            while(rs.next()){
                if(!"null".equals(rs.getString("nombre_apellido_madre")) && rs.getString("nombre_apellido_madre") != null)
                    n++;
                if(!"null".equals(rs.getString("nombre_apellido_padre")) && rs.getString("nombre_apellido_padre") != null)
                    n++;
                if(!"null".equals(rs.getString("nombre_apellido_rlegal")) && rs.getString("nombre_apellido_rlegal") != null)
                    n++;
            }            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡Error!", "Información de Estudiante", JOptionPane.ERROR_MESSAGE);
        }
        return n;
    }
}
