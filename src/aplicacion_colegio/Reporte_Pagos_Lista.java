package aplicacion_colegio;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Reporte_Pagos_Lista {
    public Ventana_Principal Ventana;
    
    public Reporte_Pagos_Lista(String ruta, String identificador, Ventana_Principal Ventana){
        this.Ventana = Ventana;
                
        File LISTA_EXCEL = new File(ruta);
        
        Workbook workbook = new HSSFWorkbook();
        
        Sheet sheet_1 = workbook.createSheet("LISTA DE PAGOS");
        
        Row row = sheet_1.createRow(0);
        Cell IDENTIFICADOR = row.createCell(0);
        IDENTIFICADOR.setCellValue("IDENTIFICADOR DE ESTUDIANTE");
        Cell NOMBRES = row.createCell(1);
        NOMBRES.setCellValue("NOMBRES Y APELLIDOS");
        Cell GRADO = row.createCell(2);
        GRADO.setCellValue("GRADO ESCOLAR");
        Cell PERIODO = row.createCell(3);
        PERIODO.setCellValue("PERIODO ESCOLAR");
        Cell COMPROBANTE = row.createCell(4);
        COMPROBANTE.setCellValue("N°. COMPROBANTE");
        Cell MONTO = row.createCell(5);
        MONTO.setCellValue("MONTO");
        Cell FECHA = row.createCell(6);
        FECHA.setCellValue("FECHA DE PAGO");
        Cell CONCEPTO = row.createCell(7);
        CONCEPTO.setCellValue("CONCEPTO");
        Cell DESCRIPCION = row.createCell(8);
        DESCRIPCION.setCellValue("DESCRIPCION");
        
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        
        org.apache.poi.ss.usermodel.Font font = workbook.createFont();
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setBold(true);
        font.setFontHeightInPoints((short)11);
        font.setFontName("Helvetica");
        
        style.setFont(font);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        
        IDENTIFICADOR.setCellStyle(style);
        NOMBRES.setCellStyle(style);
        GRADO.setCellStyle(style);
        PERIODO.setCellStyle(style);
        COMPROBANTE.setCellStyle(style);
        MONTO.setCellStyle(style);
        FECHA.setCellStyle(style);
        CONCEPTO.setCellStyle(style);
        DESCRIPCION.setCellStyle(style);
        
        //CARGAR DATOS
        
        CellStyle style_2 = workbook.createCellStyle();
        
        org.apache.poi.ss.usermodel.Font font_2 = workbook.createFont();
        font_2.setColor(IndexedColors.BLACK.getIndex());
        font_2.setFontHeightInPoints((short)11);
        font_2.setFontName("Helvetica");
        
        style_2.setFont(font_2);
        style_2.setBorderBottom(BorderStyle.THIN);
        style_2.setBorderLeft(BorderStyle.THIN);
        style_2.setBorderRight(BorderStyle.THIN);
        style_2.setBorderTop(BorderStyle.THIN);
        
        int fila = 1;
        int con_filas = 1;
        
        List<String> estudiantes = getEstudiante(identificador);
        List<String> pagos = getPagos(identificador);
        
        for(int i=0;i<pagos.size();i+=5){
            con_filas++;
            Row archivo = sheet_1.createRow(fila++);
            Cell info_identificador = archivo.createCell(0);
            Cell info_nombres = archivo.createCell(1);
            Cell info_grado = archivo.createCell(2);
            Cell info_periodo = archivo.createCell(3);
            Cell info_comprobante = archivo.createCell(4);
            Cell info_monto = archivo.createCell(5);
            Cell info_fecha = archivo.createCell(6);
            Cell info_concepto = archivo.createCell(7);
            Cell info_descripcion = archivo.createCell(8);
            
            info_identificador.setCellValue(identificador);
            info_nombres.setCellValue(estudiantes.get(0)+" "+estudiantes.get(1));
            info_grado.setCellValue(estudiantes.get(2));
            info_periodo.setCellValue(estudiantes.get(3));
            info_comprobante.setCellValue(pagos.get(i));
            info_monto.setCellValue(pagos.get(i+1));
            info_fecha.setCellValue(pagos.get(i+2));
            info_concepto.setCellValue(pagos.get(i+3));
            info_descripcion.setCellValue(pagos.get(i+4));

            info_identificador.setCellStyle(style_2);
            info_nombres.setCellStyle(style_2);
            info_grado.setCellStyle(style_2);
            info_periodo.setCellStyle(style_2);
            info_comprobante.setCellStyle(style_2);
            info_monto.setCellStyle(style_2);
            info_fecha.setCellStyle(style_2);
            info_concepto.setCellStyle(style_2);
            info_descripcion.setCellStyle(style_2);
        }
        
        //RESIZE COLUMNS
        sheet_1.autoSizeColumn(0);
        sheet_1.autoSizeColumn(1);
        sheet_1.autoSizeColumn(2);
        sheet_1.autoSizeColumn(3);
        sheet_1.autoSizeColumn(4);
        sheet_1.autoSizeColumn(5);
        sheet_1.autoSizeColumn(6);
        sheet_1.autoSizeColumn(7);
        sheet_1.autoSizeColumn(8);

        Object [] reporte = {"Aceptar","Abrir Pagos"};
            
        int opcion = JOptionPane.showOptionDialog(
                null, "¡Tu archivo ha sido creado correctamente!", "NOTIFICACIÓN DE PAGOS",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                reporte, reporte
            );
        
        if(opcion==1){
            try {
                File archivo = new File(ruta);
                Desktop.getDesktop().open(archivo);
            } catch (Exception e) {
                try{
                    File archivo = new File(ruta);
                    Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + archivo.toURI());
                }catch(Exception ea){
                    JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                            + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                }
            }
        }      
        
        try {
            FileOutputStream output = new FileOutputStream(LISTA_EXCEL);
            workbook.write(output);
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(Reporte_Estudiante_Lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<String> getEstudiante(String identificador){
        List<String> estudiantes = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+identificador+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("nombres"));
                estudiantes.add(rs.getString("apellidos"));
                estudiantes.add(Ventana.getGradoStr(Integer.parseInt(rs.getString("grado_id"))));
                estudiantes.add(rs.getString("periodo_escolar"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return estudiantes;
    }
    
    
    public List<String> getPagos(String identificador){
        List<String> pagos = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM pagos WHERE identificador_estudiante='"+identificador+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                pagos.add(rs.getString("comprobante"));
                pagos.add(rs.getString("monto"));
                pagos.add(rs.getString("fecha"));
                pagos.add(rs.getString("concepto"));
                pagos.add(rs.getString("descripcion"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Pagos", JOptionPane.ERROR_MESSAGE);
        }
        
        return pagos;
    }
}
