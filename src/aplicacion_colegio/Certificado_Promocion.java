package aplicacion_colegio;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Certificado_Promocion {
    private static final Font FONT_FECHA =
            new Font(Font.FontFamily.COURIER, 14, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_ETIQUETA =
            new Font(Font.FontFamily.COURIER, 16, Font.BOLD, BaseColor.WHITE);
    
    private static final Font FONT_ETIQUETA_2 =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TEXT =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TITULO =
            new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FIRMA =
            new Font(Font.FontFamily.HELVETICA, 11, Font.UNDERLINE, BaseColor.BLACK);
    
    private static final Font NOMBRE_FIRMA =
            new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_TITULO_2 =
            new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);    
    
    private static final Font FONT_TITULO_3 =
            new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);   
    
    private static final Font FONT_FECHA_TABLA =
            new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    
    public Certificado_Promocion(
            String RUTA, String nombre_estudiante, String identificador, String grado, String nuevoGrado,
            String periodo, String notaDefinitiva, String dia, String mes, String ano, String docente,
            Ventana_Principal Ventana
    ){
        //CREACIPON DEL DOCUMENTO PDF
        try {
            Document document = new Document();            
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RUTA));
            
            document.open();
            document.setPageSize(PageSize.LETTER);
            document.setMargins(70, 70, 50, 30);
            //ANCHO: 590 - ALTO: 780
            
            //METADATOS DEL PDF
            document.addTitle("Constancia de Promoción");
            document.addSubject("Usando Aplicación Colegio");
            document.addKeywords("Java, PDF, Aplicación Colegio");
            document.addAuthor("Aplicación Colegio");
            document.addCreator("Aplicación Colegio");
            
            //PRIMERA PAGINA
            Image ENCABEZADO;
            ENCABEZADO = Image.getInstance(Certificado_Promocion.class.getResource("/Imagenes/escudovenezuela.png"));
            ENCABEZADO.scaleAbsolute(100, 90);
            ENCABEZADO.setAbsolutePosition(255,680);
            
            Paragraph MARGEN_SUPERIOR = new Paragraph();
            MARGEN_SUPERIOR.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO
            Paragraph TITULO = new Paragraph(
                    "REPÚBLICA BOLIVARIANA DE VENEZUELA\n"
                    +"MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN\n"
                    +"Viceministerio de Participación y Apoyo Académico\n"
                    +"Dirección General de Registro y Control de Académico\n"
                    +"Código DEA:PD13630304\n"
                    +"Barcelona - Estado Anzoátegui", 
                    FONT_TITULO);
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 2
            Paragraph TITULO_2 = new Paragraph(
                    "CONSTANCIA DE PROMOCIÓN\n"
                    +"EN EL NIVEL DE EDUCACIÓN BÁSICA",
                    FONT_TITULO_2);
            TITULO_2.add(new Phrase(Chunk.NEWLINE));
            TITULO_2.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 3
            Paragraph PARRAFO = new Paragraph(
                    "Quien suscribe, Lcda. María Marín de Martínez, titular de la Cédula de Identidad "
                    +"Nº 4.006.441, en su condición de Director(a) del (la) U.E. Rodolfo Romero certifica "
                    +"por medio de la presente que el (la) estudiante "+nombre_estudiante+", "
                    +"portador(a) de la Cédula escolar Nº "+identificador+", cursó y aprobó el "+grado+ " donde "
                    +"alcanzó las competencias previstas, durante el Año Escolar "+periodo+" y fue promovido al "
                    +nuevoGrado+", con el literal "+notaDefinitiva+", previo cumplimiento de los requisitos exigidos en la normativa legal vigente.\n\n"
                    +"Certificado que se expide en Barcelona, a los "+dia+" días del mes de "+mes+" de "+ano+".",
                    FONT_TITULO_3);
            PARRAFO.add(new Phrase(Chunk.NEWLINE));
            PARRAFO.add(new Phrase(Chunk.NEWLINE));
            PARRAFO.setFirstLineIndent(40);
            PARRAFO.setLeading(30);
            PARRAFO.setAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            //ENCABEZADO
            Chapter chapter = new Chapter(MARGEN_SUPERIOR, 1);
            chapter.add(ENCABEZADO);
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.setNumberDepth(0);
            chapter.add(TITULO);
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(TITULO_2);
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(PARRAFO);            
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            PdfPTable table_firma;
            
            //REPRESENTANTE                        
            PdfPCell NOMBRE_DIRECTORA = new PdfPCell(
                    new Paragraph("Lic. María de Martínez\nDirectora General", NOMBRE_FIRMA)
            );
            NOMBRE_DIRECTORA.setBorder(1);
            NOMBRE_DIRECTORA.setBorderColorBottom(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorLeft(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorRight(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorTop(BaseColor.BLACK);
            NOMBRE_DIRECTORA.setColspan(2);
            NOMBRE_DIRECTORA.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            NOMBRE_DIRECTORA.setLeading(15, 0);
                        
            PdfPCell SEPARADOR = new PdfPCell(
                    new Paragraph("      ", NOMBRE_FIRMA)
            );
            SEPARADOR.setBorder(0);
            SEPARADOR.setBorderColorBottom(BaseColor.WHITE);
            SEPARADOR.setBorderColorLeft(BaseColor.WHITE);
            SEPARADOR.setBorderColorRight(BaseColor.WHITE);
            SEPARADOR.setBorderColorTop(BaseColor.WHITE);
            SEPARADOR.setColspan(1);
            SEPARADOR.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell NOMBRE_DOCENTE = new PdfPCell(
                    new Paragraph(docente+"\nDocente", NOMBRE_FIRMA)
            );
            NOMBRE_DOCENTE.setBorder(1);
            NOMBRE_DOCENTE.setBorderColorBottom(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorLeft(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorRight(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorTop(BaseColor.BLACK);
            NOMBRE_DOCENTE.setColspan(2);
            NOMBRE_DOCENTE.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            NOMBRE_DOCENTE.setLeading(15, 0);
            
            table_firma = new PdfPTable(5);
            table_firma.setWidthPercentage(102);
            table_firma.addCell(NOMBRE_DIRECTORA);
            table_firma.addCell(SEPARADOR);
            table_firma.addCell(NOMBRE_DOCENTE);
            chapter.add(table_firma);
            
            document.add(chapter);
            document.close();
            
            Object [] reporte = {"Aceptar","Abrir Constancia"};
            
            int opcion = JOptionPane.showOptionDialog(
                    null, "¡Tu archivo ha sido creado correctamente!", "CONSTANCIA DE PROMOCIÓN",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                    reporte, reporte
                );
            if(opcion==1){
                try {
                    File archivo = new File(RUTA);
                    Desktop.getDesktop().open(archivo);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                            + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                }
            }
            
        } catch (DocumentException documentException) {
            JOptionPane.showMessageDialog(null, "¡Tu archivo no ha podido ser creado!",
                    "CONSTANCIA DE PROMOCIÓN", 0);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Boletin_Digital.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Boletin_Digital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
