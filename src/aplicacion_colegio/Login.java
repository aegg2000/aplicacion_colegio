package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import org.apache.commons.codec.binary.Base64;

public class Login extends JFrame{
    public int Ancho, Alto;
    public boolean login;
    public JPanel fondo;
    public JLabel banner, h_user, h_pass, manual;
    public JTextField user;
    public JPasswordField pass;
    public JButton Aceptar, Salir;
    public RoundedCornerBorder Borde;
    public String EMAIL_PATTERN = 
    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public MySQL db;
    public String Nombre_User, Apellido_User, Cargo_User, Cedula_User, Grado_user;
    
    public Login(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Inicio de Sesión");
        Ancho = 400;
        Alto = 450;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(300,450));
        getContentPane().setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);
        setIconImage(cargarImagen("logo_icon.png",300,300).getImage());
        
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
        
        login = false;
        Borde = new RoundedCornerBorder();
        
        //PANEL
        fondo = new JPanel();
        fondo.setSize(Ancho,Alto);
        fondo.setLocation(0,0);
        fondo.setBackground(Color.WHITE);
        fondo.setLayout(null);
        add(fondo);
        
        //BANNER
        banner = new JLabel(cargarImagen("login_banner.png",Ancho,(int)(Alto*0.45)));
        banner.setSize(Ancho,(int)(Alto*0.45));
        banner.setLocation(0,0);
        fondo.add(banner);
        
        //H_USER
        h_user = new JLabel("   Usuario o Correo Electrónico...");
        h_user.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        h_user.setLocation((int)(Ancho*0.12),(int)(Alto*0.5));
        h_user.setForeground(Color.GRAY);
        h_user.setFont(new Font("Arial",Font.ITALIC,16));
        fondo.add(h_user);
        
        //USER
        user = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        user.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        user.setLocation((int)(Ancho*0.1),(int)(Alto*0.5));
        user.setBackground(Color.WHITE);
        user.setForeground(Color.GRAY);
        user.setFont(new Font("Arial",Font.ITALIC,16));
        user.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(!user.getText().matches(EMAIL_PATTERN)){
                    Borde.setColor(Color.RED);
                }else{
                    Borde.setColor(Color.GREEN);
                }
                if(!user.getText().equalsIgnoreCase("")){
                    h_user.setVisible(false);
                }else{
                    h_user.setVisible(true);
                    Borde.setColor(Color.GRAY);
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                int key = ke.getKeyCode();
                if (key == KeyEvent.VK_ENTER) {
                    Aceptar.doClick();
                }
                if(!user.getText().matches(EMAIL_PATTERN)){
                    Borde.setColor(Color.RED);
                }else{
                    Borde.setColor(Color.GREEN);
                }
                if(!user.getText().equalsIgnoreCase("")){
                    h_user.setVisible(false);
                }else{
                    h_user.setVisible(true);
                    Borde.setColor(Color.GRAY);
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(!user.getText().matches(EMAIL_PATTERN)){
                    Borde.setColor(Color.RED);
                }else{
                    Borde.setColor(Color.GREEN);
                }
                if(!user.getText().equalsIgnoreCase("")){
                    h_user.setVisible(false);
                }else{
                    h_user.setVisible(true);
                    Borde.setColor(Color.GRAY);
                }
            }
        });
        user.setEditable(true);
        fondo.add(user);
        
        int pos_y = (int)(Alto*0.5)+(int)(Alto*0.08)+1;
        
        //h_pass
        h_pass = new JLabel("   Contraseña de Usuario...");
        h_pass.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        h_pass.setLocation((int)(Ancho*0.12),pos_y);
        h_pass.setForeground(Color.GRAY);
        h_pass.setFont(new Font("Arial",Font.ITALIC,16));
        fondo.add(h_pass);
        
        //PASS
        pass = new JPasswordField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.GRAY);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        pass.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        pass.setLocation((int)(Ancho*0.1),pos_y);
        pass.setBackground(Color.WHITE);
        pass.setForeground(Color.GRAY);
        pass.setFont(new Font("Arial",Font.ITALIC,16));
        pass.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                int key = ke.getKeyCode();
                if (key == KeyEvent.VK_ENTER) {
                    Aceptar.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(!pass.getText().equalsIgnoreCase("")){
                    h_pass.setVisible(false);
                }else{
                    h_pass.setVisible(true);
                }
            }
        });
        pass.setEditable(true);
        fondo.add(pass);
        
        pos_y += (int)(Alto*0.08)+20;
        
        //ACEPTAR
        Aceptar = new JButton("Iniciar Sesión");
        Aceptar.setSize((int)(Ancho*0.8),(int)(Alto*0.069));
        Aceptar.setLocation((int)(Ancho*0.1),pos_y);
        Aceptar.setBackground(Color.WHITE);
        Aceptar.setForeground(Color.GRAY);
        Aceptar.setFocusable(false);
        Aceptar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        Aceptar.setFont(new Font("Arial",Font.PLAIN,20));
        Aceptar.setBorder(new RoundedBorder(20,Color.GRAY,1f));
        Aceptar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                Aceptar.setForeground(new Color(93, 193, 185));
                Aceptar.setBorder(new RoundedBorder(20,new Color(93, 193, 185),1f));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                Aceptar.setForeground(Color.GRAY);
                Aceptar.setBorder(new RoundedBorder(20,Color.GRAY,1f));
            }
        });
        fondo.add(Aceptar);
        
        pos_y += (int)(Alto*0.069)+2;
        
        //Salir
        Salir = new JButton("Salir");
        Salir.setSize((int)(Ancho*0.8),(int)(Alto*0.07));
        Salir.setLocation((int)(Ancho*0.1),pos_y);
        Salir.setBackground(Color.WHITE);
        Salir.setForeground(Color.GRAY);
        Salir.setFocusable(false);
        Salir.setCursor(new Cursor(Cursor.HAND_CURSOR));
        Salir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
        Salir.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                Salir.setForeground(new Color(93, 193, 185));
                Salir.setBorder(new RoundedBorder(20,new Color(93, 193, 185),1f));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                Salir.setForeground(Color.GRAY);
                Salir.setBorder(new RoundedBorder(20,Color.GRAY,1f));
            }
        });
        Salir.setFont(new Font("Arial",Font.PLAIN,20));
        Salir.setBorder(new RoundedBorder(20,Color.GRAY,1f));
        fondo.add(Salir);
        
        pos_y += (int)(Alto*0.07)+8;
        
        //manual
        manual = new JLabel("Manual de Ayuda");
        manual.setSize(Ancho,30);
        manual.setLocation(0,pos_y);
        manual.setForeground(Color.GRAY);
        manual.setHorizontalAlignment(SwingConstants.CENTER);
        manual.setFont(new Font("Arial",Font.PLAIN,12));
        
        Font font = manual.getFont();
        Map attributes = font.getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        manual.setFont(font.deriveFont(attributes));
        
        manual.setCursor(new Cursor(Cursor.HAND_CURSOR));
        manual.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if(me.getClickCount()==2){
                    try {
                        File archivo = new File(System.getenv("ProgramFiles")+"\\Gestion Colegio\\user_manual.pdf");
                        Desktop.getDesktop().open(archivo);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                                + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                                "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
                
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                manual.setForeground(Color.BLUE);
            }

            @Override
            public void mouseExited(MouseEvent me) {
                manual.setForeground(Color.GRAY);
            }
        });
        fondo.add(manual);
        
        this.repaint();
    }
    
    public void comprobarLogin(){
        List<String> empleados = new ArrayList<>();
        List<String> contraseñas = new ArrayList<>();
        List<String> nombres = new ArrayList<>();
        List<String> apellidos = new ArrayList<>();
        List<String> cargos = new ArrayList<>();
        List<String> cedulas = new ArrayList<>();
        List<String> grados = new ArrayList<>();
        
        try{            
            String Query = "SELECT * FROM empleado";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                nombres.add(rs.getString("nombres"));
                apellidos.add(rs.getString("apellidos"));
                cargos.add(rs.getString("cargo"));
                cedulas.add(rs.getString("cedula"));
                empleados.add(rs.getString("correo_electronico"));
                contraseñas.add(Desencriptar(rs.getString("contrasena_acceso")));
                grados.add(rs.getString("grado_id"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Personal", JOptionPane.ERROR_MESSAGE);
        }
                
        for(int i=0;i<empleados.size();i++){
            if(user.getText().equalsIgnoreCase(empleados.get(i))){
                if(pass.getText().equalsIgnoreCase(contraseñas.get(i))){
                    Nombre_User = nombres.get(i);
                    Apellido_User = apellidos.get(i);
                    Cargo_User = cargos.get(i);
                    Cedula_User = cedulas.get(i);
                    Grado_user = grados.get(i);
                    login = true;
                    break;
                }else{
                    JOptionPane.showMessageDialog(null,"¡Contraseña Errónea!","ERROR DE INICIO DE SESIÓN", JOptionPane.ERROR_MESSAGE);
                    break;
                }
            }else if( i == empleados.size()-1){
                JOptionPane.showMessageDialog(null,"¡Usuario no Existe!","ERROR DE INICIO DE SESIÓN", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
    
    public static String Desencriptar(String textoEncriptado){
        String secretKey = "qualityinfosolutions"; //llave para desenciptar datos
        String ret = "";

        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            ret = new String(plainText, "UTF-8");

        } catch (Exception ex) {
        }
        return ret;
    }
    
    //METODOS PARA COLOCAR IMAGENES
    public ImageIcon cargarImagen(String nombreDeImagen){
        try{
            return new 
            ImageIcon( ImageIO.read(
            getClass().getResource("/Imagenes/"+nombreDeImagen)
            ));
        }
        catch(IOException ioe){ 
            JOptionPane.showMessageDialog(null, "No se pudo cargar la imagen: "+nombreDeImagen);
            return null;
        }
    }
    public ImageIcon cargarImagen(String nombreDeImagen, int eW, int eH){
        ImageIcon original  = cargarImagen(nombreDeImagen);
        return new ImageIcon(original.getImage()
        .getScaledInstance(eW, eH, Image.SCALE_SMOOTH));
    } 
}
