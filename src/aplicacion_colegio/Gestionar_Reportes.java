package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Gestionar_Reportes extends JPanel{
    public JButton boletin, estudiantes, pagos, promocion, estudio;
    public int Ancho,Alto;
    public JLabel reseña;
    public Ventana_Principal Ventana;
    
    public Gestionar_Reportes(Ventana_Principal Ventana, int Anc, int Alt){
        Ancho = Anc;
        Alto = Alt;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setBackground(Color.WHITE);
        setLayout(null);
        
        int posx = (int) (Ancho*0.05);
        int anchox = (int)(Ancho/5.5);
        //boletin
        boletin = new JButton(Ventana.cargarImagen("b_boletin.png",anchox,(int)(Alto*0.5)));
        boletin.setRolloverIcon(Ventana.cargarImagen("b_boletin_hover.png",anchox,(int)(Alto*0.5)));
        boletin.setSize(anchox,(int)(Alto*0.5));
        boletin.setLocation(posx,0);
        boletin.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        boletin.setContentAreaFilled(false);
        boletin.setCursor(new Cursor(Cursor.HAND_CURSOR));
        boletin.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                generarBoletin(null);
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                reseña.setText("Genere el boletín de notas de un estudiante");
            }

            @Override
            public void mouseExited(MouseEvent me) {
                reseña.setText("");
            }
        });
        boletin.setFocusable(false);
        add(boletin);
        
        posx += anchox;
        
        //estudiantes
        estudiantes = new JButton(Ventana.cargarImagen("b_estudiantes.png",anchox,(int)(Alto*0.5)));
        estudiantes.setRolloverIcon(Ventana.cargarImagen("b_estudiantes_hover.png",anchox,(int)(Alto*0.5)));
        estudiantes.setSize(anchox,(int)(Alto*0.5));
        estudiantes.setLocation(posx,0);
        estudiantes.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        estudiantes.setContentAreaFilled(false);
        estudiantes.setCursor(new Cursor(Cursor.HAND_CURSOR));
        estudiantes.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                String grado = "";
                
                if(!Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") &&
                    !Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
                    grado = Ventana.obtenerGradoACargo(Ventana.Login.Cedula_User);
                }else{
                    String[] OPCION = new String[9];
                    OPCION[0] = "PRIMER GRUPO";
                    OPCION[1] = "SEGUNDO GRUPO";
                    OPCION[2] = "TERCER GRUPO";
                    OPCION[3] = "PRIMER GRADO";
                    OPCION[4] = "SEGUNDO GRADO";
                    OPCION[5] = "TERCER GRADO";
                    OPCION[6] = "CUARTO GRADO";
                    OPCION[7] = "QUINTO GRADO";
                    OPCION[8] = "SEXTO GRADO";
                    grado = (
                    (String) JOptionPane.showInputDialog(null, 
                        "Seleccione un Grado",
                        "Selección de Grado",
                        JOptionPane.QUESTION_MESSAGE, 
                        null, 
                        OPCION, 
                        OPCION[0])
                    );
                    
                    if(grado == null){ return; }
                }
                
                JFileChooser RUTA = new JFileChooser();
                RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                RUTA.setFileFilter(new FileNameExtensionFilter("*.XLS", "xls"));
                int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
                
                if(OPCION_SELECCIONADA == 1){ return; }
                
                String ruta = RUTA.getSelectedFile().toString();
                ruta = ruta.split(Pattern.quote("."))[0];                  
                File archivo = new File(ruta+".xls");
                
                if(archivo.exists()){
                    int opcion = JOptionPane.showOptionDialog(null, "¿Desea reemplazar el archivo?",
                            "¡Archivo existente!", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,
                            null, null, 0);
                    if(opcion==0){
                        RUTA.getSelectedFile().delete();
                    }else{
                        return;
                    }
                }
                if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                    Reporte_Estudiante_Lista Tabla = new Reporte_Estudiante_Lista(
                            archivo.getAbsolutePath(),
                            grado,
                            Ventana
                    );
                } 
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                reseña.setText("Genere un listado de estudiantes");
            }

            @Override
            public void mouseExited(MouseEvent me) {
                reseña.setText("");
            }
        });
        estudiantes.setFocusable(false);
        add(estudiantes);
        
        posx += anchox;
        
        //pagos
        pagos = new JButton(Ventana.cargarImagen("b_pagos.png",anchox,(int)(Alto*0.5)));
        pagos.setRolloverIcon(Ventana.cargarImagen("b_pagos_hover.png",anchox,(int)(Alto*0.5)));
        pagos.setSize(anchox,(int)(Alto*0.5));
        pagos.setLocation(posx,0);
        pagos.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        pagos.setContentAreaFilled(false);
        pagos.setCursor(new Cursor(Cursor.HAND_CURSOR));
        pagos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if(pagos.isEnabled()){
                    String identificador = "";
                    
                    String grado = "";
                
                    if(!Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") &&
                            !Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
                        grado = Ventana.obtenerGradoACargo(Ventana.Login.Cedula_User);
                    }else{
                        String[] OPCION_GRADO = new String[10];
                        OPCION_GRADO[0] = "TODOS";
                        OPCION_GRADO[1] = "PRIMER GRUPO";
                        OPCION_GRADO[2] = "SEGUNDO GRUPO";
                        OPCION_GRADO[3] = "TERCER GRUPO";
                        OPCION_GRADO[4] = "PRIMER GRADO";
                        OPCION_GRADO[5] = "SEGUNDO GRADO";
                        OPCION_GRADO[6] = "TERCER GRADO";
                        OPCION_GRADO[7] = "CUARTO GRADO";
                        OPCION_GRADO[8] = "QUINTO GRADO";
                        OPCION_GRADO[9] = "SEXTO GRADO";
                        grado = (
                        (String) JOptionPane.showInputDialog(null, 
                            "Seleccione un Grado",
                            "Selección de Grado",
                            JOptionPane.QUESTION_MESSAGE, 
                            null, 
                            OPCION_GRADO, 
                            OPCION_GRADO[0])
                        );
                        
                        if(grado == null){ return; }
                    }
                    String[] OPCION;
                    if(grado.equalsIgnoreCase("TODOS"))
                        OPCION = obtenerEstudiantes();
                    else
                        OPCION = obtenerEstudiantes(grado);
                    
                    if(OPCION.length<1){
                        JOptionPane.showMessageDialog(null, "No existe estudiante inscrito en el grado "+
                                grado, "¡Alerta!", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    
                    identificador = (
                    (String) JOptionPane.showInputDialog(null, 
                        "Seleccione un Estudiante",
                        "Selección de Estudiante",
                        JOptionPane.QUESTION_MESSAGE, 
                        null, 
                        OPCION, 
                        OPCION[0])
                    );
                    
                    if(identificador == null){ return; }
                    
                    JFileChooser RUTA = new JFileChooser();
                    RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    RUTA.setFileFilter(new FileNameExtensionFilter("*.XLS", "xls"));
                    int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
                    
                    if(OPCION_SELECCIONADA == 1){ return; }
                
                    String ruta = RUTA.getSelectedFile().toString();
                    ruta = ruta.split(Pattern.quote("."))[0];                  
                    File archivo = new File(ruta+".xls");

                    while(true){
                        if(archivo.exists()){
                            if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                                    "Archivo Existente",
                                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                                RUTA.getSelectedFile().delete();
                            }else{
                                return;
                            }
                        }
                        
                        if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                            Reporte_Pagos_Lista Tabla = new Reporte_Pagos_Lista(
                                archivo.getAbsolutePath(),
                                getIdentidicador(identificador),
                                Ventana
                            );
                        }
                        break;
                    }
                } 
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                reseña.setText("Genere una lista de los pagos de un estudiante");
            }

            @Override
            public void mouseExited(MouseEvent me) {
                reseña.setText("");
            }
        });
        pagos.setFocusable(false);
        if(Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") ||
            Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
            pagos.setEnabled(true);
        }else{
            pagos.setEnabled(false);
        }
        add(pagos);
        
        posx += anchox;
        
        //Certificado de Promocion
        promocion = new JButton(Ventana.cargarImagen("b_promocion.png",anchox,(int)(Alto*0.5)));
        promocion.setRolloverIcon(Ventana.cargarImagen("b_promocion_hover.png",anchox,(int)(Alto*0.5)));
        promocion.setSize(anchox,(int)(Alto*0.5));
        promocion.setLocation(posx,0);
        promocion.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        promocion.setContentAreaFilled(false);
        promocion.setCursor(new Cursor(Cursor.HAND_CURSOR));
        promocion.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if(promocion.isEnabled()){
                    String ESTUDIANTE_SELECCIONADO = "";
                    
                    String grado = "";
                
                    if(!Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") &&
                            !Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
                        grado = Ventana.obtenerGradoACargo(Ventana.Login.Cedula_User);
                    }else{
                        String[] OPCION_GRADO = new String[10];
                        OPCION_GRADO[0] = "TODOS";
                        OPCION_GRADO[1] = "PRIMER GRUPO";
                        OPCION_GRADO[2] = "SEGUNDO GRUPO";
                        OPCION_GRADO[3] = "TERCER GRUPO";
                        OPCION_GRADO[4] = "PRIMER GRADO";
                        OPCION_GRADO[5] = "SEGUNDO GRADO";
                        OPCION_GRADO[6] = "TERCER GRADO";
                        OPCION_GRADO[7] = "CUARTO GRADO";
                        OPCION_GRADO[8] = "QUINTO GRADO";
                        OPCION_GRADO[9] = "SEXTO GRADO";
                        grado = (
                        (String) JOptionPane.showInputDialog(null, 
                            "Seleccione un Grado",
                            "Selección de Grado",
                            JOptionPane.QUESTION_MESSAGE, 
                            null, 
                            OPCION_GRADO, 
                            OPCION_GRADO[0])
                        );
                        
                        if(grado == null){ return; }
                    }
                    String[] OPCION;
                    if(grado.equalsIgnoreCase("TODOS"))
                        OPCION = obtenerEstudiantes();
                    else
                        OPCION = obtenerEstudiantes(grado);
                    
                    if(OPCION.length<1){
                        JOptionPane.showMessageDialog(null, "No existe estudiante inscrito en el grado "+
                                grado, "¡Alerta!", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    
                    ESTUDIANTE_SELECCIONADO = (
                    (String) JOptionPane.showInputDialog(null, 
                        "Seleccione un Estudiante",
                        "Selección de Estudiante",
                        JOptionPane.QUESTION_MESSAGE, 
                        null, 
                        OPCION, 
                        OPCION[0])
                    );
                    
                    if(ESTUDIANTE_SELECCIONADO == null){ return; }
                    System.out.println(ESTUDIANTE_SELECCIONADO);
                    
                    JFileChooser RUTA = new JFileChooser();
                    RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    RUTA.setFileFilter(new FileNameExtensionFilter("*.PDF", "pdf"));
                    int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
                    
                    if(OPCION_SELECCIONADA == 1){ return; }
                
                    String ruta = RUTA.getSelectedFile().toString();
                    ruta = ruta.split(Pattern.quote("."))[0];                  
                    File archivo = new File(ruta+".pdf");

                    while(true){
                        if(archivo.exists()){
                            if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                                    "Archivo Existente",
                                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                                RUTA.getSelectedFile().delete();
                            }else{
                                return;
                            }
                        }
                        
                        if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                            Certificado_Promocion Tabla = new Certificado_Promocion(
                                    archivo.getAbsolutePath(),
                                    getEstudiante(ESTUDIANTE_SELECCIONADO),
                                    getIdentidicador(ESTUDIANTE_SELECCIONADO),
                                    getGrado(ESTUDIANTE_SELECCIONADO),
                                    getGradoNuevo(getGrado(ESTUDIANTE_SELECCIONADO)),
                                    getPeriodo(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                    getNotaDefinitiva(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                    getDia(), getMes(), getAno(),
                                    getDocente(getGrado(ESTUDIANTE_SELECCIONADO)),
                                    Ventana
                            );
                        }
                        break;
                    }
                } 
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                reseña.setText("Genere certificado de promoción");
            }

            @Override
            public void mouseExited(MouseEvent me) {
                reseña.setText("");
            }
        });
        promocion.setFocusable(false);
        if(Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") ||
            Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
            promocion.setEnabled(true);
        }else{
            promocion.setEnabled(false);
        }
        add(promocion);
        
        posx += anchox;
        
        //Constancia de Estudio
        estudio = new JButton(Ventana.cargarImagen("b_estudios.png",anchox,(int)(Alto*0.5)));
        estudio.setRolloverIcon(Ventana.cargarImagen("b_estudios_hover.png",anchox,(int)(Alto*0.5)));
        estudio.setSize(anchox,(int)(Alto*0.5));
        estudio.setLocation(posx,0);
        estudio.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        estudio.setContentAreaFilled(false);
        estudio.setCursor(new Cursor(Cursor.HAND_CURSOR));
        estudio.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if(estudio.isEnabled()){
                    String ESTUDIANTE_SELECCIONADO = "";
                    
                    String grado = "";
                    
                    String[] OPCION_GRADO = new String[10];
                    OPCION_GRADO[0] = "TODOS";
                    OPCION_GRADO[1] = "PRIMER GRUPO";
                    OPCION_GRADO[2] = "SEGUNDO GRUPO";
                    OPCION_GRADO[3] = "TERCER GRUPO";
                    OPCION_GRADO[4] = "PRIMER GRADO";
                    OPCION_GRADO[5] = "SEGUNDO GRADO";
                    OPCION_GRADO[6] = "TERCER GRADO";
                    OPCION_GRADO[7] = "CUARTO GRADO";
                    OPCION_GRADO[8] = "QUINTO GRADO";
                    OPCION_GRADO[9] = "SEXTO GRADO";
                    
                    grado = (
                    (String) JOptionPane.showInputDialog(null, 
                        "Seleccione un Grado",
                        "Selección de Grado",
                        JOptionPane.QUESTION_MESSAGE, 
                        null, 
                        OPCION_GRADO, 
                        OPCION_GRADO[0])
                    );

                    if(grado == null){ return; }
                    
                    String[] OPCION;
                    if(grado.equalsIgnoreCase("TODOS"))
                        OPCION = obtenerEstudiantes();
                    else
                        OPCION = obtenerEstudiantes(grado);
                    
                    if(OPCION.length<1){
                        JOptionPane.showMessageDialog(null, "No existe estudiante inscrito en el grado "+
                                grado, "¡Alerta!", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    
                    ESTUDIANTE_SELECCIONADO = (
                    (String) JOptionPane.showInputDialog(null, 
                        "Seleccione un Estudiante",
                        "Selección de Estudiante",
                        JOptionPane.QUESTION_MESSAGE, 
                        null, 
                        OPCION, 
                        OPCION[0])
                    );
                    
                    if(ESTUDIANTE_SELECCIONADO == null){ return; }
                    
                    JFileChooser RUTA = new JFileChooser();
                    RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    RUTA.setFileFilter(new FileNameExtensionFilter("*.PDF", "pdf"));
                    int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
                    
                    if(OPCION_SELECCIONADA == 1){ return; }
                
                    String ruta = RUTA.getSelectedFile().toString();
                    ruta = ruta.split(Pattern.quote("."))[0];                  
                    File archivo = new File(ruta+".pdf");

                    while(true){
                        if(archivo.exists()){
                            if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                                    "Archivo Existente",
                                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                                RUTA.getSelectedFile().delete();
                            }else{
                                return;
                            }
                        }
                        
                        if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                            Constancia_Estudio Tabla = new Constancia_Estudio(
                                archivo.getAbsolutePath(), 
                                getEstudiante(ESTUDIANTE_SELECCIONADO),
                                getGrado(ESTUDIANTE_SELECCIONADO),
                                getPeriodo(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                getDia(), getMes(), getAno(), 
                                getRepresentante(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                getRepresentanteCedula(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                Ventana
                            );
                        }
                        break;
                    }
                } 
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                reseña.setText("Genere constancia de estudio");
            }

            @Override
            public void mouseExited(MouseEvent me) {
                reseña.setText("");
            }
        });
        estudio.setFocusable(false);
        if(Ventana.Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") ||
            Ventana.Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
            estudio.setEnabled(true);
        }else{
            estudio.setEnabled(false);
        }
        add(estudio);
        
        //reseña
        reseña = new JLabel();
        reseña.setSize((int)(Ancho*0.9),(int)(Alto*0.2));
        reseña.setLocation((int)(Ancho*0.05),(int)(Alto*0.6));
        reseña.setFont(new Font("Arial",Font.BOLD,25));
        reseña.setBackground(Color.WHITE);
        reseña.setHorizontalAlignment(SwingConstants.CENTER);
        add(reseña);
    }
    
    public String[] obtenerEstudiantes(){
        List<String> estudiantes = Ventana.obtenerListaEstudiante();
        String[] lista = new String[estudiantes.size()/4];
        for(int i=0,x=0;i<estudiantes.size();i+=4){
            lista[x++] = estudiantes.get(i)+"-"+estudiantes.get(i+1)+" "+estudiantes.get(i+2)+"-"+Ventana.getGradoStr(Integer.parseInt(estudiantes.get(i+3)));
        }
        return lista;
    }
    
    public String[] obtenerEstudiantes(String grado){
        List<String> estudiantes = Ventana.obtenerListaEstudiante(grado);
        String[] lista = new String[estudiantes.size()/4];
        for(int i=0,x=0;i<estudiantes.size();i+=4){
            lista[x++] = estudiantes.get(i)+"-"+estudiantes.get(i+1)+" "+estudiantes.get(i+2)+"-"+Ventana.getGradoStr(Integer.parseInt(estudiantes.get(i+3)));
        }
        return lista;
    }
    
    public String[] obtenerLapso1(String estudiante){
        List<String> retorno = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM boletin WHERE identificador_estudiante='"+estudiante+"' AND lapso=1";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno.add(rs.getString("nota"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Lapso 1", JOptionPane.ERROR_MESSAGE);
        }
        String[] aux = new String[retorno.size()];
        return retorno.toArray(aux);
    }
    
    public String[] obtenerLapso2(String estudiante){
        List<String> retorno = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM boletin WHERE identificador_estudiante='"+estudiante+"' AND lapso=2";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno.add(rs.getString("nota"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Lapso 2", JOptionPane.ERROR_MESSAGE);
        }
        String[] aux = new String[retorno.size()];
        return retorno.toArray(aux);
    }
    
    public String[] obtenerLapso3(String estudiante){
        List<String> retorno = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM boletin WHERE identificador_estudiante='"+estudiante+"' AND lapso=3";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno.add(rs.getString("nota"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Lapso 3", JOptionPane.ERROR_MESSAGE);
        }
        String[] aux = new String[retorno.size()];
        return retorno.toArray(aux);
    }
    
    public String getRepresentante(String estudiante){
        String retorno = "";
        try{            
            String Query = "SELECT * FROM representante WHERE identificador_estudiante='"+estudiante+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                System.out.println(rs.getString("nombre_apellido_madre"));
                System.out.println(rs.getString("nombre_apellido_padre"));
                System.out.println(rs.getString("nombre_apellido_rlegal"));
                
                if(!"null".equals(rs.getString("nombre_apellido_madre"))) 
                    retorno += rs.getString("nombre_apellido_madre");
                else if(!"null".equals(rs.getString("nombre_apellido_padre")))
                    retorno += rs.getString("nombre_apellido_padre");
                else if(!"null".equals(rs.getString("nombre_apellido_rlegal")))
                    retorno += rs.getString("nombre_apellido_rlegal");
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR REPRESENTANTE!", "Representante", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getRepresentanteCedula(String estudiante){
        String retorno = "";
        try{            
            String Query = "SELECT * FROM representante WHERE identificador_estudiante='"+estudiante+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                
                if(!"null".equals(rs.getString("nombre_apellido_madre"))) 
                    retorno += rs.getString("cedula_madre");
                else if(!"null".equals(rs.getString("nombre_apellido_padre")))
                    retorno += rs.getString("cedula_padre");
                else if(!"null".equals(rs.getString("nombre_apellido_rlegal")))
                    retorno += rs.getString("cedula_rlegal");
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR REPRESENTANTE!", "Representante", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getDocente(String grado){
        String retorno = "";
        int gradoid = Ventana.getGrado(grado);
        System.out.println(gradoid);
        try{            
            String Query = "SELECT * FROM empleado WHERE grado_id='"+gradoid+"' and eliminado=0";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno += rs.getString("nombres");
                retorno += " "+rs.getString("apellidos");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR DOCENTE!", "Empleado", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getCedula(String estudiante){
        String retorno = "";
        try{            
            String Query = "SELECT * FROM representante WHERE identificador_estudiante='"+estudiante+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno += rs.getString("cedula_madre");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Cedula", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getIdentidicador(String estudiante){
        return estudiante.split(Pattern.quote("-"))[0];
    }
    
    public String getEstudiante(String estudiante){
        return estudiante.split(Pattern.quote("-"))[1];
    }
    
    public String getGrado(String estudiante){
        return estudiante.split(Pattern.quote("-"))[2];
    }
    
    public String getGradoNuevo(String grado){
        int gradoid = Ventana.getGrado(grado);
        if(gradoid == 6){
            return "SEPTIMO GRADO";
        }
        if(gradoid == 10){
            return "PRIMER GRADO";
        }
        return Ventana.getGradoStr(gradoid+1);
    }
    
    public String getNotaDefinitiva(String id){
        String nota = "";         
        int [] not = new int[7];
        int def = 0;
        
        not[0] = getDefinitiva(id, "LENGUA Y LITERATURA");
        not[1] = getDefinitiva(id, "MATEMÁTICA");
        not[2] = getDefinitiva(id, "CIENCIAS DE LA NATURALEZA");
        not[3] = getDefinitiva(id, "CIENCIAS SOCIALES");
        not[4] = getDefinitiva(id, "EDUCACION ESTÉTICA");
        not[5] = getDefinitiva(id, "EDUCACION FÍSICA");
        not[6] = getDefinitiva(id, "INGLÉS");
        
        def = (not[0]+not[1]+not[2]+not[3]+not[4]+not[5]+not[6])/7;
        
        if(def>=9) nota = "A";
        if(def>=7 && def<9) nota = "B";
        if(def>=5 && def<7) nota = "C";
        if(def>=3 && def<5) nota = "D";
        if(def>=1 && def<3) nota = "E";
        if(def<1) nota = "";
        
        return nota;
    }
    
    public int getDefinitiva(String id, String nombre_materia){
        int nota = 0;
        try{
            String [] n = new String[3];
            String Query = "SELECT * FROM boletin WHERE identificador_estudiante='"+id+"' and nombre_materia='"+nombre_materia+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            rs.next();
            n[0] = rs.getString("nota");
            rs.next();
            n[1] = rs.getString("nota");
            rs.next();
            n[2] = rs.getString("nota");            
            
            nota = notaDefinitiva(n[0], n[1], n[2]);            
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR ESTUDIANTE!", "Estudiante", JOptionPane.ERROR_MESSAGE);
        }
        return nota;
    }
    
    public int notaDefinitiva(String nota1,String nota2,String nota3){
        int nota = 0;
        int n1 = 0, n2 = 0, n3 =0, def = 0;
        //NOTA 1
        if(nota1.equalsIgnoreCase("A")) n1 = 10;
        if(nota1.equalsIgnoreCase("B")) n1 = 8;
        if(nota1.equalsIgnoreCase("C")) n1 = 6;
        if(nota1.equalsIgnoreCase("D")) n1 = 4;
        if(nota1.equalsIgnoreCase("E")) n1 = 2;
        if(nota1.equalsIgnoreCase("")){
            n1 = 0;
            return 0;
        }
        
        //NOTA 2
        if(nota2.equalsIgnoreCase("A")) n2 = 10;
        if(nota2.equalsIgnoreCase("B")) n2 = 8;
        if(nota2.equalsIgnoreCase("C")) n2 = 6;
        if(nota2.equalsIgnoreCase("D")) n2 = 4;
        if(nota2.equalsIgnoreCase("E")) n2 = 2;
        if(nota2.equalsIgnoreCase("")){
            n2 = 0;
            return 0;
        }
        
        //NOTA 3
        if(nota3.equalsIgnoreCase("A")) n3 = 10;
        if(nota3.equalsIgnoreCase("B")) n3 = 8;
        if(nota3.equalsIgnoreCase("C")) n3 = 6;
        if(nota3.equalsIgnoreCase("D")) n3 = 4;
        if(nota3.equalsIgnoreCase("E")) n3 = 2;
        if(nota3.equalsIgnoreCase("")){
            n3 = 0;
            return 0;
        }
        
        def = (n1+n2+n3)/3;
        
        if(def>=9) nota = 10;
        if(def>=7 && def<9) nota = 8;
        if(def>=5 && def<7) nota = 6;
        if(def>=3 && def<5) nota = 4;
        if(def>=1 && def<3) nota = 2;
        if(def<1) nota = 0;
                
        return nota;
    }
    
    public String getDia(){
        DateFormat df = new SimpleDateFormat("dd");
        Date date = new Date();
        return df.format(date);
    }
    public String getMes(){
        DateFormat df = new SimpleDateFormat("MMMM");
        Date date = new Date();
        return df.format(date);
    }
    public String getAno(){
        DateFormat df = new SimpleDateFormat("yyyy");
        Date date = new Date();
        return df.format(date);
    }
    
    public String getPeriodo(String estudiante){
        String retorno = "";
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+estudiante+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno += rs.getString("periodo_escolar");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR ESTUDIANTE!", "Estudiante", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getFecha(String identificador){
        String retorno = "";
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+identificador+"'";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            while(rs.next()){
                retorno += rs.getString("dia_nacimiento");
                retorno += "/"+rs.getString("mes_nacimiento");
                retorno += "/"+rs.getString("ano_nacimiento");
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR FECHA!", "Estudiante", JOptionPane.ERROR_MESSAGE);
        }
        
        return retorno;
    }
    
    public String getEdad(String fecha){
        int dia = Integer.parseInt(fecha.split(Pattern.quote("/"))[0]);
        int mes = Integer.parseInt(fecha.split(Pattern.quote("/"))[1]);
        int ano = Integer.parseInt(fecha.split(Pattern.quote("/"))[2]);
        
        Calendar c = Calendar.getInstance();
        int dia_a = c.get(Calendar.DATE);
        int mes_a = c.get(Calendar.MONTH)+1;
        int ano_a = c.get(Calendar.YEAR);
        
        if(mes<mes_a)
            return ""+(ano_a-ano);
        
        if(mes>mes_a)
            return ""+((ano_a-ano)-1);
        
        if(mes==mes_a){
            if(dia<=dia_a)
                return ""+(ano_a-ano);
            if(dia>dia_a)
                return ""+((ano_a-ano)-1);
        }
        
        return ""+(ano_a-ano);
    }
    
    public String obtenerRutaSinArchivo(String file){
        String [] aux = file.split(Pattern.quote("/"));
        String retorno = "";
        for(int i=0;i<aux.length-1;i++){
            retorno += aux[i];
        }
        return retorno;
    }
    
    public String obtenerArchivo(String file){
        return file.split(Pattern.quote("//"))[file.split(Pattern.quote("//")).length-1];
    }
    
    public void generarBoletin(String ESTUDIANTE_SELECCIONADO){   
        if(ESTUDIANTE_SELECCIONADO == null){
            String[] OPCION;

            if(Ventana.Login.Cargo_User.equalsIgnoreCase("PROFESOR")){
                OPCION = Gestionar_Reportes.this.obtenerEstudiantes(Ventana.obtenerGradoACargo(Ventana.Login.Cedula_User));
            }else{
                OPCION = Gestionar_Reportes.this.obtenerEstudiantes();
            }
            
            List<String> aux = new ArrayList<>();
            for(int i = 0; i<OPCION.length; i++){
                if(!(OPCION[i].contains("PRIMER GRUPO") || OPCION[i].contains("SEGUNDO GRUPO") || OPCION[i].contains("TERCER GRUPO")))
                    aux.add(OPCION[i]);
            } 
            
            String[] OPCION2 = new String[aux.size()];
            OPCION2 = aux.toArray(OPCION2);

            ESTUDIANTE_SELECCIONADO = (
            (String) JOptionPane.showInputDialog(null, 
                "Seleccione un Estudiante",
                "Selección de Estudiante",
                JOptionPane.QUESTION_MESSAGE, 
                null, 
                OPCION2, 
                OPCION2[0])
            );

            if(ESTUDIANTE_SELECCIONADO == null){ return; }
        }
        
        String [] materia = new String[7];
        materia[0] = "LENGUA Y LITERATURA";
        materia[1] = "MATEMÁTICA";
        materia[2] = "CIENCIAS DE LA NATURALEZA";
        materia[3] = "CIENCIAS SOCIALES";
        materia[4] = "EDUCACION ESTÉTICA";
        materia[5] = "EDUCACION FÍSICA";
        materia[6] = "INGLÉS";
        JFileChooser RUTA = new JFileChooser();
        int OPCION_SELECCIONADA = 0;

        while(true){
            System.out.println(ESTUDIANTE_SELECCIONADO);
            RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            RUTA.setFileFilter(new FileNameExtensionFilter("*.PDF", "pdf"));
            OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
            
            if(OPCION_SELECCIONADA == 1){ return; }
            
            String ruta = RUTA.getSelectedFile().toString();
            ruta = ruta.split(Pattern.quote("."))[0];
            File archivo = new File(ruta+".pdf");
            
            if(archivo.exists()){
                if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                    "Archivo Existente",
                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                    RUTA.getSelectedFile().delete();
                }else{
                    return;
                }
            }
            if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){

                Boletin_Digital Tabla = new Boletin_Digital(
                        archivo.getAbsolutePath(),
                        materia,
                        obtenerLapso1(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        obtenerLapso2(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        obtenerLapso3(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        getEstudiante(ESTUDIANTE_SELECCIONADO),
                        getGrado(ESTUDIANTE_SELECCIONADO),
                        getPeriodo(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        getDocente(getGrado(ESTUDIANTE_SELECCIONADO)),
                        getFecha(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        getEdad(getFecha(getIdentidicador(ESTUDIANTE_SELECCIONADO))),
                        getRepresentante(getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                        Ventana
                );
            }
            break;
        }
    }
}
