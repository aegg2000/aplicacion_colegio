package aplicacion_colegio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.apache.commons.codec.binary.Base64;

public class Ventana_Principal extends JFrame{
    public int Ancho, Alto;
    public Menu_Home menu_home;
    public MySQL db;
    public Login Login;
    
    public Ventana_Principal(Login Login){
        super("Sistema Colegio");
        this.Login = Login;
        
        cuerpo();
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
                
        //MENU_HOME
        menu_home = new Menu_Home(this,Login);
        menu_home.setLocation(0,0);
        menu_home.setSize(Ancho,Alto);
        add(menu_home);
    }

    public void cuerpo(){
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                if (JOptionPane.showConfirmDialog(rootPane, "¿Desea realmente salir del sistema?",
                    "Salir del sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                    System.exit(0);
                }
            }
        });
        
        UIManager UI=new UIManager();
        UI.put("OptionPane.background", Color.white);
        UI.put("Panel.background", Color.white);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        setIconImage(cargarImagen("logo_icon.png",300,300).getImage());
        //OBTENER RESOLUCION DE PANTALLA
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Ancho = screenSize.width; 
        Alto = screenSize.height - ((int)(screenSize.height*0.05));
        setSize(Ancho,Alto);
        getContentPane().setLayout(null);
        try {
            UIManager.setLookAndFeel(
                UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
        }
    }
    public List<String> ordenar(List<String> aux){
        Collections.sort(aux);
        return aux;
    }
    public int getGrado(String grado_str){
        int grado;
        switch (grado_str) {
            case "PRIMER GRADO":
                grado = 1;
                break;
            case "SEGUNDO GRADO":
                grado = 2;
                break;
            case "TERCER GRADO":
                grado = 3;
                break;
            case "CUARTO GRADO":
                grado = 4;
                break;
            case "QUINTO GRADO":
                grado = 5;
                break;
            case "SEXTO GRADO":
                grado = 6;
                break;
            case "PRIMER GRUPO":
                grado = 8;
                break;
            case "SEGUNDO GRUPO":
                grado = 9;
                break;
            case "TERCER GRUPO":
                grado = 10;
                break;
            default:
                grado = 7;
                break;
        }
        return grado;
    }
    public String getGradoStr(int grado){
        String grado_str;
        switch (grado) {
            case 1:
                grado_str = "PRIMER GRADO";
                break;
            case 2:
                grado_str = "SEGUNDO GRADO";
                break;
            case 3:
                grado_str = "TERCER GRADO";
                break;
            case 4:
                grado_str = "CUARTO GRADO";
                break;
            case 5:
                grado_str = "QUINTO GRADO";
                break;
            case 6:
                grado_str = "SEXTO GRADO";
                break;
            case 8:
                grado_str = "PRIMER GRUPO";
                break;
            case 9:
                grado_str = "SEGUNDO GRUPO";
                break;
            case 10:
                grado_str = "TERCER GRUPO";
                break;
            default:
                grado_str = "NINGUNO";
                break;
        }
        return grado_str;
    }
    public void registrarPersonal(Registro_Personal personal){
        String dir_trabajo = personal.dir_trabajo.getText();
        String dir_habitacion = personal.dir_habitacion.getText();
        String tlf_trabajo = personal.tlf_trabajo.getText();
        String tlf_celular = personal.tlf_celular.getText();
        String tlf_habitacion = personal.tlf_habitacion.getText();
        
        String mensaje = "";
        if(empleadoRepetidoID(personal.cedula.getText())){
            mensaje += "CEDULA: !El documento de identidad "+personal.cedula.getText()+" ya se encuentra\n"
                    + "en nuestra base de datos!\n";
        }
        if(empleadoRepetidoCorreo(personal.correo.getText())){
            mensaje += "CORREO ELECTRÓNICO: !El correo "+personal.correo.getText()+" ya se encuentra\n"
                    + "en nuestra base de datos!\n";
        }
        if(empleadoRepetidoGrado(personal.año_escolar,(String)(personal.cargo.getSelectedItem()))){
            mensaje += "CARGO: !El cargo "+(String)(personal.cargo.getSelectedItem())+" para el grado\n"
                    + "escolar "+personal.año_escolar+" ya se encuentra ocupado!";
        }
        
        if(mensaje.length()>0){
            JOptionPane.showMessageDialog(null, mensaje, "AVISO",JOptionPane.PLAIN_MESSAGE);
            return;
        }
        
        if(dir_trabajo.equalsIgnoreCase("Dirección de Trabajo..."))
            dir_trabajo = "NONE";

        if(dir_habitacion.equalsIgnoreCase("Dirección de Habitación..."))
                    dir_habitacion = "NONE";        
        
        if(tlf_trabajo.equalsIgnoreCase("Teléfono de Trabajo..."))
            tlf_trabajo = "NONE";        
        
        if(tlf_celular.equalsIgnoreCase("Teléfono de Celular..."))
            tlf_celular = "NONE";
        
        if(tlf_habitacion.equalsIgnoreCase("Teléfono de Habitación..."))
            tlf_habitacion = "NONE";
            
        int grado = getGrado(personal.año_escolar);
        try{            
            String Query = "INSERT INTO empleado "
                + "(cedula,nombres,apellidos,cargo,grado_id,direccion_trabajo,direccion_habitacion,"
                + "telefono_trabajo,telefono_habitacion,telefono_celular,correo_electronico,"
                + "contrasena_acceso) VALUES ('"+personal.cedula_combo.getSelectedItem()+""+personal.cedula.getText()+"',"
                + "'"+personal.nombre.getText()+"','"+personal.apellido.getText()+"',"
                + "'"+(String)(personal.cargo.getSelectedItem())+"',"
                + "'"+grado+"',"
                + "'"+dir_trabajo+"', '"+dir_habitacion+"',"
                + "'"+tlf_trabajo+"', '"+tlf_habitacion+"',"
                + "'"+tlf_celular+"', '"+personal.correo.getText()+"',"
                + "'"+Encriptar(personal.contraseña.getText())+"')";
        
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);
                        
            JOptionPane.showMessageDialog(null, "¡Personal Registrado!", "Registro de Personal", JOptionPane.INFORMATION_MESSAGE);
            personal.limpiarCampos();
        }catch(Exception e){
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Registro de Personal", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void registrarEstudiante(
            Registro_Estudiante estudiante,String Comprobante,
            String Concepto, String monto, String descripcion)
    {
        String año = (String)(estudiante.año.getSelectedItem());
        String identificador = "error";
        if(estudiante.grado.getSelectedItem() == "CUARTO GRADO" || estudiante.grado.getSelectedItem() == "QUINTO GRADO" || estudiante.grado.getSelectedItem() == "SEXTO GRADO"){
            identificador = estudiante.cedula_combo.getSelectedItem()+estudiante.e_cedula.getText();
        }
        else if(estudiante.b_check_m){
            identificador = "1"+año.substring(2,4)+""+estudiante.r_cedula_m.getText();
        }else if(estudiante.b_check_p){
            identificador = "1"+año.substring(2,4)+""+estudiante.r_cedula_p.getText();
        }else if(estudiante.b_check_l){
            identificador = "1"+año.substring(2,4)+""+estudiante.r_cedula_l.getText();
        }     
        
        if(estudianteRepetido(identificador)){
            JOptionPane.showMessageDialog(null, "¡El estudiante que desea registrar se encuentra\n"
                    + "en el base de datos!","AVISO", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        try{
            db.comando = db.conexion.createStatement();
            //REGISTRO ESTUDIANTE-----------------------------------------------
            String grado = (String)(estudiante.grado.getSelectedItem());
            String dia = (String)(estudiante.dia.getSelectedItem());
            String mes = (String)(estudiante.mes.getSelectedItem());
            String año_e = (String)(estudiante.año.getSelectedItem());
            
            String periodo = "";
            Calendar c = Calendar.getInstance();
            int mes_actual = (c.get(Calendar.MONTH));
            int año_actual = (c.get(Calendar.YEAR));
            if((mes_actual+1)>6)
                periodo = (año_actual)+"-"+(año_actual+1);
            else
                periodo = (año_actual-1)+"-"+(año_actual);
            
            int grado_id = getGrado(grado);
            
            String Query = "INSERT INTO estudiante "
                + "(identificador_estudiante,nombres,apellidos,grado_id,"
                + "periodo_escolar,dia_nacimiento,mes_nacimiento,ano_nacimiento)"
                + " VALUES ("
                + "'"+identificador+"',"
                + "'"+estudiante.e_nombre.getText()+"',"
                + "'"+estudiante.e_apellido.getText()+"',"
                + "'"+grado_id+"',"
                + "'"+periodo+"',"
                + "'"+dia+"',"
                + "'"+mes+"',"
                + "'"+año_e+"')";
        
            db.comando.executeUpdate(Query);
            
            new Thread(){
                public void run(){
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Ventana_Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }.run();
            
            
            //REGISTRO REPRESENTANTE--------------------------------------------
            Query = "INSERT INTO representante "
                + "(cedula_madre,nombre_apellido_madre,direccion_habitacion_madre,telefono_habitacion_madre,"
                + "telefono_celular_madre,cedula_padre,nombre_apellido_padre,direccion_habitacion_padre,"
                + "telefono_habitacion_padre,telefono_celular_padre,cedula_rlegal,nombre_apellido_rlegal,direccion_habitacion_rlegal,"
                + "telefono_habitacion_rlegal, telefono_celular_rlegal, parentesco_rlegal, identificador_estudiante)"
                + " VALUES (";
            if(estudiante.b_check_m){
                Query += "'"+estudiante.cedula_r_combo[0].getSelectedItem()+estudiante.r_cedula_m.getText()+"',"
                        +"'"+estudiante.r_nombre_m.getText()+"',"
                        + "'"+estudiante.r_direccion_m.getText()+"',"
                        + "'"+estudiante.r_tlf_habitacion_m.getText()+"',"
                        + "'"+estudiante.r_tlf_celular_m.getText()+"',";
            }else{
                Query += "'null', 'null', 'null', 'null', 'null',";
            }
            if(estudiante.b_check_p){
                Query += "'"+estudiante.cedula_r_combo[0].getSelectedItem()+estudiante.r_cedula_p.getText()+"',"
                        + "'"+estudiante.r_nombre_p.getText()+"',"
                        + "'"+estudiante.r_direccion_p.getText()+"',"
                        + "'"+estudiante.r_tlf_habitacion_p.getText()+"',"
                        + "'"+estudiante.r_tlf_celular_p.getText()+"',";
            }else{
                Query += "'null', 'null', 'null', 'null', 'null',";
            }
            if(estudiante.b_check_l){
                Query += "'"+estudiante.cedula_r_combo[0].getSelectedItem()+estudiante.r_cedula_l.getText()+"',"
                        + "'"+estudiante.r_nombre_l.getText()+"',"
                        + "'"+estudiante.r_direccion_l.getText()+"',"
                        + "'"+estudiante.r_tlf_habitacion_l.getText()+"',"
                        + "'"+estudiante.r_tlf_celular_l.getText()+"',"
                        + "'"+estudiante.r_parentesco_l.getText()+"',";
            }else{
                Query += "'null', 'null', 'null', 'null', 'null', 'null',";
            }
                Query += "'"+identificador+"')";
                
            System.out.println(Query);
            
            db.comando.executeUpdate(Query);
            
            new Thread(){
                public void run(){
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Ventana_Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }.run();
            
            //REGISTRO PAGOS---------------------------------------------------
            Query = "INSERT INTO pagos "
                + "(identificador_estudiante,comprobante,monto,fecha,concepto,descripcion)"
                + " VALUES ('"
                +identificador+"',"
                + "'"+Comprobante+"',"
                + "'"+monto+"','"
                +menu_home.dia+"/"+menu_home.mes+"/"+menu_home.año+"',"
                + "'"+Concepto+"',"
                + "'"+descripcion+"')";
        
            db.comando.executeUpdate(Query);
            
            //CREAR BOLETIN----------------------------------------------------
            String [] materia = new String[7];
            materia[0] = "LENGUA Y LITERATURA";
            materia[1] = "MATEMÁTICA";
            materia[2] = "CIENCIAS DE LA NATURALEZA";
            materia[3] = "CIENCIAS SOCIALES";
            materia[4] = "EDUCACION ESTÉTICA";
            materia[5] = "EDUCACION FÍSICA";
            materia[6] = "INGLÉS";
            for(int i=0;i<7;i++){
                for(int x=0;x<3;x++){
                    Query = "INSERT INTO boletin "
                        + "(identificador_estudiante,nombre_materia,nota,lapso)"
                        + "VALUES ('"
                        +identificador+"',"
                        + "'"+materia[i]+"',"
                        + "'',"
                        + "'"+(x+1)+"')";
                    db.comando.executeUpdate(Query);
                }
            }
        
            JOptionPane.showMessageDialog(null, "¡"+estudiante.e_nombre.getText()+" "+estudiante.e_apellido.getText()+" ha sido incrito correctamente!",
                    "Registro de Estudiante", JOptionPane.PLAIN_MESSAGE);
            JOptionPane.showMessageDialog(null, "CUPOS DISPONIBLES "+cuposDisponibles(grado, periodo), "CUPOS DISPONIBLES "+grado, JOptionPane.PLAIN_MESSAGE);
            estudiante.limpiarCampos();
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Registro de Estudiante", JOptionPane.ERROR_MESSAGE);
        }
    }
    public List<String> obtenerListaPersonal(int eliminado){
        List<String> empleados = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM empleado WHERE eliminado = "+eliminado+" ORDER BY cargo, apellidos";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                empleados.add(rs.getString("cedula"));
                empleados.add(rs.getString("nombres"));
                empleados.add(rs.getString("apellidos"));
                empleados.add(rs.getString("cargo"));
            }
            empleados.add(""+eliminado+"");
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Personal", JOptionPane.ERROR_MESSAGE);
        }
        
        return empleados;
    }
    public List<String> obtenerEstudiante(String ID){
        List<String> estudiantes = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante = '"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("identificador_estudiante"));
                estudiantes.add(rs.getString("apellidos"));
                estudiantes.add(rs.getString("nombres"));
                estudiantes.add(rs.getString("grado_id"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Lista de Estudiante", JOptionPane.ERROR_MESSAGE);
        }
        return estudiantes;
    }
    public List<String> obtenerListaEstudiante(){
        List<String> estudiantes = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM estudiante WHERE eliminado = 0 ORDER BY grado_id, apellidos";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("identificador_estudiante"));
                estudiantes.add(rs.getString("apellidos"));
                estudiantes.add(rs.getString("nombres"));
                estudiantes.add(rs.getString("grado_id"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return estudiantes;
    }
    public List<String> obtenerListaEstudiante(String grado){
        List<String> estudiantes = new ArrayList<>();
        String Query;
        int gradoid = 0;
        try{
            if(grado.equals("INHABILITADOS")){
                Query = "SELECT * FROM estudiante WHERE eliminado = 1 ORDER BY grado_id, apellidos";
            }else if(grado.equals("SOLVENTES")){
                Query = "SELECT * FROM estudiante WHERE eliminado = 0 AND deuda = 0 ORDER BY grado_id, apellidos";
            }else if(grado.equals("INSOLVENTES")){
                Query = "SELECT * FROM estudiante WHERE eliminado = 0 AND deuda > 0 ORDER BY grado_id, apellidos";
            }else{
                gradoid = getGrado(grado);
                Query = "SELECT * FROM estudiante WHERE grado_id='"+gradoid+"' AND eliminado = 0 ORDER BY apellidos";
            }
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("identificador_estudiante"));
                estudiantes.add(rs.getString("apellidos"));
                estudiantes.add(rs.getString("nombres"));
                estudiantes.add(rs.getString("grado_id"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return estudiantes;
    }
    public String obtenerListaEstudianteID(String identificador){
        String estudiantes = "";
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+identificador+"' ORDER BY apellidos";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes += rs.getString("identificador_estudiante");
                estudiantes += "-"+rs.getString("apellidos");
                estudiantes += " "+rs.getString("nombres");
                estudiantes += "-"+rs.getString("grado_id");
                estudiantes += "-"+rs.getString("eliminado");
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return estudiantes;
    }
    public boolean estudianteRepetido(String identificador){
        List<String> estudiantes = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM estudiante WHERE identificador_estudiante='"+identificador+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiantes.add(rs.getString("identificador_estudiante"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        if(estudiantes.size()>0)
            return true;
        else
            return false;
    }
    public boolean empleadoRepetidoID(String id){
        List<String> empleado = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM empleado WHERE cedula='"+id+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                empleado.add(rs.getString("cedula"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        if(empleado.size()>0)
            return true;
        else
            return false;
    }
    public boolean empleadoRepetidoGrado(String grado, String cargo){
        List<String> empleado = new ArrayList<>();
        int gradoid = getGrado(grado);
        if(gradoid == 7){
            return false;
        }
        try{            
            String Query = "SELECT * FROM empleado WHERE grado_id='"+gradoid+"' and"
                    + " cargo='"+cargo+"' and eliminado=0";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                empleado.add(rs.getString("cedula"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        if(empleado.size()>0)
            return true;
        else
            return false;
    }
    public boolean empleadoRepetidoCorreo(String correo){
        List<String> empleado = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM empleado WHERE correo_electronico='"+correo+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                empleado.add(rs.getString("cedula"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        if(empleado.size()>0)
            return true;
        else
            return false;
    }
    public int cuposDisponibles(String grado, String periodo){
        List<String> estudiante = new ArrayList<>();
        int gradoid = getGrado(grado);
        try{            
            String Query = "SELECT * FROM estudiante WHERE grado_id='"+gradoid+"' and periodo_escolar='"+periodo+"' and eliminado = 0";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                estudiante.add(rs.getString("identificador_estudiante"));
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        
        return (20-estudiante.size());
    }
    public String obtenerGradoACargo(String ID){
        String grado = "";
        try{            
            String Query = "SELECT * FROM empleado WHERE cedula='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                grado = rs.getString("grado_id");
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Personal", JOptionPane.ERROR_MESSAGE);
        }
        
        grado = getGradoStr(Integer.parseInt(grado));
        
        return grado;
    }
    public List<String> obtenerListaPagos(String ID){
        List<String> pagos = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM pagos WHERE identificador_estudiante = '"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                pagos.add(rs.getString("identificador_estudiante"));
                
                String nombre = "";
                try{            
                    String Query_2 = "SELECT * FROM estudiante"
                        + " WHERE"
                        + " identificador_estudiante='"+rs.getString("identificador_estudiante")+"'";

                    db.comando = db.conexion.createStatement();
                    ResultSet rs_2 = db.comando.executeQuery(Query_2);

                    while(rs_2.next()){
                        nombre += rs_2.getString("apellidos")+" "+rs_2.getString("nombres");
                    }

                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "¡ERROR!", "Lista de Estudiante", JOptionPane.ERROR_MESSAGE);
                }
                
                pagos.add(nombre);
                pagos.add(rs.getString("concepto"));
                pagos.add(rs.getString("comprobante"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Pagos", JOptionPane.ERROR_MESSAGE);
        }
        return pagos;
    }
    public List<String> obtenerListaPagos(){
        List<String> pagos = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM pagos";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                pagos.add(rs.getString("identificador_estudiante"));
                
                String nombre = "";
                try{            
                    String Query_2 = "SELECT * FROM estudiante"
                        + " WHERE"
                        + " identificador_estudiante='"+rs.getString("identificador_estudiante")+"'";

                    db.comando = db.conexion.createStatement();
                    ResultSet rs_2 = db.comando.executeQuery(Query_2);

                    while(rs_2.next()){
                        nombre += rs_2.getString("apellidos")+" "+rs_2.getString("nombres");
                    }

                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "¡ERROR!", "Lista de Estudiante", JOptionPane.ERROR_MESSAGE);
                }
                
                pagos.add(nombre);
                pagos.add(rs.getString("concepto"));
                pagos.add(rs.getString("comprobante"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Pagos", JOptionPane.ERROR_MESSAGE);
        }
        
        return pagos;
    }
    public List<String> obtenerBoletin(String ID){
        List<String> notas = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM boletin WHERE identificador_estudiante='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                notas.add(rs.getString("nombre_materia"));
                notas.add(rs.getString("lapso"));
                notas.add(rs.getString("nota"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Notas", JOptionPane.ERROR_MESSAGE);
        }
        
        return notas;
    }
    public List<String> obtenerMateria(String ID){
        List<String> materia = new ArrayList<>();
        try{            
            String Query = "SELECT * FROM boletin WHERE id_materia="+ID;
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                materia.add(rs.getString("nombre"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Materias", JOptionPane.ERROR_MESSAGE);
        }
        
        return materia;
    }
    public boolean comprobanteRepetido(String comprobante){
        boolean grado = false;
        try{            
            String Query = "SELECT * FROM pagos WHERE comprobante='"+comprobante+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                grado = true;
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Pagos", JOptionPane.ERROR_MESSAGE);
        }
        
        return grado;
    }
    public void registrarPago(Registro_Pago registro, String identificador, String comprobante,
            String concepto, String descripcion, String monto){
        if(comprobanteRepetido(comprobante)){
            JOptionPane.showMessageDialog(
                    null, "El comprobante que intenta registrar\n"
                + "ya se encuentra en la base de datos.\n\n"
                + "Verifíque los datos e intente nuevamente", "¡ALERTA!",
                JOptionPane.INFORMATION_MESSAGE
            );
            return;
        }
        try{         
            //REGISTRO PAGOS---------------------------------------------------
            String Query = "INSERT INTO pagos "
                + "(identificador_estudiante,comprobante,monto,fecha,concepto,descripcion)"
                + " VALUES ("
                + "'"+identificador+"',"
                + "'"+comprobante+"',"
                + "'"+monto+"',"
                + "'"+menu_home.dia+"/"+menu_home.mes+"/"+menu_home.año+"',"
                + "'"+concepto+"',"
                + "'"+descripcion+"')";
        
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);            
            
            JOptionPane.showMessageDialog(null, "¡El pago "+comprobante+" ha sido resgistrado correctamente!",
                    "Registro de Pago", JOptionPane.PLAIN_MESSAGE);
            registro.limpiarCampos();
            
            Query = "UPDATE estudiante SET deuda = deuda - "+monto+" WHERE identificador_estudiante = '"+identificador+"'";
            db.comando.executeUpdate(Query);
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Registro de Pago", JOptionPane.ERROR_MESSAGE);
        }
        
        menu_home.Listar_Pagos.cargarListado(obtenerListaPagos(identificador));
        
    }
    public void actualizarNota(String identificador,String materia,String nota,String lapso){
        try{         
            //REGISTRO NOTAS---------------------------------------------------
            String Query = "UPDATE boletin SET "
            +"nota='"+nota+"' "
            + "WHERE "
            + "identificador_estudiante='"+identificador+"' AND  "
            + "nombre_materia='"+materia+"' AND  "
            + "lapso='"+lapso+"'";
        
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);
            
            
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Registro de Notas", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void actualizarEmpleado(String ci,String materia,String nota,String lapso){
        try{         
            //REGISTRO EMPLEADO---------------------------------------------------
            String Query = "UPDATE empleado SET "
            +"nota='"+nota+"' "
            + "WHERE "
            + "cedula='"+ci+"'";
        
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);
            
            JOptionPane.showMessageDialog(null, "¡Empleado actualizado exitosamente!", "Actualización de Empleado", JOptionPane.PLAIN_MESSAGE);
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Actualización de Empleado", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void eliminarEstudiante(String ID)
    {
        try{
            String Query = "UPDATE estudiante SET eliminado = 1 WHERE identificador_estudiante ='"+ID+"'";
            
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);
            JOptionPane.showMessageDialog(null, "Inhabilitación Exitosa", "Inhabilitación de Estudiante", JOptionPane.PLAIN_MESSAGE);
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Inhabilitación de Estudiante", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarEmpleado(String cedula)
    {
        try{
            String Query = "UPDATE empleado SET eliminado = 1 WHERE cedula ='"+cedula+"'";
            
            db.comando = db.conexion.createStatement();
            db.comando.executeUpdate(Query);
            JOptionPane.showMessageDialog(null, "Inhabilitación Exitosa", "Inhabilitación de Empleado", JOptionPane.PLAIN_MESSAGE);
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Inhabilitación de Empleado", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public boolean checkLastAdmin()
    {
        int cantidad = 0;
        try{            
            String Query = "SELECT COUNT(*) AS cantidad FROM empleado WHERE cargo = 'ADMINISTRADOR' AND eliminado = 0";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
              cantidad = Integer.parseInt(rs.getString("cantidad"));
            }
            return cantidad == 1;
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Check Last Admin", JOptionPane.ERROR_MESSAGE);
            return true;
        }
    }
    
    public String obtenerDeuda(String ID)
    {
        String deuda = "";
        try{            
            String Query = "SELECT deuda FROM estudiante WHERE identificador_estudiante='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){
                deuda = rs.getString("deuda");
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Obtener Deuda", JOptionPane.ERROR_MESSAGE);
        }
        return deuda;
    }
    
    public static String Encriptar(String texto) {
        String secretKey = "qualityinfosolutions"; //llave para encriptar datos
        String ret = "";
 
        try {
 
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
 
            byte[] plainTextBytes = texto.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            ret = new String(base64Bytes);
 
        } catch (Exception ex) {
        }
        return ret;
    }
    public static String Desencriptar(String textoEncriptado){
        String secretKey = "qualityinfosolutions"; //llave para desenciptar datos
        String ret = "";

        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            ret = new String(plainText, "UTF-8");

        } catch (Exception ex) {
        }
        return ret;
    }
    
    //METODOS PARA COLOCAR IMAGENES
    public ImageIcon cargarImagen(String nombreDeImagen){
        try{
            return new 
            ImageIcon( ImageIO.read(
            getClass().getResource("/Imagenes/"+nombreDeImagen)
            ));
        }
        catch(IOException ioe){ 
            JOptionPane.showMessageDialog(null, "No se pudo cargar la imagen: "+nombreDeImagen);
            return null;
        }
    }
    public ImageIcon cargarImagen(String nombreDeImagen, int eW, int eH){
        ImageIcon original  = cargarImagen(nombreDeImagen);
        return new ImageIcon(original.getImage()
        .getScaledInstance(eW, eH, Image.SCALE_SMOOTH));
    } 
}
