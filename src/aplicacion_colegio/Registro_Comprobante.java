package aplicacion_colegio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Registro_Comprobante extends JPanel{
    public JLabel h_comprobante, h_monto, h_concepto;
    public JTextField comprobante, monto, concepto;
    public int Ancho,Alto;
    public RoundedCornerBorder Borde;
    public Ventana_Principal Ventana;
    
    public Registro_Comprobante(Ventana_Principal Ventana){
        Ancho = 300;
        Alto = 120;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        setBackground(Color.WHITE);
        
        this.Ventana = Ventana;
        Borde = new RoundedCornerBorder();
        
        //H_USER
        h_comprobante = new JLabel("   Comprobante Electrónico...");
        h_comprobante.setSize((int)(Ancho*0.95),(int)(Alto/3));
        h_comprobante.setLocation((int)(Ancho*0.025),0);
        h_comprobante.setForeground(Color.GRAY);
        h_comprobante.setFont(new Font("Arial",Font.ITALIC,16));
        add(h_comprobante);
        
        //USER
        comprobante = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        comprobante.setSize((int)(Ancho*0.95),(int)(Alto/3));
        comprobante.setLocation((int)(Ancho*0.025),0);
        comprobante.setBackground(Color.WHITE);
        comprobante.setForeground(Color.GRAY);
        comprobante.setFont(new Font("Arial",Font.ITALIC,16));
        comprobante.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(comprobante.getText().length()==0)
                    comprobante.setForeground(Color.GRAY);
                
                if(comprobante.getText().length()>0)
                    h_comprobante.setVisible(false);
                else
                    h_comprobante.setVisible(true);
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        comprobante.setEditable(true);
        add(comprobante);
        
        //H_USER
        h_monto = new JLabel("   Monto Comprobante...");
        h_monto.setSize((int)(Ancho*0.95),(int)(Alto/3));
        h_monto.setLocation((int)(Ancho*0.025),(int)(Alto/3));
        h_monto.setForeground(Color.GRAY);
        h_monto.setFont(new Font("Arial",Font.ITALIC,16));
        h_monto.setVisible(false);
        add(h_monto);
        
        //USER
        monto = new JTextField(Double.toString(Ventana.menu_home.Configuracion.montoInscripcion)){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        monto.setSize((int)(Ancho*0.95),(int)(Alto/3));
        monto.setLocation((int)(Ancho*0.025),(int)(Alto/3));
        monto.setBackground(Color.WHITE);
        monto.setForeground(Color.GRAY);
        monto.setFont(new Font("Arial",Font.ITALIC,16));
        monto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(monto.getText().length()==0)
                    monto.setForeground(Color.GRAY);
                
                if(monto.getText().length()>0)
                    h_monto.setVisible(false);
                else
                    h_monto.setVisible(true);
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        monto.setEditable(true);
        add(monto);
        
        //H_USER
//        h_concepto = new JLabel("INSCRIPCION");
//        h_concepto.setSize((int)(Ancho*0.95),(int)(Alto/3));
//        h_concepto.setLocation((int)(Ancho*0.025),(int)(Alto/3)+(int)(Alto/3));
//        h_concepto.setForeground(Color.GRAY);
//        h_concepto.setFont(new Font("Arial",Font.ITALIC,16));
//        add(h_concepto);
        
        //USER
        concepto = new JTextField("INSCRIPCION"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        concepto.setSize((int)(Ancho*0.95),(int)(Alto/3));
        concepto.setLocation((int)(Ancho*0.025),(int)(Alto/3)+(int)(Alto/3));
        concepto.setBackground(Color.WHITE);
        concepto.setForeground(Color.GRAY);
        concepto.setFont(new Font("Arial",Font.ITALIC,16));
        concepto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(concepto.getText().length()==0)
                    concepto.setForeground(Color.GRAY);
                
//                if(concepto.getText().length()>0)
//                    h_concepto.setVisible(false);
//                else
//                    h_concepto.setVisible(true);
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        concepto.setEditable(false);
        add(concepto);
    }
}
