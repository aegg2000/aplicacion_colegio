package aplicacion_colegio;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Calendar;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Informacion_Cupos extends JPanel{
    public JTextField [] GRADO, CUPOS;
    public String [] Grados;
    public int Ancho, Alto;
    
    public Informacion_Cupos(Ventana_Principal Ventana){
        Ancho = 350;
        Alto = 250;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setBackground(Color.WHITE);
        setLayout(null);
        
        Grados = new String[9];
        Grados[0] = "PRIMER GRADO";
        Grados[1] = "SEGUNDO GRADO";
        Grados[2] = "TERCER GRADO";
        Grados[3] = "CUARTO GRADO";
        Grados[4] = "QUINTO GRADO";
        Grados[5] = "SEXTO GRADO";
        Grados[6] = "PRIMER GRUPO";
        Grados[7] = "SEGUNDO GRUPO";
        Grados[8] = "TERCER GRUPO";
        
        
        GRADO = new JTextField[9];
        CUPOS = new JTextField[9];
        
        int pos_y = 0;
        String periodo = "";
        Calendar c = Calendar.getInstance();
        int mes_actual = (c.get(Calendar.MONTH));
        int año_actual = (c.get(Calendar.YEAR));
        if((mes_actual+1)>6)
            periodo = (año_actual)+"-"+(año_actual+1);
        else
            periodo = (año_actual-1)+"-"+(año_actual);
        
        for(int i=0;i<9;i++){
            GRADO[i] = new JTextField(Grados[i]);
            GRADO[i].setSize((int)(Ancho*0.5),(int)(Alto/9));
            GRADO[i].setLocation(0,pos_y);
            GRADO[i].setEditable(false);
            GRADO[i].setFont(new Font("Arial",Font.BOLD,14));
            GRADO[i].setForeground(Color.WHITE);
            GRADO[i].setBackground(new Color(93, 193, 185));
            GRADO[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(GRADO[i]);
            
            CUPOS[i] = new JTextField(""+Ventana.cuposDisponibles(Grados[i], periodo));
            CUPOS[i].setSize((int)(Ancho*0.5),(int)(Alto/9));
            CUPOS[i].setLocation((int)(Ancho*0.5),pos_y);
            CUPOS[i].setEditable(false);
            CUPOS[i].setFont(new Font("Arial",Font.BOLD,14));
            CUPOS[i].setForeground(Color.BLACK);
            CUPOS[i].setBackground(Color.WHITE);
            CUPOS[i].setHorizontalAlignment(SwingConstants.CENTER);
            add(CUPOS[i]);
            
            pos_y += (int)(Alto/9);
        }
    }
}
