package aplicacion_colegio;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Boletin_Digital {
    private static final Font FONT_FECHA =
            new Font(Font.FontFamily.COURIER, 14, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_ETIQUETA =
            new Font(Font.FontFamily.COURIER, 16, Font.BOLD, BaseColor.WHITE);
    
    private static final Font FONT_ETIQUETA_2 =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TEXT =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TITULO =
            new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FIRMA =
            new Font(Font.FontFamily.HELVETICA, 11, Font.UNDERLINE, BaseColor.BLACK);
    
    private static final Font NOMBRE_FIRMA =
            new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_TITULO_2 =
            new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);    
    
    private static final Font FONT_TITULO_3 =
            new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, new BaseColor(0,110,188));   
    
    private static final Font FONT_FECHA_TABLA =
            new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);

    public Boletin_Digital(
            String RUTA, String[] materias, String[] l1, String[] l2, String[] l3,
            String nombre_estudiante,
            String grado,
            String periodo,
            String docente,
            String fecha_nacimiento,
            String edad,
            String nombre_apellido_madre,
            Ventana_Principal Ventana){
        //CREACIPON DEL DOCUMENTO PDF
        try {
            Document document = new Document();            
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RUTA));
            
            document.open();
            document.setPageSize(new Rectangle(612, 792));
            //ANCHO: 590 - ALTO: 780
            
            //METADATOS DEL PDF
            document.addTitle("Boletin de calificaciones");
            document.addSubject("Usando Aplicación Colegio");
            document.addKeywords("Java, PDF, Aplicación Colegio");
            document.addAuthor("Aplicación Colegio");
            document.addCreator("Aplicación Colegio");
            
            //PRIMERA PAGINA
            Paragraph MARGEN_SUPERIOR = new Paragraph();
            MARGEN_SUPERIOR.setAlignment(Paragraph.ALIGN_CENTER);
            
            Chapter chapter = new Chapter(MARGEN_SUPERIOR, 1);
            try{                
                Image ENCABEZADO;
                ENCABEZADO = Image.getInstance(Boletin_Digital.class.getResource("/Imagenes/logo.png"));
                ENCABEZADO.scaleAbsolute(120, 72);
                ENCABEZADO.setAbsolutePosition(15,690);
                chapter.add(ENCABEZADO);
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, e,
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
            }
            
            
            chapter.setNumberDepth(0);
            
            //TITULO
            Paragraph TITULO = new Paragraph(
                    "Preescolar U.E. “Rodolfo Romero”\n"
                    +"Inscrito en el Ministerio del Poder Popular para la Educación\n"
                    +"Código DEA: PD13630304\n"
                    +"Urb. Tricentenaria Calle C Nº 52\n"
                    +"Barcelona — Anzoátegui\n"
                    +"RIF: J-31324819-3", 
                    FONT_TITULO);
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 2
            Paragraph TITULO_2 = new Paragraph(
                    "Año Escolar "+periodo, 
                    FONT_TITULO_2);
            TITULO_2.add(new Phrase(Chunk.NEWLINE));
            TITULO_2.add(new Phrase(Chunk.NEWLINE));
            TITULO_2.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 3
            Paragraph TITULO_3 = new Paragraph(
                    "BOLETÍN INFORMATIVO", 
                    FONT_TITULO_3);
            TITULO_3.add(new Phrase(Chunk.NEWLINE));
            TITULO_3.add(new Phrase(Chunk.NEWLINE));
            TITULO_3.setAlignment(Paragraph.ALIGN_CENTER);
            
            //ENCABEZADO
            chapter.add(TITULO);
            chapter.add(TITULO_2);
            chapter.add(TITULO_3);
            
            PdfPTable [] table = new PdfPTable[13];
            int pos = 0;
            
            //DATOS
            PdfPCell DATOS_ESTUDIANTE = new PdfPCell(new Paragraph("DATOS DEL ESTUDIANTE", FONT_ETIQUETA));
            DATOS_ESTUDIANTE.setBackgroundColor(new BaseColor(93, 193, 185));
            DATOS_ESTUDIANTE.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            
            table[pos] = new PdfPTable(1);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(DATOS_ESTUDIANTE);
            chapter.add(table[pos++]);
            
            PdfPCell NOMBRE_ESTUDIANTE = new PdfPCell(new Paragraph("Nombres y Apellidos: "+nombre_estudiante, FONT_ETIQUETA_2));
            NOMBRE_ESTUDIANTE.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            table[pos] = new PdfPTable(1);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(NOMBRE_ESTUDIANTE);
            chapter.add(table[pos++]);
            
            PdfPCell GRADO_ESTUDIANTE = new PdfPCell(new Paragraph("Grado: "+grado, FONT_ETIQUETA_2));
            GRADO_ESTUDIANTE.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            PdfPCell DOCENTE = new PdfPCell(new Paragraph("Docente: "+docente, FONT_ETIQUETA_2));
            DOCENTE.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            table[pos] = new PdfPTable(2);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(GRADO_ESTUDIANTE);
            table[pos].addCell(DOCENTE);
            chapter.add(table[pos++]);
            
            PdfPCell FECHA = new PdfPCell(new Paragraph("Fecha de Nacimiento: "+fecha_nacimiento, FONT_ETIQUETA_2));
            FECHA.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            PdfPCell EDAD = new PdfPCell(new Paragraph("Edad: "+edad, FONT_ETIQUETA_2));
            EDAD.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            table[pos] = new PdfPTable(2);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(FECHA);
            table[pos].addCell(EDAD);
            chapter.add(table[pos++]);
            
            PdfPCell REPRESENTANTE = new PdfPCell(new Paragraph("Representante: "+nombre_apellido_madre, FONT_ETIQUETA_2));
            REPRESENTANTE.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            table[pos] = new PdfPTable(1);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(REPRESENTANTE);
            chapter.add(table[pos++]);
            
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            //CALIFICACIONES            
            PdfPCell T_MATERIA = new PdfPCell(new Paragraph("ASIGNATURA", FONT_ETIQUETA));
            T_MATERIA.setBackgroundColor(new BaseColor(93, 193, 185));
            T_MATERIA.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            T_MATERIA.setColspan(2);
            
            PdfPCell LAPSO_1 = new PdfPCell(new Paragraph("LAPSO 1", FONT_ETIQUETA));
            LAPSO_1.setBackgroundColor(new BaseColor(93, 193, 185));
            LAPSO_1.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            LAPSO_1.setColspan(1);
            
            PdfPCell LAPSO_2 = new PdfPCell(new Paragraph("LAPSO 2", FONT_ETIQUETA));
            LAPSO_2.setBackgroundColor(new BaseColor(93, 193, 185));
            LAPSO_2.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            LAPSO_2.setColspan(1);
            
            PdfPCell LAPSO_3 = new PdfPCell(new Paragraph("LAPSO 3", FONT_ETIQUETA));
            LAPSO_3.setBackgroundColor(new BaseColor(93, 193, 185));
            LAPSO_3.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            LAPSO_3.setColspan(1);
            
            PdfPCell DEFINITIVA = new PdfPCell(new Paragraph("DEF.", FONT_ETIQUETA));
            DEFINITIVA.setBackgroundColor(new BaseColor(93, 193, 185));
            DEFINITIVA.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            DEFINITIVA.setColspan(1);
            
            table[pos] = new PdfPTable(6);
            table[pos].setWidthPercentage(102);
            table[pos].addCell(T_MATERIA);
            table[pos].addCell(LAPSO_1);
            table[pos].addCell(LAPSO_2);
            table[pos].addCell(LAPSO_3);
            table[pos].addCell(DEFINITIVA);
            chapter.add(table[pos++]);
            
            int tope = pos;
            
            for(int i=pos;i<13;i++){
                boolean definitiva = true;
                table[i] = new PdfPTable(6);
                table[i].setWidthPercentage(102);
                
                PdfPCell nombre_materia = new PdfPCell(new Paragraph(materias[i-pos], FONT_TEXT));
                nombre_materia.setBackgroundColor(BaseColor.WHITE);
                nombre_materia.setColspan(2);
                table[i].addCell(nombre_materia);
                
                PdfPCell nota_l1 = new PdfPCell(new Paragraph(l1[i-pos], FONT_TEXT));
                nota_l1.setBackgroundColor(BaseColor.WHITE);
                nota_l1.setColspan(1);
                nota_l1.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
                table[i].addCell(nota_l1);
                
                PdfPCell nota_l2 = new PdfPCell(new Paragraph(l2[i-pos], FONT_TEXT));
                nota_l2.setBackgroundColor(BaseColor.WHITE);
                nota_l2.setColspan(1);
                nota_l2.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
                table[i].addCell(nota_l2);
                
                PdfPCell nota_l3 = new PdfPCell(new Paragraph(l3[i-pos], FONT_TEXT));
                nota_l3.setBackgroundColor(BaseColor.WHITE);
                nota_l3.setColspan(1);
                nota_l3.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
                table[i].addCell(nota_l3);
                
                int valor_n1 = 0, valor_n2 = 0, valor_n3 = 0, valor_def = 0;
                
                if(l1[i-pos].equalsIgnoreCase("A")) valor_n1 = 10;
                if(l1[i-pos].equalsIgnoreCase("B")) valor_n1 = 8;
                if(l1[i-pos].equalsIgnoreCase("C")) valor_n1 = 6;
                if(l1[i-pos].equalsIgnoreCase("D")) valor_n1 = 4;
                if(l1[i-pos].equalsIgnoreCase("E")) valor_n1 = 2;
                if(l1[i-pos].equalsIgnoreCase("")){
                    valor_n1 = 0;
                    definitiva = false;
                }
                
                if(l2[i-pos].equalsIgnoreCase("A")) valor_n2 = 10;
                if(l2[i-pos].equalsIgnoreCase("B")) valor_n2 = 8;
                if(l2[i-pos].equalsIgnoreCase("C")) valor_n2 = 6;
                if(l2[i-pos].equalsIgnoreCase("D")) valor_n2 = 4;
                if(l2[i-pos].equalsIgnoreCase("E")) valor_n2 = 2;
                if(l2[i-pos].equalsIgnoreCase("")){
                    valor_n2 = 0;
                    definitiva = false;
                }
                
                if(l3[i-pos].equalsIgnoreCase("A")) valor_n3 = 10;
                if(l3[i-pos].equalsIgnoreCase("B")) valor_n3 = 8;
                if(l3[i-pos].equalsIgnoreCase("C")) valor_n3 = 6;
                if(l3[i-pos].equalsIgnoreCase("D")) valor_n3 = 4;
                if(l3[i-pos].equalsIgnoreCase("E")) valor_n3 = 2;
                if(l3[i-pos].equalsIgnoreCase("")){
                    valor_n3 = 0;
                    definitiva = false;
                }
                
                valor_def = (valor_n1+valor_n2+valor_n3)/3;
                String t_definitiva = "";
                if(definitiva){
                    if(valor_def>=9) t_definitiva = "A";
                    if(valor_def>=7 && valor_def<9) t_definitiva = "B";
                    if(valor_def>=5 && valor_def<7) t_definitiva = "C";
                    if(valor_def>=3 && valor_def<5) t_definitiva = "D";
                    if(valor_def>=1 && valor_def<3) t_definitiva = "E";
                    if(valor_def<1) t_definitiva = "";
                }
                
                PdfPCell nota_def = new PdfPCell(new Paragraph(t_definitiva, FONT_TEXT));
                nota_def.setBackgroundColor(BaseColor.WHITE);
                nota_def.setColspan(1);
                nota_def.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
                table[i].addCell(nota_def);
                
                chapter.add(table[i]);
            }
            
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            PdfPTable [] table_firma = new PdfPTable[2];
            
            //REPRESENTANTE            
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            PdfPCell NOMBRE_REPRESENTANTE = new PdfPCell(
                    new Paragraph(nombre_apellido_madre+"\nREPRESENTANTE", NOMBRE_FIRMA)
            );
            NOMBRE_REPRESENTANTE.setBorder(1);
            NOMBRE_REPRESENTANTE.setBorderColorBottom(BaseColor.WHITE);
            NOMBRE_REPRESENTANTE.setBorderColorLeft(BaseColor.WHITE);
            NOMBRE_REPRESENTANTE.setBorderColorRight(BaseColor.WHITE);
            NOMBRE_REPRESENTANTE.setBorderColorTop(BaseColor.BLACK);
            NOMBRE_REPRESENTANTE.setColspan(2);
            NOMBRE_REPRESENTANTE.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
                        
            PdfPCell SEPARADOR = new PdfPCell(
                    new Paragraph("      ", NOMBRE_FIRMA)
            );
            SEPARADOR.setBorder(0);
            SEPARADOR.setBorderColorBottom(BaseColor.WHITE);
            SEPARADOR.setBorderColorLeft(BaseColor.WHITE);
            SEPARADOR.setBorderColorRight(BaseColor.WHITE);
            SEPARADOR.setBorderColorTop(BaseColor.WHITE);
            SEPARADOR.setColspan(1);
            SEPARADOR.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell NOMBRE_DOCENTE = new PdfPCell(
                    new Paragraph(docente+"\nDOCENTE", NOMBRE_FIRMA)
            );
            NOMBRE_DOCENTE.setBorder(1);
            NOMBRE_DOCENTE.setBorderColorBottom(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorLeft(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorRight(BaseColor.WHITE);
            NOMBRE_DOCENTE.setBorderColorTop(BaseColor.BLACK);
            NOMBRE_DOCENTE.setColspan(2);
            NOMBRE_DOCENTE.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            
            table_firma[0] = new PdfPTable(5);
            table_firma[0].setWidthPercentage(102);
            table_firma[0].addCell(NOMBRE_REPRESENTANTE);
            table_firma[0].addCell(SEPARADOR);
            table_firma[0].addCell(NOMBRE_DOCENTE);
            chapter.add(table_firma[0]);
            
            PdfPCell DIRECTORA = new PdfPCell(
                    new Paragraph(
                            "Lic. María de Martínez\n"
                            + "Directora General", 
                            NOMBRE_FIRMA)
            );
            DIRECTORA.setBorder(1);
            DIRECTORA.setBorderColorBottom(BaseColor.WHITE);
            DIRECTORA.setBorderColorLeft(BaseColor.WHITE);
            DIRECTORA.setBorderColorRight(BaseColor.WHITE);
            DIRECTORA.setBorderColorTop(BaseColor.BLACK);
            DIRECTORA.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            table_firma[1] = new PdfPTable(1);
            table_firma[1].setWidthPercentage(50);
            table_firma[1].addCell(DIRECTORA);
            chapter.add(table_firma[1]);
            
            //SELLO
//            Image SELLO;
//            SELLO = Image.getInstance(System.getenv("ProgramFiles")+"\\Gestion Colegio\\Imagenes\\sello.png");
//            SELLO.scaleAbsolute(100, 100);
//            SELLO.setAbsolutePosition(450,30);
//            chapter.add(SELLO);
            
            document.add(chapter);
            document.close();
            
            Object [] reporte = {"Aceptar","Abrir Boletín"};
            
            int opcion = JOptionPane.showOptionDialog(
                    null, "¡Tu archivo ha sido creado correctamente!", "NOTIFICACIÓN DE BOLETIN",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                    reporte, reporte
                );
            if(opcion==1){
                try {
                    File archivo = new File(RUTA);
                    Desktop.getDesktop().open(archivo);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                            + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                }
            }
            
        } catch (DocumentException documentException) {
            JOptionPane.showMessageDialog(null, "¡Tu archivo no ha podido ser creado!",
                    "NOTIFICACIÓN DE BOLETIN", 0);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "¡Tu archivo no ha podido ser creado correctamente!",
                    "NOTIFICACIÓN DE BOLETIN", 0);
            JOptionPane.showMessageDialog(null, "Error: "+ex,
                    "NOTIFICACIÓN DE BOLETIN", 0);
            Logger.getLogger(Boletin_Digital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}