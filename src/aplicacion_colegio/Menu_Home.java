package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Menu_Home extends JPanel{
    public Ventana_Principal Ventana;
    public JPanel panel_menu, panel_usuario, panel_gestion;
    public JLabel logo, Nombre_Apellido, Fecha_Hora;
    public Reloj Reloj;
    public Calendar c1 = Calendar.getInstance();
    public Calendar c2 = new GregorianCalendar();
    public int dia,mes,año,hora,min,am_pm;
    public String s_mes,am_pm_aux;
    public Login Login;
    public RoundedCornerBorder Borde;
    public boolean profesor;
    
    //PANEL USUARIO
    public JButton gestion_personal, gestion_estudiantes, gestion_reportes, gestion_notas,
            gestion_perfil, cerrar_sesion, boton_configuracion;
    public JTextField busqueda_estudiante;
    public JLabel busqueda_estudiante_ph;
    
    //GESTION PERFIL
    public Gestion_Perfil Gestion_Perfil;
    
    //GESTION PERSONAL
    public Registro_Personal Registro_Personal;
    public Listar_Personal Listar_Personal;  
    public JButton registrar_personal, listar_personal, filtro_personal;
    public JLabel titulo_personal;
    
    //GESTIONAR ESTUDIANTE
    public Registro_Estudiante Registro_Estudiante; 
    public Listar_Estudiante Listar_Estudiante;  
    public JButton inscribir_estudiante, listar_estudiante;
    public JLabel titulo_estudiantes;
    public JButton filtro_estudiante;
    
    //GESTIONAR PAGOS
    public Registro_Pago Registro_Pago;
    public Listar_Pagos Listar_Pagos;  
    public JButton añadir_pago, listar_pagos;
    public JLabel titulo_pagos;
    
    //GESTIONAR REPORTES
    public JLabel titulo_reportes;
    public Gestionar_Reportes Gestionar_Reportes;
    
    //GESTIONAR NOTAS
    public JLabel titulo_notas;
    public Gestionar_Notas Gestionar_Notas;
    
    //CONFIGURACION
    public Configuracion Configuracion;
    public JLabel titulo_configuracion;
        
    public Menu_Home(Ventana_Principal Ventana, Login Login){
        this.Ventana = Ventana;
        setSize(Ventana.Ancho,Ventana.Alto);
        setLayout(null);
        setBackground(Color.WHITE);
        
        this.Login = Login;
        Borde = new RoundedCornerBorder();
        
        profesor = false;
      
        dia = c1.get(Calendar.DATE);
        mes = c1.get(Calendar.MONTH)+1;
        año = c1.get(Calendar.YEAR);
        hora = c1.get(Calendar.HOUR_OF_DAY);
        min = c1.get(Calendar.MINUTE);
        am_pm = c1.get(Calendar.AM_PM);
        
        if(am_pm==0){
            am_pm_aux = "AM";
            if(hora==0)
                hora = 12;
        }
        else{
            am_pm_aux = "PM";
            hora = hora-12;
            if(hora==0)
                hora = 12;
        }
        
        if(mes==1){s_mes = "Enero";}
        if(mes==2){s_mes = "Febrero";}
        if(mes==3){s_mes = "Marzo";}
        if(mes==4){s_mes = "Abril";}
        if(mes==5){s_mes = "Mayo";}
        if(mes==6){s_mes = "Junio";}
        if(mes==7){s_mes = "Julio";}
        if(mes==8){s_mes = "Agosto";}
        if(mes==9){s_mes = "Septiembre";}
        if(mes==10){s_mes = "Octubre";}
        if(mes==11){s_mes = "Noviembre";}
        if(mes==12){s_mes = "Diciembre";}
                  
        //PANEL_MENU-------------------------------
        panel_menu = new JPanel();
        panel_menu.setSize((int)(Ventana.Ancho*0.23),(int)(Ventana.Alto));
        panel_menu.setLocation(0,0);
        panel_menu.setLayout(null);
        panel_menu.setBackground(new Color(93, 193, 185));
        add(panel_menu);
        
        //PANEL_USUARIO------------------------------
        panel_usuario = new JPanel();
        panel_usuario.setSize((int)(Ventana.Ancho*0.77),(int)(Ventana.Alto*0.155));
        panel_usuario.setLocation((int)(Ventana.Ancho*0.23),0);
        panel_usuario.setLayout(null);
        panel_usuario.setBackground(Color.WHITE);
        add(panel_usuario);
        
        //PANEL_GESTION------------------------------
        panel_gestion = new JPanel();
        panel_gestion.setSize((int)(Ventana.Ancho*0.77),(int)(Ventana.Alto*0.845));
        panel_gestion.setLocation((int)(Ventana.Ancho*0.23),(int)(Ventana.Alto*0.155));
        panel_gestion.setLayout(null);
        panel_gestion.setBackground(Color.WHITE);
        add(panel_gestion);
        
        //CONFIGURACION------------------------------
        titulo_configuracion = new JLabel("Configuración");
        titulo_configuracion.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_configuracion.setLocation(0,0);
        titulo_configuracion.setVisible(false);
        titulo_configuracion.setForeground(Color.GRAY);
        titulo_configuracion.setFont(new Font("Arial",Font.BOLD,24));
        titulo_configuracion.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_configuracion);
        
        //GESTIONAR PERSONAL-------------------------
        titulo_personal = new JLabel("Gestionar Empleados");
        titulo_personal.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_personal.setLocation(0,0);
        titulo_personal.setVisible(false);
        titulo_personal.setForeground(Color.GRAY);
        titulo_personal.setFont(new Font("Arial",Font.BOLD,24));
        titulo_personal.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_personal);
        
        registrar_personal = new JButton("Registrar Empleado"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        registrar_personal.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        registrar_personal.setLocation((int)(panel_gestion.getWidth()*0.05),(int)(panel_gestion.getHeight()*0.1));
        registrar_personal.setFocusable(false);
        registrar_personal.setVisible(false);
        registrar_personal.setCursor(new Cursor(Cursor.HAND_CURSOR));
        registrar_personal.setFont(new Font("Arial",Font.BOLD,16));
        registrar_personal.setContentAreaFilled(false);
        registrar_personal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Personal.setVisible(true);
                registrar_personal.setFont(new Font("Arial",Font.BOLD,24));
                listar_personal.setFont(new Font("Arial",Font.BOLD,16));
                Listar_Personal.setVisible(false);
                filtro_personal.setVisible(false);
            }
        });
        panel_gestion.add(registrar_personal);
        
        listar_personal = new JButton("Listar Empleados"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        listar_personal.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        listar_personal.setLocation((int)(panel_gestion.getWidth()*0.5),(int)(panel_gestion.getHeight()*0.1));
        listar_personal.setFocusable(false);
        listar_personal.setVisible(false);
        listar_personal.setCursor(new Cursor(Cursor.HAND_CURSOR));
        listar_personal.setFont(new Font("Arial",Font.BOLD,24));
        listar_personal.setContentAreaFilled(false);
        listar_personal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Personal.setVisible(false);
                registrar_personal.setFont(new Font("Arial",Font.BOLD,16));
                listar_personal.setFont(new Font("Arial",Font.BOLD,24));
                Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(0),Login);
                Listar_Personal.setVisible(true);
                filtro_personal.setVisible(true);
            }
        });
        panel_gestion.add(listar_personal);

        filtro_personal = new JButton(Ventana.cargarImagen("filtro.png", 30,30));
        filtro_personal.setSize(30,30);
        filtro_personal.setLocation((int)(panel_gestion.getWidth()*0.95),(int)(panel_gestion.getHeight()*0.1));
        filtro_personal.setFocusable(false);
        filtro_personal.setVisible(false);
        filtro_personal.setCursor(new Cursor(Cursor.HAND_CURSOR));
        filtro_personal.setFont(new Font("Arial",Font.BOLD,16));
        filtro_personal.setContentAreaFilled(false);
        filtro_personal.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Personal.setVisible(false);
                registrar_personal.setFont(new Font("Arial",Font.BOLD,16));
                listar_personal.setFont(new Font("Arial",Font.BOLD,24));
                String[] OPCION = {"HABILITADOS","INHABILITADOS"};
                String aux = (
                (String) JOptionPane.showInputDialog(null, 
                    "¿Tipo?",
                    "Lista de Empleados",
                    JOptionPane.QUESTION_MESSAGE, 
                    null, 
                    OPCION, 
                    OPCION[0])
                );
                if(aux == "INHABILITADOS")
                    Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(1), Login);
                else if(aux != null)
                    Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(0), Login);
                Listar_Personal.setVisible(true);
            }
        });
        panel_gestion.add(filtro_personal);
        
        //Gestion_Perfil
        Gestion_Perfil = new Gestion_Perfil(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75),Login.Cedula_User);
        Gestion_Perfil.setLocation(0,(int)(panel_gestion.getHeight()*0.18));
        Gestion_Perfil.setVisible(true);
        panel_gestion.add(Gestion_Perfil);
        
        //Registro_Personal
        Registro_Personal = new Registro_Personal(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75));
        Registro_Personal.setLocation(0,(int)(panel_gestion.getHeight()*0.18));
        Registro_Personal.setVisible(false);
        panel_gestion.add(Registro_Personal);
        
        //Listar_Personal
        Listar_Personal = new Listar_Personal(Ventana,Login,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.8));
        Listar_Personal.setLocation(0,(int)(panel_gestion.getHeight()*0.16));
        Listar_Personal.setVisible(false);
        panel_gestion.add(Listar_Personal);
        
        //Configuracion
        Configuracion = new Configuracion(this, Ventana, (int)(panel_gestion.getWidth()), (int)(panel_gestion.getHeight()*0.8));
        Configuracion.setLocation(0,(int)(panel_gestion.getHeight()*0.16));
        Configuracion.setVisible(false);
        panel_gestion.add(Configuracion);
        //--------------------------------------------------
        //GESTIONAR ESTUDIANTES-----------------------------
        titulo_estudiantes = new JLabel("Gestionar Estudiantes");
        titulo_estudiantes.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_estudiantes.setLocation(0,0);
        titulo_estudiantes.setVisible(false);
        titulo_estudiantes.setForeground(Color.GRAY);
        titulo_estudiantes.setFont(new Font("Arial",Font.BOLD,24));
        titulo_estudiantes.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_estudiantes);
        
        inscribir_estudiante = new JButton("Inscribir Estudiante"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        inscribir_estudiante.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        inscribir_estudiante.setLocation((int)(panel_gestion.getWidth()*0.05),(int)(panel_gestion.getHeight()*0.1));
        inscribir_estudiante.setFocusable(false);
        inscribir_estudiante.setVisible(false);
        inscribir_estudiante.setCursor(new Cursor(Cursor.HAND_CURSOR));
        inscribir_estudiante.setFont(new Font("Arial",Font.BOLD,24));
        inscribir_estudiante.setContentAreaFilled(false);
        inscribir_estudiante.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Estudiante.setVisible(true);
                inscribir_estudiante.setFont(new Font("Arial",Font.BOLD,24));
                listar_estudiante.setFont(new Font("Arial",Font.BOLD,16));
                Listar_Estudiante.setVisible(false);
                filtro_estudiante.setVisible(false);
            }
        });
        panel_gestion.add(inscribir_estudiante);
        
        listar_estudiante = new JButton("Listar Estudiantes"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        listar_estudiante.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        listar_estudiante.setLocation((int)(panel_gestion.getWidth()*0.5),(int)(panel_gestion.getHeight()*0.1));
        listar_estudiante.setFocusable(false);
        listar_estudiante.setVisible(false);
        listar_estudiante.setCursor(new Cursor(Cursor.HAND_CURSOR));
        listar_estudiante.setFont(new Font("Arial",Font.BOLD,16));
        listar_estudiante.setContentAreaFilled(false);
        listar_estudiante.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Estudiante.setVisible(false);
                inscribir_estudiante.setFont(new Font("Arial",Font.BOLD,16));
                listar_estudiante.setFont(new Font("Arial",Font.BOLD,24));
                if(Login.Cargo_User.equalsIgnoreCase("PROFESOR")){
                    Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(Ventana.obtenerGradoACargo(Login.Cedula_User)), false, profesor);
                }else{
                    Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(), false, profesor);
                }
                Listar_Estudiante.setVisible(true);
                filtro_estudiante.setVisible(true);
            }
        });
        panel_gestion.add(listar_estudiante);
        
        filtro_estudiante = new JButton(Ventana.cargarImagen("filtro.png", 30,30));
        filtro_estudiante.setSize(30,30);
        filtro_estudiante.setLocation((int)(panel_gestion.getWidth()*0.95),(int)(panel_gestion.getHeight()*0.1));
        filtro_estudiante.setFocusable(false);
        filtro_estudiante.setVisible(false);
        filtro_estudiante.setCursor(new Cursor(Cursor.HAND_CURSOR));
        filtro_estudiante.setFont(new Font("Arial",Font.BOLD,16));
        filtro_estudiante.setContentAreaFilled(false);
        filtro_estudiante.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Estudiante.setVisible(false);
                inscribir_estudiante.setFont(new Font("Arial",Font.BOLD,16));
                listar_estudiante.setFont(new Font("Arial",Font.BOLD,24));
                String[] OPCION = {"PRIMER GRUPO","SEGUNDO GRUPO","TERCER GRUPO","PRIMER GRADO","SEGUNDO GRADO","TERCER GRADO",
                    "CUARTO GRADO","QUINTO GRADO","SEXTO GRADO","INHABILITADOS", "SOLVENTES", "INSOLVENTES"};
                String aux = (
                (String) JOptionPane.showInputDialog(null, 
                    "Seleccione",
                    "Lista de Estudiantes",
                    JOptionPane.QUESTION_MESSAGE, 
                    null, 
                    OPCION, 
                    OPCION[0])
                );
                if(aux == "INHABILITADOS")
                    Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(aux), true, profesor);
                else if(aux != null)
                    Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(aux), false, profesor);
                Listar_Estudiante.setVisible(true);
            }
        });
        panel_gestion.add(filtro_estudiante);
        
        //Registro_Estudiante
        Registro_Estudiante = new Registro_Estudiante(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75));
        Registro_Estudiante.setLocation(0,(int)(panel_gestion.getHeight()*0.18));
        Registro_Estudiante.setVisible(false);
        panel_gestion.add(Registro_Estudiante);
        
        //Listar_Estudiante
        Listar_Estudiante = new Listar_Estudiante(this, Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.8));
        Listar_Estudiante.setLocation(0,(int)(panel_gestion.getHeight()*0.16));
        Listar_Estudiante.setVisible(false);
        panel_gestion.add(Listar_Estudiante);
        
        
        //GESTIONAR PAGOS-------------------------
        titulo_pagos = new JLabel("Gestionar Pagos");
        titulo_pagos.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_pagos.setLocation(0,0);
        titulo_pagos.setVisible(false);
        titulo_pagos.setForeground(Color.GRAY);
        titulo_pagos.setFont(new Font("Arial",Font.BOLD,24));
        titulo_pagos.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_pagos);
        
        añadir_pago = new JButton("Registrar Pago"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        añadir_pago.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        añadir_pago.setLocation((int)(panel_gestion.getWidth()*0.05),(int)(panel_gestion.getHeight()*0.1));
        añadir_pago.setFocusable(false);
        añadir_pago.setVisible(false);
        añadir_pago.setCursor(new Cursor(Cursor.HAND_CURSOR));
        añadir_pago.setFont(new Font("Arial",Font.BOLD,16));
        añadir_pago.setContentAreaFilled(false);
        añadir_pago.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
//                Registro_Pago.actualizarListaEstudiantes();
//                Registro_Pago.concepto.setText("PAGO DE MENSUALIDAD");
//                Registro_Pago.ph_motivo.setVisible(false);
//                Registro_Pago.monto.setText(Double.toString(Configuracion.montoMes));
//                Registro_Pago.ph_monto.setVisible(false);
                Registro_Pago.setVisible(true);
                añadir_pago.setFont(new Font("Arial",Font.BOLD,24));
                listar_pagos.setFont(new Font("Arial",Font.BOLD,16));
                Listar_Pagos.setVisible(false);
            }
        });
        panel_gestion.add(añadir_pago);
        
        listar_pagos = new JButton("Listar Pagos"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        listar_pagos.setSize((int)(panel_gestion.getWidth()*0.45),(int)(panel_gestion.getHeight()*0.06));
        listar_pagos.setLocation((int)(panel_gestion.getWidth()*0.5),(int)(panel_gestion.getHeight()*0.1));
        listar_pagos.setFocusable(false);
        listar_pagos.setVisible(false);
        listar_pagos.setCursor(new Cursor(Cursor.HAND_CURSOR));
        listar_pagos.setFont(new Font("Arial",Font.BOLD,24));
        listar_pagos.setContentAreaFilled(false);
        listar_pagos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Registro_Pago.setVisible(false);
                añadir_pago.setFont(new Font("Arial",Font.BOLD,16));
                listar_pagos.setFont(new Font("Arial",Font.BOLD,24));
                Listar_Pagos.setVisible(true);
            }
        });
        panel_gestion.add(listar_pagos); 
        
        //Registro_Pago
        Registro_Pago = new Registro_Pago(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75));
        Registro_Pago.setLocation(0,(int)(panel_gestion.getHeight()*0.18));
        Registro_Pago.setVisible(false);
        panel_gestion.add(Registro_Pago);
        
        //Listar_Pagos
        Listar_Pagos = new Listar_Pagos(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.8));
        Listar_Pagos.setLocation(0,(int)(panel_gestion.getHeight()*0.16));
        Listar_Pagos.setVisible(false);
        panel_gestion.add(Listar_Pagos);
        
        //GESTIONAR REPORTES----------------------------------------------------
        titulo_reportes = new JLabel("Gestionar Reportes");
        titulo_reportes.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_reportes.setLocation(0,0);
        titulo_reportes.setVisible(false);
        titulo_reportes.setForeground(Color.GRAY);
        titulo_reportes.setFont(new Font("Arial",Font.BOLD,24));
        titulo_reportes.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_reportes);
        
        //Gestionar_Reportes
        Gestionar_Reportes = new Gestionar_Reportes(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75));
        Gestionar_Reportes.setLocation(0,(int)(panel_gestion.getHeight()*0.18));
        Gestionar_Reportes.setVisible(false);
        panel_gestion.add(Gestionar_Reportes);
        
        
        //GESTIONAR NOTAS----------------------------------------------------
        titulo_notas = new JLabel("Gestionar Notas");
        titulo_notas.setSize((int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.1));
        titulo_notas.setLocation(0,0);
        titulo_notas.setVisible(false);
        titulo_notas.setForeground(Color.GRAY);
        titulo_notas.setFont(new Font("Arial",Font.BOLD,24));
        titulo_notas.setHorizontalAlignment(SwingConstants.CENTER);
        panel_gestion.add(titulo_notas);
        
        //Gestionar_Notas
        Gestionar_Notas = new Gestionar_Notas(Ventana,(int)(panel_gestion.getWidth()),(int)(panel_gestion.getHeight()*0.75),Login.Cargo_User,Login.Cedula_User);
        Gestionar_Notas.setLocation(0,(int)(panel_gestion.getHeight()*0.1));
        Gestionar_Notas.setVisible(false);
        panel_gestion.add(Gestionar_Notas);
        
        cargarPanelUsuario();
        cargarPanelMenu();     
        
        verificarUsuario();
    }
    
    public void cargarPanelUsuario(){
        //LOGO
        logo = new JLabel(Ventana.cargarImagen("logo.png", 100,78));
        logo.setSize(100,78);
        logo.setLocation(5,0);
        panel_usuario.add(logo);
        
        //Nombre_Apellido
        Nombre_Apellido = new JLabel("Bienvenido, "+Login.Nombre_User+" "+Login.Apellido_User+"      ");
        Nombre_Apellido.setFont(new Font("Arial",Font.BOLD,18));
        Nombre_Apellido.setHorizontalAlignment(SwingConstants.RIGHT);
        Nombre_Apellido.setForeground(Color.GRAY);
        Nombre_Apellido.setSize((int)(panel_usuario.getWidth()*0.6),(int)(panel_usuario.getHeight()*0.2));
        Nombre_Apellido.setLocation((int)(panel_usuario.getWidth()*0.4),(int)(panel_usuario.getHeight()*0.1));
        panel_usuario.add(Nombre_Apellido);
        
        //Fecha
        Fecha_Hora = new JLabel();
        Fecha_Hora.setFont(new Font("Arial",Font.BOLD,18));
        Fecha_Hora.setHorizontalAlignment(SwingConstants.RIGHT);
        Fecha_Hora.setForeground(Color.GRAY);
        Fecha_Hora.setSize((int)(panel_usuario.getWidth()*0.6),(int)(panel_usuario.getHeight()*0.2));
        Fecha_Hora.setLocation((int)(panel_usuario.getWidth()*0.4),(int)(panel_usuario.getHeight()*0.3));
        panel_usuario.add(Fecha_Hora);
        Reloj = new Reloj(this);
    }
    
    public void cargarPanelMenu(){
        int y = (int)(panel_menu.getHeight()*0.1);
        
        //gestion_perfil
        gestion_perfil = new JButton(Ventana.cargarImagen("perfil.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_perfil.setRolloverIcon(Ventana.cargarImagen("perfil_hover.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_perfil.setSize((int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4));
        gestion_perfil.setLocation((int)(panel_menu.getWidth()*0.05),y);
        gestion_perfil.setOpaque(true);
        gestion_perfil.setContentAreaFilled(false);
        gestion_perfil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ocultarPaneles();
                Gestion_Perfil.setVisible(true);
                filtro_personal.setVisible(false);
            }
        });
        gestion_perfil.setFocusable(false);
        gestion_perfil.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_menu.add(gestion_perfil);
        
        //gestion_personal
        gestion_personal = new JButton(Ventana.cargarImagen("gestion_empleados.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_personal.setRolloverIcon(Ventana.cargarImagen("gestion_empleados_hover.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_personal.setSize((int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4));
        gestion_personal.setLocation((int)(panel_menu.getWidth()*0.55),y);
        gestion_personal.setOpaque(true);
        gestion_personal.setContentAreaFilled(false);
        gestion_personal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ocultarPaneles();
                registrar_personal.setFont(new Font("Arial",Font.BOLD,16));
                listar_personal.setFont(new Font("Arial",Font.BOLD,24));
                titulo_personal.setVisible(true);
                registrar_personal.setVisible(true);
                listar_personal.setVisible(true);
                Registro_Personal.setVisible(false);
                Listar_Personal.setVisible(true);
                filtro_estudiante.setVisible(false);
                filtro_personal.setVisible(true);
            }
        });
        gestion_personal.setFocusable(false);
        gestion_personal.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_menu.add(gestion_personal);
        
        y += (int)(panel_menu.getWidth()*0.55);
        
        //gestion_cuentas
        gestion_reportes = new JButton(Ventana.cargarImagen("gestion_cuentas.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_reportes.setRolloverIcon(Ventana.cargarImagen("gestion_cuentas_hover.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_reportes.setSize((int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4));
        gestion_reportes.setLocation((int)(panel_menu.getWidth()*0.05),y);
        gestion_reportes.setOpaque(true);
        gestion_reportes.setContentAreaFilled(false);
        gestion_reportes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ocultarPaneles();
                Gestionar_Reportes.setVisible(true);
                titulo_reportes.setVisible(true);
                filtro_personal.setVisible(false);
            }
        });
        gestion_reportes.setFocusable(false);
        gestion_reportes.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_menu.add(gestion_reportes);
        
        //gestion_estudiantes
        gestion_estudiantes = new JButton(Ventana.cargarImagen("gestion_estudiantes.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_estudiantes.setRolloverIcon(Ventana.cargarImagen("gestion_estudiantes_hover.png", (int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4)));
        gestion_estudiantes.setSize((int)(panel_menu.getWidth()*0.4),(int)(panel_menu.getWidth()*0.4));
        gestion_estudiantes.setLocation((int)(panel_menu.getWidth()*0.55),y);
        gestion_estudiantes.setOpaque(true);
        gestion_estudiantes.setContentAreaFilled(false);
        gestion_estudiantes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ocultarPaneles();
                listar_estudiante.setFont(new Font("Arial",Font.BOLD,24));
                inscribir_estudiante.setFont(new Font("Arial",Font.BOLD,16));
                Registro_Estudiante.setVisible(false);
                Listar_Estudiante.setVisible(true);
                inscribir_estudiante.setVisible(true);
                listar_estudiante.setVisible(true);
                titulo_estudiantes.setVisible(true); 
                filtro_personal.setVisible(false);
                filtro_estudiante.setVisible(true);
                if(Login.Cargo_User.equalsIgnoreCase("PROFESOR")){
                    Registro_Estudiante.setVisible(false);
                    Listar_Estudiante.setVisible(true);
                    inscribir_estudiante.setEnabled(false);
                    listar_estudiante.setEnabled(true);
                    filtro_estudiante.setEnabled(false);
                }
            }
        });
        gestion_estudiantes.setFocusable(false);
        gestion_estudiantes.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_menu.add(gestion_estudiantes);
        
        
        y += (int)(panel_menu.getWidth()*0.75);
        
        //buscador place holder
        busqueda_estudiante_ph = new JLabel("    Identificador, Nombre, Apellido o Grado...");
        busqueda_estudiante_ph.setSize((int)(panel_menu.getWidth()*0.8),(int)(panel_menu.getHeight()*0.05));
        busqueda_estudiante_ph.setLocation((int)(panel_menu.getWidth()*0.1),y);
        busqueda_estudiante_ph.setForeground(Color.GRAY);
        busqueda_estudiante_ph.setFont(new Font("Arial",Font.ITALIC,12));
        busqueda_estudiante_ph.setToolTipText("Búsqueda a estudiante por Nombre o Apellido");
        panel_menu.add(busqueda_estudiante_ph);
        
        //buscador
        busqueda_estudiante = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        busqueda_estudiante.setSize((int)(panel_menu.getWidth()*0.8),(int)(panel_menu.getHeight()*0.05));
        busqueda_estudiante.setLocation((int)(panel_menu.getWidth()*0.1),y);
        busqueda_estudiante.setFont(new Font("Arial",Font.PLAIN,13));
        busqueda_estudiante.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(busqueda_estudiante.getText().length()>0)
                    busqueda_estudiante_ph.setVisible(false);
                else
                    busqueda_estudiante_ph.setVisible(true);
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(busqueda_estudiante.getText().length()>0)
                    busqueda_estudiante_ph.setVisible(false);
                else
                    busqueda_estudiante_ph.setVisible(true);
                if(ke.getKeyCode()==ke.VK_ENTER){
                    if(busqueda_estudiante.getText().length()>0){
                        Estudiante_Resultado estudiante_busqueda = new Estudiante_Resultado(Login.Cargo_User, buscarEstudiantes(busqueda_estudiante.getText()));
                        String [] opciones = new String[2];
                        opciones[0] = "Aceptar";
                        opciones[1] = "Cancelar";
                        if(JOptionPane.showOptionDialog(
                            null,
                            estudiante_busqueda,
                            "Resultados de "+busqueda_estudiante.getText(),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.PLAIN_MESSAGE,
                            null,
                            opciones,opciones[1])==0){
                            if(!estudiante_busqueda.identificador.equalsIgnoreCase("")){
                                //BOLETIN
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Generar Boletín de Calificaciones"){
                                    generarBoletin(estudiante_busqueda);
                                }

                                //REPORTE DE PAGOS
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Generar Reporte de Pagos"){
                                    generarListaPagos(estudiante_busqueda);
                                }
                                
                                //CONSTANCIA DE PROMOCION
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Generar Constancia de Promoción"){
                                    String estudiante = Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador);
                                    String identificador = estudiante.split(Pattern.quote("-"))[0];
                                    String nombre = estudiante.split(Pattern.quote("-"))[1];
                                    String grado = Ventana.getGradoStr(Integer.parseInt(estudiante.split(Pattern.quote("-"))[2]));
                                    String ESTUDIANTE_SELECCIONADO = identificador+"-"+nombre+"-"+grado;
        
                                    JFileChooser RUTA = new JFileChooser();
                                    RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                                    RUTA.setFileFilter(new FileNameExtensionFilter("*.PDF", "pdf"));
                                    int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);

                                    if(OPCION_SELECCIONADA == 1){ return; }

                                    String ruta = RUTA.getSelectedFile().toString();
                                    ruta = ruta.split(Pattern.quote("."))[0];                  
                                    File archivo = new File(ruta+".pdf");

                                    while(true){
                                        if(archivo.exists()){
                                            if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                                                    "Archivo Existente",
                                                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                                                RUTA.getSelectedFile().delete();
                                            }else{
                                                return;
                                            }
                                        }

                                        if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                                            Certificado_Promocion Tabla = new Certificado_Promocion(
                                                archivo.getAbsolutePath(),
                                                Gestionar_Reportes.getEstudiante(ESTUDIANTE_SELECCIONADO),
                                                Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO),
                                                Gestionar_Reportes.getGrado(ESTUDIANTE_SELECCIONADO),
                                                Gestionar_Reportes.getGradoNuevo(Gestionar_Reportes.getGrado(ESTUDIANTE_SELECCIONADO)),
                                                Gestionar_Reportes.getPeriodo(Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                                Gestionar_Reportes.getNotaDefinitiva(Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                                Gestionar_Reportes.getDia(), Gestionar_Reportes.getMes(), Gestionar_Reportes.getAno(),
                                                Gestionar_Reportes.getDocente(Gestionar_Reportes.getGrado(ESTUDIANTE_SELECCIONADO)),
                                                Ventana
                                            );
                                        }
                                        break;
                                    }
                                }
                                
                                //CONSTANCIA DE ESTUDIO
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Generar Constancia de Estudio"){
                                    String estudiante = Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador);
                                    String identificador = estudiante.split(Pattern.quote("-"))[0];
                                    String nombre = estudiante.split(Pattern.quote("-"))[1];
                                    String grado = Ventana.getGradoStr(Integer.parseInt(estudiante.split(Pattern.quote("-"))[2]));
                                    String ESTUDIANTE_SELECCIONADO = identificador+"-"+nombre+"-"+grado;
        
                                    JFileChooser RUTA = new JFileChooser();
                                    RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                                    RUTA.setFileFilter(new FileNameExtensionFilter("*.PDF", "pdf"));
                                    int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);

                                    if(OPCION_SELECCIONADA == 1){ return; }

                                    String ruta = RUTA.getSelectedFile().toString();
                                    ruta = ruta.split(Pattern.quote("."))[0];                  
                                    File archivo = new File(ruta+".pdf");

                                    while(true){
                                        if(archivo.exists()){
                                            if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                                                    "Archivo Existente",
                                                    JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                                                RUTA.getSelectedFile().delete();
                                            }else{
                                                return;
                                            }
                                        }

                                        if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                                            Constancia_Estudio Tabla = new Constancia_Estudio(
                                                archivo.getAbsolutePath(), 
                                                Gestionar_Reportes.getEstudiante(ESTUDIANTE_SELECCIONADO),
                                                Gestionar_Reportes.getGrado(ESTUDIANTE_SELECCIONADO),
                                                Gestionar_Reportes.getPeriodo(Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                                Gestionar_Reportes.getDia(), Gestionar_Reportes.getMes(), Gestionar_Reportes.getAno(), 
                                                Gestionar_Reportes.getRepresentante(Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                                Gestionar_Reportes.getRepresentanteCedula(Gestionar_Reportes.getIdentidicador(ESTUDIANTE_SELECCIONADO)),
                                                Ventana
                                            );
                                        }
                                        break;
                                    }
                                }

                                //GESTIONAR NOTAS
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Gestionar Notas"){
                                    ocultarPaneles();
                                    Gestionar_Notas.setVisible(true);
                                    titulo_notas.setVisible(true);
                                    Gestionar_Notas.Lista_Estudiantes.setSelectedItem(Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador));
                                    Gestionar_Notas.cargarInformacion(
                                       (Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador)).split("-")[2],
                                       (Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador))
                                    );
                                }

                                //REGISTRAR PAGO
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Registrar Pago"){
                                    Listar_Pagos.cargarListado(Ventana.obtenerListaPagos(estudiante_busqueda.identificador));
                                    Registro_Pago.actualizarListaEstudianteID(estudiante_busqueda.identificador);
                                    ocultarPaneles();
                                    titulo_pagos.setVisible(true);
                                    añadir_pago.setVisible(true);
                                    listar_pagos.setVisible(true);
                                    Registro_Pago.setVisible(true);
                                }
                                
                                //INHABILITAR ESTUDIANTE
                                if(estudiante_busqueda.lista_seleccion.getSelectedItem() == "Inhabilitar Estudiante"){
                                    int dialogResult = JOptionPane.showConfirmDialog(null, "Seguro que desea inhabilitar?", "Inhabilitar Estudiante", JOptionPane.YES_NO_OPTION);
                                    if(dialogResult == JOptionPane.YES_OPTION){
                                        Ventana.eliminarEstudiante(estudiante_busqueda.identificador);
                                        Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(), false, profesor);
                                    } 
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(busqueda_estudiante.getText().length()>0)
                    busqueda_estudiante_ph.setVisible(false);
                else
                    busqueda_estudiante_ph.setVisible(true);
            }
        });
        panel_menu.add(busqueda_estudiante);
        
        y += (int)(panel_menu.getWidth()*0.20);
        
        if(Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR")){
            //configuracion
            boton_configuracion = new JButton("Configuración"){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.WHITE);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(Borde);
                }
            };
            boton_configuracion.setFont(new Font("Arial",Font.BOLD,24));
            boton_configuracion.setForeground(Color.GRAY);
            boton_configuracion.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    boton_configuracion.setFont(new Font("Arial",Font.BOLD,26));
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    boton_configuracion.setFont(new Font("Arial",Font.BOLD,24));
                }
            });
            boton_configuracion.setSize((int)(panel_menu.getWidth()*0.8),(int)(panel_menu.getHeight()*0.075));
            boton_configuracion.setLocation((int)(panel_menu.getWidth()*0.1),y);
            boton_configuracion.setOpaque(true);
            boton_configuracion.setContentAreaFilled(false);
            boton_configuracion.setFocusable(false);
            boton_configuracion.setCursor(new Cursor(Cursor.HAND_CURSOR));
            boton_configuracion.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    ocultarPaneles();
                    titulo_configuracion.setVisible(true);
                    Configuracion.setVisible(true);
                }
            });
            panel_menu.add(boton_configuracion);
        }
        
        y += (int)(panel_menu.getWidth()*0.20);
        
        //cerrar_sesion
        cerrar_sesion = new JButton("Salir"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.WHITE);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        cerrar_sesion.setFont(new Font("Arial",Font.BOLD,24));
        cerrar_sesion.setForeground(Color.GRAY);
        cerrar_sesion.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                cerrar_sesion.setFont(new Font("Arial",Font.BOLD,26));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                cerrar_sesion.setFont(new Font("Arial",Font.BOLD,24));
            }
        });
        cerrar_sesion.setSize((int)(panel_menu.getWidth()*0.8),(int)(panel_menu.getHeight()*0.075));
        cerrar_sesion.setLocation((int)(panel_menu.getWidth()*0.1),y);
        cerrar_sesion.setOpaque(true);
        cerrar_sesion.setContentAreaFilled(false);
        cerrar_sesion.setFocusable(false);
        cerrar_sesion.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_menu.add(cerrar_sesion);
        
//        if(!Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR") &&
//            !Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR"))
//            busqueda_estudiante.setEditable(false);
    }
    
    public void ocultarPaneles(){
        //PERSONAL
        titulo_personal.setVisible(false);
        registrar_personal.setVisible(false);
        listar_personal.setVisible(false);
        Registro_Personal.setVisible(false);
        Listar_Personal.setVisible(false);
        filtro_personal.setVisible(false);
        
        //ESTUDIANTE
        Registro_Estudiante.setVisible(false);
        inscribir_estudiante.setVisible(false);
        Listar_Estudiante.setVisible(false);
        listar_estudiante.setVisible(false);
        titulo_estudiantes.setVisible(false);
        filtro_estudiante.setVisible(false);
        
        //GESTION PERFIL
        Gestion_Perfil.setVisible(false);
        
        //GESTIONAR PAGO
        titulo_pagos.setVisible(false);
        añadir_pago.setVisible(false);
        listar_pagos.setVisible(false);
        Registro_Pago.setVisible(false);
        Listar_Pagos.setVisible(false);
        
        //GESTIONAR REPORTES
        Gestionar_Reportes.setVisible(false);
        titulo_reportes.setVisible(false);
        
        //GESTIONAR NOTAS
        Gestionar_Notas.setVisible(false);
        titulo_notas.setVisible(false);
        
        //Configuración
        Configuracion.setVisible(false);
        titulo_configuracion.setVisible(false);
    }
    
    public void verificarUsuario(){
        if(Login.Cargo_User.equalsIgnoreCase("PROFESOR")){
            profesor = true;
            gestion_personal.setEnabled(false);
//            gestion_pagos.setEnabled(false);
        }
        if(Login.Cargo_User.equalsIgnoreCase("ASISTENTE")){
            gestion_personal.setEnabled(false);
//            gestion_notas.setEnabled(false);
        }
    }
    
    public List<String> buscarEstudiantes(String busqueda){
        List<String> estudiantes = new ArrayList<>();
        List<String> retorno = new ArrayList<>();
                
        try{
            String Query = "SELECT * FROM estudiante";
            
            if("PROFESOR".equals(Login.Cargo_User)){
                int gradoid = Integer.parseInt(Login.Grado_user);
                Query = "SELECT * FROM estudiante WHERE grado_id='"+gradoid+"'";
            }
                    
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            String inhabilitado;
            while(rs.next()){
                inhabilitado = "";
                if(Integer.parseInt(rs.getString("eliminado")) == 1){
                    inhabilitado = "INHABILITADO";
                }
                estudiantes.add(
                    rs.getString("identificador_estudiante")+" "+
                    rs.getString("apellidos")+" "+rs.getString("nombres")+" "+
                    Ventana.getGradoStr(Integer.parseInt(rs.getString("grado_id"))) +" "+
                    inhabilitado
                );
            }
            
            for(int i=0;i<estudiantes.size();i++){
                String [] aux = estudiantes.get(i).split(" ");
                for(int x=0;x<aux.length;x++){
                    if(aux[x].equalsIgnoreCase(busqueda)){
                        retorno.add(estudiantes.get(i));
                    }
                }
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Listado de Estudiantes", JOptionPane.ERROR_MESSAGE);
        }
        System.out.println("Resultados: "+retorno.size());
        return retorno;
    }
    
    public void generarBoletin(Estudiante_Resultado estudiante_busqueda){
        String estudiante = Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador);
        String identificador = estudiante.split(Pattern.quote("-"))[0];
        String nombre = estudiante.split(Pattern.quote("-"))[1];
        String grado = Ventana.getGradoStr(Integer.parseInt(estudiante.split(Pattern.quote("-"))[2]));
        String ESTUDIANTE_SELECCIONADO = identificador+"-"+nombre+"-"+grado;
        Gestionar_Reportes.generarBoletin(ESTUDIANTE_SELECCIONADO);
    }
    
    public void generarListaPagos(Estudiante_Resultado estudiante_busqueda){
        String identificador = Ventana.obtenerListaEstudianteID(estudiante_busqueda.identificador);
        
        JFileChooser RUTA = new JFileChooser();
        RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        RUTA.setFileFilter(new FileNameExtensionFilter("*.XLS", "xls"));
        int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);
       
        if(OPCION_SELECCIONADA == 1){ return; }
                
        String ruta = RUTA.getSelectedFile().toString();
        ruta = ruta.split(Pattern.quote("."))[0];                  
        File archivo = new File(ruta+".xls");
        
        while(true){
            if(archivo.exists()){
                if(JOptionPane.showConfirmDialog(null, "¿Desea reemplazar el archivo?",
                        "Archivo Existente",
                        JOptionPane.OK_CANCEL_OPTION)==JFileChooser.APPROVE_OPTION){
                    RUTA.getSelectedFile().delete();
                }else{
                    return;
                }
            }
            
            if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                Reporte_Pagos_Lista Tabla = new Reporte_Pagos_Lista(
                    archivo.getAbsolutePath(),
                    Gestionar_Reportes.getIdentidicador(identificador),
                    Ventana
                );
            }
            break;
        }
    }
}
