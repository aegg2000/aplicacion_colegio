package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Informacion_Personal extends JPanel{
    public JTextField T_CEDULA,T_NOMBRE,T_APELLIDO,T_CARGO,T_AÑO,T_DIR_TRABAJO,T_DIR_HABITACION,T_TLF_TRABAJO,
        T_TLF_HABITACION,T_TLF_CELULAR,T_CORREO;
    public JTextField CEDULA,NOMBRE,APELLIDO,CARGO,AÑO,DIR_TRABAJO,DIR_HABITACION,TLF_TRABAJO,
        TLF_HABITACION,TLF_CELULAR,CORREO;
    public JComboBox cargo,grado_a_cargo;
    public JButton Editar;
    public Ventana_Principal Ventana;
    public int Ancho,Alto,pos_y;
    public boolean edicion;
    public MySQL db;
    
    public Informacion_Personal(Ventana_Principal Ventana, Login Login, String ID){
        Ancho = 350;
        Alto = 450;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        
        try{
            db = new MySQL();
            db.MySQLConnect();
        }catch(Exception e){}
        
        edicion = false;
        pos_y = 0;
        
        T_CEDULA = new JTextField("CEDULA");
        T_CEDULA.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_CEDULA.setLocation(0,pos_y);
        T_CEDULA.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_CEDULA.setEditable(false);
        T_CEDULA.setFont(new Font("Arial",Font.BOLD,12));
        T_CEDULA.setForeground(Color.WHITE);
        T_CEDULA.setBackground(new Color(93, 193, 185));
        add(T_CEDULA);
        
        pos_y += (int)(Alto*0.083);
        
        T_NOMBRE = new JTextField("NOMBRES");
        T_NOMBRE.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_NOMBRE.setLocation(0,pos_y);
        T_NOMBRE.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_NOMBRE.setEditable(false);
        T_NOMBRE.setFont(new Font("Arial",Font.BOLD,12));
        T_NOMBRE.setForeground(Color.WHITE);
        T_NOMBRE.setBackground(new Color(93, 193, 185));
        add(T_NOMBRE);
        
        pos_y += (int)(Alto*0.083);
        
        T_APELLIDO = new JTextField("APELLIDOS");
        T_APELLIDO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_APELLIDO.setLocation(0,pos_y);
        T_APELLIDO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_APELLIDO.setEditable(false);
        T_APELLIDO.setFont(new Font("Arial",Font.BOLD,12));
        T_APELLIDO.setForeground(Color.WHITE);
        T_APELLIDO.setBackground(new Color(93, 193, 185));
        add(T_APELLIDO);
        
        pos_y += (int)(Alto*0.083);
        
        T_CARGO = new JTextField("CARGO");
        T_CARGO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_CARGO.setLocation(0,pos_y);
        T_CARGO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_CARGO.setEditable(false);
        T_CARGO.setFont(new Font("Arial",Font.BOLD,12));
        T_CARGO.setForeground(Color.WHITE);
        T_CARGO.setBackground(new Color(93, 193, 185));
        add(T_CARGO);
        
        pos_y += (int)(Alto*0.083);
        
        T_AÑO = new JTextField("GRADO A CARGO");
        T_AÑO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_AÑO.setLocation(0,pos_y);
        T_AÑO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_AÑO.setEditable(false);
        T_AÑO.setFont(new Font("Arial",Font.BOLD,12));
        T_AÑO.setForeground(Color.WHITE);
        T_AÑO.setBackground(new Color(93, 193, 185));
        add(T_AÑO);
        
        pos_y += (int)(Alto*0.083);
        
        T_DIR_TRABAJO = new JTextField("DIR. TRABAJO");
        T_DIR_TRABAJO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_DIR_TRABAJO.setLocation(0,pos_y);
        T_DIR_TRABAJO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_DIR_TRABAJO.setEditable(false);
        T_DIR_TRABAJO.setFont(new Font("Arial",Font.BOLD,12));
        T_DIR_TRABAJO.setForeground(Color.WHITE);
        T_DIR_TRABAJO.setBackground(new Color(93, 193, 185));
        add(T_DIR_TRABAJO);
        
        pos_y += (int)(Alto*0.083);
        
        T_DIR_HABITACION = new JTextField("DIR. HABITACIÓN");
        T_DIR_HABITACION.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_DIR_HABITACION.setLocation(0,pos_y);
        T_DIR_HABITACION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_DIR_HABITACION.setEditable(false);
        T_DIR_HABITACION.setFont(new Font("Arial",Font.BOLD,12));
        T_DIR_HABITACION.setForeground(Color.WHITE);
        T_DIR_HABITACION.setBackground(new Color(93, 193, 185));
        add(T_DIR_HABITACION);
        
        pos_y += (int)(Alto*0.083);
        
        T_TLF_TRABAJO = new JTextField("TLF. TRABAJO");
        T_TLF_TRABAJO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_TLF_TRABAJO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_TLF_TRABAJO.setLocation(0,pos_y);
        T_TLF_TRABAJO.setEditable(false);
        T_TLF_TRABAJO.setFont(new Font("Arial",Font.BOLD,12));
        T_TLF_TRABAJO.setForeground(Color.WHITE);
        T_TLF_TRABAJO.setBackground(new Color(93, 193, 185));
        add(T_TLF_TRABAJO);
        
        pos_y += (int)(Alto*0.083);
        
        T_TLF_HABITACION = new JTextField("TLF. HABITACIÓN");
        T_TLF_HABITACION.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_TLF_HABITACION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_TLF_HABITACION.setLocation(0,pos_y);
        T_TLF_HABITACION.setEditable(false);
        T_TLF_HABITACION.setFont(new Font("Arial",Font.BOLD,12));
        T_TLF_HABITACION.setForeground(Color.WHITE);
        T_TLF_HABITACION.setBackground(new Color(93, 193, 185));
        add(T_TLF_HABITACION);
        
        pos_y += (int)(Alto*0.083);
        
        T_TLF_CELULAR = new JTextField("TLF. CELULAR");
        T_TLF_CELULAR.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_TLF_CELULAR.setLocation(0,pos_y);
        T_TLF_CELULAR.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_TLF_CELULAR.setEditable(false);
        T_TLF_CELULAR.setFont(new Font("Arial",Font.BOLD,12));
        T_TLF_CELULAR.setForeground(Color.WHITE);
        T_TLF_CELULAR.setBackground(new Color(93, 193, 185));
        add(T_TLF_CELULAR);
        
        pos_y += (int)(Alto*0.083);
        
        T_CORREO = new JTextField("CORREO");
        T_CORREO.setSize((int)(Ancho*0.3),(int)(Alto*0.083));
        T_CORREO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        T_CORREO.setLocation(0,pos_y);
        T_CORREO.setEditable(false);
        T_CORREO.setFont(new Font("Arial",Font.BOLD,12));
        T_CORREO.setForeground(Color.WHITE);
        T_CORREO.setBackground(new Color(93, 193, 185));
        add(T_CORREO);
        
        pos_y += (int)(Alto*0.083);
        
        Editar = new JButton("Modificar Personal"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        Editar.setSize((int)(Ancho),(int)(Alto*0.083));
        Editar.setLocation(0,pos_y);
        if(Login.Cargo_User.equalsIgnoreCase("ADMINISTRADOR"))
            Editar.setEnabled(true);
        else
            Editar.setEnabled(false);
        Editar.setContentAreaFilled(false);
        Editar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        Editar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(Editar.getText().equalsIgnoreCase("Modificar Personal")){
                    String aux_cargo = getTexto(CARGO.getText());
                    //ENVIAR TEXTO SIN ESPACIO
                    CEDULA.setText(getTexto(CEDULA.getText()));
                    NOMBRE.setText(getTexto(NOMBRE.getText()));
                    APELLIDO.setText(getTexto(APELLIDO.getText()));
                    DIR_TRABAJO.setText(getTexto(DIR_TRABAJO.getText()));
                    DIR_HABITACION.setText(getTexto(DIR_HABITACION.getText()));
                    TLF_TRABAJO.setText(getTexto(TLF_TRABAJO.getText()));
                    TLF_HABITACION.setText(getTexto(TLF_HABITACION.getText()));
                    TLF_CELULAR.setText(getTexto(TLF_CELULAR.getText()));
                    CORREO.setText(getTexto(CORREO.getText()));
                    
                    //HABILITAR CAMPOS
//                    CEDULA.setEditable(true);
                    NOMBRE.setEditable(true);
                    APELLIDO.setEditable(true);
                    CARGO.setVisible(false);
                    cargo.setVisible(true);
                    AÑO.setVisible(false);
                    grado_a_cargo.setVisible(true);
                    DIR_TRABAJO.setEditable(true);
                    DIR_HABITACION.setEditable(true);
                    TLF_TRABAJO.setEditable(true);
                    TLF_HABITACION.setEditable(true);
                    TLF_CELULAR.setEditable(true);
                    CORREO.setEditable(true);
                    if(aux_cargo.equalsIgnoreCase("ADMINISTRADOR") ||
                        aux_cargo.equalsIgnoreCase("ASISTENTE")){
                        AÑO.setVisible(true);
                        grado_a_cargo.setVisible(false);
                    }
                    Editar.setText("Guardar Cambios");
                    Informacion_Personal.this.repaint();
                }else{
                    CEDULA.setEditable(false);
                    NOMBRE.setEditable(false);
                    APELLIDO.setEditable(false);
                    CARGO.setVisible(true);
                    cargo.setVisible(false);
                    AÑO.setVisible(true);
                    grado_a_cargo.setVisible(false);
                    DIR_TRABAJO.setEditable(false);
                    DIR_HABITACION.setEditable(false);
                    TLF_TRABAJO.setEditable(false);
                    TLF_HABITACION.setEditable(false);
                    TLF_CELULAR.setEditable(false);
                    CORREO.setEditable(false);
                    Editar.setText("Modificar Personal");
                    editarInformacion(ID);
                    cargarInformacion(ID);    
                    Informacion_Personal.this.repaint();
                    Ventana.menu_home.Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(0),Login);
                }
            }
        });
        Editar.setFont(new Font("Arial",Font.BOLD,12));
        add(Editar);
        
        cargarInformacion(ID);
    }
    
    public int getGradoID(String grado_str){
        int grado;
        switch (grado_str) {
            case "PRIMER GRADO":
                grado = 1;
                break;
            case "SEGUNDO GRADO":
                grado = 2;
                break;
            case "TERCER GRADO":
                grado = 3;
                break;
            case "CUARTO GRADO":
                grado = 4;
                break;
            case "QUINTO GRADO":
                grado = 5;
                break;
            case "SEXTO GRADO":
                grado = 6;
                break;
            default:
                grado = 7;
                break;
        }
        return grado;
    }
    
    public String getGrado(int grado){
        String grado_str;
        switch (grado) {
            case 1:
                grado_str = "PRIMER GRADO";
                break;
            case 2:
                grado_str = "SEGUNDO GRADO";
                break;
            case 3:
                grado_str = "TERCER GRADO";
                break;
            case 4:
                grado_str = "CUARTO GRADO";
                break;
            case 5:
                grado_str = "QUINTO GRADO";
                break;
            case 6:
                grado_str = "SEXTO GRADO";
                break;
            default:
                grado_str = "NINGUNO";
                break;
        }
        return grado_str;
    }
    
    public void cargarInformacion(String ID){
        //OBTENER EMPLEADO
        try{            
            String Query = "SELECT * FROM empleado WHERE cedula='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            ResultSet rs = db.comando.executeQuery(Query);
           
            while(rs.next()){             
                pos_y = 0;
        
                CEDULA = new JTextField("  "+rs.getString("cedula"));
                CEDULA.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                CEDULA.setLocation((int)(Ancho*0.3),pos_y);
                CEDULA.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                CEDULA.setEditable(false);
                CEDULA.setFont(new Font("Arial",Font.PLAIN,12));
                CEDULA.setForeground(Color.BLACK);
                CEDULA.setBackground(Color.WHITE);
                setMaximo(CEDULA,9);
                setNumber(CEDULA);
                add(CEDULA);

                pos_y += (int)(Alto*0.083);

                NOMBRE = new JTextField("  "+rs.getString("nombres"));
                NOMBRE.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                NOMBRE.setLocation((int)(Ancho*0.3),pos_y);
                NOMBRE.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                NOMBRE.setEditable(false);
                NOMBRE.setFont(new Font("Arial",Font.PLAIN,12));
                NOMBRE.setForeground(Color.BLACK);
                NOMBRE.setBackground(Color.WHITE);
                setMaximo(NOMBRE,65);
                add(NOMBRE);

                pos_y += (int)(Alto*0.083);

                APELLIDO = new JTextField("  "+rs.getString("apellidos"));
                APELLIDO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                APELLIDO.setLocation((int)(Ancho*0.3),pos_y);
                APELLIDO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                APELLIDO.setEditable(false);
                APELLIDO.setFont(new Font("Arial",Font.PLAIN,12));
                APELLIDO.setForeground(Color.BLACK);
                APELLIDO.setBackground(Color.WHITE);
                setMaximo(APELLIDO,65);
                add(APELLIDO);

                pos_y += (int)(Alto*0.083);

                CARGO = new JTextField("  "+rs.getString("cargo"));
                CARGO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                CARGO.setLocation((int)(Ancho*0.3),pos_y);
                CARGO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                CARGO.setEditable(false);
                CARGO.setFont(new Font("Arial",Font.PLAIN,12));
                CARGO.setForeground(Color.BLACK);
                CARGO.setBackground(Color.WHITE);
                add(CARGO);
        
                cargo = new JComboBox();
                cargo.addItem("PROFESOR");
                cargo.addItem("ASISTENTE");
                cargo.addItem("ADMINISTRADOR");
                cargo.setSelectedItem(getTexto(CARGO.getText()));
                cargo.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent ie) {                        
                        String seleccion = (String)(cargo.getSelectedItem());
                        if(seleccion.equalsIgnoreCase("PROFESOR")){
                            grado_a_cargo.setEnabled(true);
                            grado_a_cargo.setVisible(true);
                            AÑO.setVisible(false);
                        }
                        else{
                            grado_a_cargo.setEnabled(false);
                            grado_a_cargo.setSelectedItem(getTexto(AÑO.getText()));
                            grado_a_cargo.setVisible(false);
                            AÑO.setVisible(true);
                        }
                    }
                });
                cargo.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                cargo.setLocation((int)(Ancho*0.3),pos_y);
                cargo.setBackground(Color.WHITE);
                cargo.setForeground(Color.GRAY);
                cargo.setInheritsPopupMenu(true);
                cargo.setFocusable(false);
                cargo.setVisible(false);
                cargo.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
                cargo.setFont(new Font("Arial",Font.ITALIC,14));
                add(cargo);
        
                pos_y += (int)(Alto*0.083);
                
                String grado_str = getGrado(Integer.parseInt(rs.getString("grado_id")));

                AÑO = new JTextField("  "+grado_str);
                AÑO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                AÑO.setLocation((int)(Ancho*0.3),pos_y);
                AÑO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                AÑO.setEditable(false);
                AÑO.setFont(new Font("Arial",Font.PLAIN,12));
                AÑO.setForeground(Color.BLACK);
                AÑO.setBackground(Color.WHITE);
                add(AÑO);
                
                grado_a_cargo = new JComboBox();
                grado_a_cargo.addItem("PRIMER GRADO");
                grado_a_cargo.addItem("SEGUNDO GRADO");
                grado_a_cargo.addItem("TERCER GRADO");
                grado_a_cargo.addItem("CUARTO GRADO");
                grado_a_cargo.addItem("QUINTO GRADO");
                grado_a_cargo.addItem("SEXTO GRADO");
                grado_a_cargo.setSelectedItem(getTexto(AÑO.getText()));
                grado_a_cargo.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent ie) {
                    }
                });
                grado_a_cargo.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                grado_a_cargo.setLocation((int)(Ancho*0.3),pos_y);
                grado_a_cargo.setBackground(Color.WHITE);
                grado_a_cargo.setForeground(Color.GRAY);
                grado_a_cargo.setInheritsPopupMenu(true);
                grado_a_cargo.setFocusable(false);
                grado_a_cargo.setVisible(false);
                grado_a_cargo.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
                grado_a_cargo.setFont(new Font("Arial",Font.ITALIC,14));
                add(grado_a_cargo);

                pos_y += (int)(Alto*0.083);

                DIR_TRABAJO = new JTextField("  "+rs.getString("direccion_trabajo"));
                DIR_TRABAJO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                DIR_TRABAJO.setLocation((int)(Ancho*0.3),pos_y);
                DIR_TRABAJO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                DIR_TRABAJO.setEditable(false);
                DIR_TRABAJO.setFont(new Font("Arial",Font.PLAIN,12));
                DIR_TRABAJO.setForeground(Color.BLACK);
                DIR_TRABAJO.setBackground(Color.WHITE);
                add(DIR_TRABAJO);
                setMaximo(DIR_TRABAJO,65);

                pos_y += (int)(Alto*0.083);

                DIR_HABITACION = new JTextField("  "+rs.getString("direccion_habitacion"));
                DIR_HABITACION.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                DIR_HABITACION.setLocation((int)(Ancho*0.3),pos_y);
                DIR_HABITACION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                DIR_HABITACION.setEditable(false);
                DIR_HABITACION.setFont(new Font("Arial",Font.PLAIN,12));
                DIR_HABITACION.setForeground(Color.BLACK);
                DIR_HABITACION.setBackground(Color.WHITE);
                add(DIR_HABITACION);
                setMaximo(DIR_HABITACION,65);

                pos_y += (int)(Alto*0.083);

                TLF_TRABAJO = new JTextField("  "+rs.getString("telefono_trabajo"));
                TLF_TRABAJO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                TLF_TRABAJO.setLocation((int)(Ancho*0.3),pos_y);
                TLF_TRABAJO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                TLF_TRABAJO.setEditable(false);
                TLF_TRABAJO.setFont(new Font("Arial",Font.PLAIN,12));
                TLF_TRABAJO.setForeground(Color.BLACK);
                TLF_TRABAJO.setBackground(Color.WHITE);
                setMaximo(TLF_TRABAJO,12);
                setNumber(TLF_TRABAJO);
                add(TLF_TRABAJO);

                pos_y += (int)(Alto*0.083);

                TLF_HABITACION = new JTextField("  "+rs.getString("telefono_habitacion"));
                TLF_HABITACION.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                TLF_HABITACION.setLocation((int)(Ancho*0.3),pos_y);
                TLF_HABITACION.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                TLF_HABITACION.setEditable(false);
                TLF_HABITACION.setFont(new Font("Arial",Font.PLAIN,12));
                TLF_HABITACION.setForeground(Color.BLACK);
                TLF_HABITACION.setBackground(Color.WHITE);
                setMaximo(TLF_HABITACION,12);
                setNumber(TLF_HABITACION);
                add(TLF_HABITACION);

                pos_y += (int)(Alto*0.083);

                TLF_CELULAR = new JTextField("  "+rs.getString("telefono_celular"));
                TLF_CELULAR.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                TLF_CELULAR.setLocation((int)(Ancho*0.3),pos_y);
                TLF_CELULAR.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                TLF_CELULAR.setEditable(false);
                TLF_CELULAR.setFont(new Font("Arial",Font.PLAIN,12));
                TLF_CELULAR.setForeground(Color.BLACK);
                TLF_CELULAR.setBackground(Color.WHITE);
                setMaximo(TLF_CELULAR,12);
                setNumber(TLF_CELULAR);
                add(TLF_CELULAR);

                pos_y += (int)(Alto*0.083);

                CORREO = new JTextField("  "+rs.getString("correo_electronico"));
                CORREO.setSize((int)(Ancho*0.7),(int)(Alto*0.083));
                CORREO.setLocation((int)(Ancho*0.3),pos_y);
                CORREO.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                CORREO.setEditable(false);
                CORREO.setFont(new Font("Arial",Font.PLAIN,12));
                CORREO.setForeground(Color.BLACK);
                CORREO.setBackground(Color.WHITE);
                add(CORREO);
            }
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡Error!", "Información de Personal", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void editarInformacion(String ID){
        //MODIFICAR EMPLEADO
        String aux_cargo = (String)(cargo.getSelectedItem());
        int grado_id;
        if(getTexto(CARGO.getText()).equalsIgnoreCase("ADMINISTRADOR") && Ventana.checkLastAdmin()){
            String mensaje = "!No puede modificar el último administrador!";
            JOptionPane.showMessageDialog(null, mensaje, "AVISO",JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        if(cargo.getSelectedItem().toString().equalsIgnoreCase("ADMINISTRADOR") || cargo.getSelectedItem().toString().equalsIgnoreCase("ASISTENTE")){
            grado_id = 7;
        }else{
            String aux_grado = (String)(grado_a_cargo.getSelectedItem());
            if(Ventana.empleadoRepetidoGrado(aux_grado, "PROFESOR")){
                String mensaje = "!El cargo PROFESOR para el grado escolar "+aux_grado+" ya se encuentra ocupado!";
                JOptionPane.showMessageDialog(null, mensaje, "AVISO",JOptionPane.WARNING_MESSAGE);
                return;
            }
            grado_id = getGradoID(aux_grado);
        }
        
        try{            
            String Query = "UPDATE empleado SET " +
//                "cedula='"+CEDULA.getText()+"'," +
                "nombres='"+NOMBRE.getText()+"'," +
                "apellidos='"+APELLIDO.getText()+"'," +
                "cargo='"+aux_cargo+"'," +
                "grado_id='"+grado_id+"'," +
                "direccion_trabajo='"+(DIR_TRABAJO.getText())+"'," +
                "direccion_habitacion='"+(DIR_HABITACION.getText())+"'," +
                "telefono_trabajo='"+(TLF_TRABAJO.getText())+"'," +
                "telefono_habitacion='"+(TLF_HABITACION.getText())+"'," +
                "telefono_celular='"+(TLF_CELULAR.getText())+"'," +
                "correo_electronico='"+(CORREO.getText())+"' " +
                "WHERE cedula='"+ID+"'";
        
            db.comando = db.conexion.createStatement();
            db.comando.execute(Query);
             
            JOptionPane.showMessageDialog(null, "¡Modificación Exitosa!", "Información de Personal", JOptionPane.PLAIN_MESSAGE);
            Informacion_Personal.this.repaint();
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡Error de Registro!", "Información de Personal", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String getTexto(String texto){
        return texto.substring(2, texto.length());
    }
    
    public void setMaximo(JTextField campo, int limite){
        campo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }
        });
    }
    
    public void setNumber(JTextField campo){
        campo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }
        });
    }
}
