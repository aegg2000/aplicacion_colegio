package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Listar_Personal extends JPanel{
    public Ventana_Principal Ventana;
    public JTextField nombre, apellido, cedula, cargo, informacion;
    public JTextField [] t_nombre, t_apellido, t_cedula, t_cargo;
    public int Ancho, Alto;
    public JPanel panel;
    public JScrollPane scroll;
    public JButton [] Info, eliminar;
    
    public Listar_Personal(Ventana_Principal Ventana,Login Login,int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        setBackground(Color.WHITE);
        
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setLocation(0,(int)(Alto*0.07));
        scroll.setSize(Ancho,(int)(Alto*0.9));
        panel.setLocation(0,0);
        panel.setSize((int)(Ancho*0.95),(int)(Alto*0.95));
        scroll.setViewportView(panel);
        scroll.setVisible(true);
        add(scroll);
        
        cargarListado(Ventana.obtenerListaPersonal(0), Login);
    }
    
    public void cargarListado(List<String> empleados, Login Login){
       panel.removeAll();
       
       t_cedula = new JTextField[empleados.size()/4];
       t_nombre = new JTextField[empleados.size()/4];
       t_apellido = new JTextField[empleados.size()/4];
       t_cargo = new JTextField[empleados.size()/4];
       Info = new JButton[empleados.size()/4];
       eliminar = new JButton[empleados.size()/4];
              
       int pos_x = 0, pos_y = 0;
       
       cedula = new JTextField("CEDULA");
       cedula.setLocation(pos_x,pos_y);
       cedula.setHorizontalAlignment(SwingConstants.CENTER);
       cedula.setSize((int)(Ancho/5),(int)(Alto*0.07));
       cedula.setBackground(new Color(93, 193, 185));
       cedula.setForeground(Color.WHITE);
       cedula.setFont(new Font("Arial",Font.BOLD,16));
       cedula.setEditable(false);
       add(cedula);
       
       pos_x += (int)(Ancho/5);
       
       nombre = new JTextField("NOMBRES");
       nombre.setLocation(pos_x,pos_y);
       nombre.setHorizontalAlignment(SwingConstants.CENTER);
       nombre.setSize((int)(Ancho/5),(int)(Alto*0.07));
       nombre.setBackground(new Color(93, 193, 185));
       nombre.setForeground(Color.WHITE);
       nombre.setFont(new Font("Arial",Font.BOLD,16));
       nombre.setEditable(false);
       add(nombre);
       
       pos_x += (int)(Ancho/5);
       
       apellido = new JTextField("APELLIDOS");
       apellido.setLocation(pos_x,pos_y);
       apellido.setHorizontalAlignment(SwingConstants.CENTER);
       apellido.setSize((int)(Ancho/5),(int)(Alto*0.07));
       apellido.setBackground(new Color(93, 193, 185));
       apellido.setForeground(Color.WHITE);
       apellido.setFont(new Font("Arial",Font.BOLD,16));
       apellido.setEditable(false);
       add(apellido);
       
       pos_x += (int)(Ancho/5);
       
       cargo = new JTextField("CARGO");
       cargo.setLocation(pos_x,pos_y);
       cargo.setHorizontalAlignment(SwingConstants.CENTER);
       cargo.setSize((int)(Ancho/5),(int)(Alto*0.07));
       cargo.setBackground(new Color(93, 193, 185));
       cargo.setForeground(Color.WHITE);
       cargo.setFont(new Font("Arial",Font.BOLD,16));
       cargo.setEditable(false);
       add(cargo);
       
       pos_x += (int)(Ancho/5);
       
       informacion = new JTextField("INFORMACIÓN");
       informacion.setLocation(pos_x,pos_y);
       informacion.setHorizontalAlignment(SwingConstants.CENTER);
       informacion.setSize((int)(Ancho/5),(int)(Alto*0.07));
       informacion.setBackground(new Color(93, 193, 185));
       informacion.setForeground(Color.WHITE);
       informacion.setFont(new Font("Arial",Font.BOLD,16));
       informacion.setEditable(false);
       add(informacion);
       
       pos_x = 0;
       
       int aux = 0;
       boolean last_admin = Ventana.checkLastAdmin();
       
       ImageIcon icon_vermas = Ventana.cargarImagen("b_vermas.png",(int)(Ancho*0.04),(int)(Alto*0.05));
       ImageIcon icon_vermas_hover = Ventana.cargarImagen("b_vermas_hover.png",(int)(Ancho*0.04),(int)(Alto*0.05));
       ImageIcon icon_delete = Ventana.cargarImagen("b_inhabilitar.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       ImageIcon icon_delete_hover = Ventana.cargarImagen("b_inhabilitar_hover.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       
        for(int i=0;i<empleados.size()/4;i++){
            int x = i;
            t_cedula[i] = new JTextField(empleados.get(aux++));
            t_cedula[i].setLocation(pos_x,pos_y);
            t_cedula[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_cedula[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_cedula[i].setBackground(Color.white);
            t_cedula[i].setEditable(false);
            panel.add(t_cedula[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_nombre[i] = new JTextField(empleados.get(aux++));
            t_nombre[i].setLocation(pos_x,pos_y);
            t_nombre[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_nombre[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_nombre[i].setBackground(Color.white);
            t_nombre[i].setEditable(false);
            panel.add(t_nombre[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_apellido[i] = new JTextField(empleados.get(aux++));
            t_apellido[i].setLocation(pos_x,pos_y);
            t_apellido[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_apellido[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_apellido[i].setBackground(Color.white);
            t_apellido[i].setEditable(false);
            panel.add(t_apellido[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_cargo[i] = new JTextField(empleados.get(aux++));
            t_cargo[i].setLocation(pos_x,pos_y);
            t_cargo[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_cargo[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_cargo[i].setBackground(Color.white);
            t_cargo[i].setEditable(false);
            panel.add(t_cargo[i]);
            
            pos_x += (int)(Ancho/5);
            
            Info[i] = new JButton(icon_vermas){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.RED);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(new RoundedCornerBorder());
                }
            };
            Info[i].setRolloverIcon(icon_vermas_hover);
            Info[i].setLocation(pos_x+2,pos_y);
            Info[i].setSize((int)(Ancho/5/2.5),(int)(Alto*0.07));
            Info[i].setFocusable(false);
            Info[i].setContentAreaFilled(false);
            Info[i].setFont(new Font("Arial",Font.PLAIN,14));
            Info[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
//                    Info[x].setFont(new Font("Arial",Font.BOLD,16));
                }

                @Override
                public void mouseExited(MouseEvent me) {
//                    Info[x].setFont(new Font("Arial",Font.PLAIN,14));
                }
            });
            Info[i].setOpaque(false);
            Info[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            Info[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    JOptionPane.showMessageDialog(null, new Informacion_Personal(Ventana,Login,t_cedula[x].getText()),"Información",JOptionPane.PLAIN_MESSAGE);
                }
            });
            panel.add(Info[i]);
            
            pos_x += (int)(Ancho/5/2.5);
            
            eliminar[i] = new JButton(icon_delete){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.RED);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(new RoundedCornerBorder());
                }
            };
            eliminar[i].setRolloverIcon(icon_delete_hover);
            eliminar[i].setLocation(pos_x+3,pos_y);
            eliminar[i].setSize((int)(Ancho/5/2),(int)(Alto*0.07));
            eliminar[i].setFocusable(false);
            eliminar[i].setContentAreaFilled(false);
            eliminar[i].setFont(new Font("Arial",Font.PLAIN,14));
            eliminar[i].setOpaque(false);
            eliminar[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            if("0".equals(empleados.get(empleados.size()-1)) && (!"ADMINISTRADOR".equals(t_cargo[i].getText()) || !last_admin)){
                eliminar[i].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent me) {
                    }

                    @Override
                    public void mousePressed(MouseEvent me) {
                    }

                    @Override
                    public void mouseReleased(MouseEvent me) {
                    }

                    @Override
                    public void mouseEntered(MouseEvent me) {
                        eliminar[x].setFont(new Font("Arial",Font.BOLD,16));
                    }

                    @Override
                    public void mouseExited(MouseEvent me) {
                        eliminar[x].setFont(new Font("Arial",Font.PLAIN,14));
                    }
                });
                eliminar[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int dialogResult = JOptionPane.showConfirmDialog(null, "Seguro que desea inhabilitar?", "Inhabilitar Empleado", JOptionPane.YES_NO_OPTION);
                        if(dialogResult == JOptionPane.YES_OPTION){
                            Ventana.eliminarEmpleado(t_cedula[x].getText());
                            cargarListado(Ventana.obtenerListaPersonal(0), Login);
                        }
                    }
                });
                eliminar[i].setEnabled(true);
            }else{
                eliminar[i].setEnabled(false);
            }
            panel.add(eliminar[i]);
            
            pos_x = 0;
            pos_y += (int)(Alto*0.07);
        }
        
        panel.setSize((int)(Ancho*0.92),(int)((int)(Alto*0.07*empleados.size()/4)));
        panel.setPreferredSize(new Dimension((int)(Ancho*0.92),(int)((int)(Alto*0.07*empleados.size()/4))));
        panel.repaint();
   }
}
