package aplicacion_colegio;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Notas_Estudiante extends JPanel{
    public int Ancho, Alto, pos_y;
    public JTextField nombre_apellido, identificador, grado, periodo, boletin;
    public JTextField t_materia,t_lapso_1,t_lapso_2,t_lapso_3,definitiva;
    
    //MATERIAS
    public JTextField lengua,matematica,ciencias_naturaleza,ciencias_sociales,
            educacion_estetica,educacion_fisica,ingles;
    public JComboBox [] nota_lengua,
                        nota_matematica,
                        nota_ciencias_naturaleza,
                        nota_ciencias_sociales,
                        nota_educacion_estetica,
                        nota_educacion_fisica,
                        nota_ingles;
    public JTextField [] nota_definitiva;
        
    public Notas_Estudiante(Ventana_Principal Ventana, int Anc, int Alt){
        Ancho = Anc;
        Alto = Alt;
        setSize(Ancho,Alto);
        setBackground(Color.WHITE);
        setLayout(null);
        setBorder(BorderFactory.createLineBorder(Color.WHITE));
        
        nota_definitiva = new JTextField[7];
        pos_y = 0;
        
        //ENCABEZADO
        nombre_apellido = new JTextField("ESTUDIANTE");
        nombre_apellido.setSize((int)(Ancho/1.5),(int)(Alto*0.1));
        nombre_apellido.setLocation(0,pos_y);
        nombre_apellido.setBackground(Color.WHITE);
        nombre_apellido.setForeground(Color.GRAY);
        nombre_apellido.setFont(new Font("Arial",Font.BOLD,14));
        nombre_apellido.setEditable(false);
        nombre_apellido.setHorizontalAlignment(SwingConstants.CENTER);
        add(nombre_apellido);
        
        //IDENTIFICADOR
        identificador = new JTextField("IDENTIFICADOR");
        identificador.setSize((int)(Ancho/3),(int)(Alto*0.1));
        identificador.setLocation((int)(Ancho/1.5),pos_y);
        identificador.setBackground(Color.WHITE);
        identificador.setForeground(Color.GRAY);
        identificador.setFont(new Font("Arial",Font.BOLD,14));
        identificador.setEditable(false);
        identificador.setHorizontalAlignment(SwingConstants.CENTER);
        add(identificador);
        
        pos_y += (int)(Alto*0.1);
        //GRADO
        grado = new JTextField("GRADO ESCOLAR");
        grado.setSize((int)(Ancho/1.5),(int)(Alto*0.1));
        grado.setLocation(0,pos_y);
        grado.setBackground(Color.WHITE);
        grado.setForeground(Color.GRAY);
        grado.setFont(new Font("Arial",Font.BOLD,14));
        grado.setEditable(false);
        grado.setHorizontalAlignment(SwingConstants.CENTER);
        add(grado);
        
        //PERIODO
        periodo = new JTextField("PERIODO");
        periodo.setSize((int)(Ancho/3),(int)(Alto*0.1));
        periodo.setLocation((int)(Ancho/1.5),pos_y);
        periodo.setBackground(Color.WHITE);
        periodo.setForeground(Color.GRAY);
        periodo.setFont(new Font("Arial",Font.BOLD,14));
        periodo.setEditable(false);
        periodo.setHorizontalAlignment(SwingConstants.CENTER);
        add(periodo);
        
        pos_y += (int)(Alto*0.1);
        
        //BOLETIN
        boletin = new JTextField("Notas");
        boletin.setSize((int)(Ancho),(int)(Alto*0.1));
        boletin.setLocation(0,pos_y);
        boletin.setBackground(Color.WHITE);
        boletin.setForeground(Color.GRAY);
        boletin.setFont(new Font("Arial",Font.BOLD,16));
        boletin.setEditable(false);
        boletin.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        boletin.setHorizontalAlignment(SwingConstants.CENTER);
        add(boletin);
        
        pos_y += (int)(Alto*0.1);
        
        //MATERIA
        t_materia = new JTextField("MATERIA");
        t_materia.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        t_materia.setLocation(0,pos_y);
        t_materia.setBackground(new Color(93, 193, 185));
        t_materia.setForeground(Color.WHITE);
        t_materia.setFont(new Font("Arial",Font.BOLD,14));
        t_materia.setEditable(false);
        t_materia.setHorizontalAlignment(SwingConstants.CENTER);
        add(t_materia);
        
        //LAPSO1
        t_lapso_1 = new JTextField("L.1");
        t_lapso_1.setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        t_lapso_1.setLocation((int)(Ancho*0.6),pos_y);
        t_lapso_1.setBackground(new Color(93, 193, 185));
        t_lapso_1.setForeground(Color.WHITE);
        t_lapso_1.setFont(new Font("Arial",Font.BOLD,14));
        t_lapso_1.setEditable(false);
        t_lapso_1.setHorizontalAlignment(SwingConstants.CENTER);
        add(t_lapso_1);
        
        //LAPSO2
        t_lapso_2 = new JTextField("L.2");
        t_lapso_2.setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        t_lapso_2.setLocation((int)(Ancho*0.7),pos_y);
        t_lapso_2.setBackground(new Color(93, 193, 185));
        t_lapso_2.setForeground(Color.WHITE);
        t_lapso_2.setFont(new Font("Arial",Font.BOLD,14));
        t_lapso_2.setEditable(false);
        t_lapso_2.setHorizontalAlignment(SwingConstants.CENTER);
        add(t_lapso_2);
        
        //LAPSO3
        t_lapso_3 = new JTextField("L.3");
        t_lapso_3.setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        t_lapso_3.setLocation((int)(Ancho*0.8),pos_y);
        t_lapso_3.setBackground(new Color(93, 193, 185));
        t_lapso_3.setForeground(Color.WHITE);
        t_lapso_3.setFont(new Font("Arial",Font.BOLD,14));
        t_lapso_3.setEditable(false);
        t_lapso_3.setHorizontalAlignment(SwingConstants.CENTER);
        add(t_lapso_3);
        
        //DEF
        definitiva = new JTextField("DEF");
        definitiva.setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        definitiva.setLocation((int)(Ancho*0.9),pos_y);
        definitiva.setBackground(new Color(93, 193, 185));
        definitiva.setForeground(Color.WHITE);
        definitiva.setFont(new Font("Arial",Font.BOLD,14));
        definitiva.setEditable(false);
        definitiva.setHorizontalAlignment(SwingConstants.CENTER);
        add(definitiva);
        
        pos_y += (int)(Alto*0.1);
        
        //LENGUA
        lengua = new JTextField("LENGUA Y LITERATURA");
        lengua.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        lengua.setLocation(0,pos_y);
        lengua.setBackground(new Color(93, 193, 185));
        lengua.setForeground(Color.WHITE);
        lengua.setFont(new Font("Arial",Font.PLAIN,14));
        lengua.setEditable(false);
        lengua.setHorizontalAlignment(SwingConstants.LEFT);
        add(lengua);
        
        nota_lengua = new JComboBox[3];
        int x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_lengua[i] = new JComboBox();
            nota_lengua[i].addItem("");
            nota_lengua[i].addItem("A");
            nota_lengua[i].addItem("B");
            nota_lengua[i].addItem("C");
            nota_lengua[i].addItem("D");
            nota_lengua[i].addItem("E");
            nota_lengua[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_lengua[i].setLocation(x,pos_y);
            nota_lengua[i].setBackground(Color.WHITE);
            nota_lengua[i].setForeground(Color.BLACK);
            nota_lengua[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_lengua[i].setEditable(false);
            nota_lengua[i].setFocusable(false);
            add(nota_lengua[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[0] = new JTextField();
        nota_definitiva[0].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[0].setLocation(x,pos_y);
        nota_definitiva[0].setBackground(Color.WHITE);
        nota_definitiva[0].setForeground(Color.BLACK);
        nota_definitiva[0].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[0].setEditable(false);
        nota_definitiva[0].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[0]);
        
        pos_y += (int)(Alto*0.1);
        
        //MATEMATICA
        matematica = new JTextField("MATEMÁTICA");
        matematica.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        matematica.setLocation(0,pos_y);
        matematica.setBackground(new Color(93, 193, 185));
        matematica.setForeground(Color.WHITE);
        matematica.setFont(new Font("Arial",Font.PLAIN,14));
        matematica.setEditable(false);
        matematica.setHorizontalAlignment(SwingConstants.LEFT);
        add(matematica);
        
        nota_matematica = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_matematica[i] = new JComboBox();
            nota_matematica[i].addItem("");
            nota_matematica[i].addItem("A");
            nota_matematica[i].addItem("B");
            nota_matematica[i].addItem("C");
            nota_matematica[i].addItem("D");
            nota_matematica[i].addItem("E");
            nota_matematica[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_matematica[i].setLocation(x,pos_y);
            nota_matematica[i].setBackground(Color.WHITE);
            nota_matematica[i].setForeground(Color.BLACK);
            nota_matematica[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_matematica[i].setEditable(false);
            nota_matematica[i].setFocusable(false);
            add(nota_matematica[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[1] = new JTextField();
        nota_definitiva[1].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[1].setLocation(x,pos_y);
        nota_definitiva[1].setBackground(Color.WHITE);
        nota_definitiva[1].setForeground(Color.BLACK);
        nota_definitiva[1].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[1].setEditable(false);
        nota_definitiva[1].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[1]);
        
        pos_y += (int)(Alto*0.1);
        
        //CIENCIAS DE LA NATURALEZA
        ciencias_naturaleza = new JTextField("CIENCIAS DE LA NATURALEZA");
        ciencias_naturaleza.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        ciencias_naturaleza.setLocation(0,pos_y);
        ciencias_naturaleza.setBackground(new Color(93, 193, 185));
        ciencias_naturaleza.setForeground(Color.WHITE);
        ciencias_naturaleza.setFont(new Font("Arial",Font.PLAIN,14));
        ciencias_naturaleza.setEditable(false);
        ciencias_naturaleza.setHorizontalAlignment(SwingConstants.LEFT);
        add(ciencias_naturaleza);
        
        nota_ciencias_naturaleza = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_ciencias_naturaleza[i] = new JComboBox();
            nota_ciencias_naturaleza[i].addItem("");
            nota_ciencias_naturaleza[i].addItem("A");
            nota_ciencias_naturaleza[i].addItem("B");
            nota_ciencias_naturaleza[i].addItem("C");
            nota_ciencias_naturaleza[i].addItem("D");
            nota_ciencias_naturaleza[i].addItem("E");
            nota_ciencias_naturaleza[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_ciencias_naturaleza[i].setLocation(x,pos_y);
            nota_ciencias_naturaleza[i].setBackground(Color.WHITE);
            nota_ciencias_naturaleza[i].setForeground(Color.BLACK);
            nota_ciencias_naturaleza[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_ciencias_naturaleza[i].setEditable(false);
            nota_ciencias_naturaleza[i].setFocusable(false);
            add(nota_ciencias_naturaleza[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[2] = new JTextField();
        nota_definitiva[2].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[2].setLocation(x,pos_y);
        nota_definitiva[2].setBackground(Color.WHITE);
        nota_definitiva[2].setForeground(Color.BLACK);
        nota_definitiva[2].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[2].setEditable(false);
        nota_definitiva[2].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[2]);
        
        pos_y += (int)(Alto*0.1);
        
        //CIENCIAS SOCIALES
        ciencias_sociales = new JTextField("CIENCIAS SOCIALES");
        ciencias_sociales.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        ciencias_sociales.setLocation(0,pos_y);
        ciencias_sociales.setBackground(new Color(93, 193, 185));
        ciencias_sociales.setForeground(Color.WHITE);
        ciencias_sociales.setFont(new Font("Arial",Font.PLAIN,14));
        ciencias_sociales.setEditable(false);
        ciencias_sociales.setHorizontalAlignment(SwingConstants.LEFT);
        add(ciencias_sociales);
        
        nota_ciencias_sociales = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_ciencias_sociales[i] = new JComboBox();
            nota_ciencias_sociales[i].addItem("");
            nota_ciencias_sociales[i].addItem("A");
            nota_ciencias_sociales[i].addItem("B");
            nota_ciencias_sociales[i].addItem("C");
            nota_ciencias_sociales[i].addItem("D");
            nota_ciencias_sociales[i].addItem("E");
            nota_ciencias_sociales[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_ciencias_sociales[i].setLocation(x,pos_y);
            nota_ciencias_sociales[i].setBackground(Color.WHITE);
            nota_ciencias_sociales[i].setForeground(Color.BLACK);
            nota_ciencias_sociales[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_ciencias_sociales[i].setEditable(false);
            nota_ciencias_sociales[i].setFocusable(false);
            add(nota_ciencias_sociales[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[3] = new JTextField();
        nota_definitiva[3].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[3].setLocation(x,pos_y);
        nota_definitiva[3].setBackground(Color.WHITE);
        nota_definitiva[3].setForeground(Color.BLACK);
        nota_definitiva[3].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[3].setEditable(false);
        nota_definitiva[3].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[3]);
        
        pos_y += (int)(Alto*0.1);
        
        //EDUCACION ESTETICA
        educacion_estetica = new JTextField("EDUCACION ESTÉTICA");
        educacion_estetica.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        educacion_estetica.setLocation(0,pos_y);
        educacion_estetica.setBackground(new Color(93, 193, 185));
        educacion_estetica.setForeground(Color.WHITE);
        educacion_estetica.setFont(new Font("Arial",Font.PLAIN,14));
        educacion_estetica.setEditable(false);
        educacion_estetica.setHorizontalAlignment(SwingConstants.LEFT);
        add(educacion_estetica);
        
        nota_educacion_estetica = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_educacion_estetica[i] = new JComboBox();
            nota_educacion_estetica[i].addItem("");
            nota_educacion_estetica[i].addItem("A");
            nota_educacion_estetica[i].addItem("B");
            nota_educacion_estetica[i].addItem("C");
            nota_educacion_estetica[i].addItem("D");
            nota_educacion_estetica[i].addItem("E");
            nota_educacion_estetica[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_educacion_estetica[i].setLocation(x,pos_y);
            nota_educacion_estetica[i].setBackground(Color.WHITE);
            nota_educacion_estetica[i].setForeground(Color.BLACK);
            nota_educacion_estetica[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_educacion_estetica[i].setEditable(false);
            nota_educacion_estetica[i].setFocusable(false);
            add(nota_educacion_estetica[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[4] = new JTextField();
        nota_definitiva[4].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[4].setLocation(x,pos_y);
        nota_definitiva[4].setBackground(Color.WHITE);
        nota_definitiva[4].setForeground(Color.BLACK);
        nota_definitiva[4].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[4].setEditable(false);
        nota_definitiva[4].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[4]);
        
        pos_y += (int)(Alto*0.1);
        
        //EDUCACION FÍSICA
        educacion_fisica = new JTextField("EDUCACION FÍSICA");
        educacion_fisica.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        educacion_fisica.setLocation(0,pos_y);
        educacion_fisica.setBackground(new Color(93, 193, 185));
        educacion_fisica.setForeground(Color.WHITE);
        educacion_fisica.setFont(new Font("Arial",Font.PLAIN,14));
        educacion_fisica.setEditable(false);
        educacion_fisica.setHorizontalAlignment(SwingConstants.LEFT);
        add(educacion_fisica);
        
        nota_educacion_fisica = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_educacion_fisica[i] = new JComboBox();
            nota_educacion_fisica[i].addItem("");
            nota_educacion_fisica[i].addItem("A");
            nota_educacion_fisica[i].addItem("B");
            nota_educacion_fisica[i].addItem("C");
            nota_educacion_fisica[i].addItem("D");
            nota_educacion_fisica[i].addItem("E");
            nota_educacion_fisica[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_educacion_fisica[i].setLocation(x,pos_y);
            nota_educacion_fisica[i].setBackground(Color.WHITE);
            nota_educacion_fisica[i].setForeground(Color.BLACK);
            nota_educacion_fisica[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_educacion_fisica[i].setEditable(false);
            nota_educacion_fisica[i].setFocusable(false);
            add(nota_educacion_fisica[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[5] = new JTextField();
        nota_definitiva[5].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[5].setLocation(x,pos_y);
        nota_definitiva[5].setBackground(Color.WHITE);
        nota_definitiva[5].setForeground(Color.BLACK);
        nota_definitiva[5].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[5].setEditable(false);
        nota_definitiva[5].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[5]);
        
        pos_y += (int)(Alto*0.1);
        
        //INGLÉS
        ingles = new JTextField("INGLÉS");
        ingles.setSize((int)(Ancho*0.6),(int)(Alto*0.1));
        ingles.setLocation(0,pos_y);
        ingles.setBackground(new Color(93, 193, 185));
        ingles.setForeground(Color.WHITE);
        ingles.setFont(new Font("Arial",Font.PLAIN,14));
        ingles.setEditable(false);
        ingles.setHorizontalAlignment(SwingConstants.LEFT);
        add(ingles);
        
        nota_ingles = new JComboBox[3];
        x = (int)(Ancho*0.6);
        for(int i=0;i<3;i++){
            nota_ingles[i] = new JComboBox();
            nota_ingles[i].addItem("");
            nota_ingles[i].addItem("A");
            nota_ingles[i].addItem("B");
            nota_ingles[i].addItem("C");
            nota_ingles[i].addItem("D");
            nota_ingles[i].addItem("E");
            nota_ingles[i].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
            nota_ingles[i].setLocation(x,pos_y);
            nota_ingles[i].setBackground(Color.WHITE);
            nota_ingles[i].setForeground(Color.BLACK);
            nota_ingles[i].setFont(new Font("Arial",Font.PLAIN,14));
            nota_ingles[i].setEditable(false);
            nota_ingles[i].setFocusable(false);
            add(nota_ingles[i]);
            x += (int)(Ancho*0.101);
        }
        nota_definitiva[6] = new JTextField();
        nota_definitiva[6].setSize((int)(Ancho*0.1),(int)(Alto*0.1));
        nota_definitiva[6].setLocation(x,pos_y);
        nota_definitiva[6].setBackground(Color.WHITE);
        nota_definitiva[6].setForeground(Color.BLACK);
        nota_definitiva[6].setFont(new Font("Arial",Font.PLAIN,14));
        nota_definitiva[6].setEditable(false);
        nota_definitiva[6].setHorizontalAlignment(SwingConstants.CENTER);
        add(nota_definitiva[6]);
    }
    
    public void cargarNotas(String ID){
        
    }
}
