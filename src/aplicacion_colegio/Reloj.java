package aplicacion_colegio;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Reloj extends Thread{
    public Menu_Home Header;
    public String horas, minutos, segundos, ampm;
    public Calendar calendario;
    
    public Reloj(Menu_Home Header){
        this.Header = Header;
        calendario = new GregorianCalendar();
    }
    
    public void calcula () {        
        Calendar calendario = new GregorianCalendar();
        Date fechaHoraActual = new Date();


        calendario.setTime(fechaHoraActual);
        ampm = calendario.get(Calendar.AM_PM)==Calendar.AM?"AM":"PM";

        if(ampm.equals("PM")){
            int h = calendario.get(Calendar.HOUR_OF_DAY)-12;
            horas = h>9?""+h:"0"+h;
        }else{
            horas = calendario.get(Calendar.HOUR_OF_DAY)>9?""+calendario.get(Calendar.HOUR_OF_DAY):"0"+calendario.get(Calendar.HOUR_OF_DAY);            
        }
        if(horas.equalsIgnoreCase("0") || horas.equalsIgnoreCase("00"))
            horas = "12";
        minutos = calendario.get(Calendar.MINUTE)>9?""+calendario.get(Calendar.MINUTE):"0"+calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND)>9?""+calendario.get(Calendar.SECOND):"0"+calendario.get(Calendar.SECOND); 
    }
    
    public void run(){
        try {
            while(true){
                calcula();
                Header.Fecha_Hora.setText(
                    Header.dia+" de "+Header.s_mes+" de "+Header.año
                    +" - Hora: "+horas+":"+minutos+":"+segundos+" "+ampm+"      "
                );
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Reloj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
