package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Listar_Estudiante extends JPanel{
    public Ventana_Principal Ventana;
    public Menu_Home Menu;
    public JTextField nombre, apellido, identificador, grado, acciones;
    public JTextField [] t_nombre, t_apellido, t_identificador, t_cargo;
    public int Ancho, Alto;
    public JPanel panel;
    public JScrollPane scroll;
    public JButton [] Info, pagos, notas, eliminar;
    
    public Listar_Estudiante(Menu_Home Menu,Ventana_Principal Ventana,int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        this.Menu = Menu;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        setBackground(Color.WHITE);
        
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setLocation(0,(int)(Alto*0.07));
        scroll.setSize(Ancho,(int)(Alto*0.9));
        panel.setLocation(0,0);
        panel.setSize((int)(Ancho*0.95),(int)(Alto*0.95));
        scroll.setViewportView(panel);
        scroll.setVisible(true);
        add(scroll);
        
        if(Ventana.Login.Cargo_User.equalsIgnoreCase("PROFESOR")){
            cargarListado(Ventana.obtenerListaEstudiante(Ventana.obtenerGradoACargo(Ventana.Login.Cedula_User)), false, true);
        }else{
            cargarListado(Ventana.obtenerListaEstudiante(), false, false);
        }
    }
    
    public void cargarListado(List<String> estudiantes, boolean inhabilitado, boolean profesor){
       panel.removeAll();
       
       t_identificador = new JTextField[estudiantes.size()/4];
       t_nombre = new JTextField[estudiantes.size()/4];
       t_apellido = new JTextField[estudiantes.size()/4];
       t_cargo = new JTextField[estudiantes.size()/4];
       Info = new JButton[estudiantes.size()/4];
       notas = new JButton[estudiantes.size()/4];
       pagos = new JButton[estudiantes.size()/4];
       eliminar = new JButton[estudiantes.size()/4];
              
       int pos_x = 0, pos_y = 0;
       
       identificador = new JTextField("IDENTIFICADOR");
       identificador.setLocation(pos_x,pos_y);
       identificador.setHorizontalAlignment(SwingConstants.CENTER);
       identificador.setSize((int)(Ancho/6),(int)(Alto*0.07));
       identificador.setBackground(new Color(93, 193, 185));
       identificador.setForeground(Color.WHITE);
       identificador.setFont(new Font("Arial",Font.BOLD,16));
       identificador.setEditable(false);
       add(identificador);
       
       pos_x += (int)(Ancho/6);
       
       apellido = new JTextField("APELLIDOS");
       apellido.setLocation(pos_x,pos_y);
       apellido.setHorizontalAlignment(SwingConstants.CENTER);
       apellido.setSize((int)(Ancho/6),(int)(Alto*0.07));
       apellido.setBackground(new Color(93, 193, 185));
       apellido.setForeground(Color.WHITE);
       apellido.setFont(new Font("Arial",Font.BOLD,16));
       apellido.setEditable(false);
       add(apellido);
       
       pos_x += (int)(Ancho/6);
       
       nombre = new JTextField("NOMBRES");
       nombre.setLocation(pos_x,pos_y);
       nombre.setHorizontalAlignment(SwingConstants.CENTER);
       nombre.setSize((int)(Ancho/6),(int)(Alto*0.07));
       nombre.setBackground(new Color(93, 193, 185));
       nombre.setForeground(Color.WHITE);
       nombre.setFont(new Font("Arial",Font.BOLD,16));
       nombre.setEditable(false);
       add(nombre);
       
       pos_x += (int)(Ancho/6);
       
       grado = new JTextField("GRADO ESCOLAR");
       grado.setLocation(pos_x,pos_y);
       grado.setHorizontalAlignment(SwingConstants.CENTER);
       grado.setSize((int)(Ancho/6),(int)(Alto*0.07));
       grado.setBackground(new Color(93, 193, 185));
       grado.setForeground(Color.WHITE);
       grado.setFont(new Font("Arial",Font.BOLD,16));
       grado.setEditable(false);
       add(grado);
       
       pos_x += (int)(Ancho/6);
       
       acciones = new JTextField("ACCIONES");
       acciones.setLocation(pos_x,pos_y);
       acciones.setHorizontalAlignment(SwingConstants.CENTER);
       acciones.setSize((int)(Ancho/6)*2,(int)(Alto*0.07));
       acciones.setBackground(new Color(93, 193, 185));
       acciones.setForeground(Color.WHITE);
       acciones.setFont(new Font("Arial",Font.BOLD,16));
       acciones.setEditable(false);
       add(acciones);
              
       pos_x = 0;
       
       int aux = 0;
        
       ImageIcon icon_vermas = Ventana.cargarImagen("b_vermas.png",(int)(Ancho*0.04),(int)(Alto*0.05));
       ImageIcon icon_vermas_hover = Ventana.cargarImagen("b_vermas_hover.png",(int)(Ancho*0.04),(int)(Alto*0.05));
       
       ImageIcon icon_notas = Ventana.cargarImagen("b_notas.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       ImageIcon icon_notas_hover = Ventana.cargarImagen("b_notas_hover.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       
       ImageIcon icon_pagos = Ventana.cargarImagen("b_pagos_list.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       icon_pagos.setDescription("Pagos");
       ImageIcon icon_pagos_hover = Ventana.cargarImagen("b_pagos_list_hover.png",(int)(Ancho*0.03),(int)(Alto*0.05));       
       icon_pagos_hover.setDescription("Pagos");
       
       ImageIcon icon_delete = Ventana.cargarImagen("b_inhabilitar.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       ImageIcon icon_delete_hover = Ventana.cargarImagen("b_inhabilitar_hover.png",(int)(Ancho*0.03),(int)(Alto*0.05));
       
        for(int i=0;i<estudiantes.size()/4;i++){
            int x = i;
            t_identificador[i] = new JTextField(estudiantes.get(aux++));
            t_identificador[i].setLocation(pos_x,pos_y);
            t_identificador[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_identificador[i].setSize((int)(Ancho/6),(int)(Alto*0.07));
            t_identificador[i].setBackground(Color.white);
            t_identificador[i].setEditable(false);
            panel.add(t_identificador[i]);
            
            pos_x += (int)(Ancho/6);
            
            t_apellido[i] = new JTextField(estudiantes.get(aux++));
            t_apellido[i].setLocation(pos_x,pos_y);
            t_apellido[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_apellido[i].setSize((int)(Ancho/6),(int)(Alto*0.07));
            t_apellido[i].setBackground(Color.white);
            t_apellido[i].setEditable(false);
            panel.add(t_apellido[i]);
            
            pos_x += (int)(Ancho/6);
            
            t_nombre[i] = new JTextField(estudiantes.get(aux++));
            t_nombre[i].setLocation(pos_x,pos_y);
            t_nombre[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_nombre[i].setSize((int)(Ancho/6),(int)(Alto*0.07));
            t_nombre[i].setBackground(Color.white);
            t_nombre[i].setEditable(false);
            panel.add(t_nombre[i]);
            
            pos_x += (int)(Ancho/6);
            
            int gradoid = Integer.parseInt(estudiantes.get(aux++));
            String grado = Ventana.getGradoStr(gradoid);
            t_cargo[i] = new JTextField(grado);
            t_cargo[i].setLocation(pos_x,pos_y);
            t_cargo[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_cargo[i].setSize((int)(Ancho/6),(int)(Alto*0.07));
            t_cargo[i].setBackground(Color.white);
            t_cargo[i].setEditable(false);
            panel.add(t_cargo[i]);
            
            pos_x += (int)(Ancho/6)+3;
                        
            Info[i] = new JButton(icon_vermas){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.RED);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(new RoundedCornerBorder());
                }
            };
            Info[i].setRolloverIcon(icon_vermas_hover);
            Info[i].setLocation(pos_x,pos_y);
            Info[i].setSize((int)(Ancho*2/6/4)-10,(int)(Alto*0.07));
            Info[i].setFocusable(false);
            Info[i].setOpaque(false);
            Info[i].setContentAreaFilled(false);
            Info[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            Info[i].setFont(new Font("Arial",Font.PLAIN,14));
            Info[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    Info[x].setFont(new Font("Arial",Font.BOLD,16));
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    Info[x].setFont(new Font("Arial",Font.PLAIN,14));
                }
            });
            Info[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    JOptionPane.showMessageDialog(null, new Informacion_Estudiante(Ventana,t_identificador[x].getText()),"Información",JOptionPane.PLAIN_MESSAGE);
                }
            });
            panel.add(Info[i]);
            
            pos_x += (int)(Ancho*2/6/4)-10;
            
            String ID = t_identificador[i].getText();
            
            notas[i] = new JButton(icon_notas){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.RED);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(new RoundedCornerBorder());
                }
            };
            notas[i].setRolloverIcon(icon_notas_hover);
            notas[i].setLocation(pos_x+2,pos_y);
            notas[i].setSize((int)(Ancho*2/6/4)-10,(int)(Alto*0.07));
            notas[i].setFocusable(false);
            notas[i].setOpaque(false);
            notas[i].setContentAreaFilled(false);
            notas[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            notas[i].setFont(new Font("Arial",Font.PLAIN,14));
            notas[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    notas[x].setFont(new Font("Arial",Font.BOLD,16));
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    notas[x].setFont(new Font("Arial",Font.PLAIN,14));
                }
            });
            notas[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    Menu.ocultarPaneles();
                    Menu.titulo_notas.setVisible(true);
                    Menu.Gestionar_Notas.setVisible(true);
                    Menu.Gestionar_Notas.notasEstudiante(ID);
                    if(inhabilitado){
                        Menu.Gestionar_Notas.notasEstudianteInhabilitado();
                    }
                }
            });
            panel.add(notas[i]);
            
            if(gradoid > 7){
                notas[i].setEnabled(false);
            }
            
            if(!profesor){
                pos_x += (int)(Ancho*2/6/4)-10;
            
                pagos[i] = new JButton(icon_pagos){
                    protected void paintComponent(Graphics g) {
                        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                            Graphics2D g2 = (Graphics2D) g.create();
                            g2.setPaint(getBackground());
                            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                                0, 0, getWidth() - 1, getHeight() - 1));
                            g2.dispose();
                            g2.setPaint(Color.RED);
                        }
                        super.paintComponent(g);
                    }
                    @Override public void updateUI() {
                      super.updateUI();
                      setOpaque(false);
                      setBorder(new RoundedCornerBorder());
                    }
                }; 
                pagos[i].setRolloverIcon(icon_pagos_hover);
                pagos[i].setLocation(pos_x+4,pos_y);
                pagos[i].setSize((int)(Ancho*2/6/4)-10,(int)(Alto*0.07));
                pagos[i].setFocusable(false);
                pagos[i].setOpaque(false);
                pagos[i].setContentAreaFilled(false);
                pagos[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
                pagos[i].setFont(new Font("Arial",Font.PLAIN,14));
                pagos[i].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent me) {
                    }

                    @Override
                    public void mousePressed(MouseEvent me) {
                    }

                    @Override
                    public void mouseReleased(MouseEvent me) {
                    }

                    @Override
                    public void mouseEntered(MouseEvent me) {
                        pagos[x].setFont(new Font("Arial",Font.BOLD,16));
                    }

                    @Override
                    public void mouseExited(MouseEvent me) {
                        pagos[x].setFont(new Font("Arial",Font.PLAIN,14));
                    }
                });
                pagos[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        Menu.ocultarPaneles();
                        Menu.añadir_pago.setFont(new Font("Arial",Font.BOLD,16));
                        Menu.listar_pagos.setFont(new Font("Arial",Font.BOLD,24));
                        Menu.titulo_pagos.setVisible(true);
                        Menu.añadir_pago.setVisible(true);
                        Menu.listar_pagos.setVisible(true);
                        Menu.Listar_Pagos.cargarListado(Menu.Ventana.obtenerListaPagos(ID));
                        Menu.Registro_Pago.actualizarListaEstudianteID(ID);
                        String deuda = Menu.Ventana.obtenerDeuda(ID);
                        Menu.Registro_Pago.monto.setText(deuda);
                        Menu.Registro_Pago.deuda.setText(deuda);
                        Menu.Registro_Pago.ph_monto.setVisible(false);
                        Menu.Listar_Pagos.setVisible(true);
                        if(inhabilitado){
                            Menu.añadir_pago.setEnabled(false);
                        }else{
                            Menu.añadir_pago.setEnabled(true);
                        }

                    }
                });
                panel.add(pagos[i]);

                pos_x += (int)(Ancho*2/6/4)-10;

                eliminar[i] = new JButton(icon_delete){
                    protected void paintComponent(Graphics g) {
                        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                            Graphics2D g2 = (Graphics2D) g.create();
                            g2.setPaint(getBackground());
                            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                                0, 0, getWidth() - 1, getHeight() - 1));
                            g2.dispose();
                            g2.setPaint(Color.RED);
                        }
                        super.paintComponent(g);
                    }
                    @Override public void updateUI() {
                      super.updateUI();
                      setOpaque(false);
                      setBorder(new RoundedCornerBorder());
                    }
                };    
                eliminar[i].setRolloverIcon(icon_delete_hover);
                eliminar[i].setLocation(pos_x+4,pos_y);
                eliminar[i].setSize((int)(Ancho*2/6/4)-10,(int)(Alto*0.07));
                eliminar[i].setFocusable(false);
                eliminar[i].setOpaque(false);
                eliminar[i].setContentAreaFilled(false);
                eliminar[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
                eliminar[i].setFont(new Font("Arial",Font.PLAIN,14));
                if(!inhabilitado){
                    eliminar[i].addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(MouseEvent me) {
                        }

                        @Override
                        public void mousePressed(MouseEvent me) {
                        }

                        @Override
                        public void mouseReleased(MouseEvent me) {
                        }

                        @Override
                        public void mouseEntered(MouseEvent me) {
                            eliminar[x].setFont(new Font("Arial",Font.BOLD,16));
                        }

                        @Override
                        public void mouseExited(MouseEvent me) {
                            eliminar[x].setFont(new Font("Arial",Font.PLAIN,14));
                        }
                    });
                    eliminar[i].addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent ae) {
                            int dialogResult = JOptionPane.showConfirmDialog(null, "Seguro que desea inhabilitar?", "Inhabilitar Estudiante", JOptionPane.YES_NO_OPTION);
                            if(dialogResult == JOptionPane.YES_OPTION){
                                Menu.Ventana.eliminarEstudiante(ID);
                                cargarListado(Menu.Ventana.obtenerListaEstudiante(), false, profesor);
                            }                       
                        }
                    });
                }else{
                    eliminar[i].setEnabled(false);
                }
                panel.add(eliminar[i]);
            }            
            
            pos_x = 0;
            pos_y += (int)(Alto*0.07);
        }
        
        panel.setSize((int)(Ancho*0.92),(int)((int)(Alto*0.07*estudiantes.size()/4)));
        panel.setPreferredSize(new Dimension((int)(Ancho*0.92),(int)((int)(Alto*0.07*estudiantes.size()/4))));
        panel.repaint();
   }
}
