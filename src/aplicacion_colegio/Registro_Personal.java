package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Registro_Personal extends JPanel{
    public JLabel nombreLabel, apellidoLabel, cedulaLabel, dir_trabajoLabel, dir_habitacionLabel,
            tlf_trabajoLabel, tlf_habitacionLabel, tlf_celularLabel, correoLabel, contraseñaLabel, cargoLabel;
    public JTextField nombre, apellido, cedula, dir_trabajo, dir_habitacion, tlf_trabajo,
            tlf_habitacion, tlf_celular, correo;
    public JPasswordField contraseña;
    public JLabel h_pass;
    public JComboBox cargo;
    public Ventana_Principal Ventana;
    public int Ancho, Alto, LabelFuenteSize;
    public RoundedCornerBorder Borde, Borde_R;
    public JButton registrar;
    public boolean bnombre,bapellido,bcedula,bdir_habitacion,btlf_habitacion,bcorreo,bcontraseña;
    public String mensaje, año_escolar;
    public String[] escolar = { "PRIMER GRADO", "SEGUNDO GRADO", "TERCER GRADO", 
        "CUARTO GRADO", "QUINTO GRADO", "SEXTO GRADO" };
    public String EMAIL_PATTERN = 
    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public JComboBox cedula_combo;
    public int maximo_c;
    
    public Registro_Personal(Ventana_Principal Ventana, int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        setSize(Ancho,Alto);
        setLayout(null);
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(Ancho,Alto));
        
        int pos_y = 0;
        maximo_c = 0;
        Borde = new RoundedCornerBorder();
        Borde_R = new RoundedCornerBorder();
        
//        mensaje = "";
        año_escolar = "NINGUNO";
        
        bnombre = false;
        bapellido = false;
        bcedula = false;
        bdir_habitacion = false;
        btlf_habitacion = false;
        bcorreo = false;
        bcontraseña = false;
        
        LabelFuenteSize = (int) (Ancho*0.018);
        
        int alSize = (int)(Alto*0.08);
        //AÑADIR MENSAJES DE ERROR
        
        nombreLabel = new JLabel("Nombres:");
        nombreLabel.setSize((int)(Ancho*0.1),alSize);
        nombreLabel.setLocation((int)(Ancho*0.055), pos_y);
        nombreLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(nombreLabel);
        
        nombre = new JTextField("Ingrese Nombres..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        nombre.setSize((int)(Ancho*0.35),alSize);
        nombre.setLocation((int)(Ancho*0.14),pos_y);
        nombre.setBackground(Color.WHITE);
        nombre.setForeground(Color.GRAY);
        nombre.setFont(new Font("Arial",Font.ITALIC,14));
        nombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(nombre.getText().equalsIgnoreCase("Ingrese Nombres...")){
                    nombre.setText("");
                    nombre.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(nombre.getText().equalsIgnoreCase("")){
                    nombre.setText("Ingrese Nombres...");
                    nombre.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        nombre.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(nombre.getText().length()==65) 
                        ke.consume();
                
                if(nombre.getText().length()>2)
                    bnombre = true;
                else
                    bnombre = false;
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(nombre.getText().length()==65) 
                        ke.consume();
                if(nombre.getText().length()>2)
                    bnombre = true;
                else
                    bnombre = false;
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(nombre.getText().length()==65) 
                        ke.consume();
            }
        });
        add(nombre);
        
        apellidoLabel = new JLabel("Apellidos:");
        apellidoLabel.setSize((int)(Ancho*0.1),alSize);
        apellidoLabel.setLocation((int)(Ancho*0.51), pos_y);
        apellidoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(apellidoLabel);
        
        apellido = new JTextField("Ingrese Apellidos..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        apellido.setSize((int)(Ancho*0.35),alSize);
        apellido.setLocation((int)(Ancho*0.6),pos_y);
        apellido.setBackground(Color.WHITE);
        apellido.setForeground(Color.GRAY);
        apellido.setFont(new Font("Arial",Font.ITALIC,14));
        apellido.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(apellido.getText().length()>2)
                    bapellido = true;
                else
                    bapellido = false;
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(apellido.getText().length()==65) 
                        ke.consume();
                if(apellido.getText().length()>2)
                    bapellido = true;
                else
                    bapellido = false;
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        apellido.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(apellido.getText().equalsIgnoreCase("Ingrese Apellidos...")){
                    apellido.setText("");
                    apellido.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(apellido.getText().equalsIgnoreCase("")){
                    apellido.setText("Ingrese Apellidos...");
                    apellido.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(apellido);
        
        pos_y += alSize + 7;
        
        cedulaLabel = new JLabel("Doc. Identidad:");
        cedulaLabel.setSize((int)(Ancho*0.3),alSize);
        cedulaLabel.setLocation((int)(Ancho*0.055), pos_y);
        cedulaLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(cedulaLabel);
        
        cedula_combo = new JComboBox();
        
        cedula_combo.addItem("V");
        cedula_combo.addItem("E");
        cedula_combo.setSelectedIndex(0);
        cedula_combo.setSize((int)(Ancho*0.04),alSize);            
        cedula_combo.setBackground(Color.WHITE);
        cedula_combo.setForeground(Color.GRAY);
        cedula_combo.setFocusable(false);
        cedula_combo.setCursor(new Cursor(Cursor.HAND_CURSOR));
        cedula_combo.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        cedula_combo.setFont(new Font("Arial",Font.PLAIN,14));
        cedula_combo.setEnabled(true);
        cedula_combo.setLocation((int)(Ancho*0.2),alSize+5);
        cedula_combo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cedula_combo.getSelectedItem().equals("V")){
                    if(cedula.getText().length() >= maximo_c-1){
                        cedula.setText(cedula.getText().substring(0, 8));
                    }
                    maximo_c = 9;
                }
                else if(cedula_combo.getSelectedItem().equals("E")){
                    maximo_c = 10;
                }
            }
        });
        add(cedula_combo);
        maximo_c = 9;

        
        cedula = new JTextField("Documento de Identidad..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };        
        cedula.setSize((int)(Ancho*0.25),alSize);
        cedula.setLocation((int)(Ancho*0.25),pos_y);
        cedula.setBackground(Color.WHITE);
        cedula.setForeground(Color.GRAY);
        cedula.setFont(new Font("Arial",Font.ITALIC,14));
        cedula.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(cedula.getText().length() == maximo_c-1) 
                        e.consume();
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(cedula.getText().length()== maximo_c-1) 
                        ke.consume();
                if(cedula.getText().length()>=7)
                    bcedula = true;
                else
                    bcedula = false;
            }
        });
        cedula.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(cedula.getText().equalsIgnoreCase("Documento de Identidad...")){
                    cedula.setText("");
                    cedula.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(cedula.getText().equalsIgnoreCase("")){
                    cedula.setText("Documento de Identidad...");
                    cedula.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(cedula);
        
        cargoLabel = new JLabel("Cargo:");
        cargoLabel.setSize((int)(Ancho*0.3), alSize);
        cargoLabel.setLocation((int)(Ancho*0.51), pos_y);
        cargoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(cargoLabel);
        
        cargo = new JComboBox(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        cargo.addItem("PROFESOR");
        cargo.addItem("ASISTENTE");
        cargo.addItem("ADMINISTRADOR");
        cargo.setSelectedItem(0);
        cargo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                System.out.println("CARGO: "+cargo.getSelectedItem().toString());
            }
        });
        cargo.setSize((int)(Ancho*0.33),alSize);
        cargo.setLocation((int)(Ancho*0.61),pos_y);
        cargo.setBackground(Color.WHITE);
        cargo.setForeground(Color.GRAY);
        cargo.setInheritsPopupMenu(true);
        cargo.setFocusable(false);
        cargo.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        cargo.setFont(new Font("Arial",Font.ITALIC,14));
        add(cargo);
        
        pos_y += alSize + 7;
        
        dir_trabajoLabel = new JLabel("Dir. Trabajo:");
        dir_trabajoLabel.setSize((int)(Ancho*0.3),alSize);
        dir_trabajoLabel.setLocation((int)(Ancho*0.055), pos_y);
        dir_trabajoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(dir_trabajoLabel);
        
        dir_trabajo = new JTextField("Dirección de Trabajo..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        dir_trabajo.setSize((int)(Ancho*0.75),(int)(Alto/12));
        dir_trabajo.setLocation((int)(Ancho*0.2),pos_y);
        dir_trabajo.setBackground(Color.WHITE);
        dir_trabajo.setForeground(Color.GRAY);
        dir_trabajo.setFont(new Font("Arial",Font.ITALIC,14));
        dir_trabajo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(dir_trabajo.getText().equalsIgnoreCase("Dirección de Trabajo...")){
                    dir_trabajo.setText("");
                    dir_trabajo.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(dir_trabajo.getText().equalsIgnoreCase("")){
                    dir_trabajo.setText("Dirección de Trabajo...");
                    dir_trabajo.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(dir_trabajo);
        setMaximo(dir_trabajo,65);
        
        pos_y += alSize + 7;
        
        dir_habitacionLabel = new JLabel("Dir. Habitación:");
        dir_habitacionLabel.setSize((int)(Ancho*0.3),(int)(Alto/12));
        dir_habitacionLabel.setLocation((int)(Ancho*0.055), pos_y);
        dir_habitacionLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(dir_habitacionLabel);
                
        dir_habitacion = new JTextField("Dirección de Habitación..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        dir_habitacion.setSize((int)(Ancho*0.75),(int)(Alto/12));
        dir_habitacion.setLocation((int)(Ancho*0.2),pos_y);
        dir_habitacion.setBackground(Color.WHITE);
        dir_habitacion.setForeground(Color.GRAY);
        dir_habitacion.setFont(new Font("Arial",Font.ITALIC,14));
        dir_habitacion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(dir_habitacion.getText().length()>4)
                    bdir_habitacion = true;
                else
                    bdir_habitacion = false;
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(dir_habitacion.getText().length()==65) 
                        ke.consume();
                if(dir_habitacion.getText().length()>4)
                    bdir_habitacion = true;
                else
                    bdir_habitacion = false;
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(dir_habitacion.getText().length()>4)
                    bdir_habitacion = true;
                else
                    bdir_habitacion = false;
            }
        });
        dir_habitacion.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(dir_habitacion.getText().equalsIgnoreCase("Dirección de Habitación...")){
                    dir_habitacion.setText("");
                    dir_habitacion.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(dir_habitacion.getText().equalsIgnoreCase("")){
                    dir_habitacion.setText("Dirección de Habitación...");
                    dir_habitacion.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(dir_habitacion);
        
        pos_y += alSize + 7;
        
        tlf_trabajoLabel = new JLabel("Telf. Trabajo:");
        tlf_trabajoLabel.setSize((int)(Ancho*0.3),alSize);
        tlf_trabajoLabel.setLocation((int)(Ancho*0.055), pos_y);
        tlf_trabajoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(tlf_trabajoLabel);
        
        tlf_trabajo = new JTextField("Teléfono de Trabajo..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        tlf_trabajo.setSize((int)(Ancho*0.15),alSize);
        tlf_trabajo.setLocation((int)(Ancho*0.2),pos_y);
        tlf_trabajo.setBackground(Color.WHITE);
        tlf_trabajo.setForeground(Color.GRAY);
        tlf_trabajo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(tlf_trabajo.getText().length()==11) 
                        e.consume();
            }
        });
        tlf_trabajo.setFont(new Font("Arial",Font.ITALIC,14));
        tlf_trabajo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(tlf_trabajo.getText().equalsIgnoreCase("Teléfono de Trabajo...")){
                    tlf_trabajo.setText("");
                    tlf_trabajo.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(tlf_trabajo.getText().equalsIgnoreCase("")){
                    tlf_trabajo.setText("Teléfono de Trabajo...");
                    tlf_trabajo.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(tlf_trabajo);
              
        tlf_habitacionLabel = new JLabel("Telf. Habitación:");
        tlf_habitacionLabel.setSize((int)(Ancho*0.3), alSize);
        tlf_habitacionLabel.setLocation((int)(Ancho*0.355), pos_y);
        tlf_habitacionLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(tlf_habitacionLabel);
                
        tlf_habitacion = new JTextField("Teléfono de Habitación..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        tlf_habitacion.setSize((int)(Ancho*0.15),alSize);
        tlf_habitacion.setLocation((int)(Ancho*0.51),pos_y);
        tlf_habitacion.setBackground(Color.WHITE);
        tlf_habitacion.setForeground(Color.GRAY);
        tlf_habitacion.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(tlf_habitacion.getText().length()==11) 
                        e.consume();
                
                if(tlf_habitacion.getText().length()>9)
                    btlf_habitacion = true;
                else
                    btlf_habitacion = false;
            }
        });
        tlf_habitacion.setFont(new Font("Arial",Font.ITALIC,14));
        tlf_habitacion.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(tlf_habitacion.getText().equalsIgnoreCase("Teléfono de Habitación...")){
                    tlf_habitacion.setText("");
                    tlf_habitacion.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(tlf_habitacion.getText().equalsIgnoreCase("")){
                    tlf_habitacion.setText("Teléfono de Habitación...");
                    tlf_habitacion.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(tlf_habitacion);
        
        tlf_celularLabel = new JLabel("Telf. Celular:");
        tlf_celularLabel.setSize((int)(Ancho*0.3),alSize);
        tlf_celularLabel.setLocation((int)(Ancho*0.67), pos_y);
        tlf_celularLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(tlf_celularLabel);
                
        tlf_celular = new JTextField("Teléfono de Celular..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        tlf_celular.setSize((int)(Ancho*0.15),alSize);
        tlf_celular.setLocation((int)(Ancho*0.8),pos_y);
        tlf_celular.setBackground(Color.WHITE);
        tlf_celular.setForeground(Color.GRAY);
        tlf_celular.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(tlf_celular.getText().length()==11) 
                        e.consume();
            }
            
            @Override
            public void keyPressed(KeyEvent ke) {
                
            }
        });
        tlf_celular.setFont(new Font("Arial",Font.ITALIC,14));
        tlf_celular.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(tlf_celular.getText().equalsIgnoreCase("Teléfono de Celular...")){
                    tlf_celular.setText("");
                    tlf_celular.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(tlf_celular.getText().equalsIgnoreCase("")){
                    tlf_celular.setText("Teléfono de Celular...");
                    tlf_celular.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(tlf_celular);
        
        pos_y += alSize + 7;
        
        correoLabel = new JLabel("Correo Electronico:");
        correoLabel.setSize((int)(Ancho*0.3),alSize);
        correoLabel.setLocation((int)(Ancho*0.05), pos_y);
        correoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize-1));
        add(correoLabel);
                
        correo = new JTextField("Correo Electrónico..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        correo.setSize((int)(Ancho*0.3),alSize);
        correo.setLocation((int)(Ancho*0.2),pos_y);
        correo.setBackground(Color.WHITE);
        correo.setForeground(Color.GRAY);
        correo.setFont(new Font("Arial",Font.ITALIC,14));
        correo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(!correo.getText().matches(EMAIL_PATTERN)){
                    correo.setForeground(Color.RED);
                    bcorreo = false;
                }else{
                    correo.setForeground(Color.GREEN);
                    bcorreo = true;
                }
                if(correo.getText().equalsIgnoreCase("Correo Electrónico...")){
                    correo.setForeground(Color.GRAY);
                    bcorreo = false;
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        correo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(correo.getText().equalsIgnoreCase("Correo Electrónico...")){
                    correo.setText("");
                    correo.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(correo.getText().equalsIgnoreCase("")){
                    correo.setText("Correo Electrónico...");
                    correo.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(correo);
           
        contraseñaLabel = new JLabel("Contraseña:");
        contraseñaLabel.setSize((int)(Ancho*0.3),alSize);
        contraseñaLabel.setLocation((int)(Ancho*0.51), pos_y);
        contraseñaLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(contraseñaLabel);
        
        h_pass = new JLabel("  Contraseña...");
        h_pass.setSize((int)(Ancho*0.34),alSize);
        h_pass.setLocation((int)(Ancho*0.61),pos_y);
        h_pass.setForeground(Color.GRAY);
        h_pass.setFont(new Font("Arial",Font.ITALIC,14));
        add(h_pass);
                
        contraseña = new JPasswordField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde);
            }
        };
        contraseña.setSize((int)(Ancho*0.34),alSize);
        contraseña.setLocation((int)(Ancho*0.61),pos_y);
        contraseña.setBackground(Color.WHITE);
        contraseña.setForeground(Color.GRAY);
        contraseña.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(contraseña.getText().length()>0)
                    h_pass.setVisible(false);
                else
                    h_pass.setVisible(true);
                if(contraseña.getText().length()==8 && ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    ke.consume(); 
                if(!isContraseña()){
                    contraseña.setForeground(Color.RED);
                    bcontraseña = false;
                }
                else{
                    contraseña.setForeground(Color.GREEN);
                    bcontraseña = true;
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(contraseña.getText().length()>0)
                    h_pass.setVisible(false);
                else
                    h_pass.setVisible(true);
                if(contraseña.getText().length()==8 && ke.getKeyCode()!=ke.VK_BACK_SPACE){
                    ke.consume();
                } 
                if(!isContraseña()){
                    contraseña.setForeground(Color.RED);
                    bcontraseña = false;
                }
                else{
                    contraseña.setForeground(Color.GREEN);
                    bcontraseña = true;
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(contraseña.getText().length()>0)
                    h_pass.setVisible(false);
                else
                    h_pass.setVisible(true);
                if(contraseña.getText().length()==8 && ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    ke.consume(); 
                if(!isContraseña()){
                    contraseña.setForeground(Color.RED);
                    bcontraseña = false;
                }
                else{
                    contraseña.setForeground(Color.GREEN);
                    bcontraseña = true;
                }
            }
        });
        add(contraseña);
        
        pos_y += alSize + 7;
                
        registrar = new JButton("Registrar"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(Borde_R);
            }
        };
        registrar.setSize((int)(Ancho*0.9),alSize);
        registrar.setLocation((int)(Ancho*0.05),pos_y);
        registrar.setForeground(Color.GRAY);
        registrar.setFont(new Font("Arial",Font.BOLD,22));
        registrar.setContentAreaFilled(false);
        registrar.setFocusable(false);
        registrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        registrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(verificarCampos()){
                    if(cargo.getSelectedItem().toString().equalsIgnoreCase("PROFESOR")){
                        año_escolar = (String) JOptionPane.showInputDialog(null, 
                            "¿Año escolar a cargo?",
                            "Año escolar",
                            JOptionPane.QUESTION_MESSAGE, 
                            null, 
                            escolar, 
                            escolar[0]);
                        if(año_escolar == null){ return; }
                    }else{
                        año_escolar = "NINGUNO";
                    }
                    Ventana.registrarPersonal(Registro_Personal.this);
                    Ventana.menu_home.Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(0),Ventana.Login);
                }
            }
        });
        registrar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                registrar.setFont(new Font("Arial",Font.BOLD,26));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                registrar.setFont(new Font("Arial",Font.BOLD,22));
            }
        });
        add(registrar);
    }
    
    public boolean verificarCampos(){
        boolean campos = true;
        String mensaje = "";
        if(nombre.getText().length()<3 || nombre.getText().equalsIgnoreCase("Ingrese Nombres...")){
            mensaje += "Nombre debe contener al menos 3 caracteres.\n";
            campos = false;
        }
        if(apellido.getText().length()<3 || apellido.getText().equalsIgnoreCase("Ingrese Apellidos...")){
            mensaje += "Apellido debe contener al menos 3 caracteres.\n";
            campos = false;
        }
        if(cedula.getText().length()<7 || cedula.getText().equalsIgnoreCase("Documento de Identidad...")){
            mensaje += "Cédula muy corta (Min. 7 dígitos)\n";
            campos = false;
        }
        if(
            (dir_habitacion.getText().length()<5 || dir_habitacion.getText().equalsIgnoreCase("Dirección de Habitación...")) 
            &&
            (dir_trabajo.getText().length()<5 || dir_trabajo.getText().equalsIgnoreCase("Dirección de Trabajo..."))
        ){
            mensaje += "Debe ingresar una dirección de al menos 5 caracteres\n";
            campos = false;
        }
        if(
            (tlf_habitacion.getText().length()<9 || tlf_habitacion.getText().equalsIgnoreCase("Teléfono de Habitación..."))
            &&
            (tlf_celular.getText().length()<9 || tlf_celular.getText().equalsIgnoreCase("Teléfono de Celular..."))
            &&
            (tlf_trabajo.getText().length()<9 || tlf_trabajo.getText().equalsIgnoreCase("Teléfono de Trabajo..."))
        
        ){
            mensaje += "Debe ingresar al menos un telefono valido. Ejemplo (4140000000).\n";
            campos = false;
        }
        if(!bcorreo){
            mensaje += "Correo electrónico inválido (Ej: algo@algo.com).\n";
            campos = false;
        }
        if(!isContraseña()){
            mensaje += "Contraseña debe ser de 8 caracteres, compuesta por al menos un número,\n"
                    + " un caracter especial (_,-,*) y letras";
            campos = false;
        }
        if(!campos){
            JOptionPane.showMessageDialog(null,mensaje,"Campos Incorrectos",JOptionPane.ERROR_MESSAGE);
            mensaje = "";
        }        
        return campos;
    }
    
    public void limpiarCampos(){
        nombre.setText("Ingrese Nombres...");
        apellido.setText("Ingrese Apellidos...");
        cedula.setText("Documento de Identidad...");
        cargo.setSelectedItem(0);
        dir_trabajo.setText("Dirección de Trabajo...");
        dir_habitacion.setText("Dirección de Habitación...");
        tlf_trabajo.setText("Teléfono de Trabajo...");
        tlf_habitacion.setText("Teléfono de Habitación...");
        tlf_celular.setText("Teléfono de Celular...");
        correo.setText("Correo Electrónico...");
        correo.setForeground(Color.GRAY);
        contraseña.setText("");
    }
    
    public boolean isContraseña(){
        String pw = contraseña.getText();
        boolean numero = false, caracter_esp = false, letra = false;
        if(pw.length()!=8 || pw.equalsIgnoreCase("Contraseña...")){
            return false;
        }else{
            for(int i=0;i<pw.length();i++){
                String caracter = pw.substring(i, i+1);
                
                //NUMERO
                try{
                    if(Integer.parseInt(caracter)>0){ numero = true; }
                }catch(Exception e){}
                
                //CARACTER
                if(caracter.equalsIgnoreCase("_") || caracter.equalsIgnoreCase("-")
                    || caracter.equalsIgnoreCase("*"))
                    caracter_esp = true;
                
                //LETRAS_M
                System.out.println(""+(int)caracter.charAt(0));
                if((int)caracter.charAt(0)>=65 &&
                    (int)caracter.charAt(0)<91)
                    letra = true;
                
                //LETRAS_m
                if((int)caracter.charAt(0)>=97 &&
                    (int)caracter.charAt(0)<123)
                    letra = true;
            }
            if(numero && caracter_esp && letra)
                return true;
        }
        return false;
    }
    
    public void setMaximo(JTextField campo, int limite){
        campo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }
        });
    }
    
}
