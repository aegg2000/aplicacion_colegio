package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Registro_Estudiante extends JPanel implements ItemListener{
    public int Ancho, Alto, pos_y, Altura, LabelFuenteSize;
    public JLabel REPRESENTANTE, ESTUDIANTE, MADRE, PADRE, LEGAL; 
    public JLabel nombreRLabel, cedulaRLabel, tlfHabRLabel, tlfCelRLabel, dirHabRLabel, 
            nombreLabel, apellidoLabel, gradoLabel, fechaNacLabel, cedulaLabel, parentescoLabel;
    
    public boolean b_cedula_r,b_nombre_r,b_direccion_r,b_tlf_habitacion,b_tlf_celular,
        b_nombre_e,b_apellido_e, b_cedula_e, registro, b_check_m, b_check_p, b_check_l, b_parentesco_l;
    
    //REPRESENTANTE-MADRE
    public JTextField r_cedula_m,r_nombre_m,r_apellido_m,r_direccion_m,r_tlf_habitacion_m,r_tlf_celular_m;
    
    //REPRESENTANTE-PADRE
    public JTextField r_cedula_p,r_nombre_p,r_apellido_p,r_direccion_p,r_tlf_habitacion_p,r_tlf_celular_p;
    
    //REPRESENTANTE LEGAL
    public JTextField r_cedula_l,r_nombre_l,r_apellido_l,r_direccion_l,r_tlf_habitacion_l,r_tlf_celular_l, r_parentesco_l;
    
    public JComboBox[] cedula_r_combo;
    
    public JCheckBox cb_madre, cb_padre, cb_legal;
    
    public Color disabledColor;
        
    //ESTUDIANTE
    public JTextField e_nombre,e_apellido, e_cedula;
    public JComboBox grado,dia,mes,año, cedula_combo;
    
    public JButton registrar,cupos_disponibles;
    public int bise;
    
    public Ventana_Principal Ventana;
    public Calendar c = Calendar.getInstance();
    
    public int maximo_c_m, maximo_c_p, maximo_c_l, maximo_c_e;
    
    public Registro_Estudiante(Ventana_Principal Ventana, int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        this.Ventana = Ventana;
        setSize(this.Ancho,this.Alto);
        setLayout(null);
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(Ancho,Alto));
        
        bise = 28;
        
        maximo_c_m = maximo_c_p = maximo_c_l = maximo_c_e = 0;
        
        Altura = (int)(Alto/15);
        pos_y = (int)(Alto/15)+25;
        LabelFuenteSize = (int) (Ancho*0.018)-1;
          
        b_cedula_r = false;
        b_nombre_r = false;
        b_direccion_r = false;
        b_tlf_habitacion = false;
        b_tlf_celular = false;
        b_nombre_e = false;
        b_apellido_e = false;
        b_cedula_e = false;
        b_check_m = false;
        b_check_p = false;
        b_check_l = false;
        b_parentesco_l = false;
        
        disabledColor = new Color(220, 220, 220);
        
        //REPRESENTANTE---------------------------------------------------------
        REPRESENTANTE = new JLabel("REPRESENTANTE");
        REPRESENTANTE.setSize(Ancho,Altura);
        REPRESENTANTE.setLocation(0,0);
        REPRESENTANTE.setHorizontalAlignment(SwingConstants.CENTER);
        REPRESENTANTE.setFont(new Font("Arial",Font.BOLD,18));
        REPRESENTANTE.setForeground(Color.GRAY);
        add(REPRESENTANTE);
        
        cb_madre = new JCheckBox();
        cb_madre.setBounds((int)(Ancho*0.2), pos_y-27, 20, 20);
        cb_madre.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
               if(cb_madre.isSelected())
                   enabledInputs("m");
               else
                   disabledInputs("m");
            }
        });
        add(cb_madre);
        
        MADRE = new JLabel("MADRE");
        MADRE.setSize(Ancho,Altura);
        MADRE.setLocation((int)(Ancho*0.22),pos_y-30);
        MADRE.setHorizontalAlignment(SwingConstants.LEFT);
        MADRE.setFont(new Font("Arial",Font.BOLD,18));
        MADRE.setForeground(Color.GRAY);
        add(MADRE);
        
        cb_padre = new JCheckBox();
        cb_padre.setBounds((int)(Ancho*0.45), pos_y-27, 20, 20);
        cb_padre.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cb_padre.isSelected())
                    enabledInputs("p");
                else
                    disabledInputs("p");
            }
        });
        add(cb_padre);
        
        PADRE = new JLabel("PADRE");
        PADRE.setSize(Ancho,Altura);
        PADRE.setLocation((int)(Ancho*0.47),pos_y-30);
        PADRE.setHorizontalAlignment(SwingConstants.LEFT);
        PADRE.setFont(new Font("Arial",Font.BOLD,18));
        PADRE.setForeground(Color.GRAY);
        add(PADRE);
        
        cb_legal = new JCheckBox();
        cb_legal.setBounds((int)(Ancho*0.70), pos_y-27, 20, 20);
        cb_legal.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cb_legal.isSelected())
                    enabledInputs("l");
                else
                    disabledInputs("l");
            }
        });
        add(cb_legal);
        
        LEGAL = new JLabel("REPRESENTANTE LEGAL");
        LEGAL.setSize(Ancho,Altura);
        LEGAL.setLocation((int)(Ancho*0.72),pos_y-30);
        LEGAL.setHorizontalAlignment(SwingConstants.LEFT);
        LEGAL.setFont(new Font("Arial",Font.BOLD,18));
        LEGAL.setForeground(Color.GRAY);
        add(LEGAL);
        
        //Nombre y Apellidos
        int anchoInput = (int)(Ancho*0.23);   
        
        nombreRLabel = new JLabel("Nombre Completo: ");
        nombreRLabel.setSize(Ancho, Altura);
        nombreRLabel.setLocation((int)(Ancho*0.05), pos_y);
        nombreRLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(nombreRLabel);
        
        r_nombre_m = new JTextField("Nombres y Apellidos..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_nombre_m.setSize(anchoInput,Altura);
        r_nombre_m.setLocation((int)(Ancho*0.2),pos_y);
        r_nombre_m.setBackground(disabledColor);
        r_nombre_m.setForeground(Color.GRAY);
        r_nombre_m.setFont(new Font("Arial",Font.ITALIC,14));
        r_nombre_m.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_nombre_m.getText().equalsIgnoreCase("Nombres y Apellidos...")){
                    r_nombre_m.setText("");
                    r_nombre_m.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_nombre_m.getText().equalsIgnoreCase("")){
                    r_nombre_m.setText("Nombres y Apellidos...");
                    r_nombre_m.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_nombre_m.setEnabled(false);
        add(r_nombre_m);
        setMaximo(r_nombre_m,65);
                
        r_nombre_p = new JTextField("Nombres y Apellidos..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_nombre_p.setSize(anchoInput,Altura);
        r_nombre_p.setLocation((int)(Ancho*0.45),pos_y);
        r_nombre_p.setBackground(disabledColor);
        r_nombre_p.setForeground(Color.GRAY);
        r_nombre_p.setFont(new Font("Arial",Font.ITALIC,14));
        r_nombre_p.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_nombre_p.getText().equalsIgnoreCase("Nombres y Apellidos...")){
                    r_nombre_p.setText("");
                    r_nombre_p.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_nombre_p.getText().equalsIgnoreCase("")){
                    r_nombre_p.setText("Nombres y Apellidos...");
                    r_nombre_p.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_nombre_p.setEnabled(false);
        add(r_nombre_p);
        setMaximo(r_nombre_p,65);
        
        r_nombre_l = new JTextField("Nombres y Apellidos..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_nombre_l.setSize(anchoInput,Altura);
        r_nombre_l.setLocation((int)(Ancho*0.70),pos_y);
        r_nombre_l.setBackground(disabledColor);
        r_nombre_l.setForeground(Color.GRAY);
        r_nombre_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_nombre_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_nombre_l.getText().equalsIgnoreCase("Nombres y Apellidos...")){
                    r_nombre_l.setText("");
                    r_nombre_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_nombre_l.getText().equalsIgnoreCase("")){
                    r_nombre_l.setText("Nombres y Apellidos...");
                    r_nombre_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_nombre_l.setEnabled(false);
        add(r_nombre_l);
        setMaximo(r_nombre_l,65);
        
        pos_y += (int)(Alto/15)+5;
        
        //Cedula
        cedula_r_combo = new JComboBox[3];
        int anchoInputCedula = (int)(Ancho*0.18);
        for(int i=0; i<3; i++ ){ 
            cedula_r_combo[i] = new JComboBox();
            cedula_r_combo[i].addItem("V");
            cedula_r_combo[i].addItem("E");
            cedula_r_combo[i].setSelectedIndex(0);
            cedula_r_combo[i].setSize((int)(Ancho*0.04),Altura);            
            cedula_r_combo[i].setBackground(Color.WHITE);
            cedula_r_combo[i].setForeground(Color.GRAY);
            cedula_r_combo[i].setFocusable(false);
            cedula_r_combo[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            cedula_r_combo[i].setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
            cedula_r_combo[i].setFont(new Font("Arial",Font.PLAIN,14));
            cedula_r_combo[i].setEnabled(false);
        }
        
        cedulaRLabel = new JLabel("Cedula: ");
        cedulaRLabel.setSize(Ancho, Altura);
        cedulaRLabel.setLocation((int)(Ancho*0.05), pos_y);
        cedulaRLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(cedulaRLabel);
        
        cedula_r_combo[0].setLocation((int)(Ancho*0.2),pos_y);
        
        r_cedula_m = new JTextField("C.I. de la Madre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_cedula_m.setSize(anchoInputCedula,Altura);
        r_cedula_m.setLocation((int)(Ancho*0.25),pos_y);
        r_cedula_m.setBackground(disabledColor);
        r_cedula_m.setForeground(Color.GRAY);
        r_cedula_m.setFont(new Font("Arial",Font.ITALIC,14));
        r_cedula_m.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(r_cedula_m.getText().length() == maximo_c_m-1) 
                        e.consume();
            }
        });
        r_cedula_m.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_cedula_m.getText().equalsIgnoreCase("C.I. de la Madre...")){
                    r_cedula_m.setText("");
                    r_cedula_m.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_cedula_m.getText().equalsIgnoreCase("")){
                    r_cedula_m.setText("C.I. de la Madre...");
                    r_cedula_m.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_cedula_m.setEnabled(false);
        
        cedula_r_combo[0].addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cedula_r_combo[0].getSelectedItem().equals("V")){
                    if(r_cedula_m.getText().length() >= maximo_c_m-1){
                        r_cedula_m.setText(r_cedula_m.getText().substring(0, 8));
                    }
                    maximo_c_m = 9;
                }
                else if(cedula_r_combo[0].getSelectedItem().equals("E")){
                    maximo_c_m = 10;
                }
            }
        });
        add(cedula_r_combo[0]);
        
        add(r_cedula_m);
        maximo_c_m = 9;
        
        cedula_r_combo[1].setLocation((int)(Ancho*0.45),pos_y);
        
        r_cedula_p = new JTextField("C.I. del Padre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_cedula_p.setSize(anchoInputCedula,Altura);
        r_cedula_p.setLocation((int)(Ancho*0.50),pos_y);
        r_cedula_p.setBackground(disabledColor);
        r_cedula_p.setForeground(Color.GRAY);
        r_cedula_p.setFont(new Font("Arial",Font.ITALIC,14));
        r_cedula_p.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(r_cedula_p.getText().length() == maximo_c_p-1) 
                        e.consume();
            }
        });
        r_cedula_p.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_cedula_p.getText().equalsIgnoreCase("C.I. del Padre...")){
                    r_cedula_p.setText("");
                    r_cedula_p.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_cedula_p.getText().equalsIgnoreCase("")){
                    r_cedula_p.setText("C.I. del Padre...");
                    r_cedula_p.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_cedula_p.setEnabled(false);
        
        cedula_r_combo[1].addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cedula_r_combo[1].getSelectedItem().equals("V")){
                    if(r_cedula_p.getText().length() >= maximo_c_p-1){
                        r_cedula_p.setText(r_cedula_p.getText().substring(0, 8));
                    }
                    maximo_c_p = 9;
                }
                else if(cedula_r_combo[1].getSelectedItem().equals("E"))
                    maximo_c_p = 10;
            }
        });
        add(cedula_r_combo[1]);
        add(r_cedula_p);
        maximo_c_p = 9;
        
        cedula_r_combo[2].setLocation((int)(Ancho*0.7),pos_y);
        
        r_cedula_l = new JTextField("C.I. del Representante Legal..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_cedula_l.setSize(anchoInputCedula,Altura);
        r_cedula_l.setLocation((int)(Ancho*0.75),pos_y);
        r_cedula_l.setBackground(disabledColor);
        r_cedula_l.setForeground(Color.GRAY);
        r_cedula_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_cedula_l.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(r_cedula_l.getText().length() == maximo_c_l-1) 
                        e.consume();
            }
        });
        r_cedula_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_cedula_l.getText().equalsIgnoreCase("C.I. del Representante Legal...")){
                    r_cedula_l.setText("");
                    r_cedula_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_cedula_l.getText().equalsIgnoreCase("")){
                    r_cedula_l.setText("C.I. del Representante Legal...");
                    r_cedula_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_cedula_l.setEnabled(false);
        
        cedula_r_combo[2].addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cedula_r_combo[2].getSelectedItem().equals("V")){
                    if(r_cedula_l.getText().length() >= maximo_c_l-1){
                        r_cedula_l.setText(r_cedula_l.getText().substring(0, 8));
                    }
                    maximo_c_l = 9;
                }
                else if(cedula_r_combo[2].getSelectedItem().equals("E"))
                    maximo_c_l = 10;
            }
        });
        add(cedula_r_combo[2]);        
        add(r_cedula_l);
        maximo_c_l = 9;
        
        pos_y += (int)(Alto/15)+5;
        
        tlfHabRLabel = new JLabel("Telef. Habitación: ");
        tlfHabRLabel.setSize(Ancho, Altura);
        tlfHabRLabel.setLocation((int)(Ancho*0.05), pos_y);
        tlfHabRLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(tlfHabRLabel);
                
        r_tlf_habitacion_m = new JTextField("Tlf. Habitación de la Madre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_habitacion_m.setSize(anchoInput,Altura);
        r_tlf_habitacion_m.setLocation((int)(Ancho*0.2),pos_y);
        r_tlf_habitacion_m.setBackground(disabledColor);
        r_tlf_habitacion_m.setForeground(Color.GRAY);
        r_tlf_habitacion_m.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_habitacion_m.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_habitacion_m.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_habitacion_m.getText().equalsIgnoreCase("Tlf. Habitación de la Madre...")){
                    r_tlf_habitacion_m.setText("");
                    r_tlf_habitacion_m.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_habitacion_m.getText().equalsIgnoreCase("")){
                    r_tlf_habitacion_m.setText("Tlf. Habitación de la Madre...");
                    r_tlf_habitacion_m.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_habitacion_m.setEnabled(false);
        add(r_tlf_habitacion_m);
        setMaximo(r_tlf_habitacion_m,12);
        
        r_tlf_habitacion_p = new JTextField("Tlf. Habitación del Padre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_habitacion_p.setSize(anchoInput,Altura);
        r_tlf_habitacion_p.setLocation((int)(Ancho*0.45),pos_y);
        r_tlf_habitacion_p.setBackground(disabledColor);
        r_tlf_habitacion_p.setForeground(Color.GRAY);
        r_tlf_habitacion_p.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_habitacion_p.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_habitacion_p.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_habitacion_p.getText().equalsIgnoreCase("Tlf. Habitación del Padre...")){
                    r_tlf_habitacion_p.setText("");
                    r_tlf_habitacion_p.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_habitacion_p.getText().equalsIgnoreCase("")){
                    r_tlf_habitacion_p.setText("Tlf. Habitación del Padre...");
                    r_tlf_habitacion_p.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_habitacion_p.setEnabled(false);
        add(r_tlf_habitacion_p);
        setMaximo(r_tlf_habitacion_p,12);
        
        r_tlf_habitacion_l = new JTextField("Tlf. Habitación del R. Legal..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_habitacion_l.setSize(anchoInput,Altura);
        r_tlf_habitacion_l.setLocation((int)(Ancho*0.7),pos_y);
        r_tlf_habitacion_l.setBackground(disabledColor);
        r_tlf_habitacion_l.setForeground(Color.GRAY);
        r_tlf_habitacion_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_habitacion_l.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_habitacion_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_habitacion_l.getText().equalsIgnoreCase("Tlf. Habitación del R. Legal...")){
                    r_tlf_habitacion_l.setText("");
                    r_tlf_habitacion_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_habitacion_l.getText().equalsIgnoreCase("")){
                    r_tlf_habitacion_l.setText("Tlf. Habitación del R. Legal...");
                    r_tlf_habitacion_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_habitacion_l.setEnabled(false);
        add(r_tlf_habitacion_l);
        setMaximo(r_tlf_habitacion_l,12);
        
        pos_y += (int)(Alto/15)+5;
        
        tlfCelRLabel = new JLabel("Telef. Celular: ");
        tlfCelRLabel.setSize(Ancho, Altura);
        tlfCelRLabel.setLocation((int)(Ancho*0.05), pos_y);
        tlfCelRLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(tlfCelRLabel);
           
        r_tlf_celular_m = new JTextField("Tlf. Celular de la Madre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_celular_m.setSize(anchoInput,Altura);
        r_tlf_celular_m.setLocation((int)(Ancho*0.2),pos_y);
        r_tlf_celular_m.setBackground(disabledColor);
        r_tlf_celular_m.setForeground(Color.GRAY);
        r_tlf_celular_m.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_celular_m.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_celular_m.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_celular_m.getText().equalsIgnoreCase("Tlf. Celular de la Madre...")){
                    r_tlf_celular_m.setText("");
                    r_tlf_celular_m.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_celular_m.getText().equalsIgnoreCase("")){
                    r_tlf_celular_m.setText("Tlf. Celular de la Madre...");
                    r_tlf_celular_m.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_celular_m.setEnabled(false);
        add(r_tlf_celular_m);
        setMaximo(r_tlf_celular_m,12);
        
        r_tlf_celular_p = new JTextField("Tlf. Celular del Padre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_celular_p.setSize(anchoInput,Altura);
        r_tlf_celular_p.setLocation((int)(Ancho*0.45),pos_y);
        r_tlf_celular_p.setBackground(disabledColor);
        r_tlf_celular_p.setForeground(Color.GRAY);
        r_tlf_celular_p.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_celular_p.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_celular_p.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_celular_p.getText().equalsIgnoreCase("Tlf. Celular del Padre...")){
                    r_tlf_celular_p.setText("");
                    r_tlf_celular_p.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_celular_p.getText().equalsIgnoreCase("")){
                    r_tlf_celular_p.setText("Tlf. Celular del Padre...");
                    r_tlf_celular_p.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_celular_p.setEnabled(false);
        add(r_tlf_celular_p);
        setMaximo(r_tlf_celular_p,12);
        
        r_tlf_celular_l = new JTextField("Tlf. Celular del R. Legal..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_tlf_celular_l.setSize(anchoInput,Altura);
        r_tlf_celular_l.setLocation((int)(Ancho*0.7),pos_y);
        r_tlf_celular_l.setBackground(disabledColor);
        r_tlf_celular_l.setForeground(Color.GRAY);
        r_tlf_celular_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_tlf_celular_l.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });
        r_tlf_celular_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_tlf_celular_l.getText().equalsIgnoreCase("Tlf. Celular del R. Legal...")){
                    r_tlf_celular_l.setText("");
                    r_tlf_celular_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_tlf_celular_l.getText().equalsIgnoreCase("")){
                    r_tlf_celular_l.setText("Tlf. Celular del R. Legal...");
                    r_tlf_celular_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_tlf_celular_l.setEnabled(false);
        add(r_tlf_celular_l);
        setMaximo(r_tlf_celular_l,12);
        
        pos_y += (int)(Alto/15)+5;
        
        dirHabRLabel = new JLabel("Dir. de Habitación: ");
        dirHabRLabel.setSize(Ancho, Altura);
        dirHabRLabel.setLocation((int)(Ancho*0.05), pos_y);
        dirHabRLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(dirHabRLabel);
        
        r_direccion_m = new JTextField("Dir. de Habitación de la Madre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_direccion_m.setSize(anchoInput,Altura);
        r_direccion_m.setLocation((int)(Ancho*0.2),pos_y);
        r_direccion_m.setBackground(disabledColor);
        r_direccion_m.setForeground(Color.GRAY);
        r_direccion_m.setFont(new Font("Arial",Font.ITALIC,14));
        r_direccion_m.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_direccion_m.getText().equalsIgnoreCase("Dir. de Habitación de la Madre...")){
                    r_direccion_m.setText("");
                    r_direccion_m.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_direccion_m.getText().equalsIgnoreCase("")){
                    r_direccion_m.setText("Dir. de Habitación de la Madre...");
                    r_direccion_m.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_direccion_m.setEnabled(false);
        add(r_direccion_m);
        setMaximo(r_direccion_m,65);        
        
        r_direccion_p = new JTextField("Dir. de Habitación del Padre..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_direccion_p.setSize(anchoInput,Altura);
        r_direccion_p.setLocation((int)(Ancho*0.45),pos_y);
        r_direccion_p.setBackground(disabledColor);
        r_direccion_p.setForeground(Color.GRAY);
        r_direccion_p.setFont(new Font("Arial",Font.ITALIC,14));
        r_direccion_p.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_direccion_p.getText().equalsIgnoreCase("Dir. de Habitación del Padre...")){
                    r_direccion_p.setText("");
                    r_direccion_p.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_direccion_p.getText().equalsIgnoreCase("")){
                    r_direccion_p.setText("Dir. de Habitación del Padre...");
                    r_direccion_p.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_direccion_p.setEnabled(false);
        add(r_direccion_p);
        setMaximo(r_direccion_p,65);
        
        r_direccion_l = new JTextField("Dir. de Habitación del R. Legal..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_direccion_l.setSize(anchoInput,Altura);
        r_direccion_l.setLocation((int)(Ancho*0.7),pos_y);
        r_direccion_l.setBackground(disabledColor);
        r_direccion_l.setForeground(Color.GRAY);
        r_direccion_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_direccion_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_direccion_l.getText().equalsIgnoreCase("Dir. de Habitación del R. Legal...")){
                    r_direccion_l.setText("");
                    r_direccion_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_direccion_l.getText().equalsIgnoreCase("")){
                    r_direccion_l.setText("Dir. de Habitación del R. Legal...");
                    r_direccion_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_direccion_l.setEnabled(false);
        add(r_direccion_l);
        setMaximo(r_direccion_l,65);
        
        pos_y += (int)(Alto/15)+5;
        
        r_parentesco_l = new JTextField("Parentesco del R. Legal..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        r_parentesco_l.setSize(anchoInput,Altura);
        r_parentesco_l.setLocation((int)(Ancho*0.7),pos_y);
        r_parentesco_l.setBackground(disabledColor);
        r_parentesco_l.setForeground(Color.GRAY);
        r_parentesco_l.setFont(new Font("Arial",Font.ITALIC,14));
        r_parentesco_l.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(r_parentesco_l.getText().equalsIgnoreCase("Parentesco del R. Legal...")){
                    r_parentesco_l.setText("");
                    r_parentesco_l.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(r_parentesco_l.getText().equalsIgnoreCase("")){
                    r_parentesco_l.setText("Parentesco del R. Legal...");
                    r_parentesco_l.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        r_parentesco_l.setEnabled(false);
        add(r_parentesco_l);
        setMaximo(r_parentesco_l,15);
        
        pos_y += (int)(Alto/15);
        
        //ESTUDIANTE------------------------------------------------------------
        ESTUDIANTE = new JLabel("ESTUDIANTE");
        ESTUDIANTE.setSize(Ancho,(int)(Alto*0.05));
        ESTUDIANTE.setLocation(0,pos_y);
        ESTUDIANTE.setHorizontalAlignment(SwingConstants.CENTER);
        ESTUDIANTE.setFont(new Font("Arial",Font.BOLD,18));
        ESTUDIANTE.setForeground(Color.GRAY);
        add(ESTUDIANTE);
        
        pos_y += (int)(Alto/15)+5;
        
        nombreLabel = new JLabel("Nombres: ");
        nombreLabel.setSize(Ancho, Altura);
        nombreLabel.setLocation((int)(Ancho*0.05), pos_y);
        nombreLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(nombreLabel); 
        
        apellidoLabel = new JLabel("Apellidos: ");
        apellidoLabel.setSize(Ancho, Altura);
        apellidoLabel.setLocation((int)(Ancho*0.35), pos_y);
        apellidoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(apellidoLabel); 
        
        cedulaLabel = new JLabel("Cedula: ");
        cedulaLabel.setSize(Ancho, Altura);
        cedulaLabel.setLocation((int)(Ancho*0.66), pos_y);
        cedulaLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(cedulaLabel); 
        
        pos_y += (int)(Alto/15)+5;
        
        e_nombre = new JTextField("Nombres..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        e_nombre.setSize((int)(Ancho*0.28),Altura);
        e_nombre.setLocation((int)(Ancho*0.05),pos_y);
        e_nombre.setBackground(Color.WHITE);
        e_nombre.setForeground(Color.GRAY);
        e_nombre.setFont(new Font("Arial",Font.ITALIC,14));
        e_nombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(e_nombre.getText().equalsIgnoreCase("Nombres...")){
                    e_nombre.setText("");
                    e_nombre.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(e_nombre.getText().equalsIgnoreCase("")){
                    e_nombre.setText("Nombres...");
                    e_nombre.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(e_nombre);
        setMaximo(e_nombre,65);
        
        e_apellido = new JTextField("Apellidos..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        e_apellido.setSize((int)(Ancho*0.28),Altura);
        e_apellido.setLocation((int)(Ancho*0.35),pos_y);
        e_apellido.setBackground(Color.WHITE);
        e_apellido.setForeground(Color.GRAY);
        e_apellido.setFont(new Font("Arial",Font.ITALIC,14));
        e_apellido.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(e_apellido.getText().equalsIgnoreCase("Apellidos...")){
                    e_apellido.setText("");
                    e_apellido.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(e_apellido.getText().equalsIgnoreCase("")){
                    e_apellido.setText("Apellidos...");
                    e_apellido.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        add(e_apellido);
        setMaximo(e_apellido,65);
        
        cedula_combo = new JComboBox();
        cedula_combo.addItem("V");
        cedula_combo.addItem("E");
        cedula_combo.setSelectedIndex(0);
        cedula_combo.setSize((int)(Ancho*0.04),Altura);
        cedula_combo.setLocation((int)(Ancho*0.66),pos_y);
        cedula_combo.setBackground(Color.WHITE);
        cedula_combo.setForeground(Color.GRAY);
        cedula_combo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(cedula_combo.getSelectedItem().equals("V")){
                    if(e_cedula.getText().length() >= maximo_c_e-1){
                        e_cedula.setText(e_cedula.getText().substring(0, 8));
                    }
                    maximo_c_e = 9;
                }else if(cedula_combo.getSelectedItem().equals("E"))
                    maximo_c_e = 10;
            }
        });
        cedula_combo.setFocusable(false);
        cedula_combo.setCursor(new Cursor(Cursor.HAND_CURSOR));
        cedula_combo.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        cedula_combo.setFont(new Font("Arial",Font.PLAIN,14));
        cedula_combo.setEnabled(false);
        add(cedula_combo);
        
        e_cedula = new JTextField("Cedula..."){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };        
        e_cedula.setSize((int)(Ancho*0.22),Altura);
        e_cedula.setLocation((int)(Ancho*0.71),pos_y);
        e_cedula.setBackground(disabledColor);
        e_cedula.setForeground(Color.GRAY);
        e_cedula.setFont(new Font("Arial",Font.ITALIC,14));
        e_cedula.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
                if(e.getKeyCode()!=e.VK_BACK_SPACE)
                    if(e_cedula.getText().length() == maximo_c_e-1) 
                        e.consume();
            }
        });
        e_cedula.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if(e_cedula.getText().equalsIgnoreCase("Cedula...")){
                    e_cedula.setText("");
                    e_cedula.setFont(new Font("Arial",Font.PLAIN,14));
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if(e_cedula.getText().equalsIgnoreCase("")){
                    e_cedula.setText("Cedula...");
                    e_cedula.setFont(new Font("Arial",Font.ITALIC,14));
                }
            }
        });
        e_cedula.setEnabled(false);
        add(e_cedula);
        maximo_c_e = 9;
        
        pos_y += (int)(Alto/15)+7;
        
        gradoLabel = new JLabel("Grado: ");
        gradoLabel.setSize(Ancho, Altura);
        gradoLabel.setLocation((int)(Ancho*0.05), pos_y);
        gradoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(gradoLabel); 
        
        grado = new JComboBox();
        grado.addItem("SELECCIONE GRADO");
        if(mostrarGrado("PRIMER GRUPO"))
            grado.addItem("PRIMER GRUPO");
        if(mostrarGrado("SEGUNDO GRUPO"))
            grado.addItem("SEGUNDO GRUPO");
        if(mostrarGrado("TERCER GRUPO"))
            grado.addItem("TERCER GRUPO");
        if(mostrarGrado("PRIMER GRADO"))
            grado.addItem("PRIMER GRADO");
        if(mostrarGrado("SEGUNDO GRADO"))
            grado.addItem("SEGUNDO GRADO");
        if(mostrarGrado("TERCER GRADO"))
            grado.addItem("TERCER GRADO");
        if(mostrarGrado("CUARTO GRADO"))
            grado.addItem("CUARTO GRADO");
        if(mostrarGrado("QUINTO GRADO"))
            grado.addItem("QUINTO GRADO");
        if(mostrarGrado("SEXTO GRADO"))
            grado.addItem("SEXTO GRADO");
        
        grado.setSelectedIndex(0);
        grado.setSize((int)(Ancho*0.36)-30,Altura);
        grado.setLocation((int)(Ancho*0.13),pos_y);
        grado.setBackground(Color.WHITE);
        grado.setForeground(Color.GRAY);
        grado.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if(grado.getSelectedItem() == "CUARTO GRADO" || grado.getSelectedItem() == "QUINTO GRADO" || grado.getSelectedItem() == "SEXTO GRADO"){
                    e_cedula.setBackground(Color.WHITE);
                    e_cedula.setEnabled(true);
                    cedula_combo.setEnabled(true);
                }else{
                    e_cedula.setBackground(disabledColor);
                    e_cedula.setEnabled(false);
                    cedula_combo.setEnabled(false);
                }
                cambiarFecha((String) grado.getSelectedItem());
                año.setSelectedIndex(0);
                mes.setSelectedIndex(0);
                dia.setSelectedIndex(0);
            }
        });
        grado.setFocusable(false);
        grado.setCursor(new Cursor(Cursor.HAND_CURSOR));
        grado.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        grado.setFont(new Font("Arial",Font.PLAIN,14));
        add(grado);
        
        cupos_disponibles = new JButton(Ventana.cargarImagen("cupos_icon.png", 30,30));
        cupos_disponibles.setSize(30,30);
        cupos_disponibles.setLocation((int)(Ancho*0.49)-30,pos_y);
        cupos_disponibles.setFocusable(false);
        cupos_disponibles.setCursor(new Cursor(Cursor.HAND_CURSOR));
        cupos_disponibles.setFont(new Font("Arial",Font.BOLD,16));
        cupos_disponibles.setContentAreaFilled(false);
        cupos_disponibles.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                JOptionPane.showMessageDialog(null, new Informacion_Cupos(Ventana),"CUPOS DISPONIBLES",JOptionPane.PLAIN_MESSAGE);
            }
        });
        add(cupos_disponibles);
        
        fechaNacLabel = new JLabel("Fec. Nacimiento: ");
        fechaNacLabel.setSize(Ancho, Altura);
        fechaNacLabel.setLocation((int)(Ancho*0.5), pos_y);
        fechaNacLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(fechaNacLabel);
        
        año = new JComboBox();
        año.addItem("Año Nac.");
        año.setSelectedItem(0);
        año.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                mes.setSelectedIndex(0);
                dia.setSelectedIndex(0);
            }
        });
        año.setSize((int)(Ancho*0.09),Altura);
        año.setLocation((int)(Ancho*0.66),pos_y);
        año.setBackground(Color.WHITE);
        año.setForeground(Color.GRAY);
        año.setInheritsPopupMenu(true);
        año.setFocusable(false);
        año.setCursor(new Cursor(Cursor.HAND_CURSOR));
        año.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        año.setFont(new Font("Arial",Font.PLAIN,12));
        add(año);
        
        mes = new JComboBox();
        mes.addItem("Mes Nac.");
        for(int i=0;i<12;i++){
            mes.addItem(""+(i+1));
        }
        mes.setSelectedIndex(0);
        mes.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                dia.removeAllItems();
                dia.addItem("Día Nac.");
                if(mes.getSelectedIndex()==1) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==2){
                    GregorianCalendar calendar = new GregorianCalendar();
                    if (calendar.isLeapYear(Integer.parseInt((String) año.getSelectedItem()))){
                        System.out.println("bisiesto");
                        bise = 29;
                    }
                    else{
                        bise = 28;
                    }
                    for(int i=0;i<bise;i++) dia.addItem(""+(i+1));
                }                    
                if(mes.getSelectedIndex()==3) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==4) for(int i=0;i<30;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==5) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==6) for(int i=0;i<30;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==7) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==8) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==9) for(int i=0;i<30;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==10) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==11) for(int i=0;i<30;i++) dia.addItem(""+(i+1));
                if(mes.getSelectedIndex()==12) for(int i=0;i<31;i++) dia.addItem(""+(i+1));
                dia.setSelectedIndex(0);
            }
        });
        mes.setSize((int)(Ancho*0.09),Altura);
        mes.setLocation((int)(Ancho*0.75),pos_y);
        mes.setBackground(Color.WHITE);
        mes.setForeground(Color.GRAY);
        mes.setInheritsPopupMenu(true);
        mes.setFocusable(false);
        mes.setCursor(new Cursor(Cursor.HAND_CURSOR));
        mes.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        mes.setFont(new Font("Arial",Font.PLAIN,12));
        add(mes);
        
        dia = new JComboBox(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        dia.addItem("Día Nac.");
        dia.setSelectedIndex(0);
        dia.setSize((int)(Ancho*0.09),Altura);
        dia.setLocation((int)(Ancho*0.84),pos_y);
        dia.setBackground(Color.WHITE);
        dia.setForeground(Color.GRAY);
        dia.setInheritsPopupMenu(true);
        dia.setFocusable(false);
        dia.setCursor(new Cursor(Cursor.HAND_CURSOR));
        dia.setBorder(BorderFactory.createLineBorder(Color.yellow, 0));
        dia.setFont(new Font("Arial",Font.PLAIN,12));
        add(dia);
        
        pos_y += (int)(Alto/15)+20;
        
        registrar = new JButton("Inscribir Estudiante"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        registrar.setSize((int)(Ancho*0.9),Altura);
        registrar.setLocation((int)(Ancho*0.05),pos_y);
        registrar.setForeground(Color.GRAY);
        registrar.setFont(new Font("Arial",Font.BOLD,22));
        registrar.setContentAreaFilled(false);
        registrar.setFocusable(false);
        registrar.setEnabled(false);
        registrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        registrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String mensaje = "";
                Registro_Comprobante Comprobante = new Registro_Comprobante(Ventana);
                
                boolean campos = verificarCampos();
                
                if((b_check_m||b_check_p||b_check_l)&&campos&&
                        dia.getSelectedIndex()>0&&mes.getSelectedIndex()>0&&año.getSelectedIndex()>0){
                    JOptionPane.showMessageDialog(null, Comprobante,"Registro de Pago",JOptionPane.PLAIN_MESSAGE);
                    if(Comprobante.comprobante.getText().length()>4 && Comprobante.monto.getText().length()>0){
                        Ventana.registrarEstudiante(
                                Registro_Estudiante.this,
                                Comprobante.comprobante.getText(),
                                Comprobante.concepto.getText(),
                                Comprobante.monto.getText(),
                                "NUEVO ESTUDIANTE");
//                        limpiarCampos();
                    }else{
                        JOptionPane.showMessageDialog(null, "¡Error de comprobante, verifique los datos!",
                            "Error de Comprobante",JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    if(!(b_check_m || b_check_p || b_check_l)){
                        mensaje += "Debe ingresar al menos 1 representate.\n";
                    }else{
                        mensaje += getMensaje();
                        if(!b_nombre_e){
                            mensaje += "Nombre del Estudiante no Válido. Al menos 3 caracteres.\n";
                        }
                        if(!b_apellido_e){
                            mensaje += "Apellido del Estudiante no Válido. Al menos 3 caracteres.\n";
                        }
                        if(dia.getSelectedIndex()==0){
                            mensaje += "Debe seleccionar un día de nacimiento válido.\n";
                        }
                        if(mes.getSelectedIndex()==0){
                            mensaje += "Debe seleccionar un mes de nacimiento válido.\n";
                        }
                        if(año.getSelectedIndex()==0){
                            mensaje += "Debe seleccionar un año de nacimiento válido.\n";
                        }
                        if(!b_cedula_e){
                            mensaje += "Cédula de Estudiante no Válida. Al menos 7 digitos.\n";
                        }
                    }                    
                    JOptionPane.showMessageDialog(null,mensaje,"Error de Inscripción",JOptionPane.ERROR_MESSAGE);
                }                 
            }
        });
        registrar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                registrar.setFont(new Font("Arial",Font.BOLD,26));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                registrar.setFont(new Font("Arial",Font.BOLD,22));
            }
        });
        add(registrar);
        
        if(mostrarGrado((String)(grado.getSelectedItem())))
            registrar.setEnabled(true);
        else
            registrar.setEnabled(false);
    }
    
    public boolean verificarCampos(){
        boolean campos_m = false, campos_p = false, campos_l = false, res = false;

        if(b_check_m){
            b_nombre_r = r_nombre_m.getText().length()>2 && !r_nombre_m.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_m.getText().length()>6 && !r_cedula_m.getText().equalsIgnoreCase("C.I. de la Madre...");
            b_tlf_habitacion = r_tlf_habitacion_m.getText().length()>10 && !r_tlf_habitacion_m.getText().equalsIgnoreCase("Tlf. Habitación de la Madre...");
            b_tlf_celular = r_tlf_celular_m.getText().length()>10 && !r_tlf_celular_m.getText().equalsIgnoreCase("Tlf. Celular de la Madre...");
            b_direccion_r = r_direccion_m.getText().length()>5 && !r_direccion_m.getText().equalsIgnoreCase("Dir. de Habitación de la Madre...");
            if(b_nombre_r && b_cedula_r && b_tlf_habitacion && b_tlf_celular && b_direccion_r){
                campos_m = true;
            }
        }
        if(b_check_p){
            b_nombre_r = r_nombre_p.getText().length()>2 && !r_nombre_p.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_p.getText().length()>6 && !r_cedula_p.getText().equalsIgnoreCase("C.I. del Padre...");
            b_tlf_habitacion = r_tlf_habitacion_p.getText().length()>10 && !r_tlf_habitacion_p.getText().equalsIgnoreCase("Tlf. Habitación del Padre...");
            b_tlf_celular = r_tlf_celular_p.getText().length()>10 && !r_tlf_celular_p.getText().equalsIgnoreCase("Tlf. Celular del Padre...");
            b_direccion_r = r_direccion_p.getText().length()>5 && !r_direccion_p.getText().equalsIgnoreCase("Dir. de Habitación del Padre...");
            if(b_nombre_r && b_cedula_r && b_tlf_habitacion && b_tlf_celular && b_direccion_r){
                campos_p = true;
            }
        }
        if(b_check_l){
            b_nombre_r = r_nombre_l.getText().length()>2 && !r_nombre_l.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_l.getText().length()>6 && !r_cedula_l.getText().equalsIgnoreCase("C.I. del Representante Legal...");
            b_tlf_habitacion = r_tlf_habitacion_l.getText().length()>10 && !r_tlf_habitacion_l.getText().equalsIgnoreCase("Tlf. Habitación del R. Legal...");
            b_tlf_celular = r_tlf_celular_l.getText().length()>10 && !r_tlf_celular_l.getText().equalsIgnoreCase("Tlf. Celular del R. Legal...");
            b_direccion_r = r_direccion_l.getText().length()>5 && !r_direccion_l.getText().equalsIgnoreCase("Dir. de Habitación del R. Legal...");
            b_parentesco_l = r_parentesco_l.getText().length()>2 && !r_parentesco_l.getText().equalsIgnoreCase("Parentesco del R. Legal...");
            if(b_nombre_r && b_cedula_r && b_tlf_habitacion && b_tlf_celular && b_direccion_r && b_parentesco_l){
                campos_l = true;
            }
        }
        
        if(b_check_m && b_check_p && b_check_l){
            res = campos_m && campos_p && campos_l;
        }else if(b_check_m && b_check_p  && !b_check_l){
            res = campos_m && campos_p;
        }else if(b_check_m && !b_check_p  && b_check_l){
            res = campos_m && campos_l;
        }else if(!b_check_m && b_check_p  && b_check_l){
            res = campos_p && campos_l;
        }else if(b_check_m && !b_check_p  && !b_check_l){
            res = campos_m;            
        }else if(!b_check_m && b_check_p  && !b_check_l){
            res = campos_p;
        }else if(!b_check_m && !b_check_p  && b_check_l){
            res = campos_l;
        }
        
        /* ESTUDIANTE */
        b_nombre_e = e_nombre.getText().length()>2 && !e_nombre.getText().equalsIgnoreCase("Nombres...");
        b_apellido_e = e_apellido.getText().length()>2 && !e_apellido.getText().equalsIgnoreCase("Apellidos...");
        b_cedula_e = e_cedula.getText().length()>6 && !e_cedula.getText().equalsIgnoreCase("Cedula...");
        
        if(
                grado.getSelectedItem() == "PRIMER GRUPO" || grado.getSelectedItem() == "SEGUNDO GRUPO" || grado.getSelectedItem() == "TERCER GRUPO" || 
                grado.getSelectedItem() == "PRIMER GRADO" || grado.getSelectedItem() == "SEGUNDO GRADO" || grado.getSelectedItem() == "TERCER GRADO"){
            b_cedula_e = true;
        }
        
        if(!b_nombre_e || !b_apellido_e || !b_cedula_e){
            res = false;
        }
        
        return res;
    }
    
    public String getMensaje(){
        String mensaje = "", representante = "";
        
        if(b_check_m){
            representante = "MADRE";
            
            b_nombre_r = r_nombre_m.getText().length()>2 && !r_nombre_m.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_m.getText().length()>6 && !r_cedula_m.getText().equalsIgnoreCase("C.I. de la Madre...");
            b_tlf_habitacion = r_tlf_habitacion_m.getText().length()>10 && !r_tlf_habitacion_m.getText().equalsIgnoreCase("Tlf. Habitación de la Madre...");
            b_tlf_celular = r_tlf_celular_m.getText().length()>10 && !r_tlf_celular_m.getText().equalsIgnoreCase("Tlf. Celular de la Madre...");
            b_direccion_r = r_direccion_m.getText().length()>5 && !r_direccion_m.getText().equalsIgnoreCase("Dir. de Habitación de la Madre...");
            
            if(!b_nombre_r){
                mensaje += "Nombre de Representante "+representante+" no Válido. Al menos 3 caracteres.\n";
            }
            if(!b_cedula_r){
                mensaje += "Cédula de Representante "+representante+" no Válida. Al menos 7 digitos.\n";
            }
            if(!b_direccion_r){
                mensaje += "Dirección de Representante "+representante+" no Válida. Al menos 5 caracteres.\n";
            }
            if(!b_tlf_habitacion){
                mensaje += "Teléfono de Habitación de Representante "+representante+" no Válido.\n";
            }
            if(!b_tlf_celular){
                mensaje += "Teléfono Celular de Representante "+representante+" no Válido.\n";
            }
            
        }
        if(b_check_p){
            representante = "PADRE";
            
            b_nombre_r = r_nombre_p.getText().length()>2 && !r_nombre_p.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_p.getText().length()>6 && !r_cedula_p.getText().equalsIgnoreCase("C.I. del Padre...");
            b_tlf_habitacion = r_tlf_habitacion_p.getText().length()>10 && !r_tlf_habitacion_p.getText().equalsIgnoreCase("Tlf. Habitación del Padre...");
            b_tlf_celular = r_tlf_celular_p.getText().length()>10 && !r_tlf_celular_p.getText().equalsIgnoreCase("Tlf. Celular del Padre...");
            b_direccion_r = r_direccion_p.getText().length()>5 && !r_direccion_p.getText().equalsIgnoreCase("Dir. de Habitación del Padre...");
            
            if(!b_nombre_r){
                mensaje += "Nombre de Representante "+representante+" no Válido. Al menos 3 caracteres.\n";
            }
            if(!b_cedula_r){
                mensaje += "Cédula de Representante "+representante+" no Válida. Al menos 7 digitos.\n";
            }
            if(!b_direccion_r){
                mensaje += "Dirección de Representante "+representante+" no Válida. Al menos 5 caracteres.\n";
            }
            if(!b_tlf_habitacion){
                mensaje += "Teléfono de Habitación de Representante "+representante+" no Válido.\n";
            }
            if(!b_tlf_celular){
                mensaje += "Teléfono Celular de Representante "+representante+" no Válido.\n";
            }
        }
        if(b_check_l){
            representante = "LEGAL";
            
            b_nombre_r = r_nombre_l.getText().length()>2 && !r_nombre_l.getText().equalsIgnoreCase("Nombres y Apellidos...");
            b_cedula_r = r_cedula_l.getText().length()>6 && !r_cedula_l.getText().equalsIgnoreCase("C.I. del Representante Legal...");
            b_tlf_habitacion = r_tlf_habitacion_l.getText().length()>10 && !r_tlf_habitacion_l.getText().equalsIgnoreCase("Tlf. Habitación del R. Legal...");
            b_tlf_celular = r_tlf_celular_l.getText().length()>10 && !r_tlf_celular_l.getText().equalsIgnoreCase("Tlf. Celular del R. Legal...");
            b_direccion_r = r_direccion_l.getText().length()>5 && !r_direccion_l.getText().equalsIgnoreCase("Dir. de Habitación del R. Legal...");
            b_parentesco_l = r_parentesco_l.getText().length()>2 && !r_parentesco_l.getText().equalsIgnoreCase("Parentesco del R. Legal...");
            
            if(!b_nombre_r){
                mensaje += "Nombre de Representante "+representante+" no Válido. Al menos 3 caracteres.\n";
            }
            if(!b_cedula_r){
                mensaje += "Cédula de Representante "+representante+" no Válida. Al menos 7 digitos.\n";
            }
            if(!b_direccion_r){
                mensaje += "Dirección de Representante "+representante+" no Válida. Al menos 5 caracteres.\n";
            }
            if(!b_tlf_habitacion){
                mensaje += "Teléfono de Habitación de Representante "+representante+" no Válido.\n";
            }
            if(!b_tlf_celular){
                mensaje += "Teléfono Celular de Representante "+representante+" no Válido.\n";
            }
            if(!b_parentesco_l){
                mensaje += "Parentesco no Válido. Al menos 3 caracteres.\n";
            }
        }
        
        return mensaje;
    }
    
    public void limpiarCampos(){
        cb_madre.setSelected(false);
        cb_padre.setSelected(false);
        cb_legal.setSelected(false);
        cedula_r_combo[0].setSelectedIndex(0);
        cedula_r_combo[1].setSelectedIndex(0);
        cedula_r_combo[2].setSelectedIndex(0);
        r_cedula_m.setText("C.I. de la Madre...");
        r_cedula_p.setText("C.I. del Padre...");
        r_cedula_l.setText("C.I. del Representante Legal...");
        r_nombre_m.setText("Nombres y Apellidos...");
        r_nombre_p.setText("Nombres y Apellidos...");
        r_nombre_l.setText("Nombres y Apellidos...");
        r_direccion_m.setText("Dir. de Habitación de la Madre...");
        r_tlf_habitacion_m.setText("Tlf. Habitación de la Madre...");
        r_tlf_celular_m.setText("Tlf. Celular de la Madre...");
        r_direccion_p.setText("Dir. de Habitación del Padre...");
        r_tlf_habitacion_p.setText("Tlf. Habitación del Padre...");
        r_tlf_celular_p.setText("Tlf. Celular del Padre...");
        r_direccion_l.setText("Dir. de Habitación del R. Legal...");
        r_tlf_habitacion_l.setText("Tlf. Habitación del R. Legal...");
        r_tlf_celular_l.setText("Tlf. Celular del R. Legal...");
        r_parentesco_l.setText("Parentesco del R. Legal...");
        e_nombre.setText("Nombres...");
        e_apellido.setText("Apellidos...");
        e_cedula.setText("Cedula...");
        cedula_combo.setSelectedIndex(0);
        grado.setSelectedIndex(0);
        dia.setSelectedIndex(0);
        mes.setSelectedIndex(0);
        año.setSelectedIndex(0);
    }
    
    public void setMaximo(JTextField campo, int limite){
        campo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }

            @Override
            public void keyReleased(KeyEvent ke) {
                if(ke.getKeyCode()!=ke.VK_BACK_SPACE)
                    if(campo.getText().length()==limite-1) 
                        ke.consume();
            }
        });
    }
    
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource().equals(grado)){
            cambiarFecha((String)(grado.getSelectedItem()));
            if(mostrarGrado((String)(grado.getSelectedItem())))
                registrar.setEnabled(true);
            else
                registrar.setEnabled(false);
        }
    }
    
    public boolean mostrarGrado(String Grado){
        String periodo = "";
        Calendar c = Calendar.getInstance();
        int mes_actual = (c.get(Calendar.MONTH));
        int año_actual = (c.get(Calendar.YEAR));
        if((mes_actual+1)>6)
            periodo = (año_actual)+"-"+(año_actual+1);
        else
            periodo = (año_actual-1)+"-"+(año_actual);

        int cuposDisp = Ventana.cuposDisponibles(Grado,periodo);
        
        if(cuposDisp>0) 
            return true;
        else
            return false;
    }
    
    public void cambiarFecha(String grado){
        año.removeAllItems();
        año.addItem("Año Nac.");
        if(grado.equalsIgnoreCase("PRIMER GRUPO")){
            for(int i=(c.get(Calendar.YEAR)-4);i<=(c.get(Calendar.YEAR)-2);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("SEGUNDO GRUPO")){
            for(int i=(c.get(Calendar.YEAR)-5);i<=(c.get(Calendar.YEAR)-3);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("TERCER GRUPO")){
            for(int i=(c.get(Calendar.YEAR)-6);i<=(c.get(Calendar.YEAR)-4);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("PRIMER GRADO")){
            for(int i=(c.get(Calendar.YEAR)-7);i<=(c.get(Calendar.YEAR)-5);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("SEGUNDO GRADO")){
            for(int i=(c.get(Calendar.YEAR)-8);i<=(c.get(Calendar.YEAR)-6);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("TERCER GRADO")){
            for(int i=(c.get(Calendar.YEAR)-9);i<=(c.get(Calendar.YEAR)-7);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("CUARTO GRADO")){
            for(int i=(c.get(Calendar.YEAR)-10);i<=(c.get(Calendar.YEAR)-8);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("QUINTO GRADO")){
            for(int i=(c.get(Calendar.YEAR)-11);i<=(c.get(Calendar.YEAR)-9);i++){
                año.addItem(""+(i));
            }
        }
        if(grado.equalsIgnoreCase("SEXTO GRADO")){
            for(int i=(c.get(Calendar.YEAR)-12);i<=(c.get(Calendar.YEAR)-10);i++){
                año.addItem(""+(i));
            }
        }
    }
    
    public void enabledInputs(String representante){
        if(representante.equals("m")){
            b_check_m = true;
            r_nombre_m.setBackground(Color.WHITE);
            r_nombre_m.setEnabled(true);
            
            cedula_r_combo[0].setEnabled(true);
            r_cedula_m.setBackground(Color.WHITE);
            r_cedula_m.setEnabled(true);
                        
            r_tlf_habitacion_m.setBackground(Color.WHITE);
            r_tlf_habitacion_m.setEnabled(true);
                        
            r_tlf_celular_m.setBackground(Color.WHITE);
            r_tlf_celular_m.setEnabled(true);
                        
            r_direccion_m.setBackground(Color.WHITE);
            r_direccion_m.setEnabled(true);       
        }
        else if(representante.equals("p")){
            b_check_p = true;
            
            r_nombre_p.setBackground(Color.WHITE);
            r_nombre_p.setEnabled(true);
            
            cedula_r_combo[1].setEnabled(true);
            r_cedula_p.setBackground(Color.WHITE);
            r_cedula_p.setEnabled(true);
            
            r_tlf_habitacion_p.setBackground(Color.WHITE);
            r_tlf_habitacion_p.setEnabled(true);
            
            r_tlf_celular_p.setBackground(Color.WHITE);
            r_tlf_celular_p.setEnabled(true);
            
            r_direccion_p.setBackground(Color.WHITE);
            r_direccion_p.setEnabled(true);
        }
        else if(representante.equals("l")){
            b_check_l = true;
            
            r_nombre_l.setBackground(Color.WHITE);
            r_nombre_l.setEnabled(true);
            
            cedula_r_combo[2].setEnabled(true);
            r_cedula_l.setBackground(Color.WHITE);
            r_cedula_l.setEnabled(true);
            
            r_tlf_habitacion_l.setBackground(Color.WHITE);
            r_tlf_habitacion_l.setEnabled(true);
            
            r_tlf_celular_l.setBackground(Color.WHITE);
            r_tlf_celular_l.setEnabled(true);
            
            r_direccion_l.setBackground(Color.WHITE);
            r_direccion_l.setEnabled(true);
            
            r_parentesco_l.setBackground(Color.WHITE);
            r_parentesco_l.setEnabled(true);
        }
    }
    
    public void disabledInputs(String representante){
        if(representante.equals("m")){
            b_check_m = false;
            
            r_nombre_m.setBackground(disabledColor);
            r_nombre_m.setEnabled(false);
            
            cedula_r_combo[0].setEnabled(false);
            r_cedula_m.setBackground(disabledColor);
            r_cedula_m.setEnabled(false);

            r_tlf_habitacion_m.setBackground(disabledColor);
            r_tlf_habitacion_m.setEnabled(false);

            r_tlf_celular_m.setBackground(disabledColor);
            r_tlf_celular_m.setEnabled(false);

            r_direccion_m.setBackground(disabledColor);
            r_direccion_m.setEnabled(false);
        }
        else if(representante.equals("p")){
            b_check_p = false;
            
            r_nombre_p.setBackground(disabledColor);
            r_nombre_p.setEnabled(false);
            
            cedula_r_combo[1].setEnabled(false);
            r_cedula_p.setBackground(disabledColor);
            r_cedula_p.setEnabled(false);

            r_tlf_habitacion_p.setBackground(disabledColor);
            r_tlf_habitacion_p.setEnabled(false);

            r_tlf_celular_p.setBackground(disabledColor);
            r_tlf_celular_p.setEnabled(false);

            r_direccion_p.setBackground(disabledColor);
            r_direccion_p.setEnabled(false);
        }
        else if(representante.equals("l")){
            b_check_l = false;
            
            r_nombre_l.setBackground(disabledColor);
            r_nombre_l.setEnabled(false);
            
            cedula_r_combo[2].setEnabled(false);
            r_cedula_l.setBackground(disabledColor);
            r_cedula_l.setEnabled(false);

            r_tlf_habitacion_l.setBackground(disabledColor);
            r_tlf_habitacion_l.setEnabled(false);

            r_tlf_celular_l.setBackground(disabledColor);
            r_tlf_celular_l.setEnabled(false);

            r_direccion_l.setBackground(disabledColor);
            r_direccion_l.setEnabled(false);
            
            r_parentesco_l.setBackground(disabledColor);
            r_parentesco_l.setEnabled(false);
        }
    }
}
