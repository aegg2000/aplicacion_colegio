package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Registro_Pago extends JPanel{
    public int Ancho, Alto, pos_y, LabelFuenteSize;
    public List<String> Estudiantes;
    public JComboBox Lista_Estudiantes;
    public JLabel ph_comprobante, ph_motivo, ph_descripcion, ph_monto;
    public JLabel comprobanteLabel, conceptoLabel, descripcionLabel, montoLabel, deudaLabel, pendienteLabel;
    public JTextField comprobante, concepto, descripcion, monto, deuda, pendiente;
    public JButton Opcion, Registrar, filtro_estudiante;
    public Ventana_Principal Ventana;
    
    public Color disabledColor;
    
    public Registro_Pago(Ventana_Principal Ventana, int Anc, int Alt){
        Ancho = Anc;
        Alto = Alt;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setBackground(Color.WHITE);
        setLayout(null);
        
        disabledColor = new Color(244, 244, 244);
        
        Estudiantes = Ventana.obtenerListaEstudiante();
        pos_y = 5;
        LabelFuenteSize = 16;
        
        //LISTA ESTUDIANTE
        Lista_Estudiantes = new JComboBox();
        Lista_Estudiantes.addItem("Seleccione un Estudiante...");
        for(int i=0;i<Estudiantes.size();i+=4){
            Lista_Estudiantes.addItem(Estudiantes.get(i)+"-"+Estudiantes.get(i+1)+" "+Estudiantes.get(i+2)+"-"+Estudiantes.get(i+3));
        }
        Lista_Estudiantes.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        Lista_Estudiantes.setLocation((int)(Ancho*0.1),pos_y);
        Lista_Estudiantes.setFocusable(false);
        Lista_Estudiantes.setBackground(Color.WHITE);
        Lista_Estudiantes.setFont(new Font("Arial",Font.BOLD,14));
        Lista_Estudiantes.setForeground(Color.GRAY);
        add(Lista_Estudiantes);
        
        pos_y += (int)(Alto*0.08)+5;
        
        //ph_comprobante        
        comprobanteLabel = new JLabel("Número de recibo: ");
        comprobanteLabel.setSize(Ancho, (int)(Alto*0.08));
        comprobanteLabel.setLocation((int)(Ancho*0.1), pos_y);
        comprobanteLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(comprobanteLabel); 
        
        ph_comprobante = new JLabel("   Número de recibo...");
        ph_comprobante.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        ph_comprobante.setLocation((int)(Ancho*0.24),pos_y);
        ph_comprobante.setForeground(Color.GRAY);
        ph_comprobante.setFont(new Font("Arial",Font.ITALIC,16));
        add(ph_comprobante);       
        
        //comprobante
        comprobante = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        comprobante.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        comprobante.setLocation((int)(Ancho*0.24),pos_y);
        comprobante.setBackground(Color.WHITE);
        comprobante.setForeground(Color.GRAY);
        comprobante.setFont(new Font("Arial",Font.ITALIC,16));
        comprobante.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
                if(!comprobante.getText().equalsIgnoreCase("")){
                    ph_comprobante.setVisible(false);
                }else{
                    ph_comprobante.setVisible(true);
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
                if(!comprobante.getText().equalsIgnoreCase("")){
                    ph_comprobante.setVisible(false);
                }else{
                    ph_comprobante.setVisible(true);
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        comprobante.setEditable(true);
        add(comprobante);
        
        pos_y += (int)(Alto*0.08)+5;
        
        //ph_motivo
        conceptoLabel = new JLabel("Concepto: ");
        conceptoLabel.setSize(Ancho, (int)(Alto*0.08));
        conceptoLabel.setLocation((int)(Ancho*0.1), pos_y);
        conceptoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(conceptoLabel); 
        
        ph_motivo = new JLabel("   Concepto...");
        ph_motivo.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        ph_motivo.setLocation((int)(Ancho*0.24),pos_y);
        ph_motivo.setForeground(Color.GRAY);
        ph_motivo.setFont(new Font("Arial",Font.ITALIC,16));
        ph_motivo.setVisible(false);
        add(ph_motivo);
        
        //motivo
        concepto = new JTextField("PAGO DE MENSUALIDAD"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        concepto.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        concepto.setLocation((int)(Ancho*0.24),pos_y);
        concepto.setBackground(disabledColor);
        concepto.setForeground(Color.GRAY);
        concepto.setFont(new Font("Arial",Font.ITALIC,16));
        concepto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(!concepto.getText().equalsIgnoreCase("")){
                    ph_motivo.setVisible(false);
                }else{
                    ph_motivo.setVisible(true);
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(!concepto.getText().equalsIgnoreCase("")){
                    ph_motivo.setVisible(false);
                }else{
                    ph_motivo.setVisible(true);
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        concepto.setEditable(false);
        add(concepto);
        
        //motivo
//        Opcion = new JButton(Ventana.cargarImagen("opcion.png",(int)(Ancho*0.05),(int)(Alto*0.08)));
//        Opcion.setRolloverIcon(Ventana.cargarImagen("opcion_hover.png",(int)(Ancho*0.05),(int)(Alto*0.08)));
//        Opcion.setSize((int)(Ancho*0.05),(int)(Alto*0.08));
//        Opcion.setLocation((int)(Ancho*0.85),pos_y);
//        Opcion.setContentAreaFilled(false);
//        Opcion.setFocusable(false);
//        Opcion.addMouseListener(new MouseListener() {
//            @Override
//            public void mouseClicked(MouseEvent me) {
//                String[] OPCION = { concepto.getText(),"PAGO MENSUALIDAD" };
//                concepto.setText(
//                (String) JOptionPane.showInputDialog(null, 
//                    "¿Concepto de Pago?",
//                    "Concepto de Pago",
//                    JOptionPane.QUESTION_MESSAGE, 
//                    null, 
//                    OPCION, 
//                    OPCION[0])
//                );
//                if(concepto.getText().equalsIgnoreCase("PAGO MENSUALIDAD")){
//                    monto.setText(Double.toString(Ventana.menu_home.Configuracion.montoMes));
//                    ph_monto.setVisible(false);
//                }else{
//                    monto.setText("");
//                    ph_monto.setVisible(true);                    
//                }
//                if(concepto.getText().equalsIgnoreCase("")){
//                    ph_motivo.setVisible(true);
//                }
//                if(!concepto.getText().equalsIgnoreCase("")){
//                    ph_motivo.setVisible(false);
//                }else{
//                    ph_motivo.setVisible(true);
//                }
//            }
//
//            @Override
//            public void mousePressed(MouseEvent me) {
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent me) {
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent me) {
//            }
//
//            @Override
//            public void mouseExited(MouseEvent me) {
//            }
//        });
//        Opcion.setCursor(new Cursor(Cursor.HAND_CURSOR));
//        add(Opcion);
        
        pos_y += (int)(Alto*0.08)+5;
        
        //ph_descripcion
        descripcionLabel = new JLabel("Descripción: ");
        descripcionLabel.setSize(Ancho, (int)(Alto*0.08));
        descripcionLabel.setLocation((int)(Ancho*0.1), pos_y);
        descripcionLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(descripcionLabel); 
        
        ph_descripcion = new JLabel("   Descripción...");
        ph_descripcion.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        ph_descripcion.setLocation((int)(Ancho*0.24),pos_y);
        ph_descripcion.setForeground(Color.GRAY);
        ph_descripcion.setFont(new Font("Arial",Font.ITALIC,16));
        add(ph_descripcion);
        
        //descripcion
        descripcion = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        descripcion.setSize((int)(Ancho*0.66),(int)(Alto*0.08));
        descripcion.setLocation((int)(Ancho*0.24),pos_y);
        descripcion.setBackground(Color.WHITE);
        descripcion.setForeground(Color.GRAY);
        descripcion.setFont(new Font("Arial",Font.ITALIC,16));
        descripcion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(!descripcion.getText().equalsIgnoreCase("")){
                    ph_descripcion.setVisible(false);
                }else{
                    ph_descripcion.setVisible(true);
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(!descripcion.getText().equalsIgnoreCase("")){
                    ph_descripcion.setVisible(false);
                }else{
                    ph_descripcion.setVisible(true);
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        descripcion.setEditable(true);
        add(descripcion);
        
        pos_y += (int)(Alto*0.08)+5;
        
        //ph_monto
        montoLabel = new JLabel("Monto a Pagar: ");
        montoLabel.setSize(Ancho, (int)(Alto*0.08));
        montoLabel.setLocation((int)(Ancho*0.1), pos_y);
        montoLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(montoLabel); 
        
        ph_monto = new JLabel("   Monto...");
        ph_monto.setSize((int)(Ancho*0.29),(int)(Alto*0.08));
        ph_monto.setLocation((int)(Ancho*0.24),pos_y);
        ph_monto.setForeground(Color.GRAY);
        ph_monto.setFont(new Font("Arial",Font.ITALIC,16));
        add(ph_monto);
        
        //monto
        monto = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        monto.setSize((int)(Ancho*0.29),(int)(Alto*0.08));
        monto.setLocation((int)(Ancho*0.24),pos_y);
        monto.setBackground(Color.WHITE);
        monto.setForeground(Color.GRAY);
        monto.setFont(new Font("Arial",Font.ITALIC,16));
        monto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if(!monto.getText().equalsIgnoreCase("")){
                    ph_monto.setVisible(false);
                }else{
                    ph_monto.setVisible(true);
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if(!monto.getText().equalsIgnoreCase("")){
                    ph_monto.setVisible(false);
                }else{
                    ph_monto.setVisible(true);
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        monto.setEditable(true);
        add(monto);
        
        //ph_monto
        deudaLabel = new JLabel("Deuda: ");
        deudaLabel.setSize(Ancho, (int)(Alto*0.08));
        deudaLabel.setLocation((int)(Ancho*0.55), pos_y);
        deudaLabel.setFont(new Font("Arial", Font.ITALIC, LabelFuenteSize));
        add(deudaLabel); 
        
        //monto
        deuda = new JTextField(){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        deuda.setSize((int)(Ancho*0.29),(int)(Alto*0.08));
        deuda.setLocation((int)(Ancho*0.61),pos_y);
        deuda.setBackground(disabledColor);
        deuda.setForeground(Color.GRAY);
        deuda.setFont(new Font("Arial",Font.ITALIC,16));
        deuda.setEditable(false);
        add(deuda);
        
        pos_y += (int)(Alto*0.08)+10;
        
        //motivo
        Registrar = new JButton("Registrar Pago"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        Registrar.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        Registrar.setLocation((int)(Ancho*0.1),pos_y);
        Registrar.setForeground(Color.BLACK);
        Registrar.setFont(new Font("Arial",Font.BOLD,16));
        Registrar.setContentAreaFilled(false);
        Registrar.setFocusable(false);
        Registrar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                String mensaje = "";
                String estudiante = (String)(Lista_Estudiantes.getSelectedItem());
                if(estudiante.equalsIgnoreCase("Seleccione un Estudiante...")){
                    mensaje += "Aun no ha seleccionado a un estudiante.\n";
                }
                if(comprobante.getText().length()<6){
                    mensaje += "Comprobante vacío o inválido. Al menos 6 dígitos.\n";
                }
                if(concepto.getText().length()<4){
                    mensaje += "Motivo vacío o inválido. Al menos 4 caracteres.\n";
                }
                if(descripcion.getText().length()<4){
                    mensaje += "Descripción vacío o inválido. Al menos 4 caracteres.\n";
                }
                if(monto.getText().length()<4){
                    mensaje += "Monto vacío o inválido. Al menos una cifra de 4 dígitos.";
                }
                if(mensaje.length()>0){
                    JOptionPane.showMessageDialog(null,mensaje,"ERROR DE REGISTRO DE PAGO",JOptionPane.ERROR_MESSAGE);
                }else{
                    Ventana.registrarPago(Registro_Pago.this,
                        getIdentidicador(estudiante),
                        comprobante.getText(),
                        concepto.getText(),
                        descripcion.getText(),
                        monto.getText()
                    );
                    comprobante.setText("");
                    ph_comprobante.setVisible(true);
//                    concepto.setText("");
//                    ph_motivo.setVisible(true);
                    descripcion.setText("");
                    ph_descripcion.setVisible(true);
                    Double deuda_val = Double.parseDouble(deuda.getText());
                    Double monto_val = Double.parseDouble(monto.getText());
                    Double new_deuda = deuda_val - monto_val;
                    monto.setText(new_deuda.toString());            
                    deuda.setText(new_deuda.toString());
//                    ph_monto.setVisible(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                Registrar.setFont(new Font("Arial",Font.BOLD,20));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                Registrar.setFont(new Font("Arial",Font.BOLD,16));
            }
        });
        Registrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        add(Registrar);
    }
    
    public void actualizarListaEstudiantes(){
        Estudiantes = Ventana.obtenerListaEstudiante();
        Lista_Estudiantes.removeAllItems();
        for(int i=0;i<Estudiantes.size();i+=4){
            Lista_Estudiantes.addItem(Estudiantes.get(i)+"-"+Estudiantes.get(i+1)+" "+Estudiantes.get(i+2)+"-"+Estudiantes.get(i+3));
        }
    }
    
    public void actualizarListaEstudianteID(String ID){
        Estudiantes = Ventana.obtenerEstudiante(ID);
        Lista_Estudiantes.removeAllItems();
        for(int i=0;i<Estudiantes.size();i+=4){
            Lista_Estudiantes.addItem(Estudiantes.get(i)+"-"+Estudiantes.get(i+1)+" "+Estudiantes.get(i+2)+"-"+Estudiantes.get(i+3));
        }
    }
    
    public void actualizarListaEstudiantes(String grado){
        Estudiantes = Ventana.obtenerListaEstudiante(grado);
        Lista_Estudiantes.removeAllItems();
        for(int i=0;i<Estudiantes.size();i+=4){
            Lista_Estudiantes.addItem(Estudiantes.get(i)+"-"+Estudiantes.get(i+1)+" "+Estudiantes.get(i+2)+"-"+Estudiantes.get(i+3));
        }
    }
    
    public String getIdentidicador(String estudiante){
        return estudiante.split(Pattern.quote("-"))[0];
    }
    
    public String getEstudiante(String estudiante){
        return estudiante.split(Pattern.quote("-"))[1];
    }
    
    public String getGrado(String estudiante){
        return estudiante.split(Pattern.quote("-"))[2];
    }
    
    public void limpiarCampos(){
        Lista_Estudiantes.setSelectedIndex(0);
        comprobante.setText("");
//        concepto.setText("");
        ph_comprobante.setVisible(true);
//        ph_motivo.setVisible(true);
    }
}
