package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class Estudiante_Resultado extends JPanel{
    public int Ancho, Alto;
    public List<String> estudiantes;
    public JLabel [] lista;
    public JPanel panel, panel_seleccion;
    public JScrollPane scroll;
    public int pos_y;
    public JComboBox lista_seleccion;
    public JLabel titulo_seleccion;
    public JButton regresar;
    public String identificador, opcion, cargo;
    
    public Estudiante_Resultado(String cargo, List<String> estudiantes){
        Ancho = 400;
        Alto = 150;
        this.estudiantes = estudiantes;
        this.cargo = cargo;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setBackground(Color.WHITE);
        setLayout(null);
        
        identificador = "";
        
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        panel.setLocation(0,0);
        panel.setSize(Ancho,20*estudiantes.size());
        
        scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setSize(Ancho,Alto);
        scroll.setLocation(0,0);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        scroll.setViewportView(panel);
        add(scroll);
        
        //*************PANEL SELECCION*****************
        panel_seleccion = new JPanel();
        panel_seleccion.setLayout(null);
        panel_seleccion.setBackground(Color.WHITE);
        panel_seleccion.setLocation(0,0);
        panel_seleccion.setVisible(false);
        panel_seleccion.setSize(Ancho,Alto);
        add(panel_seleccion);
        
        titulo_seleccion = new JLabel("¿Qué desea hacer?");
        titulo_seleccion.setSize(Ancho,30);
        titulo_seleccion.setLocation(0,0);
        titulo_seleccion.setHorizontalAlignment(SwingConstants.CENTER);
        titulo_seleccion.setForeground(Color.GRAY);
        titulo_seleccion.setFont(new Font("Arial",Font.BOLD,18));
        titulo_seleccion.setBackground(Color.WHITE);
        panel_seleccion.add(titulo_seleccion);
        
        lista_seleccion = new JComboBox();
        lista_seleccion.setSize(Ancho,30);
        lista_seleccion.setLocation(0,40);
        
        lista_seleccion.setFocusable(false);
        
        lista_seleccion.setBackground(Color.WHITE);
        lista_seleccion.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lista_seleccion.setFont(new Font("Arial",Font.PLAIN,14));
        lista_seleccion.setForeground(Color.BLACK);
        panel_seleccion.add(lista_seleccion);
        
        regresar = new JButton("Regresar"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.WHITE);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        regresar.setSize((int)(Ancho*0.5),30);
        regresar.setLocation((int)(Ancho*0.25),85);
        regresar.setFocusable(false);
        regresar.setContentAreaFilled(false);
        regresar.setFont(new Font("Arial",Font.PLAIN,14));
        regresar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                regresar.setFont(new Font("Arial",Font.BOLD,16));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                regresar.setFont(new Font("Arial",Font.PLAIN,14));
            }
        });
        regresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                panel_seleccion.setVisible(false);
                scroll.setVisible(true);
                identificador = "";
            }
        });
        regresar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        panel_seleccion.add(regresar);
        //*********************************************
        pos_y = 0;
        
        lista = new JLabel[estudiantes.size()];
        for(int i=0;i<lista.length;i++){
            int x = i;
            String estudiante = estudiantes.get(i);
            System.out.println(estudiante);
            System.out.println(estudiante.substring(estudiante.length()-12, estudiante.length()));
            lista[i] = new JLabel(estudiantes.get(i));
            lista[i].setSize(Ancho,20);
            lista[i].setLocation(0,pos_y);
            if("INHABILITADO".equals(estudiante.substring(estudiante.length()-12, estudiante.length()))){
                lista[i].setForeground(Color.RED);
            }else{
                lista[i].setForeground(Color.GRAY);
            }            
            lista[i].setFont(new Font("Arial",Font.PLAIN,12));
            lista[i].setBackground(Color.WHITE);
            lista[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            lista[i].setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
            lista[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                    if(me.getClickCount()==2){
                        lista_seleccion.removeAllItems();
                        scroll.setVisible(false);
                        panel_seleccion.setVisible(true);
                        identificador = estudiantes.get(x).split(" ")[0];
                        boolean bool = false;
                        if(estudiantes.get(x).contains("PRIMER GRUPO") || estudiantes.get(x).contains("SEGUNDO GRUPO") || estudiantes.get(x).contains("TERCER GRUPO")){
                            bool = true;
                        }
                        if("PROFESOR".equals(cargo)){
                            if(!bool){
                                lista_seleccion.addItem("Generar Boletín de Calificaciones");
                                lista_seleccion.addItem("Gestionar Notas");
                            }
                        }else if("INHABILITADO".equals(estudiante.substring(estudiante.length()-12, estudiante.length()))){
                            if(!bool)
                                lista_seleccion.addItem("Generar Boletín de Calificaciones");
                            lista_seleccion.addItem("Generar Reporte de Pagos");
                            lista_seleccion.addItem("Generar Constancia de Promoción");
                            lista_seleccion.addItem("Generar Constancia de Estudio");
                            if(!bool)
                                lista_seleccion.addItem("Gestionar Notas");                          
                        }else{
                            if(!bool)
                                lista_seleccion.addItem("Generar Boletín de Calificaciones");
                            lista_seleccion.addItem("Generar Reporte de Pagos");
                            lista_seleccion.addItem("Generar Constancia de Promoción");
                            lista_seleccion.addItem("Generar Constancia de Estudio");
                            if(!bool)
                                lista_seleccion.addItem("Gestionar Notas");
                            lista_seleccion.addItem("Registrar Pago");
                            lista_seleccion.addItem("Inhabilitar Estudiante");
                        }                        
                    }
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    lista[x].setFont(new Font("Arial",Font.BOLD,13));
                    lista[x].setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    lista[x].setFont(new Font("Arial",Font.PLAIN,12));
                    lista[x].setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
                }
            });
            panel.add(lista[i]);
            
            pos_y += 20;
        }
    }
}
