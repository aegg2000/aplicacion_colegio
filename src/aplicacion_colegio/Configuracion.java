package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class Configuracion extends JPanel{
    public Ventana_Principal Ventana;
    public Menu_Home Menu;
    public Respaldo Respaldo;
    public int Ancho, Alto, btnSize, btnHoverSize;
    public double montoMes, montoInscripcion;
    
    public JLabel label_monto, label_monto_inscripcion;
    public JTextField tf_monto, tf_monto_inscripcion;
    public JButton actualizar, actualizar_inscripcion, respaldo, respaldar, emitirPago;
    
    public Configuracion(Menu_Home Menu,Ventana_Principal Ventana,int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        this.Menu = Menu;
        this.Ventana = Ventana;
        Respaldo = new Respaldo(Ventana);
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        setBackground(Color.WHITE);
        
        montoMes = 0;
        montoInscripcion = 0;
        btnSize = 20;
        btnHoverSize = 24;
        
        label_monto = new JLabel("Monto Mensualidad");
        label_monto.setSize((int)(Ancho*0.3), (int)(Alto*0.1));
        label_monto.setLocation((int)(Ancho*0.05), (int)(Alto*0.04));
        label_monto.setFont(new Font("Arial",Font.BOLD,18));
        label_monto.setForeground(Color.GRAY);
        add(label_monto);
        
        tf_monto = new JTextField(Double.toString(montoMes));
        tf_monto.setSize((int)(Ancho*0.19),(int)(Alto*0.07));
        tf_monto.setLocation((int)(Ancho*0.23), (int)(Alto*0.05));
        tf_monto.setBackground(Color.WHITE);
        tf_monto.setForeground(Color.GRAY);
        tf_monto.setFont(new Font("Arial",Font.PLAIN,16));
        tf_monto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        add(tf_monto);
        
        actualizar = new JButton("Actualizar"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        actualizar.setSize((int)(Ancho*0.16),(int)(Alto*0.07));
        actualizar.setLocation((int)(Ancho*0.44),(int)(Alto*0.05));
        actualizar.setForeground(Color.GRAY);
        actualizar.setFont(new Font("Arial",Font.BOLD,btnSize));
        actualizar.setContentAreaFilled(false);
        actualizar.setFocusable(false);
        actualizar.setEnabled(true);
        actualizar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        actualizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                setMontoMes();
            }
        });
        actualizar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                actualizar.setFont(new Font("Arial",Font.BOLD,btnHoverSize));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                actualizar.setFont(new Font("Arial",Font.BOLD,btnSize));
            }
        });
        add(actualizar);
        
        emitirPago = new JButton("Emitir Pago Mensualidad"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        emitirPago.setSize((int)(Ancho*0.31),(int)(Alto*0.07));
        emitirPago.setLocation((int)(Ancho*0.61),(int)(Alto*0.05));
        emitirPago.setForeground(Color.GRAY);
        emitirPago.setFont(new Font("Arial",Font.BOLD,btnSize));
        emitirPago.setContentAreaFilled(false);
        emitirPago.setFocusable(false);
        emitirPago.setEnabled(true);
        emitirPago.setCursor(new Cursor(Cursor.HAND_CURSOR));
        emitirPago.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                emitirPago();
            }
        });
        emitirPago.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                emitirPago.setFont(new Font("Arial",Font.BOLD,btnHoverSize));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                emitirPago.setFont(new Font("Arial",Font.BOLD,btnSize));
            }
        });
        add(emitirPago);
        
        //Monto Inscripcion
        label_monto_inscripcion = new JLabel("Monto Inscripción");
        label_monto_inscripcion.setSize((int)(Ancho*0.3), (int)(Alto*0.1));
        label_monto_inscripcion.setLocation((int)(Ancho*0.05), (int)(Alto*0.14));
        label_monto_inscripcion.setFont(new Font("Arial",Font.BOLD,18));
        label_monto_inscripcion.setForeground(Color.GRAY);
        add(label_monto_inscripcion);
        
        tf_monto_inscripcion = new JTextField(Double.toString(montoInscripcion));
        tf_monto_inscripcion.setSize((int)(Ancho*0.19),(int)(Alto*0.07));
        tf_monto_inscripcion.setLocation((int)(Ancho*0.23), (int)(Alto*0.15));
        tf_monto_inscripcion.setBackground(Color.WHITE);
        tf_monto_inscripcion.setForeground(Color.GRAY);
        tf_monto_inscripcion.setFont(new Font("Arial",Font.PLAIN,16));
        tf_monto_inscripcion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                char c = ke.getKeyChar();
                if (!(Character.isDigit(c) ||
                (c == KeyEvent.VK_BACK_SPACE) ||
                (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });
        add(tf_monto_inscripcion);
        
        actualizar_inscripcion = new JButton("Actualizar"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        actualizar_inscripcion.setSize((int)(Ancho*0.48),(int)(Alto*0.07));
        actualizar_inscripcion.setLocation((int)(Ancho*0.44),(int)(Alto*0.15));
        actualizar_inscripcion.setForeground(Color.GRAY);
        actualizar_inscripcion.setFont(new Font("Arial",Font.BOLD,btnSize));
        actualizar_inscripcion.setContentAreaFilled(false);
        actualizar_inscripcion.setFocusable(false);
        actualizar_inscripcion.setEnabled(true);
        actualizar_inscripcion.setCursor(new Cursor(Cursor.HAND_CURSOR));
        actualizar_inscripcion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                setMontoInscripcion();
            }
        });
        actualizar_inscripcion.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                actualizar_inscripcion.setFont(new Font("Arial",Font.BOLD,btnHoverSize));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                actualizar_inscripcion.setFont(new Font("Arial",Font.BOLD,btnSize));
            }
        });
        add(actualizar_inscripcion);
        
        respaldo = new JButton("Crear Respaldo Datos"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        respaldo.setSize((int)(Ancho*0.43),(int)(Alto*0.07));
        respaldo.setLocation((int)(Ancho*0.05),(int)(Alto*0.25));
        respaldo.setForeground(Color.GRAY);
        respaldo.setFont(new Font("Arial",Font.BOLD,btnSize));
        respaldo.setContentAreaFilled(false);
        respaldo.setFocusable(false);
        respaldo.setEnabled(true);
        respaldo.setCursor(new Cursor(Cursor.HAND_CURSOR));
        respaldo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Respaldo.crearRespaldo();
            }
        });
        respaldo.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                respaldo.setFont(new Font("Arial",Font.BOLD,btnHoverSize));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                respaldo.setFont(new Font("Arial",Font.BOLD,btnSize));
            }
        });
        add(respaldo);
        
        respaldar = new JButton("Respaldar Datos"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        respaldar.setSize((int)(Ancho*0.43),(int)(Alto*0.07));
        respaldar.setLocation((int)(Ancho*0.49),(int)(Alto*0.25));
        respaldar.setForeground(Color.GRAY);
        respaldar.setFont(new Font("Arial",Font.BOLD,btnSize));
        respaldar.setContentAreaFilled(false);
        respaldar.setFocusable(false);
        respaldar.setEnabled(true);
        respaldar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        respaldar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Respaldo.respaldar();
            }
        });
        respaldar.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                respaldar.setFont(new Font("Arial",Font.BOLD,btnHoverSize));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                respaldar.setFont(new Font("Arial",Font.BOLD,btnSize));
            }
        });
        add(respaldar);
        
        actualizarMontos();
    }
    
    public void actualizarMontos(){
        try{            
            String Query = "SELECT * FROM monto";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
           
            rs.next();
            montoMes = Double.parseDouble(rs.getString("monto"));
            rs.next();
            montoInscripcion = Double.parseDouble(rs.getString("monto"));
            
            
            tf_monto.setText(Double.toString(montoMes));
            tf_monto_inscripcion.setText(Double.toString(montoInscripcion));
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Obtener Monto Mes", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void emitirPago(){
        try{
            String monto = tf_monto.getText();
            String Query = "UPDATE monto SET monto='"+monto+"' WHERE id=1";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            Ventana.db.comando.executeUpdate(Query);

            montoMes = Double.parseDouble(monto);
            
            Query = "UPDATE estudiante SET deuda = "+montoMes+" + deuda";
            Ventana.db.comando.executeUpdate(Query);
            
            JOptionPane.showMessageDialog(null, "!Pago mensualidad generado exitosamente!",
                    "Emitir Pago Mensualidad", JOptionPane.PLAIN_MESSAGE);
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Emitir Pago Mensualidad", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void setMontoMes(){
        try{
            String monto = tf_monto.getText();
            String Query = "UPDATE monto SET monto='"+monto+"' WHERE id=1";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            Ventana.db.comando.executeUpdate(Query);

            montoMes = Double.parseDouble(monto);
            
            JOptionPane.showMessageDialog(null, "!Monto actualizado correctamente!",
                    "Actualizar Monto", JOptionPane.PLAIN_MESSAGE);
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Actualizar Monto Mes", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void setMontoInscripcion(){
        try{
            String monto = tf_monto_inscripcion.getText();
            String Query = "UPDATE monto SET monto='"+monto+"' WHERE id=2";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            Ventana.db.comando.executeUpdate(Query);

            montoInscripcion = Double.parseDouble(monto);
            
            JOptionPane.showMessageDialog(null, "!Monto actualizado correctamente!",
                    "Actualizar Monto", JOptionPane.PLAIN_MESSAGE);
                        
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Actualizar Monto Mes", JOptionPane.ERROR_MESSAGE);
        }
    }
}
