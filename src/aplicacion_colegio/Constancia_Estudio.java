package aplicacion_colegio;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Constancia_Estudio {
    private static final Font FONT_FECHA =
            new Font(Font.FontFamily.COURIER, 14, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_ETIQUETA =
            new Font(Font.FontFamily.COURIER, 16, Font.BOLD, BaseColor.WHITE);
    
    private static final Font FONT_ETIQUETA_2 =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TEXT =
            new Font(Font.FontFamily.COURIER, 14, Font.NORMAL, BaseColor.BLACK);
    
    private static final Font FONT_TITULO =
            new Font(Font.FontFamily.HELVETICA, 14, Font.BOLDITALIC, BaseColor.BLACK);
    
    private static final Font FIRMA =
            new Font(Font.FontFamily.HELVETICA, 11, Font.UNDERLINE, BaseColor.BLACK);
    
    private static final Font NOMBRE_FIRMA =
            new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    
    private static final Font FONT_TITULO_2 =
            new Font(Font.FontFamily.HELVETICA, 12, Font.BOLDITALIC, BaseColor.BLACK);    
    
    private static final Font FONT_TITULO_3 =
            new Font(Font.FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.BLACK); 
    
    private static final Font FONT_PIE =
            new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, BaseColor.BLACK); 
    
    private static final Font FONT_FECHA_TABLA =
            new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    
    public Constancia_Estudio(
            String RUTA, String nombre_estudiante, String grado, String periodo, String dia, String mes, String ano,
            String representante, String representante_cedula,
            Ventana_Principal Ventana
    ){
        //CREACIPON DEL DOCUMENTO PDF
        try {
            Document document = new Document();            
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RUTA));
            
            document.open();
            document.setPageSize(PageSize.LETTER);
            document.setMargins(70, 70, 30, 30);
            //ANCHO: 590 - ALTO: 780
            
            //METADATOS DEL PDF
            document.addTitle("Constancia de Promoción");
            document.addSubject("Usando Aplicación Colegio");
            document.addKeywords("Java, PDF, Aplicación Colegio");
            document.addAuthor("Aplicación Colegio");
            document.addCreator("Aplicación Colegio");
            
            //PRIMERA PAGINA
            Image ENCABEZADO;
            ENCABEZADO = Image.getInstance(Constancia_Estudio.class.getResource("/Imagenes/logo.png"));
            ENCABEZADO.scaleAbsolute(110, 62);
            ENCABEZADO.setAbsolutePosition(50,670);
            
            Paragraph MARGEN_SUPERIOR = new Paragraph();
            MARGEN_SUPERIOR.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO
            Paragraph TITULO = new Paragraph(
                    "Preescolar – Unidad Educativa\n"
                    +"“Rodolfo Romero”\n"
                    +"Inscrito en el MPPE\n"
                    +"Código DEA:PD13630304",
                    FONT_TITULO);
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.add(new Phrase(Chunk.NEWLINE));
            TITULO.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 2
            Paragraph TITULO_2 = new Paragraph(
                    "CONSTANCIA DE ESTUDIO",
                    FONT_TITULO_2);
            TITULO_2.add(new Phrase(Chunk.NEWLINE));
            TITULO_2.add(new Phrase(Chunk.NEWLINE));
            TITULO_2.setAlignment(Paragraph.ALIGN_CENTER);
            
            //TITULO 3
            Paragraph PARRAFO = new Paragraph(
                    "Quien suscribe, Lic. María Marín de Martínez, titular de la Cédula de Identidad Nº "
                    +"V-4.006.441, Directora General de la U.E. “Rodolfo Romero” que funciona en la "
                    +"Urbanización Tricentenaria calle C número 52 con anexo 1era Transversal número 44. Barcelona. "
                    +"Municipio Simón Bolívar del Estado Anzoátegui hace constar que el (la) Alumno (a) "+nombre_estudiante+", "
                    +"representado del (la) Sr(a) "+representante+", titular de la cédula de identidad Nº "+representante_cedula+", "
                    +"estudia en esta institución "+grado+", año escolar "+periodo+".\n\n"
                    +"Constancia que se expide a petición de la parte interesada a los "+dia+" días del mes de "+mes+" de "+ano+".",
                    FONT_TITULO_3);
            PARRAFO.add(new Phrase(Chunk.NEWLINE));
            PARRAFO.add(new Phrase(Chunk.NEWLINE));
            PARRAFO.setFirstLineIndent(40);
            PARRAFO.setLeading(30);
            PARRAFO.setAlignment(Paragraph.ALIGN_JUSTIFIED);
            
            //ENCABEZADO
            Chapter chapter = new Chapter(MARGEN_SUPERIOR, 1);
            chapter.add(ENCABEZADO);
            chapter.setNumberDepth(0);
            chapter.add(TITULO);
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(TITULO_2);
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(PARRAFO);            
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            PdfPTable table_firma;
            
            //DIRECTORA                        
            PdfPCell NOMBRE_DIRECTORA = new PdfPCell(
                    new Paragraph("Lic. María de Martínez\nDirectora General", NOMBRE_FIRMA)
            );
            NOMBRE_DIRECTORA.setBorder(1);
            NOMBRE_DIRECTORA.setBorderColorBottom(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorLeft(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorRight(BaseColor.WHITE);
            NOMBRE_DIRECTORA.setBorderColorTop(BaseColor.BLACK);
            NOMBRE_DIRECTORA.setHorizontalAlignment(Paragraph.ALIGN_CENTER);
            NOMBRE_DIRECTORA.setLeading(15, 0);
            
            table_firma = new PdfPTable(1);
            table_firma.setWidthPercentage(50);
            table_firma.addCell(NOMBRE_DIRECTORA);
            chapter.add(table_firma);
            
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            chapter.add(new Phrase(Chunk.NEWLINE));
            
            //Pie de pagina
            Paragraph pie = new Paragraph(
                    "Urb. Tricentenaria. Calle C. Nº 52 con anexo 1era Transversal Nº 44 Barcelona. Edo. Anzoátegui.\n"
                    +"Teléfono 0281 – 274.41.01          RIF. J-31324819-3 – NIT. 0413618754",
                    FONT_PIE);
            pie.setAlignment(Paragraph.ALIGN_CENTER);
            chapter.add(pie);
            
            document.add(chapter);
            document.close();
            
            Object [] reporte = {"Aceptar","Abrir Constancia"};
            
            int opcion = JOptionPane.showOptionDialog(
                    null, "¡Tu archivo ha sido creado correctamente!", "CONSTANCIA DE ESTUDIO",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
                    reporte, reporte
                );
            if(opcion==1){
                try {
                    File archivo = new File(RUTA);
                    Desktop.getDesktop().open(archivo);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "NO SE PUDO ABRIR EL ARCHIVO"
                            + "\nEs posible que no cuente con un programa capaz de abrir el archivo.",
                            "MENSAJE DE ARCHIVO", JOptionPane.PLAIN_MESSAGE);
                }
            }
            
        } catch (DocumentException documentException) {
            JOptionPane.showMessageDialog(null, "¡Tu archivo no ha podido ser creado!",
                    "CONSTANCIA DE ESTUDIO", 0);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Boletin_Digital.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Boletin_Digital.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
