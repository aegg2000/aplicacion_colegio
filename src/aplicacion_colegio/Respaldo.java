package aplicacion_colegio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Respaldo {
    public Ventana_Principal Ventana;
    
    public Respaldo(Ventana_Principal Ventana){
        this.Ventana = Ventana;
    }
    public void crearRespaldo(){
        try{
            JFileChooser RUTA = new JFileChooser();
            RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            RUTA.setFileFilter(new FileNameExtensionFilter("*.TXT", "txt"));
            int OPCION_SELECCIONADA = RUTA.showSaveDialog(Ventana);

            if(OPCION_SELECCIONADA == 1){ return; }

            String ruta = RUTA.getSelectedFile().toString();
            ruta = ruta.split(Pattern.quote("."))[0];                  
            File archivo = new File(ruta+".txt");

            if(archivo.exists()){
                int opcion = JOptionPane.showOptionDialog(null, "¿Desea reemplazar el archivo?",
                        "¡Archivo existente!", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,
                        null, null, 0);
                if(opcion==0){
                    RUTA.getSelectedFile().delete();
                }else{
                    return;
                }
            }
            if(OPCION_SELECCIONADA==JFileChooser.APPROVE_OPTION){
                FileWriter wr = new FileWriter(archivo);
                wr.write("RESPALDO BASE DE DATOS\n");
                
                getEmpleados(wr);                
                getEstudiantes(wr);                
                getRepresentantes(wr);                
                getBoletin(wr);                
                getPagos(wr);                
                getMontos(wr);
                
                wr.close();
            }
            JOptionPane.showMessageDialog(null, "RESPALDO CREADO EXITOSAMENTE","MENSAJE DE RESPALDO", JOptionPane.PLAIN_MESSAGE);
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "ERROR AL CREAR RESPALDO","MENSAJE DE RESPALDO", JOptionPane.ERROR_MESSAGE);
        }        
    }
    
    public void respaldar(){
        try{
            boolean res = false;
            JFileChooser RUTA = new JFileChooser();
            RUTA.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            RUTA.setFileFilter(new FileNameExtensionFilter("*.TXT", "txt"));
            int OPCION_SELECCIONADA = RUTA.showOpenDialog(Ventana);

            if(OPCION_SELECCIONADA == 1){ return; }

            String ruta = RUTA.getSelectedFile().toString();

            if(OPCION_SELECCIONADA == JFileChooser.APPROVE_OPTION){
                FileReader read = new FileReader(ruta);
                BufferedReader contenido = new BufferedReader(read);
                String [] q = new String[6];
                int i = 0;
                String text = contenido.readLine();
                if(!"RESPALDO BASE DE DATOS".equalsIgnoreCase(text)){
                    System.out.println("RESPALDO BASE DE DATOS".equals(text));
                    JOptionPane.showMessageDialog(null, "ERROR AL RESPALDAR","MENSAJE DE RESPALDO", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if("EMPLEADOS".equals(contenido.readLine())){
                    q[i] = queryEmpleados(contenido);
                    System.out.println("Empleados Listo");
                }
                if("ESTUDIANTES".equals(contenido.readLine())){
                    i++;
                    q[i] = queryEstudiantes(contenido);
                    System.out.println("ESTUDIANTES Listo");
                }
                if("REPRESENTANTES".equals(contenido.readLine())){
                    i++;
                    q[i] = queryRepresentantes(contenido);
                    System.out.println("REPRESENTANTES Listo");
                }
                if("BOLETIN".equals(contenido.readLine())){
                    i++;
                    q[i] = queryBoletin(contenido);
                    System.out.println("BOLETIN Listo");
                }
                if("PAGOS".equals(contenido.readLine())){
                    i++;
                    q[i] = queryPagos(contenido);
                    System.out.println("PAGOS Listo");
                }
                if("MONTOS".equals(contenido.readLine())){
                    i++;
                    q[i] = queryMontos(contenido);
                    System.out.println("MONTOS Listo");
                }
                res = ejecutarRespaldo(q);
            }
            if(res){
                Ventana.menu_home.Configuracion.actualizarMontos();
                Ventana.menu_home.Listar_Personal.cargarListado(Ventana.obtenerListaPersonal(0),Ventana.Login);
                Ventana.menu_home.Listar_Estudiante.cargarListado(Ventana.obtenerListaEstudiante(),false,false);
                JOptionPane.showMessageDialog(null, "RESPALDO REALIZADO EXITOSAMENTE","MENSAJE DE RESPALDO", JOptionPane.PLAIN_MESSAGE);                
            }
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "ERROR AL RESPALDAR","MENSAJE DE RESPALDO", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getEmpleados(FileWriter wr){
        try{            
            String Query = "SELECT * FROM empleado";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("EMPLEADOS\n");
            while(rs.next()){
                String registro = rs.getString("cedula")+"|||";
                registro += rs.getString("nombres")+"|||";
                registro += rs.getString("apellidos")+"|||";
                registro += rs.getString("cargo")+"|||";
                registro += rs.getString("grado_id")+"|||";
                registro += rs.getString("direccion_trabajo")+"|||";
                registro += rs.getString("direccion_habitacion")+"|||";
                registro += rs.getString("telefono_trabajo")+"|||";
                registro += rs.getString("telefono_habitacion")+"|||";
                registro += rs.getString("telefono_celular")+"|||";
                registro += rs.getString("correo_electronico")+"|||";
                registro += rs.getString("contrasena_acceso")+"|||";
                registro += rs.getString("eliminado")+"\n";
                wr.write(registro);
            }
            wr.write("FIN EMPLEADOS\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo de Empleado", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getEstudiantes(FileWriter wr){
        try{            
            String Query = "SELECT * FROM estudiante";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("ESTUDIANTES\n");
            while(rs.next()){
                String registro = rs.getString("identificador_estudiante")+"|||";
                registro += rs.getString("nombres")+"|||";
                registro += rs.getString("apellidos")+"|||";
                registro += rs.getString("grado_id")+"|||";
                registro += rs.getString("periodo_escolar")+"|||";
                registro += rs.getString("dia_nacimiento")+"|||";
                registro += rs.getString("mes_nacimiento")+"|||";
                registro += rs.getString("ano_nacimiento")+"|||";
                registro += rs.getString("eliminado")+"\n";
                wr.write(registro);
            }
            wr.write("FIN ESTUDIANTES\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo Estudiante", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getRepresentantes(FileWriter wr){
        try{            
            String Query = "SELECT * FROM representante";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("REPRESENTANTES\n");
            while(rs.next()){
                String registro = rs.getString("cedula_madre")+"|||";
                registro += rs.getString("nombre_apellido_madre")+"|||";
                registro += rs.getString("direccion_habitacion_madre")+"|||";
                registro += rs.getString("telefono_habitacion_madre")+"|||";
                registro += rs.getString("telefono_celular_madre")+"|||";
                
                registro += rs.getString("cedula_padre")+"|||";
                registro += rs.getString("nombre_apellido_padre")+"|||";
                registro += rs.getString("direccion_habitacion_padre")+"|||";
                registro += rs.getString("telefono_habitacion_padre")+"|||";
                registro += rs.getString("telefono_celular_padre")+"|||";
                
                registro += rs.getString("cedula_rlegal")+"|||";
                registro += rs.getString("nombre_apellido_rlegal")+"|||";
                registro += rs.getString("direccion_habitacion_rlegal")+"|||";
                registro += rs.getString("telefono_habitacion_rlegal")+"|||";
                registro += rs.getString("telefono_celular_rlegal")+"|||";
                registro += rs.getString("parentesco_rlegal")+"|||";
                
                registro += rs.getString("identificador_estudiante")+"\n";
                wr.write(registro);
            }
            wr.write("FIN REPRESENTANTES\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo de Representante", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getBoletin(FileWriter wr){
        try{            
            String Query = "SELECT * FROM boletin";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("BOLETIN\n");
            while(rs.next()){
                String registro = rs.getString("identificador_estudiante")+"|||";
                registro += rs.getString("nombre_materia")+"|||";
                registro += rs.getString("nota")+"|||";
                registro += rs.getString("lapso")+"\n";
                wr.write(registro);
            }
            wr.write("FIN BOLETIN\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo Boletin", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getPagos(FileWriter wr){
        try{            
            String Query = "SELECT * FROM pagos";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("PAGOS\n");
            while(rs.next()){
                String registro = rs.getString("identificador_estudiante")+"|||";
                registro += rs.getString("comprobante")+"|||";
                registro += rs.getString("monto")+"|||";
                registro += rs.getString("fecha")+"|||";
                registro += rs.getString("concepto")+"|||";
                registro += rs.getString("descripcion")+"\n";
                wr.write(registro);
            }
            wr.write("FIN PAGOS\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo Pagos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void getMontos(FileWriter wr){
        try{            
            String Query = "SELECT * FROM monto";
        
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            ResultSet rs = Ventana.db.comando.executeQuery(Query);
            
            wr.write("MONTOS\n");
            while(rs.next()){
                String registro = rs.getString("id")+"|||";
                registro += rs.getString("monto")+"\n";
                wr.write(registro);
            }
            wr.write("FIN MONTOS\n");
            
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Respaldo Montos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String queryEmpleados(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO empleado"
            + "(cedula, nombres, apellidos, cargo, grado_id, direccion_trabajo, direccion_habitacion,"
            + "telefono_trabajo, telefono_habitacion, telefono_celular, correo_electronico, contrasena_acceso, eliminado) "
            + "VALUES ";
        
        texto = contenido.readLine();
        if("FIN EMPLEADOS".equals(texto)){
            return "";
        }
        while(!"FIN EMPLEADOS".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN EMPLEADOS".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }
    
    public String queryEstudiantes(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO estudiante"
            + "(identificador_estudiante, nombres, apellidos, grado_id, periodo_escolar, "
            + "dia_nacimiento, mes_nacimiento, ano_nacimiento, eliminado) "
            + "VALUES ";
        
        texto = contenido.readLine();
        if("FIN ESTUDIANTES".equals(texto)){
            return "";
        }
        
        while(!"FIN ESTUDIANTES".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN ESTUDIANTES".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }    
    
    public String queryRepresentantes(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO representante"
            + "(cedula_madre, nombre_apellido_madre, direccion_habitacion_madre, telefono_habitacion_madre, telefono_celular_madre, "
            + "cedula_padre, nombre_apellido_padre, direccion_habitacion_padre, telefono_habitacion_padre, telefono_celular_padre, "
            + "cedula_rlegal, nombre_apellido_rlegal, direccion_habitacion_rlegal, telefono_habitacion_rlegal, telefono_celular_rlegal, parentesco_rlegal, "
            + "identificador_estudiante) VALUES ";
        
        texto = contenido.readLine();
        if("FIN REPRESENTANTES".equals(texto)){
            return "";
        }
        
        while(!"FIN REPRESENTANTES".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN REPRESENTANTES".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }
    
    public String queryBoletin(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO boletin"
            + "(identificador_estudiante, nombre_materia, nota, lapso) "
            + "VALUES ";
        
        texto = contenido.readLine();
        if("FIN BOLETIN".equals(texto)){
            return "";
        }
        
        while(!"FIN BOLETIN".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN BOLETIN".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }
    
    public String queryPagos(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO pagos"
            + "(identificador_estudiante, comprobante, monto, fecha, concepto, descripcion) "
            + "VALUES ";
        
        texto = contenido.readLine();
        if("FIN PAGOS".equals(texto)){
            return "";
        }
        
        while(!"FIN PAGOS".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN PAGOS".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }
    
    public String queryMontos(BufferedReader contenido) throws IOException{
        String texto;
        
        String query = "INSERT INTO monto"
            + "(id, monto) "
            + "VALUES ";
        
        texto = contenido.readLine();
        if("FIN MONTOS".equals(texto)){
            return "";
        }
        
        while(!"FIN MONTOS".equals(texto)){
            String[] values = texto.split(Pattern.quote("|||"));
            query += "(";
            for(int i = 0; i < values.length; i++){
                query += "'"+values[i]+"'";
                if(i != values.length-1){
                    query += ",";
                }
            }
            query += ")";
            texto = contenido.readLine();
            if(!"FIN MONTOS".equals(texto)){
                query += ",";
            }
        }
        query += ";";
        
        return query;
    }
    
    public boolean ejecutarRespaldo(String[] q){
        try{
            Ventana.db.comando = Ventana.db.conexion.createStatement();
            
            Ventana.db.comando.addBatch("DROP TABLE monto");
            Ventana.db.comando.addBatch("DROP TABLE boletin");
            Ventana.db.comando.addBatch("DROP TABLE pagos");
            Ventana.db.comando.addBatch("DROP TABLE representante");
            Ventana.db.comando.addBatch("DROP TABLE estudiante");
            Ventana.db.comando.addBatch("DROP TABLE empleado");            
            Ventana.db.comando.executeBatch();
            
            String crear_monto = "CREATE TABLE `monto` (`id` INT NOT NULL AUTO_INCREMENT, `monto` DOUBLE(15,2) NOT NULL, PRIMARY KEY (`id`));";
            String crear_empleado = "CREATE TABLE `empleado` (  \n" +
                                    "`cedula` varchar(12) PRIMARY KEY NOT NULL,  \n" +
                                    "`nombres` varchar(65) NOT NULL,  \n" +
                                    "`apellidos` varchar(65) NOT NULL,  \n" +
                                    "`cargo` varchar(50) NOT NULL,\n" +
                                    "`grado_id` INT UNSIGNED NOT NULL,   \n" +
                                    "`direccion_trabajo` varchar(65) NOT NULL,  \n" +
                                    "`direccion_habitacion` varchar(65) NOT NULL,  \n" +
                                    "`telefono_trabajo` varchar(15) NOT NULL,  \n" +
                                    "`telefono_habitacion` varchar(15) NOT NULL,  \n" +
                                    "`telefono_celular` varchar(15) NOT NULL,  \n" +
                                    "`correo_electronico` varchar(65) UNIQUE KEY NOT NULL,  \n" +
                                    "`contrasena_acceso` varchar(32) NOT NULL,\n" +
                                    "`eliminado` BOOLEAN NOT NULL DEFAULT FALSE,\n" +
                                    "\n" +
                                    "FOREIGN KEY fk_empledos_grado_id (`grado_id`)\n" +
                                    "REFERENCES grados (`id`)\n" +
                                    "ON DELETE restrict\n" +
                                    "ON UPDATE cascade\n" +
                                    "); ";
            String crear_estudiante = "CREATE TABLE `estudiante` ( \n" +
                                    "`identificador_estudiante` varchar(12) PRIMARY KEY NOT NULL,  \n" +
                                    "`nombres` varchar(65) NOT NULL,  \n" +
                                    "`apellidos` varchar(65) NOT NULL,  \n" +
                                    "`grado_id` INT UNSIGNED NOT NULL, \n" +
                                    "`periodo_escolar` varchar(10) NOT NULL,  \n" +
                                    "`dia_nacimiento` varchar(2) NOT NULL,  \n" +
                                    "`mes_nacimiento` varchar(2) NOT NULL,  \n" +
                                    "`ano_nacimiento` varchar(4) NOT NULL,\n" +
                                    "`eliminado` BOOLEAN NOT NULL DEFAULT FALSE,\n" +
                                    "\n" +
                                    "FOREIGN KEY fk_estudiantes_grado_id (`grado_id`)\n" +
                                    "REFERENCES grados (`id`)\n" +
                                    "ON DELETE restrict\n" +
                                    "ON UPDATE cascade\n" +
                                    ");";
            String crear_representante = "CREATE TABLE `representante` (\n" +
                                    "`identificador_estudiante` varchar(12) NOT NULL,  \n" +
                                    "`cedula_madre` varchar(12) NULL,  \n" +
                                    "`nombre_apellido_madre` varchar(65) NULL,  \n" +
                                    "`direccion_habitacion_madre` varchar(65) NULL,  \n" +
                                    "`telefono_habitacion_madre` varchar(15) NULL,  \n" +
                                    "`telefono_celular_madre` varchar(15) NULL,    \n" +
                                    "`cedula_padre` varchar(12) NULL,  \n" +
                                    "`nombre_apellido_padre` varchar(65) NULL,  \n" +
                                    "`direccion_habitacion_padre` varchar(65) NULL,  \n" +
                                    "`telefono_habitacion_padre` varchar(15) NULL,  \n" +
                                    "`telefono_celular_padre` varchar(15) NULL,\n" +
                                    "`cedula_rlegal` varchar(12) NULL,  \n" +
                                    "`nombre_apellido_rlegal` varchar(65) NULL,  \n" +
                                    "`direccion_habitacion_rlegal` varchar(65) NULL,  \n" +
                                    "`telefono_habitacion_rlegal` varchar(15) NULL,  \n" +
                                    "`telefono_celular_rlegal` varchar(15) NULL,\n" +
                                    "`parentesco_rlegal` varchar(15) NULL,\n" +
                                    "FOREIGN KEY fk_identificador_estudiante_representante (`identificador_estudiante`)\n" +
                                    "REFERENCES estudiante (`identificador_estudiante`)\n" +
                                    "ON DELETE restrict\n" +
                                    "ON UPDATE cascade\n" +
                                    ");";
            String crear_pagos = "CREATE TABLE `pagos` (  \n" +
                                "`identificador_estudiante` varchar(12) NOT NULL,  \n" +
                                "`comprobante` varchar(50) NOT NULL, \n" +
                                "`monto` text NOT NULL,  \n" +
                                "`fecha` varchar(12) NOT NULL,  \n" +
                                "`concepto` varchar(65) NOT NULL,\n" +
                                "`descripcion` text NOT NULL,\n" +
                                "FOREIGN KEY fk_identificador_estudiante_pagos (`identificador_estudiante`)\n" +
                                "REFERENCES estudiante (`identificador_estudiante`)\n" +
                                "ON DELETE restrict\n" +
                                "ON UPDATE cascade\n" +
                                ");";
            String crear_boletin = "CREATE TABLE `boletin` ( \n" +
                                "`identificador_estudiante` varchar(12) NOT NULL, \n" +
                                "`nombre_materia` varchar(65) NOT NULL,  \n" +
                                "`nota` varchar(1) NOT NULL,  \n" +
                                "`lapso` int(11) NOT NULL,\n" +
                                "FOREIGN KEY fk_identificador_estudiante_boletin (`identificador_estudiante`)\n" +
                                "REFERENCES estudiante (`identificador_estudiante`)\n" +
                                "ON DELETE restrict\n" +
                                "ON UPDATE cascade\n" +
                                ");";
            
            Ventana.db.comando.addBatch(crear_monto);
            Ventana.db.comando.addBatch(crear_empleado);
            Ventana.db.comando.addBatch(crear_estudiante);
            Ventana.db.comando.addBatch(crear_representante);
            Ventana.db.comando.addBatch(crear_pagos);
            Ventana.db.comando.addBatch(crear_boletin); 
            Ventana.db.comando.executeBatch(); 
            
            if(q[5].length() > 0){
                System.out.println(q[5]);
                Ventana.db.comando.executeUpdate(q[5]); //Montos
            }
            if(q[0].length() > 0){
                System.out.println(q[0]);
                Ventana.db.comando.executeUpdate(q[0]); //Empleados
            }
            if(q[1].length() > 0){
                System.out.println(q[1]);
                Ventana.db.comando.executeUpdate(q[1]); //Estudiantes
            }
            if(q[2].length() > 0){
                System.out.println(q[2]);
                Ventana.db.comando.executeUpdate(q[2]); //Representantes
            }
            if(q[3].length() > 0){
                System.out.println(q[3]);
                Ventana.db.comando.executeUpdate(q[3]); //Pagos
            }
            if(q[4].length() > 0){
                System.out.println(q[4]);
                Ventana.db.comando.executeUpdate(q[4]); //Boletin
            }
        }catch(Exception e){
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "¡ERROR!", "Error al respaldar", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
}
