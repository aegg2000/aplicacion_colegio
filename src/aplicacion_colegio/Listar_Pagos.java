package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Listar_Pagos extends JPanel{
    public Ventana_Principal Ventana;
    public JTextField nombre, concepto, identificador, comprobante, informacion;
    public JTextField [] t_nombre, t_concepto, t_identificador, t_comprobante;
    public int Ancho, Alto;
    public JPanel panel;
    public JScrollPane scroll;
    public JButton [] Info;
    
    public Listar_Pagos(Ventana_Principal Ventana,int Ancho, int Alto){
        this.Ancho = Ancho;
        this.Alto = Alto;
        setSize(Ancho,Alto);
        setPreferredSize(new Dimension(Ancho,Alto));
        setLayout(null);
        setBackground(Color.WHITE);
        
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setLocation(0,(int)(Alto*0.07));
        scroll.setSize(Ancho,(int)(Alto*0.9));
        panel.setLocation(0,0);
        panel.setSize((int)(Ancho*0.95),(int)(Alto*0.95));
        scroll.setViewportView(panel);
        scroll.setVisible(true);
        add(scroll);
        
        cargarListado(Ventana.obtenerListaPagos());
    }
    
    public void cargarListado(List<String> pagos){
       panel.removeAll();
       
       t_identificador = new JTextField[pagos.size()/4];
       t_nombre = new JTextField[pagos.size()/4];
       t_concepto = new JTextField[pagos.size()/4];
       t_comprobante = new JTextField[pagos.size()/4];
       Info = new JButton[pagos.size()/4];
              
       int pos_x = 0, pos_y = 0;
       
       identificador = new JTextField("IDENTIFICADOR");
       identificador.setLocation(pos_x,pos_y);
       identificador.setHorizontalAlignment(SwingConstants.CENTER);
       identificador.setSize((int)(Ancho/5),(int)(Alto*0.07));
       identificador.setBackground(new Color(93, 193, 185));
       identificador.setForeground(Color.WHITE);
       identificador.setFont(new Font("Arial",Font.BOLD,16));
       identificador.setEditable(false);
       add(identificador);
       
       pos_x += (int)(Ancho/5);
       
       nombre = new JTextField("ESTUDIANTE");
       nombre.setLocation(pos_x,pos_y);
       nombre.setHorizontalAlignment(SwingConstants.CENTER);
       nombre.setSize((int)(Ancho/5),(int)(Alto*0.07));
       nombre.setBackground(new Color(93, 193, 185));
       nombre.setForeground(Color.WHITE);
       nombre.setFont(new Font("Arial",Font.BOLD,16));
       nombre.setEditable(false);
       add(nombre);
       
       pos_x += (int)(Ancho/5);
       
       concepto = new JTextField("CONCEPTO");
       concepto.setLocation(pos_x,pos_y);
       concepto.setHorizontalAlignment(SwingConstants.CENTER);
       concepto.setSize((int)(Ancho/5),(int)(Alto*0.07));
       concepto.setBackground(new Color(93, 193, 185));
       concepto.setForeground(Color.WHITE);
       concepto.setFont(new Font("Arial",Font.BOLD,16));
       concepto.setEditable(false);
       add(concepto);
       
       pos_x += (int)(Ancho/5);
       
       comprobante = new JTextField("COMPROBANTE");
       comprobante.setLocation(pos_x,pos_y);
       comprobante.setHorizontalAlignment(SwingConstants.CENTER);
       comprobante.setSize((int)(Ancho/5),(int)(Alto*0.07));
       comprobante.setBackground(new Color(93, 193, 185));
       comprobante.setForeground(Color.WHITE);
       comprobante.setFont(new Font("Arial",Font.BOLD,16));
       comprobante.setEditable(false);
       add(comprobante);
       
       pos_x += (int)(Ancho/5);
       
       informacion = new JTextField("INFORMACIÓN");
       informacion.setLocation(pos_x,pos_y);
       informacion.setHorizontalAlignment(SwingConstants.CENTER);
       informacion.setSize((int)(Ancho/5),(int)(Alto*0.07));
       informacion.setBackground(new Color(93, 193, 185));
       informacion.setForeground(Color.WHITE);
       informacion.setFont(new Font("Arial",Font.BOLD,16));
       informacion.setEditable(false);
       add(informacion);
       
       pos_x = 0;
       
       int aux = 0;
       
        for(int i=0;i<pagos.size()/4;i++){
            int x = i;
            t_identificador[i] = new JTextField(pagos.get(aux++));
            t_identificador[i].setLocation(pos_x,pos_y);
            t_identificador[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_identificador[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_identificador[i].setBackground(Color.white);
            t_identificador[i].setEditable(false);
            panel.add(t_identificador[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_nombre[i] = new JTextField(pagos.get(aux++));
            t_nombre[i].setLocation(pos_x,pos_y);
            t_nombre[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_nombre[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_nombre[i].setBackground(Color.white);
            t_nombre[i].setEditable(false);
            panel.add(t_nombre[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_concepto[i] = new JTextField(pagos.get(aux++));
            t_concepto[i].setLocation(pos_x,pos_y);
            t_concepto[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_concepto[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_concepto[i].setBackground(Color.white);
            t_concepto[i].setEditable(false);
            panel.add(t_concepto[i]);
            
            pos_x += (int)(Ancho/5);
            
            t_comprobante[i] = new JTextField(pagos.get(aux++));
            t_comprobante[i].setLocation(pos_x,pos_y);
            t_comprobante[i].setHorizontalAlignment(SwingConstants.CENTER);
            t_comprobante[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            t_comprobante[i].setBackground(Color.white);
            t_comprobante[i].setEditable(false);
            panel.add(t_comprobante[i]);
            
            pos_x += (int)(Ancho/5);
            
            Info[i] = new JButton("VER MÁS"){
                protected void paintComponent(Graphics g) {
                    if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                        Graphics2D g2 = (Graphics2D) g.create();
                        g2.setPaint(getBackground());
                        g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                            0, 0, getWidth() - 1, getHeight() - 1));
                        g2.dispose();
                        g2.setPaint(Color.RED);
                    }
                    super.paintComponent(g);
                }
                @Override public void updateUI() {
                  super.updateUI();
                  setOpaque(false);
                  setBorder(new RoundedCornerBorder());
                }
            };
            Info[i].setLocation(pos_x,pos_y);
            Info[i].setHorizontalAlignment(SwingConstants.CENTER);
            Info[i].setSize((int)(Ancho/5),(int)(Alto*0.07));
            Info[i].setBackground(Color.white);
            Info[i].setContentAreaFilled(false);
            Info[i].addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent me) {
                }

                @Override
                public void mousePressed(MouseEvent me) {
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                }

                @Override
                public void mouseEntered(MouseEvent me) {
                    Info[x].setFont(new Font("Arial",Font.BOLD,16));
                }

                @Override
                public void mouseExited(MouseEvent me) {
                    Info[x].setFont(new Font("Arial",Font.PLAIN,14));
                }
            });
            Info[i].setOpaque(false);
            Info[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
            Info[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    JOptionPane.showMessageDialog(null, new Informacion_Pagos(
                            Ventana,t_identificador[x].getText(),t_comprobante[x].getText()),
                            t_nombre[x].getText(),JOptionPane.PLAIN_MESSAGE);
                }
            });
            Info[i].setFocusable(false);
            panel.add(Info[i]);
            
            pos_x = 0;
            pos_y += (int)(Alto*0.07);
        }
        
        panel.setSize((int)(Ancho*0.92),(int)((int)(Alto*0.07*pagos.size()/5)));
        panel.setPreferredSize(new Dimension((int)(Ancho*0.92),(int)((int)(Alto*0.07*pagos.size()/5))));
        panel.repaint();
   }
}
