package aplicacion_colegio;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public final class Gestionar_Notas extends JPanel{
    public JComboBox Lista_Estudiantes;
    public List<String> Estudiantes;
    public int Ancho, Alto, pos_y;
    public Notas_Estudiante Notas_Estudiante;
    public JButton guardar_notas;
    public JButton filtro_estudiante;
    public Ventana_Principal Ventana;
    public boolean prof;
    
    public Gestionar_Notas(Ventana_Principal Ventana, int Anc, int Alt,String cargo,String ID){
        Ancho = Anc;
        Alto = Alt;
        this.Ventana = Ventana;
        setSize(Ancho,Alto);
        setBackground(Color.WHITE);
        setLayout(null);
        
        prof = false;
        
        if(cargo.equalsIgnoreCase("PROFESOR")){
            Estudiantes = Ventana.obtenerListaEstudiante(Ventana.obtenerGradoACargo(ID));
            prof = true;
        }else{
            Estudiantes = Ventana.obtenerListaEstudiante();
        }
        pos_y = 5;
        
        //LISTA ESTUDIANTE
        Lista_Estudiantes = new JComboBox();
        
        //BOLETIN
        Notas_Estudiante = new Notas_Estudiante(Ventana,(int)(Ancho*0.8),(int)(Alto*0.5));
        Notas_Estudiante.setLocation((int)(Ancho*0.1),pos_y);
        add(Notas_Estudiante);
        
        addItemListenerNotas(Notas_Estudiante.nota_lengua,0);
        addItemListenerNotas(Notas_Estudiante.nota_matematica,1);
        addItemListenerNotas(Notas_Estudiante.nota_ciencias_naturaleza,2);
        addItemListenerNotas(Notas_Estudiante.nota_ciencias_sociales,3);
        addItemListenerNotas(Notas_Estudiante.nota_educacion_estetica,4);
        addItemListenerNotas(Notas_Estudiante.nota_educacion_fisica,5);
        addItemListenerNotas(Notas_Estudiante.nota_ingles,6);
        
        pos_y += (int)(Alto*0.55);
        
        //guardar_notas
        guardar_notas = new JButton("Guardar Notas"){
            protected void paintComponent(Graphics g) {
                if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
                    Graphics2D g2 = (Graphics2D) g.create();
                    g2.setPaint(getBackground());
                    g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                        0, 0, getWidth() - 1, getHeight() - 1));
                    g2.dispose();
                    g2.setPaint(Color.RED);
                }
                super.paintComponent(g);
            }
            @Override public void updateUI() {
              super.updateUI();
              setOpaque(false);
              setBorder(new RoundedCornerBorder());
            }
        };
        guardar_notas.setSize((int)(Ancho*0.8),(int)(Alto*0.08));
        guardar_notas.setLocation((int)(Ancho*0.1),pos_y);
        guardar_notas.setForeground(Color.BLACK);
        guardar_notas.setFont(new Font("Arial",Font.BOLD,16));
        guardar_notas.setContentAreaFilled(false);
        guardar_notas.setFocusable(false);
        guardar_notas.setEnabled(false);
        guardar_notas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if(Lista_Estudiantes.getSelectedIndex()>=0){
                    String id = Notas_Estudiante.identificador.getText();
                    String [] materia = new String[7];
                    materia[0] = "LENGUA Y LITERATURA";
                    materia[1] = "MATEMÁTICA";
                    materia[2] = "CIENCIAS DE LA NATURALEZA";
                    materia[3] = "CIENCIAS SOCIALES";
                    materia[4] = "EDUCACION ESTÉTICA";
                    materia[5] = "EDUCACION FÍSICA";
                    materia[6] = "INGLÉS";
                    for(int i=0;i<7;i++){
                        if(materia[i].equalsIgnoreCase("LENGUA Y LITERATURA")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_lengua[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("MATEMÁTICA")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_matematica[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("CIENCIAS DE LA NATURALEZA")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("CIENCIAS SOCIALES")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_ciencias_sociales[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("EDUCACION ESTÉTICA")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_educacion_estetica[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("EDUCACION FÍSICA")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_educacion_fisica[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                        if(materia[i].equalsIgnoreCase("INGLÉS")){
                            for(int x=0;x<3;x++){
                                Ventana.actualizarNota(
                                    id,materia[i],
                                    (String)(Notas_Estudiante.nota_ingles[x].getSelectedItem()),
                                    ""+(x+1)
                                );
                            }
                        }
                    }
                    JOptionPane.showMessageDialog(null, "¡Las notas han sido actualizadas correctamente!",
                            "Registro de Pago", JOptionPane.PLAIN_MESSAGE);
                    verificarEdicionNotas(id);
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                guardar_notas.setFont(new Font("Arial",Font.BOLD,20));
            }

            @Override
            public void mouseExited(MouseEvent me) {
                guardar_notas.setFont(new Font("Arial",Font.BOLD,16));
            }
        });
        guardar_notas.setCursor(new Cursor(Cursor.HAND_CURSOR));
        add(guardar_notas);
    }
    
    public void addItemListenerNotas(JComboBox [] nota, int pos){
        for(int i=0;i<nota.length;i++){
            nota[i].addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent ie) {
                    verificarNotaAnterior((pos)+1);
                    Notas_Estudiante.nota_definitiva[pos].setText(
                        notaDefinitiva(
                            (String)(nota[0].getSelectedItem()),
                            (String)(nota[1].getSelectedItem()),
                            (String)(nota[2].getSelectedItem())
                        )
                    );
                }
            });
        }
    }
    
    public String notaDefinitiva(String nota1,String nota2,String nota3){
        String nota= "";
        int n1 = 0, n2 = 0, n3 =0, def = 0;
        //NOTA 1
        if(nota1.equalsIgnoreCase("A")) n1 = 10;
        if(nota1.equalsIgnoreCase("B")) n1 = 8;
        if(nota1.equalsIgnoreCase("C")) n1 = 6;
        if(nota1.equalsIgnoreCase("D")) n1 = 4;
        if(nota1.equalsIgnoreCase("E")) n1 = 2;
        if(nota1.equalsIgnoreCase("")){
            n1 = 0;
            return "";
        }
        
        //NOTA 2
        if(nota2.equalsIgnoreCase("A")) n2 = 10;
        if(nota2.equalsIgnoreCase("B")) n2 = 8;
        if(nota2.equalsIgnoreCase("C")) n2 = 6;
        if(nota2.equalsIgnoreCase("D")) n2 = 4;
        if(nota2.equalsIgnoreCase("E")) n2 = 2;
        if(nota2.equalsIgnoreCase("")){
            n2 = 0;
            return "";
        }
        
        //NOTA 3
        if(nota3.equalsIgnoreCase("A")) n3 = 10;
        if(nota3.equalsIgnoreCase("B")) n3 = 8;
        if(nota3.equalsIgnoreCase("C")) n3 = 6;
        if(nota3.equalsIgnoreCase("D")) n3 = 4;
        if(nota3.equalsIgnoreCase("E")) n3 = 2;
        if(nota3.equalsIgnoreCase("")){
            n3 = 0;
            return "";
        }
        
        def = (n1+n2+n3)/3;
        
        if(def>=9) nota = "A";
        if(def>=7 && def<9) nota = "B";
        if(def>=5 && def<7) nota = "C";
        if(def>=3 && def<5) nota = "D";
        if(def>=1 && def<3) nota = "E";
        if(def<1) nota = "";
                
        return nota;
    }
    public void cargarInformacion(String cargo, String estudiante){
        Notas_Estudiante.nombre_apellido.setText(getEstudiante(estudiante));
        Notas_Estudiante.identificador.setText(getIdentificador(estudiante));
        System.out.println(estudiante);
        String gradostr = getGradoStr(Integer.parseInt(getGrado2(estudiante)));
        Notas_Estudiante.grado.setText(gradostr);
        Notas_Estudiante.periodo.setText("2017-2018");
        List<String> notas = Ventana.obtenerBoletin(getIdentificador(estudiante));
                
        for(int i=0;i<notas.size();i+=3){
            //LITERATURA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_lengua[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_lengua[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_lengua[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[0].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_lengua[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_lengua[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_lengua[2].getSelectedItem())
                 )
            );
            
            //MATEMÁTICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_matematica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_matematica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_matematica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[1].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_matematica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_matematica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_matematica[2].getSelectedItem())
                 )
            );
            
            //CIENCIAS DE LA NATURALEZA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ciencias_naturaleza[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ciencias_naturaleza[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ciencias_naturaleza[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[2].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedItem())
                 )
            );
            
            //CIENCIAS SOCIALES------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ciencias_sociales[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ciencias_sociales[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ciencias_sociales[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[3].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedItem())
                 )
            );
            
            //EDUCACION ESTÉTICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_educacion_estetica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_educacion_estetica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_educacion_estetica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[4].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_educacion_estetica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_estetica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_estetica[2].getSelectedItem())
                 )
            );
            
            //EDUCACION FÍSICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_educacion_fisica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_educacion_fisica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_educacion_fisica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[5].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_educacion_fisica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_fisica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_fisica[2].getSelectedItem())
                 )
            );
            
            //INGLÉS------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ingles[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ingles[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ingles[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[6].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ingles[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ingles[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ingles[2].getSelectedItem())
                 )
            );
        }
        verificarEdicionNotas(cargo);
        if("1".equals(estudiante.substring(estudiante.length()-1, estudiante.length()))){
            notasEstudianteInhabilitado();
        }
        repaint();
    }
    
    public void cargarInformacion(String estudiante){
        borrarinformacion();
        
        Notas_Estudiante.nombre_apellido.setText(getEstudiante(estudiante));
        Notas_Estudiante.identificador.setText(getIdentificador(estudiante));
        String gradostr = getGradoStr(Integer.parseInt(getGrado(estudiante)));
        Notas_Estudiante.grado.setText(gradostr);
        Notas_Estudiante.periodo.setText("2017-2018");
        List<String> notas = Ventana.obtenerBoletin(getIdentificador(estudiante));
                
        for(int i=0;i<notas.size();i+=3){
            //LITERATURA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_lengua[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_lengua[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("LENGUA Y LITERATURA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_lengua[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[0].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_lengua[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_lengua[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_lengua[2].getSelectedItem())
                 )
            );
            
            //MATEMÁTICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_matematica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_matematica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("MATEMÁTICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_matematica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[1].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_matematica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_matematica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_matematica[2].getSelectedItem())
                 )
            );
            
            //CIENCIAS DE LA NATURALEZA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ciencias_naturaleza[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ciencias_naturaleza[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS DE LA NATURALEZA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ciencias_naturaleza[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[2].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedItem())
                 )
            );
            
            //CIENCIAS SOCIALES------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ciencias_sociales[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ciencias_sociales[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("CIENCIAS SOCIALES") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ciencias_sociales[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[3].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedItem())
                 )
            );
            
            //EDUCACION ESTÉTICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_educacion_estetica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_educacion_estetica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION ESTÉTICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_educacion_estetica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[4].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_educacion_estetica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_estetica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_estetica[2].getSelectedItem())
                 )
            );
            
            //EDUCACION FÍSICA------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_educacion_fisica[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_educacion_fisica[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("EDUCACION FÍSICA") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_educacion_fisica[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[5].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_educacion_fisica[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_fisica[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_educacion_fisica[2].getSelectedItem())
                 )
            );
            
            //INGLÉS------------------------------------------------------------
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("1")){
                Notas_Estudiante.nota_ingles[0].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("2")){
                Notas_Estudiante.nota_ingles[1].setSelectedItem(notas.get(i+2));
            }
            if(notas.get(i).equalsIgnoreCase("INGLÉS") && notas.get(i+1).equalsIgnoreCase("3")){
                Notas_Estudiante.nota_ingles[2].setSelectedItem(notas.get(i+2));
            }
            Notas_Estudiante.nota_definitiva[6].setText(
                 notaDefinitiva(
                    (String)(Notas_Estudiante.nota_ingles[0].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ingles[1].getSelectedItem()),
                    (String)(Notas_Estudiante.nota_ingles[2].getSelectedItem())
                 )
            );
        }
        verificarNotas();
        
        repaint();
    }
    
    public void borrarinformacion(){
        Notas_Estudiante.nombre_apellido.setText("");
        Notas_Estudiante.identificador.setText("");
        Notas_Estudiante.grado.setText("");
        Notas_Estudiante.nota_lengua[0].setSelectedIndex(0);
        Notas_Estudiante.nota_lengua[1].setSelectedItem(0);
        Notas_Estudiante.nota_lengua[2].setSelectedItem(0);
        Notas_Estudiante.nota_matematica[0].setSelectedItem(0);
        Notas_Estudiante.nota_matematica[1].setSelectedItem(0);
        Notas_Estudiante.nota_matematica[2].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_naturaleza[0].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_naturaleza[1].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_naturaleza[2].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_sociales[0].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_sociales[1].setSelectedItem(0);
        Notas_Estudiante.nota_ciencias_sociales[2].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_estetica[0].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_estetica[1].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_estetica[2].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_fisica[0].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_fisica[1].setSelectedItem(0);
        Notas_Estudiante.nota_educacion_fisica[2].setSelectedItem(0);
        Notas_Estudiante.nota_ingles[0].setSelectedItem(0);
        Notas_Estudiante.nota_ingles[1].setSelectedItem(0);
        Notas_Estudiante.nota_ingles[2].setSelectedItem(0);
        Notas_Estudiante.nota_definitiva[0].setText("");
        Notas_Estudiante.nota_definitiva[1].setText("");
        Notas_Estudiante.nota_definitiva[2].setText("");
        Notas_Estudiante.nota_definitiva[3].setText("");
        Notas_Estudiante.nota_definitiva[4].setText("");
        Notas_Estudiante.nota_definitiva[5].setText("");
        Notas_Estudiante.nota_definitiva[6].setText("");
    }
        
    public void verificarNotas(){
        verificarNotaAnterior(1);
        verificarNotaAnterior(2);
        verificarNotaAnterior(3);
        verificarNotaAnterior(4);
        verificarNotaAnterior(5);
        verificarNotaAnterior(6);
        verificarNotaAnterior(7);
        
        //BLOQUEAR
        if(Notas_Estudiante.nota_lengua[0].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[0].isEnabled())
            Notas_Estudiante.nota_lengua[0].setEnabled(false);
        if(Notas_Estudiante.nota_lengua[1].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[1].isEnabled())
            Notas_Estudiante.nota_lengua[1].setEnabled(false);
        if(Notas_Estudiante.nota_lengua[2].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[2].isEnabled())
            Notas_Estudiante.nota_lengua[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_matematica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[0].isEnabled())
            Notas_Estudiante.nota_matematica[0].setEnabled(false);
        if(Notas_Estudiante.nota_matematica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[1].isEnabled())
            Notas_Estudiante.nota_matematica[1].setEnabled(false);
        if(Notas_Estudiante.nota_matematica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[2].isEnabled())
            Notas_Estudiante.nota_matematica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[0].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[1].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[2].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[0].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[1].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[2].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_educacion_estetica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[0].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_estetica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[1].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_estetica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[2].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_educacion_fisica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[0].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_fisica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[1].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_fisica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[2].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ingles[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[0].isEnabled())
            Notas_Estudiante.nota_ingles[0].setEnabled(false);
        if(Notas_Estudiante.nota_ingles[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[1].isEnabled())
            Notas_Estudiante.nota_ingles[1].setEnabled(false);
        if(Notas_Estudiante.nota_ingles[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[2].isEnabled())
            Notas_Estudiante.nota_ingles[2].setEnabled(false);
        
        //DESBLOQUEAR
        if(Notas_Estudiante.nota_lengua[0].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[0].isEnabled())
            Notas_Estudiante.nota_lengua[0].setEnabled(true);
        if(Notas_Estudiante.nota_lengua[1].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[1].isEnabled())
            Notas_Estudiante.nota_lengua[1].setEnabled(true);
        if(Notas_Estudiante.nota_lengua[2].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[2].isEnabled())
            Notas_Estudiante.nota_lengua[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_matematica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[0].isEnabled())
            Notas_Estudiante.nota_matematica[0].setEnabled(true);
        if(Notas_Estudiante.nota_matematica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[1].isEnabled())
            Notas_Estudiante.nota_matematica[1].setEnabled(true);
        if(Notas_Estudiante.nota_matematica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[2].isEnabled())
            Notas_Estudiante.nota_matematica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[0].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[1].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[2].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[0].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[1].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[2].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_educacion_estetica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[0].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_estetica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[1].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_estetica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[2].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_educacion_fisica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[0].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_fisica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[1].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_fisica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[2].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ingles[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[0].isEnabled())
            Notas_Estudiante.nota_ingles[0].setEnabled(true);
        if(Notas_Estudiante.nota_ingles[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[1].isEnabled())
            Notas_Estudiante.nota_ingles[1].setEnabled(true);
        if(Notas_Estudiante.nota_ingles[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[2].isEnabled())
            Notas_Estudiante.nota_ingles[2].setEnabled(true);
    }
    
    public void verificarEdicionNotas(String cargo){
        verificarNotaAnterior(1);
        verificarNotaAnterior(2);
        verificarNotaAnterior(3);
        verificarNotaAnterior(4);
        verificarNotaAnterior(5);
        verificarNotaAnterior(6);
        verificarNotaAnterior(7);
        
        if(cargo.equalsIgnoreCase("PROFESOR")){
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(false);
            Notas_Estudiante.nota_ingles[0].setEnabled(false);
            Notas_Estudiante.nota_ingles[1].setEnabled(false);
            Notas_Estudiante.nota_ingles[2].setEnabled(false);
        }
        
        if(cargo.equalsIgnoreCase("PROFESOR EDUC.FISICA")){
            Notas_Estudiante.nota_lengua[0].setEnabled(false);
            Notas_Estudiante.nota_lengua[1].setEnabled(false);
            Notas_Estudiante.nota_lengua[2].setEnabled(false);
            Notas_Estudiante.nota_matematica[0].setEnabled(false);
            Notas_Estudiante.nota_matematica[1].setEnabled(false);
            Notas_Estudiante.nota_matematica[2].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(false);
            Notas_Estudiante.nota_ingles[0].setEnabled(false);
            Notas_Estudiante.nota_ingles[1].setEnabled(false);
            Notas_Estudiante.nota_ingles[2].setEnabled(false);
        }
        
        if(cargo.equalsIgnoreCase("PROFESOR INGLES")){
            Notas_Estudiante.nota_lengua[0].setEnabled(false);
            Notas_Estudiante.nota_lengua[1].setEnabled(false);
            Notas_Estudiante.nota_lengua[2].setEnabled(false);
            Notas_Estudiante.nota_matematica[0].setEnabled(false);
            Notas_Estudiante.nota_matematica[1].setEnabled(false);
            Notas_Estudiante.nota_matematica[2].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(false);
        }
        
        //BLOQUEAR
        if(Notas_Estudiante.nota_lengua[0].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[0].isEnabled())
            Notas_Estudiante.nota_lengua[0].setEnabled(false);
        if(Notas_Estudiante.nota_lengua[1].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[1].isEnabled())
            Notas_Estudiante.nota_lengua[1].setEnabled(false);
        if(Notas_Estudiante.nota_lengua[2].getSelectedIndex()>0 && Notas_Estudiante.nota_lengua[2].isEnabled())
            Notas_Estudiante.nota_lengua[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_matematica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[0].isEnabled())
            Notas_Estudiante.nota_matematica[0].setEnabled(false);
        if(Notas_Estudiante.nota_matematica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[1].isEnabled())
            Notas_Estudiante.nota_matematica[1].setEnabled(false);
        if(Notas_Estudiante.nota_matematica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_matematica[2].isEnabled())
            Notas_Estudiante.nota_matematica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[0].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[1].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_naturaleza[2].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[0].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[1].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(false);
        if(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ciencias_sociales[2].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_educacion_estetica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[0].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_estetica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[1].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_estetica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_estetica[2].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_educacion_fisica[0].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[0].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_fisica[1].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[1].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(false);
        if(Notas_Estudiante.nota_educacion_fisica[2].getSelectedIndex()>0 && Notas_Estudiante.nota_educacion_fisica[2].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(false);
        
        if(Notas_Estudiante.nota_ingles[0].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[0].isEnabled())
            Notas_Estudiante.nota_ingles[0].setEnabled(false);
        if(Notas_Estudiante.nota_ingles[1].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[1].isEnabled())
            Notas_Estudiante.nota_ingles[1].setEnabled(false);
        if(Notas_Estudiante.nota_ingles[2].getSelectedIndex()>0 && Notas_Estudiante.nota_ingles[2].isEnabled())
            Notas_Estudiante.nota_ingles[2].setEnabled(false);
        
        //DESBLOQUEAR
        if(Notas_Estudiante.nota_lengua[0].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[0].isEnabled())
            Notas_Estudiante.nota_lengua[0].setEnabled(true);
        if(Notas_Estudiante.nota_lengua[1].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[1].isEnabled())
            Notas_Estudiante.nota_lengua[1].setEnabled(true);
        if(Notas_Estudiante.nota_lengua[2].getSelectedIndex()==0 && Notas_Estudiante.nota_lengua[2].isEnabled())
            Notas_Estudiante.nota_lengua[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_matematica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[0].isEnabled())
            Notas_Estudiante.nota_matematica[0].setEnabled(true);
        if(Notas_Estudiante.nota_matematica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[1].isEnabled())
            Notas_Estudiante.nota_matematica[1].setEnabled(true);
        if(Notas_Estudiante.nota_matematica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_matematica[2].isEnabled())
            Notas_Estudiante.nota_matematica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[0].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[1].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_naturaleza[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_naturaleza[2].isEnabled())
            Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[0].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[1].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(true);
        if(Notas_Estudiante.nota_ciencias_sociales[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ciencias_sociales[2].isEnabled())
            Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_educacion_estetica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[0].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[0].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_estetica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[1].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[1].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_estetica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_estetica[2].isEnabled())
            Notas_Estudiante.nota_educacion_estetica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_educacion_fisica[0].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[0].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[0].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_fisica[1].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[1].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[1].setEnabled(true);
        if(Notas_Estudiante.nota_educacion_fisica[2].getSelectedIndex()==0 && Notas_Estudiante.nota_educacion_fisica[2].isEnabled())
            Notas_Estudiante.nota_educacion_fisica[2].setEnabled(true);
        
        if(Notas_Estudiante.nota_ingles[0].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[0].isEnabled())
            Notas_Estudiante.nota_ingles[0].setEnabled(true);
        if(Notas_Estudiante.nota_ingles[1].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[1].isEnabled())
            Notas_Estudiante.nota_ingles[1].setEnabled(true);
        if(Notas_Estudiante.nota_ingles[2].getSelectedIndex()==0 && Notas_Estudiante.nota_ingles[2].isEnabled())
            Notas_Estudiante.nota_ingles[2].setEnabled(true);
        
    }
    
    public void verificarNotaAnterior(int x){
        //BLOQUEAR NOTA SIGUIENTE
        if(x==1){
            if(Notas_Estudiante.nota_lengua[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_lengua[0].setEnabled(true);
                Notas_Estudiante.nota_lengua[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_lengua[1].setEnabled(true);
            if(Notas_Estudiante.nota_lengua[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_lengua[2].setEnabled(false);
            else
                Notas_Estudiante.nota_lengua[2].setEnabled(true);
        }
        
        if(x==2){
            if(Notas_Estudiante.nota_matematica[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_matematica[0].setEnabled(true);
                Notas_Estudiante.nota_matematica[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_matematica[1].setEnabled(true);
            if(Notas_Estudiante.nota_matematica[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_matematica[2].setEnabled(false);
            else
                Notas_Estudiante.nota_matematica[2].setEnabled(true);
        }
        
        if(x==3){
             if(Notas_Estudiante.nota_ciencias_naturaleza[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_ciencias_naturaleza[0].setEnabled(true);
                Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(false);
             }                
            else
                Notas_Estudiante.nota_ciencias_naturaleza[1].setEnabled(true);
            if(Notas_Estudiante.nota_ciencias_naturaleza[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(false);
            else
                Notas_Estudiante.nota_ciencias_naturaleza[2].setEnabled(true);
        }
        
        if(x==4){
            if(Notas_Estudiante.nota_ciencias_sociales[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_ciencias_sociales[0].setEnabled(true);
                Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_ciencias_sociales[1].setEnabled(true);
            if(Notas_Estudiante.nota_ciencias_sociales[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(false);
            else
                Notas_Estudiante.nota_ciencias_sociales[2].setEnabled(true);
        }
        
        if(x==5){
            if(Notas_Estudiante.nota_educacion_estetica[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_educacion_estetica[0].setEnabled(true);
                Notas_Estudiante.nota_educacion_estetica[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_educacion_estetica[1].setEnabled(true);
            if(Notas_Estudiante.nota_educacion_estetica[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_educacion_estetica[2].setEnabled(false);
            else
                Notas_Estudiante.nota_educacion_estetica[2].setEnabled(true);
        }
                
        if(x==6){
            if(Notas_Estudiante.nota_educacion_fisica[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_educacion_fisica[0].setEnabled(true);
                Notas_Estudiante.nota_educacion_fisica[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_educacion_fisica[1].setEnabled(true);
            if(Notas_Estudiante.nota_educacion_fisica[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_educacion_fisica[2].setEnabled(false);
            else
                Notas_Estudiante.nota_educacion_fisica[2].setEnabled(true);
        }
        
        if(x==7){
            if(Notas_Estudiante.nota_ingles[0].getSelectedIndex()==0){
                Notas_Estudiante.nota_ingles[0].setEnabled(true);
                Notas_Estudiante.nota_ingles[1].setEnabled(false);
            }
            else
                Notas_Estudiante.nota_ingles[1].setEnabled(true);
            if(Notas_Estudiante.nota_ingles[1].getSelectedIndex()==0)
                Notas_Estudiante.nota_ingles[2].setEnabled(false);
            else
                Notas_Estudiante.nota_ingles[2].setEnabled(true);
        }
    }
    
    public String getIdentificador(String estudiante){
        return estudiante.split(Pattern.quote("-"))[0];
    }
    
    public String getEstudiante(String estudiante){
        return estudiante.split(Pattern.quote("-"))[1];
    }
    
    public String getGrado(String estudiante){
        String ret = estudiante.split(Pattern.quote("-"))[2];
        return ret.substring(1);
    }
    
    public String getGrado2(String estudiante){
        String ret = estudiante.split(Pattern.quote("-"))[2];
        return ret;
    }
    
    public final void notasEstudiante(String ID){
        Lista_Estudiantes.removeAllItems();
        List<String> Estudiante = new ArrayList<>();
        int i = 0;
        
        Estudiante = Ventana.obtenerEstudiante(ID);
        
        Lista_Estudiantes.addItem(Estudiante.get(i)+" - "+Estudiante.get(i+1)+" "+Estudiante.get(i+2)+" - "+Estudiante.get(i+3));
        
        Lista_Estudiantes.setSelectedIndex(0);
        
        cargarInformacion((String)(Lista_Estudiantes.getSelectedItem()));
        guardar_notas.setEnabled(true);
        guardar_notas.setVisible(true);
        
    }
    
    public void actualizarListadoEstudiantes(String cargo, String ID){
        List<String> Estudiante = new ArrayList<>();
        
        if(cargo.equalsIgnoreCase("PROFESOR") || cargo.equalsIgnoreCase("PROFESOR EDUC.FISICA")
            || cargo.equalsIgnoreCase("PROFESOR INGLES")){
            Estudiante = Ventana.obtenerListaEstudiante(Ventana.obtenerGradoACargo(ID));
            prof = true;
        }else{
            Estudiante = Ventana.obtenerListaEstudiante();
        }
        
        Lista_Estudiantes.removeAllItems();
        
        Lista_Estudiantes.addItem("Seleccione un Estudiante...");
        for(int i=0;i<Estudiante.size();i+=4){
            Lista_Estudiantes.addItem(Estudiante.get(i)+"-"+Estudiante.get(i+1)+" "+Estudiante.get(i+2)+"-"+Estudiante.get(i+3));
        }
        Lista_Estudiantes.setSelectedIndex(0);
    }
    
    public void notasEstudianteInhabilitado()
    {
        for(int i=0; i<3; i++){
            Notas_Estudiante.nota_lengua[i].setEnabled(false);
            Notas_Estudiante.nota_matematica[i].setEnabled(false);
            Notas_Estudiante.nota_ciencias_naturaleza[i].setEnabled(false);
            Notas_Estudiante.nota_ciencias_sociales[i].setEnabled(false);
            Notas_Estudiante.nota_educacion_estetica[i].setEnabled(false);
            Notas_Estudiante.nota_educacion_fisica[i].setEnabled(false);        
            Notas_Estudiante.nota_ingles[i].setEnabled(false);
        }
        guardar_notas.setVisible(false);
    }
    
    public int getGradoID(String grado_str){
        int grado;
        switch (grado_str) {
            case "PRIMER GRADO":
                grado = 1;
                break;
            case "SEGUNDO GRADO":
                grado = 2;
                break;
            case "TERCER GRADO":
                grado = 3;
                break;
            case "CUARTO GRADO":
                grado = 4;
                break;
            case "QUINTO GRADO":
                grado = 5;
                break;
            case "SEXTO GRADO":
                grado = 6;
                break;
            default:
                grado = 7;
                break;
        }
        return grado;
    }
    
    public String getGradoStr(int grado){
        String grado_str;
        switch (grado) {
            case 1:
                grado_str = "PRIMER GRADO";
                break;
            case 2:
                grado_str = "SEGUNDO GRADO";
                break;
            case 3:
                grado_str = "TERCER GRADO";
                break;
            case 4:
                grado_str = "CUARTO GRADO";
                break;
            case 5:
                grado_str = "QUINTO GRADO";
                break;
            case 6:
                grado_str = "SEXTO GRADO";
                break;
            default:
                grado_str = "NINGUNO";
                break;
        }
        return grado_str;
    }
}
